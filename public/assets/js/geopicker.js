//script from: http://api.mygeoposition.com/geopicker/

//<script src="http://api.mygeoposition.com/api/geopicker/api.js" type="text/javascript"></script>
/*
	lat = '', // Latitude
	lng = '', // Longitude
	latMin = '', // Latitude in minutes & seconds
	lngMin = '', // Longitude in minutes & seconds
	address = '', // Full address string
	country.short = '', // Country
	country.long = '', // Country
	state.short = '', // State
	state.long = '', // State
	district.short = '', // Area
	district.long = '', // Area
	city.short = '', // City
	city.long = '', // City
	suburb.short = '', // Suburb
	suburb.long = '', // Suburb
	postalCode.short = '', // ZIP
	postalCode.long = '', // ZIP
	street.short = '' // Street
	street.long = '' // Street
	streetNumber.short = '' // Street number
	streetNumber.long = '' // Street number
*/

function lookupGeoData() { 
	myGeoPositionGeoPicker({
	    returnCallback  		: 'geoCallback',
	    startAddress            : 'Toronto, Canada',
		langButtonReturn        : 'Set Location'
	    //autolocate      : 'true' 
	    //returnFieldMap  : {'locationAddress' :'<ADDRESS>'}
           
	});
}

function geoCallback(data) {
	console.log("Return SET Location");
	console.log (data);

	$("#locationAddress").val(data.address);
	$("#hiddenMap").remove();
	
	var c = '';
	
	c +='<div id="hiddenMap">';
	c +='<input type="hidden" name="MapData[]" value="' + data.address + '"/>';
	c +='<input type="hidden" name="MapData[]" value="' + data.country.short + '"/>';
	c +='<input type="hidden" name="MapData[]" value="' + data.state.long + '"/>';
	c +='<input type="hidden" name="MapData[]" value="' + data.district.long + '"/>';
	c +='<input type="hidden" name="MapData[]" value="' + data.city.long + '"/>';
	c +='<input type="hidden" name="MapData[]" value="' + data.postalCode.long + '"/>';
	c +='<input type="hidden" name="MapData[]" value="' + data.lat + '"/>';
	c +='<input type="hidden" name="MapData[]" value="' + data.lng + '"/>';
	c +='</div>';
	$("form#form-mapdata").append(c);
}
   