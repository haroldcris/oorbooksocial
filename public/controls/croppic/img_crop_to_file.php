<?php
/*
*	!!! THIS IS JUST AN EXAMPLE !!!, PLEASE USE ImageMagick or some other quality image processing libraries
*/
ob_start();

$imgUrl = $_POST['imgUrl'];
$imgInitW = $_POST['imgInitW'];
$imgInitH = $_POST['imgInitH'];
$imgW = $_POST['imgW'];
$imgH = $_POST['imgH'];
$imgY1 = $_POST['imgY1'];
$imgX1 = $_POST['imgX1'];
$cropW = $_POST['cropW'];
$cropH = $_POST['cropH']; 
$jpeg_quality = 100;
$userName = $_POST['userName'];
$module = $_POST['module'];

try {
		

	$file = 'croppedImg_' . rand();
	
	if($module=='user'){
		$output_filename = '../../users/' . $userName . '/' . $file;
	}else{
		$output_filename = '../../image/' . $file;
	}

	$what = getimagesize($imgUrl);
	switch(strtolower($what['mime']))
	{
		case 'image/bmp':
			$img_r = imagecreatefromwbmp($imgUrl);
			$source_image = imagecreatefromwbmp($imgUrl);
			$type = '.png';
			break;

		case 'image/png':
	        $img_r = imagecreatefrompng($imgUrl);
			$source_image = imagecreatefrompng($imgUrl);
			$type = '.png';
	        break;
	    case 'image/jpeg':
	        $img_r = imagecreatefromjpeg($imgUrl);
			$source_image = imagecreatefromjpeg($imgUrl);
			$type = '.jpeg';
	        break;
	    case 'image/gif':
	        $img_r = imagecreatefromgif($imgUrl);
			$source_image = imagecreatefromgif($imgUrl);
			$type = '.gif';
	        break;
	    default: die('image type not supported');
	}
		
		$resizedImage = imagecreatetruecolor($imgW, $imgH);
		imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, 
					$imgH, $imgInitW, $imgInitH);	
		
		
		$dest_image = imagecreatetruecolor($cropW, $cropH);
		imagecopyresampled($dest_image, $resizedImage, 0, 0, $imgX1, $imgY1, $cropW, 
					$cropH, $cropW, $cropH);	


		imagejpeg($dest_image, $output_filename.$type, $jpeg_quality);
	

	switch($module){
		case 'user':
			require_once('../../class/user.class.php');
			if(UserClass::setPicturePathWallpaper($userName, $file.$type )){
				$response = array(
						"status" 	=> 'success',
						"url" 		=> $output_filename.$type,
						"username" 	=> $userName
					  ); 
			} else {
				ob_clean();
				$response = array(
						"status" 	=> 'error',
						"url" 		=> $output_filename.$type,
						"message"	=> "Error on saving picture profile in database"
					  );
			}
			break;
		
		case 'banner':
			require_once('../../class/setting.class.php');
			if(SettingClass::setSetting('Banner',$file.$type)){
				$response = array(
						"status" 	=> 'success',
						"url" 		=> $output_filename.$type,
						"username" 	=> $userName
					  ); 
			} else {
				ob_clean();
				$response = array(
						"status" 	=> 'error',
						"url" 		=> $output_filename.$type,
						"message"	=> "Error on saving picture profile in database"
					  );
			}
			break;
	}
 	
 
}catch (Exception $e){ 
	$response = array(
				"status" 	=> 'error',
				"url" 		=> $output_filename.$type,
				"message" 	=> $e->getMessage()				
			  );
}

	ob_clean();
	print json_encode($response);

?>