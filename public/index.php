<?php
/**
 * Laravel - A PHP Framework For Web Artisans
 *
 * @package  Laravel
 * @author   Taylor Otwell <taylorotwell@gmail.com>
 */

$p = explode('/',__DIR__);

switch ($p[count($p) - 1]) {
	case 'public_html':
		require __DIR__.'/../_laravel/bootstrap/autoload.php';
		$app = require_once __DIR__.'/../_laravel/bootstrap/start.php';
		break;

	case 'public':
		require __DIR__.'/../bootstrap/autoload.php';
		$app = require_once __DIR__.'/../bootstrap/start.php';
		break;
}



/*
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can simply call the run method,
| which will execute the request and send the response back to
| the client's browser allowing them to enjoy the creative
| and wonderful application we have whipped up for them.
|
*/

$app->run();
