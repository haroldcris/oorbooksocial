<?php
\Debugbar::disable();

if(!Request::is('api/*'))
{
	Route::when( '*', 'csrf', array( 'post', 'put', 'delete' ) );
}

@include_once('routes-api.php');

App::error(function(Exception $exception, $code)
{
//	if (Config::get('app.debug')) return;
	switch ($code){
		case 403: /* permission denied */
			return 'Permission Denied...';
		case 404: /* not found */
			//return 'Page NOT Found...';
			return View::make('layouts.errors.404');
	}

	myLog::Error($exception);
	//return 'error occurred';
});


/*******************************
 *  AUTHENTICATED USERS
 ********************************/
Route::group(['before'=>'auth'],function(){
	Route::get('dashboard','HomeController@index');
	
	//---- Accounts Profile ----//
	Route::get('accounts/logout', 'AccountsController@logout');
	Route::post('accounts/change-password', ['as'=>'account-change-password-post', 'uses'=>'AccountsController@postChangePassword']);

	Route::get('profile', ['as'=>'userProfile','uses'=>'UserProfileController@profile']);
	Route::post('profile', 'UserProfileController@edit');

	Route::get('followers',['as' => 'followers', 'uses' => 'FollowerController@index']);
	Route::get('followings',['as' => 'followings', 'uses' => 'FollowingController@index']);

	//---- User Personal Settings ----//
	Route::post('settings','SettingsController@setSettings');
	Route::post('settings/avatar/store' ,['as' => 'storeAvatar', 'uses' => 'UserProfileController@storeAvatar']);
	Route::post('settings/avatar/delete',['as' => 'deleteAvatar', 'uses' => 'UserProfileController@deleteAvatar']);

	//---- Post / Blog Pages ----//
	Route::get('posts/create' , ['as' => 'createPost' , 'uses' => 'PostController@create']);
	Route::get('posts/{id}/removePhoto' , ['as' => 'removePostPhoto' , 'uses' => 'PostController@removePostPhoto']);
	Route::post('posts/{id}/removePhoto' , ['as' => 'storeRemovePhoto' , 'uses' => 'PostController@storeRemovePhoto']);
	Route::get('posts/create-video' , ['as' => 'createVideo' , 'uses' => 'PostController@createVideo']);
	Route::post('posts/store' , ['as' => 'storePost'  , 'uses' => 'PostController@store']);
	Route::post('posts/delete', ['as' => 'deletePost' , 'uses' => 'PostController@delete']);
	Route::post('upload',['as' => 'uploadFile','uses' => 'PostController@upload']);
	Route::delete('file', ['as' =>'deleteFile', 'uses' =>'PostController@deleteUploadedFile']);
	Route::post('posts/report',['as' => 'reportPost', 'uses' => 'PostController@reportPost']);
	//WePutIt
	Route::post('posts/create_post_master' , ['as' => 'createPostMaster' , 'uses' => 'PostController@createPostMaster']);
	Route::post('posts/store_post_master' , ['as' => 'storePostMaster' , 'uses' => 'PostController@storePostMaster']);
	
	//locationscope
	Route::post('/posts/post_geoContinent',  ['as'=>'getGeoContinent', 'uses'=>'PostController@getGeoContinent']);
	Route::post('/posts/post_geoCountry',  ['as'=>'getGeoCountry', 'uses'=>'PostController@getGeoCountry']);
	Route::post('/posts/post_geoRegion4',  ['as'=>'getGeoRegion4', 'uses'=>'PostController@getGeoRegion4']);
	Route::post('/posts/post_geoRegion3',  ['as'=>'getGeoRegion3', 'uses'=>'PostController@getGeoRegion3']);
	Route::post('/posts/post_geoRegion2',  ['as'=>'getGeoRegion2', 'uses'=>'PostController@getGeoRegion2']);
	Route::post('/posts/post_geoRegion1',  ['as'=>'getGeoRegion1', 'uses'=>'PostController@getGeoRegion1']);
	Route::post('/posts/post_geoLocality', ['as'=>'getGeoLocality', 'uses'=>'PostController@getGeoLocality']);
	Route::post('/posts/post_geo',         ['as'=>'getGeoLocations', 'uses'=>'PostController@getGeoLocations']);
	Route::post('/posts/post_geoLocal', ['as'=>'getGeoLocal', 'uses'=>'PostController@getGeoLocal']);
	
	//---- Comments ----//
	Route::post('comments/store', ['as' => 'storeComment', 'uses' => 'CommentController@store']);
	Route::post('comments/delete', ['as' => 'deleteComment', 'uses' => 'CommentController@delete']);


	Route::group(['before'=>'canManage:siteContent'],function() {
		//---- Settings GET----//
		Route::get('settings', ['as'=>'settings', 'uses'=>'SettingsController@getSettings']);
		Route::post('settings/save',['as'=>'post-save-settings', 'uses'=>'SettingsController@postSaveSettings']);
		Route::post('settings/banner/save',['as'=>'post-save-settings-banner', 'uses'=>'SettingsController@postSaveSettingsBanner']);
		Route::post('settings/upload',['as'=>'uploadSettings', 'uses'=>'SettingsController@upload']);
	});

	//---- Manage ----//
	//---- Notices ----//
	Route::get('manage/notices',['as' => 'manageNotice','uses' =>'NoticeController@index']);

	//---- User Groups / Members ----//
	Route::get('manage/groups',['as' => 'manageGroups', 'uses' => 'GroupController@viewUserGroups']);
	Route::get('manage/groups/create',['as' => 'createGroup', 'uses' => 'GroupController@create']);
	Route::get('manage/members/add',['as' => 'addGroupMember', 'uses' => 'GroupController@viewAddMember']);
	Route::get('groups/{id}',['as' => 'manageSingleGroup', 'uses' => 'GroupController@viewSingleGroup']);
	Route::get('groups/{id}/posts',['as' => 'groupPosts', 'uses' => 'PostController@viewGroupPosts']);



	//---- End Manage ----//

	//---- Calendar Events ----//
	Route::get('events', ['as' => 'calendarEvents', 'uses' => 'CalendarController@index']);
    
	Route::get('events/update', ['as' => 'updateCalendarEvent', 'uses' => 'CalendarController@update']);
	Route::get('events/create', ['as' => 'createCalendarEvent', 'uses' => 'CalendarController@create']);
	Route::get('events/createMasterCalendarEvent', ['as' => 'createMasterCalendarEvent', 'uses' => 'CalendarController@createMaster']);




	Route::get( 'manage/api/notices', [ 'as' => 'manageNoticeApi', 'uses' => 'apiNoticeController@index' ] );
	// Manage Users
	Route::group(['before' => 'canManage:user'],function() {
		Route::get( 'manage/users', [ 'as' => 'manageUser', 'uses' => 'UserController@manage' ] );
		Route::get( 'manage/users/create', [ 'as' => 'createUser', 'uses' => 'UserController@create' ] );
		Route::get( 'manage/users/{id}', [ 'as' => 'editUser', 'uses' => 'UserController@edit' ] );
		Route::post( 'manage/users/store', [ 'as' => 'storeUser', 'uses' => 'UserController@store' ] );
		Route::post( 'manage/users/deactivate', [ 'as' => 'deactivateUser', 'uses' => 'UserController@deactivate' ] );
		Route::post( 'manage/users/restore', [ 'as' => 'restoreUser', 'uses' => 'UserController@restore' ] );
		Route::post( 'manage/users/delete', [ 'as' => 'deleteUser', 'uses' => 'UserController@delete' ] );
	});

	// Manage Subjects / Interest
	Route::group(['before' => 'canManage:interest'],function() {
		Route::get( 'manage/subjects', [ 'as' => 'manageSubject', 'uses' => 'SubjectController@manage' ] );
		Route::post( 'manage/subjects/store', [ 'as' => 'storeSubject', 'uses' => 'SubjectController@store' ] );
		Route::post( 'manage/subjects/edit', [ 'as' => 'editSubject', 'uses' => 'SubjectController@edit' ] );
		Route::post( 'manage/subjects/delete', [ 'as' => 'deleteSubject', 'uses' => 'SubjectController@delete' ] );
	});

	//---- Polls ----//
	Route::group(['before' => 'canManage:poll'],function() {
		Route::get( 'manage/polls', [ 'as' => 'managePolls', 'uses' => 'PollController@index' ] );
		Route::get( 'manage/polls/create', [ 'as' => 'createPoll', 'uses' => 'PollController@create' ] );
		Route::get( 'manage/polls/scope', [ 'as' => 'pollScope', 'uses' => 'PollController@createScope' ] );

		Route::get( 'manage/polls/{id}', [ 'as' => 'viewPoll', 'uses' => 'PollController@update' ] );
	});

	//---- Manage Roles ----//
	Route::group(['before' => 'canManage:role'],function(){
		Route::get('manage/roles',['as' => 'manageRoles', 'uses' => 'RoleController@index']);
		Route::get('manage/roles/create',['as' => 'createRole', 'uses' => 'RoleController@create']);
		Route::get('manage/roles/{id}',['as' => 'editRole', 'uses' => 'RoleController@update']);
	});
	
	//---- Manage Location Group ----//
 	Route::group(['before' => 'canManage:locationGroup'],function(){
		Route::get('manage/location_groups',['as' => 'manageLocationGroups', 'uses' => 'LocationGroupController@index']);
 	});
 	
 	//---- Manage Location Scope ----//
 	Route::group(['before' => 'canManage:locationScope'],function(){
		Route::get('manage/location_scope',['as' => 'manageLocationScope', 'uses' => 'LocationScopeController@index']);
		Route::get('manage/location_scope/create',['as' => 'createLocationScope', 'uses' => 'LocationScopeController@create']);
		Route::get('manage/location_scope/create2',['as' => 'createLocationScope2', 'uses' => 'LocationScopeController@create2']);
		Route::post('manage/location_scope/store' , ['as' => 'storeLocationScope'  , 'uses' => 'LocationScopeController@store']);
 	});

	//---- Locations ----//
	Route::get('location/{id}/members',['as'=>'locationMembers', 'uses'=> 'MemberController@index']);
	// Route::get('location/{id}/members',['as'=>'locationMembers', 'uses'=> 'MemberController@index']);

});
/********* END AUTHENTICATED USER **********************/




/*******************************
 *  PUBLIC ROUTES
 ********************************/
//---- ACCOUNTS LOGIN / CREATE ----//
Route::get('accounts/resetpassword',['as'=>'accountReset', 'uses'=>'AccountsController@resetPassword']);
Route::post('accounts/resetpassword',['as'=>'accountReset', 'uses'=>'AccountsController@tryResetPassword']);

Route::get('accounts/forgotpassword',['as' =>'accountForgot', 'uses' => 'AccountsController@forgot']);
Route::post('accounts/forgotpassword',['uses' => 'AccountsController@tryForgot']);

Route::get('accounts/activate/{token}',['as'=>'accountActivate','uses'=>'AccountsController@activate']);
Route::post('accounts/create','AccountsController@create');

//Route::get('accounts/login', ['as' => 'loginForm', 'uses' => 'AccountsController@createLogin'] );
Route::post('accounts/login', ['uses'=>'AccountsController@tryLogin']);
Route::get('accounts/checkdata', ['as'=>'checkForDuplicateRecord','uses'=>'AccountsController@checkForDuplicateRecord']);
Route::get('eula',['uses'=> 'TermsConditionController@show']);
Route::get('help',['uses'=> 'TermsConditionController@help']);
Route::get('contact-us',['uses'=> 'TermsConditionController@viewContactUs']);
Route::post('contact-us',['uses'=> 'TermsConditionController@ContactUs']);

//---- End Accounts Login /Create ----//


//---- HYBRID AUTH SOCIAL MEDIA LOGIN ----//
Route::get('social/{platform}/auth',function($platform){Session::set('platform',$platform); return Redirect::to('/social');});
Route::get('social/{action?}', ["as" => "hybridauth",'uses' => 'SocialMediaController@auth']);


// Route::get('location/{id}',['as'=>'setLocation', 'uses'=> 'PostController@setLocation']);
Route::get('location/{id}',['as'=>'setLocation', 'uses'=> 'PostController@setLocation']);

Route::get('division/country/{id}',['as'=>'divisionCountry', 'uses'=> 'DivisionController@divisionCountry']);
Route::get('division/region1/{id}',['as'=>'divisionRegion1', 'uses'=> 'DivisionController@divisionRegion1']);
Route::get('division/region2/{id}',['as'=>'divisionRegion2', 'uses'=> 'DivisionController@divisionRegion2']);
Route::get('division/region3/{id}',['as'=>'divisionRegion3', 'uses'=> 'DivisionController@divisionRegion3']);
Route::get('division/region4/{id}',['as'=>'divisionRegion4', 'uses'=> 'DivisionController@divisionRegion4']);
Route::get('division/locality/{id}',['as'=>'divisionLocality', 'uses'=> 'DivisionController@divisionLocality']);

//locationScope...inadd ko
// Route::get('locationScope/country/{id}',['as'=>'locationScopeCountry', 'uses'=> 'locationScopeController@locationScopeCountry']);
// Route::get('locationScope/region1/{id}',['as'=>'locationScopeRegion1', 'uses'=> 'locationScopeController@locationScopeRegion1']);
// Route::get('locationScope/region2/{id}',['as'=>'locationScopeRegion2', 'uses'=> 'locationScopeController@locationScopeRegion2']);
// Route::get('locationScope/region3/{id}',['as'=>'locationScopeRegion3', 'uses'=> 'locationScopeController@locationScopeRegion3']);
// Route::get('locationScope/region4/{id}',['as'=>'locationScopeRegion4', 'uses'=> 'locationScopeController@locationScopeRegion4']);
// Route::get('locationScope/locality/{id}',['as'=>'locationScopeLocality', 'uses'=> 'locationScopeController@locationScopeLocality']);
//---- Interest ----//
Route::get('interest/{slug}', 'PostController@interest');

//---- User Posts ----//
Route::get('post/{id}',['as' => 'showPost','uses' => 'PostController@view'] );

//---- Guest User Profile ----//
Route::get('profile/{username}',['as' => 'guestUserProfile', 'uses' => 'UserProfileController@profile']);
Route::get('user/{id}/avatar',['as' => 'userAvatar', 'uses' => 'UserProfileController@avatar']);
Route::get('user/{id}/avatar.jpg',['as' => 'userAvatarjpg', 'uses' => 'UserProfileController@avatar']);

Route::get('followers/{username}',['as' => 'guestFollowers', 'uses' => 'FollowerController@index']);
Route::get('followings/{username}',['as' => 'guestFollowings', 'uses' => 'FollowingController@index']);

//---- User Group ----//

Route::get('addcolumn',function()
{
    // change table
    // DB::statement('ALTER TABLE post ADD isPrivate int;');
     
    // check if column is added;
    // $result = Post::first()->isPrivate;
    
    // return $result;
    // DB::statement('update post set isPrivate = 0 where isPrivate is null;');
    // $result = Post::first();  return $result;
    // $today = new Carbon\Carbon();
    // $today = $today->format('Y-m-d');
    // return $today;
});



Route::get('/', ['as'=>'accountLogin','uses'=>'AccountsController@login']);

/******** End Public Routes ***********************/



//************************************************/
//  ROUTES FOR TESTING ONLY
//************************************************ */
// Route::get('upload',function(){
// 	return View::make('test');
// });
Route::get('experiment',['as' => 'experiment', 'uses' => 'ExperimentController@index']);
Route::post('experiment/post',['as' => 'experimentPost', 'uses' => 'ExperimentController@experimentPost']);

Route::get('/test',function()
{

	if(App::environment() != 'local') return;
	$user = User::where('username','mani')->first();
	//$user = Auth::user();
	echo $user->username;
});

Route::get('/find', 'LocationScopeController@find');
// Route::get('/testing', 'TestingController@index');
Route::post('/testing/post_continent',  ['as'=>'getGeoContinent',   'uses'=>'TestingController@getGeoContinent']);
Route::post('/testing/post_country',  ['as'=>'getGeoCountry',   'uses'=>'TestingController@getGeoCountry']);
Route::post('/testing/post_region1',  ['as'=>'getGeoRegion1',   'uses'=>'TestingController@getGeoRegion1']);
Route::post('/testing/post_region2',  ['as'=>'getGeoRegion2',   'uses'=>'TestingController@getGeoRegion2']);
Route::post('/testing/post_region3',  ['as'=>'getGeoRegion3',   'uses'=>'TestingController@getGeoRegion3']);
Route::post('/testing/post_region4',  ['as'=>'getGeoRegion4',   'uses'=>'TestingController@getGeoRegion4']);
Route::post('/testing/post_locality', ['as'=>'getGeoLocality',  'uses'=>'TestingController@getGeoLocality']);
Route::post('testing/get_geo',      ['as'=>'getGeo', 'uses'=>'TestingController@getGeo']);
Route::post('testing/store_geo',      ['as'=>'storeGeo', 'uses'=>'TestingController@store']);

// Yee Test
Route::get('/yeetest', 'TestingController@index');


Event::listen('illuminate.query', function($query, $params, $time, $conn)
{
	//if (!Config::get('app.debug')) return;
	//if(App::environment() != 'local') return;
	Log::alert(json_encode(array($query, $params)));
});