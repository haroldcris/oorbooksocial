<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/08/2015
 * Time: 8:23 PM
 *
 */

class Notice extends Eloquent {
	protected $table = 'notice';
	public $primaryKey = 'noticeId';


	public function apiCredential ()
	{
		return $this->belongsTo('apiCredential','apiCredentialId','apiCredentialId');
	}
}