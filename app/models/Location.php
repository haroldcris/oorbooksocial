<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/04/2015
 * Time: 6:55 PM
 *
 */

class Location extends Eloquent {
    protected $table = 'location';
    public $primaryKey = 'locationId';


    public function members(){
        return $this->belongsToMany('User','userLocation','locationId','userId');
    }


    public function notices($localOnly = false){
        $sqlCountry = 'select apiCredentialId from apiCredential api where scope = "country" and country = :country ';
        $sqlRegion1 = 'select apiCredentialId from apiCredential api where scope = "region1" and country = :country and region1 = :region1 ';
        $sqlRegion2 = 'select apiCredentialId from apiCredential api where scope = "region2" and country = :country and region1 = :region1 and region2 = :region2 ';
        $sqlRegion3 = 'select apiCredentialId from apiCredential api where scope = "region3" and country = :country and region1 = :region1 and region2 = :region2 and region3 = :region3 ' ;
        $sqlRegion4 = 'select apiCredentialId from apiCredential api where scope = "region4" and country = :country and region1 = :region1 and region2 = :region2 and region3 = :region3 and region4 = :region4 ';
        $sqlLocality = 'select apiCredentialId from apiCredential api where scope = "locality" and country = :country and region1 = :region1 and region2 = :region2 and region3 = :region3 and region4 = :region4 and locality = :locality ';

        if($localOnly) {
            $sql = $sqlLocality;
        } else {
            $sql = $sqlCountry . ' union ' .
                   $sqlRegion1 . ' union ' .
                   $sqlRegion2 . ' union ' .
                   $sqlRegion3 . ' union ' .
                   $sqlRegion4;
        }

        $country = is_null($this->country) ? '' : $this->country;
        $region1 = is_null($this->region1) ? '' : $this->region1;
        $region2 = is_null($this->region2) ? '' : $this->region2;
        $region3 = is_null($this->region3) ? '' : $this->region3;
        $region4 = is_null($this->region4) ? '' : $this->region4;
        $locality = is_null($this->locality) ? '' : $this->locality;

        $sql = str_replace(':country','"' . $country . '"', $sql);
        $sql = str_replace(':region1','"' . $region1 . '"', $sql);
        $sql = str_replace(':region2','"' . $region2 . '"', $sql);
        $sql = str_replace(':region3','"' . $region3 . '"', $sql);
        $sql = str_replace(':region4','"' . $region4 . '"', $sql);
        $sql = str_replace(':locality','"' . $locality . '"', $sql);

        $apiList = DB::select($sql);
        if (count($apiList) == 0){
            return null;
        }

        $apiIds = array();
        foreach ($apiList as $item ){
            $apiIds[] = $item->apiCredentialId;
        }
        $notice = Notice::whereIn('apiCredentialId', $apiIds)->orderBy('created_at','desc')->get();

        return $notice;
    }






    public function posts($interest = null){
        
        $baseLocationIds_GL = Location::select('locationId')
                            ->where('continent', 'Global')
                            ->get()->toArray();
    
        $baseLocationIds_CN = Location::select('locationId')
                            ->where('continent', 'LIKE', '%'.$this->continent.'%')
                            ->where('country', '')
                            ->where('region1', '')
                            ->where('region2', '')
                            ->where('region3', '')
                            ->where('region4', '')
                            ->where('locality', '')->get()->toArray();
    
        $baseLocationIds_CT = Location::select('locationId')
                            ->where('continent', 'LIKE', '%'.$this->continent.'%')
                            ->where('country', 'LIKE', '%'.$this->country.'%')
                            ->where('region1', '')
                            ->where('region2', '')
                            ->where('region3', '')
                            ->where('region4', '')
                            ->where('locality', '')->get()->toArray();
                            
        $baseLocationIds_R1 = Location::select('locationId')
                            ->where('continent', 'LIKE', '%'.$this->continent.'%')
                            ->where('country', 'LIKE', '%'.$this->country.'%')
                            ->where('region1', 'LIKE', '%'.$this->region1.'%')
                            ->where('region2', '')
                            ->where('region3', '')
                            ->where('region4', '')
                            ->where('locality', '')->get()->toArray();
                            
        $baseLocationIds_R2 = Location::select('locationId')
                            ->where('continent', 'LIKE', '%'.$this->continent.'%')
                            ->where('country', 'LIKE', '%'.$this->country.'%')
                            ->where('region1', 'LIKE', '%'.$this->region1.'%')
                            ->where('region2', 'LIKE', '%'.$this->region2.'%')
                            ->where('region3', '')
                            ->where('region4', '')
                            ->where('locality', '')->get()->toArray();
                            
        $baseLocationIds_R3 = Location::select('locationId')
                            ->where('continent', 'LIKE', '%'.$this->continent.'%')
                            ->where('country', 'LIKE', '%'.$this->country.'%')
                            ->where('region1', 'LIKE', '%'.$this->region1.'%')
                            ->where('region2', 'LIKE', '%'.$this->region2.'%')
                            ->where('region3', 'LIKE', '%'.$this->region3.'%')
                            ->where('region4', '')
                            ->where('locality', '')->get()->toArray();
        
        $baseLocationIds_R4 = Location::select('locationId')
                            ->where('continent', 'LIKE', '%'.$this->continent.'%')
                            ->where('country', 'LIKE', '%'.$this->country.'%')
                            ->where('region1', 'LIKE', '%'.$this->region1.'%')
                            ->where('region2', 'LIKE', '%'.$this->region2.'%')
                            ->where('region3', 'LIKE', '%'.$this->region3.'%')
                            ->where('region4', 'LIKE', '%'.$this->region4.'%')
                            ->where('locality', '')->get()->toArray();
        
        $baseLocationIds_LC = Location::select('locationId')
                            ->where('continent', 'LIKE', '%'.$this->continent.'%')
                            ->where('country', 'LIKE', '%'.$this->country.'%')
                            ->where('region1', 'LIKE', '%'.$this->region1.'%')
                            ->where('region2', 'LIKE', '%'.$this->region2.'%')
                            ->where('region3', 'LIKE', '%'.$this->region3.'%')
                            ->where('region4', 'LIKE', '%'.$this->region4.'%')
                            ->where('locality', 'LIKE', '%'.$this->locality.'%')->get()->toArray();
                
        $baseLocationIds_DF = Location::select('locationId')
                            ->where('country', $this->country)
                            ->where('region1', $this->region1)
                            ->where('region2', $this->region2)
                            ->where('region3', $this->region3)
                            ->where('region4', $this->region4)
                            ->where('baseLocality', $this->baseLocality)->get()->toArray();
        
        $baseLocationIds_MERGED = array_merge($baseLocationIds_GL, $baseLocationIds_CN, $baseLocationIds_CT, $baseLocationIds_R1, $baseLocationIds_R2, $baseLocationIds_R3, $baseLocationIds_R4, $baseLocationIds_LC, $baseLocationIds_DF);
        $baseLocationIds = $baseLocationIds_MERGED;


        if( is_null($interest)) {
            $posts = Post::with( ['creator'  => function ( $query ) { $query->select( DB::raw( 'userId, username, email' ) ); },
                                  'comments' => function ( $query ) { $query->select('*')->orderBy('created_at','desc')->take(1); }
                                 ] )
                         ->select( DB::raw( 'post.*, subject.description subject, totalcomments') )
                         ->leftJoin( 'subject', 'subject.subjectId', '=', 'post.subjectId' )
                         ->leftJoin( DB::raw('(select postId, count(*) totalcomments from comment group by postId) c'), 'c.postId','=', 'post.postId')
                         ->whereIn( 'locationId', $baseLocationIds )

                         ->orderBy( 'post.created_at', 'desc' )
                         ->paginate( 10 );
            //->get();

        }else {

            $posts = Post::with( ['creator'  => function ( $query ) { $query->select( DB::raw( 'userId, username, email' ) ); },
                                  'comments' => function ( $query ) { $query->select('*')->orderBy('created_at','desc')->take(1); }
                                 ] )
                         ->select( DB::raw( 'post.*, subject.description subject, totalcomments') )
                         ->whereIn( 'locationId', $baseLocationIds )
                         ->where('subject.description', $interest)
                         ->leftJoin( 'subject', 'subject.subjectId', '=', 'post.subjectId' )
                         ->leftJoin( DB::raw('(select postId, count(*) totalcomments from comment group by postId) c'), 'c.postId','=', 'post.postId')

                         ->orderBy( 'post.created_at', 'desc' )
                         ->paginate( 10 );
        }

        return $posts;
    }

}