<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/08/2015
 * Time: 8:38 PM
 *
 */

class ApiCredential extends Eloquent {
	protected $table ='apiCredential';
	public $primaryKey = 'apiCredentialId';


	public function notices(){
		return $this->hasMany('notice','apiCredentialId');
	}
}