<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/18/2015
 * Time: 2:22 PM
 *
 */

class Poll extends Eloquent{
	protected $table = 'poll';
	public $primaryKey = 'pollId';


	public static function getPoll( $locationId, $userId )
	{
		$sqlCountry = 'select pollId from poll p where scope = "country" and country = :country and disabled = 0';
		$sqlRegion1 = 'select pollId from poll p where scope = "region1" and country = :country and region1 = :region1 and disabled = 0';
		$sqlRegion2 = 'select pollId from poll p where scope = "region2" and country = :country and region1 = :region1 and region2 = :region2 and disabled = 0';
		$sqlRegion3 = 'select pollId from poll p where scope = "region3" and country = :country and region1 = :region1 and region2 = :region2 and region3 = :region3 and disabled = 0' ;
		$sqlRegion4 = 'select pollId from poll p where scope = "region4" and country = :country and region1 = :region1 and region2 = :region2 and region3 = :region3 and region4 = :region4 and disabled = 0';
		$sqlLocality = 'select pollId from poll p where scope = "locality" and country = :country and region1 = :region1 and region2 = :region2 and region3 = :region3 and region4 = :region4 and locality = :locality and disabled = 0';


		$sqlUnion = $sqlCountry . ' union ' .
		       $sqlRegion1 . ' union ' .
		       $sqlRegion2 . ' union ' .
		       $sqlRegion3 . ' union ' .
		       $sqlRegion4 . ' union ' .
		       $sqlLocality;

		$sql = ' select x.pollId from pollAnswer right join (' . $sqlUnion . ') x '
			   .' on pollAnswer.pollId = x.pollId and pollAnswer.userId = ' . (Int)$userId
			   .' where pollAnswer.pollId is null';

		$location = Location::where('locationId', $locationId)->first();

		if(is_null($location)) {
			return null;
		}

		$country = $location->country;
		$region1 = $location->region1;
		$region2 = $location->region2;
		$region3 = $location->region3;
		$region4 = $location->region4;
		$locality = $location->locality;

		$sql = str_replace(':country','"' . $country . '"', $sql);
		$sql = str_replace(':region1','"' . $region1 . '"', $sql);
		$sql = str_replace(':region2','"' . $region2 . '"', $sql);
		$sql = str_replace(':region3','"' . $region3 . '"', $sql);
		$sql = str_replace(':region4','"' . $region4 . '"', $sql);
		$sql = str_replace(':locality','"' . $locality . '"', $sql);

		$result = DB::select($sql);
		if (count($result) == 0){
			return null;
		}

		$pollIds = [];
		foreach($result as $item){
			$pollIds[] = $item->pollId;
		}

		$poll = Poll::whereIn('pollId', $pollIds)->get();

		return $poll;
	}


	public static function summary()
	{
		$result = DB::select('select poll.*
								, sum(case when answer = 1 then 1 else 0 end) `summary1`
								, sum(case when answer = 2 then 1 else 0 end) `summary2`
								, sum(case when answer = 3 then 1 else 0 end) `summary3`
								, sum(case when answer = 4 then 1 else 0 end) `summary4`
								, sum(case when answer = 5 then 1 else 0 end) `summary5`
								, count(*) total
							from poll left join pollAnswer on poll.pollId = pollAnswer.pollId
							group by pollId, question');

		return $result;
	}
}