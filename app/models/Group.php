<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/03/2015
 * Time: 3:49 AM
 *
 */

class Group extends Eloquent
{
	protected $table = 'group';
	public $primaryKey = 'groupId';

	public function owner()
	{
		return $this->belongsTo('User', 'creatorId','userId')->select('userId','username','email');
	}

	public function members()
	{
		return $this->belongsToMany('User', 'groupMember','groupId','userId','userId')
					->select('user.userId','username','membershipDate');
	}


	public function addMember($userId)
	{
		DB::statement('INSERT INTO groupMember (UserId, groupId) select userId, :groupId from user where userId = :userId;',[':groupId' =>$this->groupId, ':userId' => $userId]);
		return DB::getPdo()->query("SELECT FOUND_ROWS()")->fetchColumn();
	}



	public function posts()
	{
		$posts = Post::with( ['creator'  => function ( $query ) { $query->select( DB::raw( 'userId, username, email' ) ); },
		                      'comments' => function ( $query ) { $query->select('*')->orderBy('created_at','desc')->take(1); }
							 ] )
		             ->select( DB::raw( 'post.*, subject.description subject, totalcomments') )
		             ->leftJoin( 'subject', 'subject.subjectId', '=', 'post.subjectId' )
		             ->leftJoin( DB::raw('(select postId, count(*) totalcomments from comment group by postId) c'), 'c.postId','=', 'post.postId')
		             ->where( 'groupId', $this->groupId )

		             ->orderBy( 'post.created_at', 'desc' )
		             ->paginate( 10 );

		return $posts;
	}
}