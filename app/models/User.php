<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user';
	public $primaryKey = 'userId';
	public $timestamps = true;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');



	public function createPersonalFolder(){
		$file = public_path() . '/users/' . $this->username;

		if (!File::exists($file)){
			File::makeDirectory( $file ,0755);
		}
	}


	public function getImageWallpaperPath(){
		if (is_null($this->imageWallpaper) || $this->imageWallpaper == '') return '/assets/images/wallpaper.jpg';

		return $this->imageWallpaper;
	}


	public function enrolledLocations(){
		return $this->belongsToMany('Location','userLocation','userId','locationId')
					->withPivot('isPrimary','address', 'userLocationId');
	}


	public function followers(){
		return $this->belongsToMany('User','followerRequest','requestedToId','requestedById')
					->withPivot('accepted');
	}

	public function followees(){
		return $this->belongsToMany('User','followerRequest','requestedById','requestedToId')
		            ->withPivot('accepted');
	}

	public function members(){
		return $this->belongsToMany('User','userLocation','locationId','userId');
	}
	
	public function logs(){
		return $this->hasMany('LogBook', 'userId');
	}

	public function userSettings(){
		//User Settings
		return $this->hasMany('UserSettings','userId');
	}

	public function getUserSettings($key){
		$setting = Session::get('userSettings'.$key);
		if (!is_null($setting)){
			return $setting;
		}

		return $this->userSettings()->where('key',$key)->pluck('value');
	}

	public function setUserSettings($key, $value){
		$item = $this->userSettings()->where('key', $key)->first();

		if(is_null($item))
		{
			$item = new UserSettings();
		}
		$item->unguard();
		$item->key = $key;
		$item->value = $value;

		Session::set('userSettings'.$key, $value);
		return $this->userSettings()->save($item);
	}


	public function hasUserSettings($key){
		$key = $this->userSettings()->where('key',$key)->pluck('value');

		return is_null($key) ? false : true;
	}


	public function selectedLocation(){
		if (! Session::has('selectedLocation')) {
			return null;
		}

		return unserialize(Session::get('selectedLocation'));

	}


	public static function getUsername($userId){
		$attribute = 'userName' . (int) $userId;

		$result = Session::get($attribute);

		if(!is_null($result)){ return $result; }

		$result = User::select('userId','username')->where('userId',$userId)->first();

		if(!is_null($result)) {
			Session::set( $attribute, $result->username );

			return $result->username;
		}

	}

	public function groupSubscriptions()
	{
		return $this->belongsToMany('Group','groupMember', 'userId', 'groupId')->select('group.groupId','name','creatorId');
	}

	public function getGroupSubscriptionDate($groupId)
	{
		//return $this->groupSubscriptions()->select('membershipDate')->where('group.groupId', $groupId)->pluck('membershipDate');
		$group = DB::select('select membershipDate from groupMember where groupId = ? and userId = ? limit 1', [$groupId, Auth::user()->userId]);

		if (count($group) == 0) {
			return null;
		}
		return $group[0]->membershipDate;
	}


	/**
	 * @param $groupId
	 *
	 * @return bool
	 */
	public function isMemberOf($groupId){
		$result = DB::table('groupMember')
							->select('groupMemberId')
							->where('userId', Auth::user()->userId)
							->where('groupId', $groupId)
							->first();

		return is_null($result) ? false : true;
	}

	public function isAdmin()
	{
		return strtolower($this->securityLevel) == 'admin';
	}

	public function isSuperUser()
	{
		return strtolower($this->securityLevel) == 'super user';
	}

	public function canManage($rights)
	{
		$field = 'manage'.ucfirst($rights);
		$role = Role::select('roleId',$field)->where( 'name', $this->securityLevel )->first();
		if ( is_null( $role ) ) {
			return false;
		}
		return $role->$field ? true : false;
	}
}