<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/01/2015
 * Time: 5:39 PM
 *
 */

class CalendarEvent extends Eloquent {
	protected $table = 'calendar';
	public $primaryKey = 'calendarId';

	public function creator()
	{
		return $this->belongsTo('User','userId', 'userId');
	}
}