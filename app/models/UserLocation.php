<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/04/2015
 * Time: 7:26 PM
 *
 */

class UserLocation extends Eloquent {
	protected $table = 'userLocation';
	public $primaryKey = 'userLocationId';
//	public $timestamps = false;

	public function user() {
		return $this->belongsTo('User');
	}
}