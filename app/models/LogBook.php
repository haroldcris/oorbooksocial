<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/06/2015
 * Time: 1:54 PM
 *
 */

class LogBook extends Eloquent{
	protected $table ='logBook';
	public $primaryKey = 'logBookId';


	public static function logAction($action, $public = true){
		$user = Auth::user();

		$action = str_replace('%username%', '<strong>' . $user->username . '</strong>' ,$action);
		$action = str_replace('%sex%', $user->sex == 'female' ? 'her': 'his' ,$action);

		$log = new LogBook();
		$log->unguard();
		$log->userId = $user->userId;;
		$log->action = $action;
		$log->public = $public;

		return $log->save();
	}


	public function user(){
		return $this->belongsTo('User','userId','userId');
	}
}