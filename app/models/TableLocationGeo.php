<?php

class TableLocationGeo  extends Eloquent {
	protected $table = 'table_locationGeoPc';
	public $primaryKey = 'id';
	public $timestamps = false;
	
	public static function search($data, $limit, $country){
		//---- Search ONLY Locality ----//
//		return TableLocationGeo::where('country',$country)
//		                       ->where('locality','like',"%$data%")
//		                       ->limit($limit)
//		                       ->get();
		//---- SEACH ENTIRE DATABASE ----//

		//---- The code below was NOT used BUT it i think this is useful in the future ----//

		$array = explode(' ',$data);
		foreach ($array as $index => $item){
			$array [$index] = '+' . $item;
		}
		$data = implode(' ',$array) . '*';

		$sql = 'select id, iso, postcode,
						IFNULL(locality,"") locality,
						IFNULL(region4,"") region4,
						IFNULL(region3,"") region3,
						IFNULL(region2,"") region2,
						IFNULL(region1,"") region1,
						IFNULL(country,"") country, match(country, region1, region2, region3, region4, locality, postcode) against (? IN BOOLEAN MODE ) as score from table_locationGeoPc where  match(country, region1, region2, region3, region4, locality, postcode) against (? IN BOOLEAN MODE) and country = ? ORDER BY locality ASC limit ?';
		return DB::select($sql,[$data, $data, $country, $limit]);
	}

}