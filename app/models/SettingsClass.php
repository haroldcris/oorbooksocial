<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 01/28/2015
 * Time: 7:33 PM
 *
 */

class SettingsClass extends Eloquent{
	protected $table = 'setting';
	public $primaryKey = 'settingId';
	public $timestamps = false;

	public static function get($key, $allowScript = false, $default = null, $getFromDb = false){
		if(!$getFromDb) {
			if ( Session::has( 'settings' . strtolower( $key ) ) ) {
				return Session::get( 'settings' . strtolower( $key ) );
			}
		}
		$item = SettingsClass::where('attribute',$key)->pluck('value');

		if(is_null($item) && !is_null($default)){
			return $default;
		}

		Session::set('settings'. strtolower($key), $item);

		if (!$allowScript) {
			$config = HTMLPurifier_Config::createDefault();
			$config->set('HTML.SafeIframe', true);
			$config->set('URI.SafeIframeRegexp', '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%'); //allow YouTube and Vimeo

			$purifier = new HTMLPurifier($config);
			return $purifier->purify($item);
		}

		return $item;
	}


	public static function set($key, $val, $allowScript = false){
		$item = SettingsClass::where('attribute',$key)->first();
		if(is_null($item))
		{
			$item = new SettingsClass();
		}

		$item->unguard();
		$item->attribute = $key;
		$item->value = $val;

		if (! $allowScript) {
			$config = HTMLPurifier_Config::createDefault();
			$config->set('HTML.SafeIframe', true);
			$config->set('URI.SafeIframeRegexp', '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%'); //allow YouTube and Vimeo

			$purifier = new HTMLPurifier($config);
			$item->value = $purifier->purify($item->value);
		}

		Session::set('settings'. strtolower($key), $val);
		return $item->save();

	}
}