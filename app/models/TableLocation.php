<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/04/2015
 * Time: 2:31 PM
 *
 */

class TableLocation  extends Eloquent {
	protected $table = 'table_location';
	public $primaryKey = 'tableLocationId';
	public $timestamps = false;


	public static function search($data, $limit){
		//$sql = 'select * from `Location` where `village` like "' . $searchItem . '%" or `PlaceCode` like "' . $searchItem . '%"';
		return TableLocation::select('tableLocationId','GNNum','Country','District','Division','GNNum','PlaceCode','Province','Village')->where('Village','like',"%$data%")
							->orWhere('PlaceCode','like',"%$data%")->get()->take($limit);

	}
}