<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/22/2015
 * Time: 9:18 PM
 *
 */

class CommentClass extends Eloquent {
	protected $table = 'comment';
	public $primaryKey = 'commentId';

}