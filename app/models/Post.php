<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/18/2015
 * Time: 1:56 PM
 *
 */

class Post extends Eloquent {
	protected $table = 'post';
	public $primaryKey = 'postId';

	public function creator(){
		return $this->belongsTo('User','userId','userId');
	}

	public function location(){
		return $this->belongsTo('Location','locationId','locationId');
	}

	public function comments(){
		return $this->hasMany('CommentClass','postId','postId');
	}


	public static function getPost($id){

//		$post = Post::with( ['creator' => function ( $query ) {$query->select( DB::raw( 'userId, username, email') ); } ,
//							 'location' => function($query) {$query->select('*');}] )
//		             ->select( DB::raw( 'post.*, subject.description subject' ) )
//		             ->join( 'subject', 'subject.subjectId', '=', 'post.subjectId' )
//		             ->where('postId', $id)
//					 ->first();
//


		$post = Post::with( ['creator'  => function ( $query ) { $query->select( DB::raw( 'userId, username, email') ); },
		                      'comments' => function ( $query ) { $query->select('*')->orderBy('created_at','asc'); }
							 ] )
		             ->select( DB::raw( 'post.*, subject.description subject, totalcomments') )
		             ->leftJoin( 'subject', 'subject.subjectId', '=', 'post.subjectId' )
		             ->leftJoin( DB::raw('(select postId, count(*) totalcomments from comment group by postId) c'), 'c.postId','=', 'post.postId')
					 ->where('post.postId', $id)
					 ->first();

		return $post;
	}


}