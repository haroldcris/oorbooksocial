<?php

class UsersTableSeeder extends Seeder {

	public function run() {
//		DB::table( 'users' )->delete();

		DB::statement('SET foreign_key_checks = 0;');

		DB::table('user')->where('userId','<',500)->delete();

		DB::statement('SET foreign_key_checks = 1;');

		$faker = Faker\Factory::create();

		foreach ( range( 1, 395 ) as $item ) {
			$username = $faker->unique()->firstName;
			$sex = $faker->randomElement($array = array('male','female'));

			if($sex =='male'){
				$avatar = 'http://api.randomuser.me/portraits/med/men/' . $faker->numberBetween(1,95) . '.jpg';
			}else{
				$avatar = 'http://api.randomuser.me/portraits/med/women/' . $faker->numberBetween(1,95) . '.jpg';
			}

			$users[] = array(
						'userId'            => 100 + $item,
						'username'          => $username,
						'email'             => $faker->unique()->safeEmail,
						'password'          => Hash::make( $username),
						'activated'         => 1,
						'imageAvatar'       => $avatar,
						'sex'               => $sex,
						'birthdate'         => $faker->dateTimeBetween('1970-1-1','2010-1-1'),
						'country'           => $faker->country,
						'created_at'        => $faker->dateTimeThisYear,
				);
		}
		DB::table( 'user' )->insert( $users );
	}

}
