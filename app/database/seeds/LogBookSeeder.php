<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/08/2015
 * Time: 2:06 AM
 *
 */

class LogBookSeeder extends Seeder {
	public function run() {


		DB::table('logBook')->where('logBookId','>',14)->delete();

		$faker = Faker\Factory::create();
		foreach ( range( 1, 2000 ) as $item ) {
			$users[] = array(
//				'logBookId'         => 100 + $item,
				'userId'            => $faker->numberBetween(101,495),
				'action'            => $faker->sentence($nbWords = 9),

				'created_at'        => $faker->dateTimeThisMonth,
			);
		}
		DB::table( 'logBook' )->insert( $users );


	}

}