<?php

class PostTableSeeder extends Seeder {

	public function run() {
		DB::table( 'post' )->delete();
//		DB::statement('SET foreign_key_checks = 0;');

		$locations = Location::all();

		$faker = Faker\Factory::create();

		$posts  = [ ];
		foreach ( $locations as $loc ) {

			echo 'seeding for ' . $loc->locality . '\n';

			foreach ( range( 1, 100 ) as $item ) {
				$posts[] = array(
					'userId'      => $faker->numberBetween( 101, 490 ),
					'subjectId'   => $faker->numberBetween( 1, 13 ),
					'locationId'  => $loc->locationId,
					'matter'      => $faker->sentence( $nbWords = 5 ),
					'title'       => $faker->unique()->sentence( $nbWords = 4 ) . ' - for ' . $loc->locality,
					'description' => $faker->sentence( $nbWords = 50 ),
					'created_at'  => $faker->dateTimeThisMonth
				);
			}


		}

		DB::table( 'post' )->insert( $posts );

	}

}
