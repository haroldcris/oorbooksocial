<?php

class ExperimentController extends BaseController {

	public function index() {
		return View::make('experiment.index');
	}
    
    public function experimentPost() {
        $item = new SettingsClass;
        
        $item->attribute = Input::get('key');
        $item->value     = Input::get('value');
        //return $item->attribute.$item->value;
        
        if($item->save()) {
            return 'saved';
        }
        
        return 'xsaved';
    }
	
}