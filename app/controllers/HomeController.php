<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{

		if(Auth::user()->isFirstTimeUser){
			Session::set('flashMessageInfo','Please update your profile.');
			return Redirect::route('userProfile');
		}

		if( Session::has('selectedLocationSlug') ) {
			$query = Location::select('locationId', 'locality')->where('slug', Session::get('selectedLocationSlug'))->first();
			if( !is_null($query) ){
				return Redirect::route('setLocation', [Session::get('selectedLocationSlug')]);
			}
			Session::forget('selectedLocation');
			Session::forget('selectedLocationId');
			return View::make('user.dashboard');
  		}

  		$primary = Auth::user()->enrolledLocations()
  							   ->select('location.locationId', 'userLocationId', 'location.locality','slug')
  							   ->where('isPrimary',1)
  							   ->first();
		
		if(!is_null($primary)){
			Session::set('selectedLocationId', $primary->locationId);
			Session::set('selectedLocationSlug', $primary->slug);
			Session::set('selectedUserLocationId', $primary->pivot->userLocationId);
			return Redirect::route('setLocation', [$primary->slug]);
		}

		return View::make('user.dashboard')->with('posts',null);
	}





}