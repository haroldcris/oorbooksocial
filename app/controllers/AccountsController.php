<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 01/28/2015
 * Time: 6:51 PM
 *
 */

class AccountsController extends BaseController{

	/**
	 * @return bool|\Illuminate\Http\JsonResponse
	 */
	public function postChangePassword() {
		$response = Config::get('site.response');

		$validator = Validator::make(Input::all(), array(
			'old_password'    => 'required|min:5',
			'new_password'    => 'required|min:5',
			'retype_password' => 'required|same:new_password'
		));

		if($validator->fails()) {
			$response['error'] = 'Please enter the valid data.';
			$response['validation'] = $validator->errors()->toArray();
			return Response::json($response);
		}

		$user = User::find(Auth::user()->userId);
		$old_password = Input::get('old_password');
		$new_password = Input::get('new_password');

		if(!Hash::check($old_password, $user->getAuthPassword())) {
			$response['error'] = 'Your old password is incorrect!';
			return Response::json($response);	
		}

		$user->password = Hash::make($new_password);
		if($user->save()) {
			Session::set('flashMessageInfo','Your password has been successfully changed.');
			$response['success'] = 'true';
			return Response::json($response);
		}
		return false;
	}

	/**
	 * @param $userData
	 *
	 * @return bool
	 */
	public static function sendEmailActivation($userData){
		Mail::send('emails.activation',array('user'=> $userData), function($message)use ($userData)
		{
			$message->to($userData->email, $userData->username)
			        ->subject('Oorbook Account Activation');
		});

		return true;
	}


	/**
	 * @return array
	 */
	public function validationRules(){
		return array(
			'email'                     => 'required|max:50|email|unique:user',
			'username'                  => 'required|max:50|min:3|unique:user',
			'password'                  => 'required|max:50|min:3',
			'password_confirmation'     => 'required|same:password',
		);
	}


	/**
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
	public function login(){
		if(Auth::check()){
			return Redirect::to('dashboard');
		}

		return View::make('accounts.index');
	}


	/**
	 * @return string
	 */
	public function create(){
		try {
			if ( ! self::hasValidCaptcha( Input::get( 'g-recaptcha-response' ) ) ) {
				return json_encode( [ 'error' => 'Invalid Captcha Value' ] );
			}

			$validation = Validator::make( Input::all(), $this->validationRules() );

			if ( $validation->fails() ) {
				$e = '';
				foreach ( $validation->getMessageBag()->all() as $item ) {
					$e .= $item;
				}

				return json_encode( [ 'error' => $e ] );
			}

			$user = new User();
			$user->unguard();
			$user->username       = Input::get( 'username' );
			$user->email          = Input::get( 'email' );
			$user->password       = Hash::make( Input::get( 'password' ) );
			$user->signupDate     = $user->freshTimestamp();
			$user->activationCode = str_random( 60 );

			if ( $user->save() ) {
				if ( self::sendEmailActivation( $user ) ) {
					return json_encode( [ 'error' => '' ] );
				}
			}
		} catch (Exception $e){
			return json_encode(['error'=> $e->getMessage()]);
		}

	}


	/**
	 * @param $response
	 *
	 * @return mixed
	 */
	public static function hasValidCaptcha($response){
		$secretKey = "6LcQrf8SAAAAAG3dumkLHhzNqe4FLkOnDUplUGiE";
		$url ='https://www.google.com/recaptcha/api/siteverify?secret=%secret%&response=%response%';

		$url = str_replace('%secret%', $secretKey, $url);
		$url = str_replace('%response%', urlencode(stripslashes($response)), $url);
		$ret = json_decode(file_get_contents($url),true);

		return $ret['success'];
	}


	/**
	 * @param $token
	 *
	 * @return bool|\Illuminate\Http\RedirectResponse|string
	 */
	public function activate($token){
		//return $token;
		$user = User::where('activationCode',$token)->first();

		if(is_null($user)){
			return "INVALID TOKEN!!!";
		}

		$user->activationCode = null;
		$user->activated = 1;
		if($user->save()){
			//Create User Personal Folder
			$user->createPersonalFolder();

			Auth::login($user);
			Event::fire('accountCreated', Auth::user());
			return Redirect::to('/dashboard');
		}

		return false;
	}


	/**
	 * @return int|string
	 */
	public function checkForDuplicateRecord(){

		if (Request::has('email')){
			$ret = false;
			$user = User::where('email',Request::query('email'))->first();

			if(!is_null($user)){
				$ret = true;
			}

			return json_encode([ 'duplicate' => $ret ]);
		}

		if (Request::has('username')){
			$ret = false;
			$user = User::where('username',Request::query('username'))->first();

			if(!is_null($user)){
				$ret = true;
			}

			return json_encode(["duplicate"=> $ret ]);
		}

		return 0;
	}


	/**
	 * @return \Illuminate\Http\JsonResponse|string
	 */
	public function tryLogin(){
		$usernameOrEmail = Input::get('loginEmail');
		$password = Input::get('loginPassword');

		$field = filter_var($usernameOrEmail, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

		$ret = ["success"   => "false",
				"url"       => URL::to('/dashboard'),
				"message"   => ""];

		if (Auth::attempt([$field => $usernameOrEmail, 'password' => $password, 'activated' => 1] ,false)) {
			$ret['success'] = "true";
			$ret['url'] = Redirect::intended(URL::to('/dashboard'))->getTargetUrl();

			//RESET ALL CACHE
			return json_encode($ret);

		}

		$ret['message'] = 'The email or password you entered is incorrect. Please try again.';
		return Response::json($ret,401);
	}

	/**
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function logout(){
		Auth::logout();
		Session::flush();
		Cache::flush();
		return Redirect::to('/');
	}

	/**
	 * @return \Illuminate\View\View
	 */
	public function forgot(){
		return View::make('accounts.forgot');
	}


	/**
	 * @return string
	 */
	public function tryForgot(){
		$ret = ['success' => false, 'error' => 'Email address you entered is not registered on our system'];

		if(Input::has('email') && Input::get('email')==''){
			$ret['error'] ='You have entered an invalid email address.';
			return json_encode($ret);
		}

		$user = User::where('email',Input::get('email'))
					->select(['userId','username', 'email', 'activationCode', 'activated'])
					->first();

		if(!is_null($user)){
			$user->activationCode = str_random(60);
			$user->save();

			static::sendResetPassword($user);
			$ret['success'] = true;
		}

		return json_encode($ret);
	}

	/**
	 * @param $userData
	 *
	 * @return bool
	 */
	public static function sendResetPassword($userData){
		Mail::send('emails.resetPassword',array('user'=> $userData), function($message)use ($userData)
		{
			$message->to($userData->email, $userData->username)
			        ->subject('Oorbook Reset Password');
		});

		return true;
	}

	/**
	 * @return \Illuminate\View\View|string
	 */
	public function resetPassword(){
		$token = Request::get('token');
		$user = User::where('activationCode',$token)->where('activated',1)->select('activationCode')->first();

		if(is_null($user)){ return "INVALID TOKEN"; }

		return View::make('accounts.reset',[ 'user' => $user]);

	}

	/**
	 * @return $this|\Illuminate\Http\RedirectResponse|string
	 */
	public function tryResetPassword(){
		$validation = Validator::make(Input::all(),['password'                  => 'required|max:50|min:3',
                                                    'password_confirmation'     => 'required|same:password']);

		if ($validation->fails()){
			return Redirect::back()->withErrors($validation);
		}

		$user = User::select(['userId','activationCode','password'])->where('activationCode',Input::get('resetCode'))->first();

		if(is_null($user)){
			return 'INVALID TOKEN !!!';
		}

		$user->password = Hash::make(Input::get('password'));
		$user->activationCode = null;
		$user->save();

		Session::flash('message','Your password has been reset! You may now login using your new created password.');
		return Redirect::route('accountLogin');
	}



}
