<?php 

class PostController extends BaseController {
    
    /**
     * @param $slug
     *
     * @return $this|\Illuminate\View\View
     */
    public function interest($slug){
        $id = Session::get('selectedLocationId');
        return $this->setLocation($id, $slug);
    }
    
    /**
     * @param $id
     * @param null $interest
     *
     * @return $this|\Illuminate\View\View
     */
    public function setLocation($id, $interest = null) {
        $field = 'locationId';

        //Check if parameter is Number
        // if Parameter is string, then query for SLUG field NOT the LOCATIONID
        if (!is_numeric($id) ) $field = 'slug';

        // SELECT * FROM location WHERE locationId = 17 LIMIT 1
        $selectedLocation = Location::where( $field, $id )->first();
        
        if( is_null($selectedLocation) ){
            //Session::set('flashMessageError','You wish to visit a location which you do not exist.');
            //return Redirect::to('dashboard');
            return View::make('layouts.errors.404');
        }

        Session::forget('selectedUserLocationId');

        Session::set('selectedLocation', serialize($selectedLocation));
        Session::set('selectedLocationId', $selectedLocation->locationId);
        Session::set('selectedLocationSlug', $selectedLocation->slug);

        if ( Auth::check() ) {
            $isEnrolled = UserLocation::where ('userId', Auth::user()->userId)
                                      ->where ('locationId', $selectedLocation->locationId)
                                      ->first();

            if (! is_null($isEnrolled)) {
                Session::set( 'selectedUserLocationId', $isEnrolled->userLocationId );
            }
        }

        $posts = $selectedLocation->posts($interest);
        $ViewPosts = View::make('user.post.posts')->with('posts', $posts);
        
        // WePutIt
        Session::set('selectedLocationContinent', $selectedLocation->continent);
        Session::set('selectedLocationCountry', $selectedLocation->country);
        Session::set('selectedLocationRegion1', $selectedLocation->region1);
        Session::set('selectedLocationRegion2', $selectedLocation->region2);
        Session::set('selectedLocationRegion3', $selectedLocation->region3);
        Session::set('selectedLocationRegion4', $selectedLocation->region4);
        Session::set('selectedLocationLocality', $selectedLocation->locality);
        
        $foo = 'hey on post';
        return View::make('user.dashboard')->with('selectedLocation', $selectedLocation)
                                           ->with('interest', $interest)
                                           ->with('posts', $ViewPosts)
                                           ->with('foo', $foo);

    }


    /**
     * @return $this
     */
    public function create(){
        $response = new ResponseClass();
        
        $subjects = SubjectClass::select('subjectId','description')->get();
        $enrolledLocations = Auth::user()->enrolledLocations;
        $groups = Auth::user()->groupSubscriptions;
        
        if(strtolower(Auth::user()->securityLevel) != 'master user')
        {
            return View::make('user.post.create')->with('subjects', $subjects)
                                             ->with('enrolledLocations', $enrolledLocations)
                                             ->with('groups', $groups);
        }
        else
        {
            // Manual Setting of Scope (For testing only)
            // $iCN = [''];    $ctrCN = 0;
            // $iCT = [''];    $ctrCT = 0;
            // $iR1 = [''];    $ctrR1 = 0;
            // $iR2 = [''];    $ctrR2 = 0;
            // $iR3 = [''];    $ctrR3 = 0;
            // $iR4 = [''];    $ctrR4 = 0;
            // $iLC = [''];    $ctrLC = 0;
            
            $tezt = '';
            
            $getMUser = UserLocation::where('userId', '=', Auth::id())->where('isPrimary', '=', 1)->first();
            $MUserLocation = Location::where('locationId', $getMUser->locationId)->first();
            
            $iCN = explode(",", $MUserLocation->continent);
            $iCT = explode(",", $MUserLocation->country);
            $iR1 = explode(",", $MUserLocation->region1);
            $iR2 = explode(",", $MUserLocation->region2);
            $iR3 = explode(",", $MUserLocation->region3);
            $iR4 = explode(",", $MUserLocation->region4);
            $iLC = explode(",", $MUserLocation->locality);
            
            if(count($iCN) == 0 || $iCN == "" || $iCN[0] == "" || $iCN[0] == null) { $ctrCN = 0; } else { $ctrCN = count($iCN); }
            if(count($iCT) == 0 || $iCT == "" || $iCT[0] == "" || $iCT[0] == null) { $ctrCT = 0; } else { $ctrCT = count($iCT); }
            if(count($iR1) == 0 || $iR1 == "" || $iR1[0] == "" || $iR1[0] == null) { $ctrR1 = 0; } else { $ctrR1 = count($iR1); }
            if(count($iR2) == 0 || $iR2 == "" || $iR2[0] == "" || $iR2[0] == null) { $ctrR2 = 0; } else { $ctrR2 = count($iR2); }
            if(count($iR3) == 0 || $iR3 == "" || $iR3[0] == "" || $iR3[0] == null) { $ctrR3 = 0; } else { $ctrR3 = count($iR3); }
            if(count($iR4) == 0 || $iR4 == "" || $iR4[0] == "" || $iR4[0] == null) { $ctrR4 = 0; } else { $ctrR4 = count($iR4); }
            if(count($iLC) == 0 || $iLC == "" || $iLC[0] == "" || $iLC[0] == null) { $ctrLC = 0; } else { $ctrLC = count($iLC); }
            
            return View::make('user.post.createPostMaster')->with('subjects', $subjects)
                                                            ->with('iCN', $iCN)->with('iCT', $iCT)
                                                            ->with('iR1', $iR1)->with('iR2', $iR2)
                                                            ->with('iR3', $iR3)->with('iR4', $iR4)
                                                            ->with('iLC', $iLC)
                                                            ->with('ctrCN', $ctrCN)->with('ctrCT', $ctrCT)
                                                            ->with('ctrR1', $ctrR1)->with('ctrR2', $ctrR2)
                                                            ->with('ctrR3', $ctrR3)->with('ctrR4', $ctrR4)
                                                            ->with('ctrLC', $ctrLC)
                                                            ->with('tezt', $tezt);
        }

    }

    public function storePostMaster(){
// P S E U D O C O D E   F L O W
//      Check If "Selected Geo" Exists on "Location Table"
//             If Exists
//             - Save "Post Fields" to "Post Table"
//             Else
//             - Check If "Selected Geo" Exists on "TableLocationGeo"
//                     If Exists
//                     - Save "Selected Geo" to "Location Table"
//                     Else
//                     - Save "Multiple Selected Geos" to "Location Table"
// E R R O R S
//     - If multiple locality has been selected

        $response = new ResponseClass();
        $location = new Location;
        
        // ---------------------------------------------------------------------
        // HAS Checkbox Values
        // ---------------------------------------------------------------------
        $hasCN = Input::has('geoContinent');
        $hasCT = Input::has('geoCountry');
        $hasR1 = Input::has('geoRegion1');
        $hasR2 = Input::has('geoRegion2');
        $hasR3 = Input::has('geoRegion3');
        $hasR4 = Input::has('geoRegion4');
        $hasLC = Input::has('geoLocality');
        
        // ---------------------------------------------------------------------
        // SET 'Location' Selected Location Fields
        // ---------------------------------------------------------------------
        $location->continent    = $hasCN ? implode(",", Input::get('geoContinent')) : '';
        $location->country      = $hasCT ? implode(",", Input::get('geoCountry'))   : '';
        $location->region1      = $hasR1 ? implode(",", Input::get('geoRegion1'))   : '';
        $location->region2      = $hasR2 ? implode(",", Input::get('geoRegion2'))   : '';
        $location->region3      = $hasR3 ? implode(",", Input::get('geoRegion3'))   : '';
        $location->region4      = $hasR4 ? implode(",", Input::get('geoRegion4'))   : '';
        $location->locality     = $hasLC ? implode(",", Input::get('geoLocality'))  : '';
        
        // ---------------------------------------------------------------------
        // CHECK If "Selected Location" Exists on "Location Table"
        // ---------------------------------------------------------------------
        $checkLocation = Location::where('continent', $location->continent)
                                    ->where('country', $location->country)
                                    ->where('region1', $location->region1)
                                    ->where('region2', $location->region2)
                                    ->where('region3', $location->region3)
                                    ->where('region4', $location->region4)
                                    ->where('locality', $location->locality)
                                    ->first();
        
        // ---------------------------------------------------------------------
        // Validations for 'Post'
        // ---------------------------------------------------------------------
        $validation = Validator::make(Input::all(),[
                             'tags'        => 'required',
                             'title'       => 'required',
                             'description' => 'required'
                            ],[ 'subjectId.required'    =>  'Interest is required',
                                'description.required'  =>  'Content is required']);
        
        if ($validation->fails()) {
            return $response->failed($validation->errors()->first());
        }
        
        // ---------------------------------------------------------------------
        // SET 'Post' Fields
        // ---------------------------------------------------------------------
        $config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.SafeIframe', true);
        $config->set('URI.SafeIframeRegexp', '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%'); //allow YouTube and Vimeo
        $purifier = new HTMLPurifier($config);

        $subjectId   = (int)Input::get('subjectId');
        $matter      = trim($purifier->purify(Input::get('tags')));
        $title       = trim($purifier->purify(Input::get('title')));
        $description = trim($purifier->purify(Input::get('description')));
        $photos      = Input::get('photos');
        $youtube     = Input::get('youtubeId');
        $youtubeImage= Input::get('youtubeImage');
        $groupId     = null;
        $isPrivate   = Input::has('isPrivate') ? 1 : 0;

        $post = new Post;
        $post->unguard();
        $post->userId       = (int)Auth::user()->userId;
        $post->subjectId    = $subjectId;
        $post->matter       = $matter;
        $post->title        = $title;
        $post->description  = $description;
        $post->photos       = $photos;
        $post->youtube      = $youtube;
        $post->youtubeImage = $youtubeImage;
        $post->groupId      = $groupId;
        $post->isPrivate    = $isPrivate;
        
        if(count($checkLocation) == 1)
        {
        // ---------------------------------------------------------------------
        // If "Selected Location" Exists on "Location Table"
        // ---------------------------------------------------------------------
            
            // ---------------------------------------------------------------------
            // - SAVE "Post Fields" to "Post Table"
            // ---------------------------------------------------------------------
            $post->locationId = $checkLocation->locationId;

            try {
                $post->save();
                return $response->successful();
            } catch (Exception $e) { return $response->failedException($e); }
        }
        else
        {
        // ---------------------------------------------------------------------
        // If "Seleted Location" Doesn't Exists on "Location Table" 
        // ---------------------------------------------------------------------
        
            // ---------------------------------------------------------------------
            // CHECK If "Selected Location" Exists on "TableLocationGeo"
            // ---------------------------------------------------------------------
            $checkTableLocationGeo = TableLocationGeo::where('continent', $location->continent)
                                                        ->where('country', $location->country)
                                                        ->where('region1', $location->region1)
                                                        ->where('region2', $location->region2)
                                                        ->where('region3', $location->region3)
                                                        ->where('region4', $location->region4)
                                                        ->where('locality', $location->locality)->first();
            
            if(count($checkTableLocationGeo) == 1)
            {
            // ---------------------------------------------------------------------
            // If "Selected Location" Exists on "TableLocationGeo"
            // ---------------------------------------------------------------------
                // Pumapasok dito ung multi-selected locality
                return $response->successful('$checkTableLocationGeo==1');
                
                // ---------------------------------------------------------------------
                // SET 'NAME' Field
                // ---------------------------------------------------------------------
                // $name = '';
                // if($location->continent  != ''){ $name .= $location->continent; }
                // if($location->country  != ''){ $name .= ' » ' . $location->country; }
                // if($location->region1  != ''){ $name .= ' » ' . $location->region1; }
                // if($location->region2  != ''){ $name .= ' » ' . $location->region2; }
                // if($location->region3  != ''){ $name .= ' » ' . $location->region3; }
                // if($location->region4  != ''){ $name .= ' » ' . $location->region4; }
                // if($location->locality != ''){ $name .= ' » ' . $location->locality; }
                // if($checkTableLocationGeo->postcode != ''){ $name .= ' » ' . $checkTableLocationGeo->postcode; }
                // $location->name = $name;
                
                // ---------------------------------------------------------------------
                // SAVE "Selected Location" to "Location Table"
                // ---------------------------------------------------------------------
                $location->save();
                
                $lastLocationId = Location::select('locationId')->orderBy('locationId', 'desc')->first();
                
                $locationId  = $lastLocationId->locationId;
                $post->locationId   = $locationId;
                
                try {
                    $post->save();
                    return $response->successful();
                } catch (Exception $e) { return $response->failedException($e); }
            }
            else
            {
            // ---------------------------------------------------------------------
            // If "Selected Location" !Exists on "TableLocationGeo"
            // ---------------------------------------------------------------------
            
                //Dito Ilalagay ung mga multiple values (Country - Locality)
                // return $response->successful('$checkTableLocationGeo == 0');
                
                // ---------------------------------------------------------------------
                // HAS Checkbox Values instead of Hidden Values
                // ---------------------------------------------------------------------
                $hasCN = Input::has('geoContinent');
                $hasCT = Input::has('geoCountry');
                $hasR1 = Input::has('geoRegion1');
                $hasR2 = Input::has('geoRegion2');
                $hasR3 = Input::has('geoRegion3');
                $hasR4 = Input::has('geoRegion4');
                $hasLC = Input::has('geoLocality');
                
                // ---------------------------------------------------------------------
                // SET 'Location' Fields
                // ---------------------------------------------------------------------
                $location->continent    = $hasCN ? implode(",", Input::get('geoContinent')) : '';
                $location->country      = $hasCT ? implode(",", Input::get('geoCountry')) : '';
                $location->region1      = $hasR1 ? implode(",", Input::get('geoRegion1')) : '';
                $location->region2      = $hasR2 ? implode(",", Input::get('geoRegion2')) : '';
                $location->region3      = $hasR3 ? implode(",", Input::get('geoRegion3')) : '';
                $location->region4      = $hasR4 ? implode(",", Input::get('geoRegion4')) : '';
                $location->locality     = $hasLC ? implode(",", Input::get('geoLocality')) : '';
                $location->baseLocality = $location->locality;
                $location->code = $this->generateNumeric();
                
                // ---------------------------------------------------------------------
                // Regenerate Code if Generated Code Already Exists
                // ---------------------------------------------------------------------
                while(!$location->save()) { $location->code = $this->generateNumeric(); }
                
                // ---------------------------------------------------------------------
                // SAVE Location Fields
                // ---------------------------------------------------------------------
                try {
                    $location->save();
                } catch (Exception $e) { return $response->failedException($e); }
                
                // ---------------------------------------------------------------------
                // SAVE Post Fields
                // ---------------------------------------------------------------------
                $lastLocationId = Location::select('locationId')->where('code', '=', $location->code)->first();
                $post->locationId   = $lastLocationId->locationId;
                
                try {
                    $post->save();
                    return $response->successful();
                } catch (Exception $e) { return $response->failedException($e); }
            }
        }
    }
    
    
    
    
    
    
    public function getGeoContinent() {
        $items = TableLocationGeo::select('continent')->distinct()
                                                      ->where('continent', '!=', '')
                                                      ->orderBy('continent', 'asc')
                                                      ->get();
        return Response::json($items);
    }
    
    public function getGeoCountry() {
        $continent = Input::get('hidContinent');
        
        $items = TableLocationGeo::select('country')->distinct()
                                                    ->where('continent', '=', $continent)
                                                    ->where('country', '!=', '')
                                                    ->orderBy('country', 'asc')
                                                    ->get();
        return Response::json($items);
    }
    
    public function getGeoRegion1() {
        $continent = Input::get('hidContinent');
        $country   = Input::get('hidCountry');
        
        $items = TableLocationGeo::select('region1')->distinct()
                                                    ->where('continent', '=', $continent)
                                                    ->where('country', '=', $country)
                                                    ->where('region1', '!=', '')
                                                    ->orderBy('region1', 'asc')
                                                    ->get();
        return Response::json($items);
    }
    
    public function getGeoRegion2() {
        $continent = Input::get('hidContinent');
        $country   = Input::get('hidCountry');
        $region1   = Input::get('hidRegion1');
        
        $items = TableLocationGeo::select('region2')->distinct()
                                                    ->where('continent', '=', $continent)
                                                    ->where('country', '=', $country)
                                                    ->where('region1', '=', $region1)
                                                    ->where('region2', '!=', '')
                                                    ->orderBy('region2', 'asc')
                                                    ->get();
        return Response::json($items);
    }
    
    public function getGeoRegion3() {
        $continent = Input::get('hidContinent');
        $country   = Input::get('hidCountry');
        $region1   = Input::get('hidRegion1');
        $region2   = Input::get('hidRegion2');
        
        $items = TableLocationGeo::select('region3')->distinct()
                                                    ->where('continent', '=', $continent)
                                                    ->where('country', '=', $country)
                                                    ->where('region1', '=', $region1)
                                                    ->where('region2', '=', $region2)
                                                    ->where('region3', '!=', '')
                                                    ->orderBy('region3', 'asc')
                                                    ->get();
        return Response::json($items);
    }
    
    public function getGeoRegion4() {
        $continent = Input::get('hidContinent');
        $country   = Input::get('hidCountry');
        $region1   = Input::get('hidRegion1');
        $region2   = Input::get('hidRegion2');
        $region3   = Input::get('hidRegion3');
        
        $items = TableLocationGeo::select('region4')->distinct()
                                                    ->where('continent', '=', $continent)
                                                    ->where('country', '=', $country)
                                                    ->where('region1', '=', $region1)
                                                    ->where('region2', '=', $region2)
                                                    ->where('region3', '=', $region3)
                                                    ->where('region4', '!=', '')
                                                    ->orderBy('region4', 'asc')
                                                    ->get();
        return Response::json($items);
    }
    
    public function getGeoLocality() {
        $continent = htmlspecialchars(Input::get('hidContinent'));
        $country   = htmlspecialchars(Input::get('hidCountry'));
        $region1   = htmlspecialchars(Input::get('hidRegion1'));
        $region2   = htmlspecialchars(Input::get('hidRegion2'));
        $region3   = htmlspecialchars(Input::get('hidRegion3'));
        $region4   = htmlspecialchars(Input::get('hidRegion4'));
        
        $items = TableLocationGeo::select('locality')->distinct()
                                                     ->where('continent', '=', $continent)
                                                     ->where('country', '=', $country)
                                                     ->where('region1', '=', $region1)
                                                     ->where('region2', '=', $region2)
                                                     ->where('region3', '=', $region3)
                                                     ->where('region4', '=', $region4)
                                                     ->where('locality', '!=', '')
                                                     ->orderBy('locality', 'asc')
                                                     ->get();                                                    
        return Response::json($items);
    }

        




    /**
     *
     */
    public function store(){
        $response = new ResponseClass();

        //start Validation
        if(Input::has('locationId')) {
            if(is_numeric(Input::get('locationId')) && !Input::has('subjectId')){
                return $response->failed('Select which Interest this post belong');
            }
        }

        $validation = Validator::make(Input::all(),[
                        'tags'        => 'required',
                        'title'         => 'required',
                        'description'   => 'required'
        ],[ 'subjectId.required'    =>  'Interest is required',
            'description.required'   =>  'Content is required']);


        if ($validation->fails()){
            return $response->failed($validation->errors()->first());
        }
        
        if(!Input::has('groupId') && !Input::has('locationId')) {
            return $response->failed('Must select option either share in Location or in Group');
        }

//      if(! Session::has('selectedUserLocationId')) {
//          $response['error'] = 'You have to select your location first in order to create a post';
//          return Response::Json($response,400);
//      }
        //end validation;


        $config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.SafeIframe', true);
        $config->set('URI.SafeIframeRegexp', '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%'); //allow YouTube and Vimeo
        $purifier = new HTMLPurifier($config);

        $subjectId   = (int)Input::get('subjectId');
        $matter      = trim($purifier->purify(Input::get('tags')));
        $title       = trim($purifier->purify(Input::get('title')));
        $description = trim($purifier->purify(Input::get('description')));
        $photos      = Input::get('photos');
        $youtube     = Input::get('youtubeId');
        $youtubeImage= Input::get('youtubeImage');
        $locationId  = Input::get('locationId');
        $groupId     = null;
        //$isPrivate   = Input::get('isPrivate');
        $isPrivate   = Input::has('isPrivate') ? 1 : 0;
        if(!is_numeric($locationId)){
            $groupId = substr($locationId,1);
            $locationId = null;
        }

        //Check for duplicate
        $post = Post::where('title',$title)->pluck('postId');

        if(!is_null($post)){
            return $response->failed('Duplicate Title. Choose another title.');
        }

        try {
            $post = new Post();
            $post->unguard();
            $post->subjectId    = $subjectId;
            $post->locationId   = $locationId;
            $post->userId       = (int)Auth::user()->userId;
            $post->matter       = $matter;
            $post->title        = $title;
            $post->description  = $description;
            $post->photos       = $photos;
            $post->youtube      = $youtube;
            $post->youtubeImage = $youtubeImage;
            $post->groupId      = $groupId;
            $post->isPrivate    = $isPrivate;
            $post->save();

            if(!Input::has('noevent')) Event::fire('postCreated', ['post' => $post]);
            return $response->successful($post);
        } catch (Exception $e) {
            return $response->failedException($e);
        }
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    function delete(){
        $response = Config::get('site.response');

        if( !Input::has('postId')) {
            $response['error'] ='Invalid post Id.';
            return Response::json($response,400);
        }

        if (Auth::user()->isAdmin()) {
            $post = Post::where( 'postId', Input::get( 'postId' ) )
                        ->first();
        } else {
            $post = Post::where( 'postId', Input::get( 'postId' ) )
                        ->where( 'userId', Auth::user()->userId )
                        ->first();
        }

        if(is_null($post)){
            $response['error'] = 'Invalid post Id';
            return Response::json( $response, 400);
        }

        try {
            if (!is_null($post->photos))
            {
                $photos = explode(Config::get('site.delimiter'),$post->photos);
                foreach($photos as $item){
                    File::delete(public_path() . $item);
                }
            }


            $post->delete();    

            $response['success']  = true;
            return Response::json($response, 200);

        } catch (Exception $e){
            if(Config::get('app.debug')) { dd($e); }
            $response['error'] = 'Error occurred while deleting post';
            return Response::json ($response, 400);
        }
        

    }

    /**
     * @param $postId
     *
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\View\View|string
     */
    public function view($postId){

        $post = Post::getPost($postId);

        if(is_null($post)){
            //Session::flash('flashMessageError','The post you wish to view seems to be ');
            //return App::abort(400,"Invalid Post Id Request");
            return View::make('layouts.errors.404');
        }

        $group = null;
        if(!is_null($post->groupId) && $post->groupId != 0) {
            $group = Group::with('owner')->where('groupId', $post->groupId)->first();

            if(is_null($group) ) return 'Invalid Group Id detected ' . $post->groupId;

            if($group->isPrivate) {

                if(Auth::guest()) {
                    Session::set('url.intended',Request::url());
                    //Session::set( 'flashMessageError', 'You must be a member of the group to view this post' );
                    //return View::make( 'user.post.viewpost' )->with( 'selectedGroup', $group );;
                    return Redirect::route('accountLogin');
                }

                if ( !strtolower(Auth::user()->securityLevel) == 'admin') {
                    if ( ! Auth::user()->isMemberOf( $group->groupId ) ) {
                        Session::set( 'flashMessageError', 'You must be a member of the group to view this post' );

                        return View::make( 'user.post.viewpost' )->with( 'selectedGroup', $group );
                    }
                }
            }
        }

        $postLocation = Location::select('locationId','country','region1','region2','region3','region4','locality','postal')
                                ->where('locationId', $post->locationId)
                                ->first();

        $comments = $post->comments()->orderBy('parentId')->orderBy('created_at', 'desc')->get();
        $commentsView = View::make('user.post.comments.comments')->with('comments', $comments)
                                                                 ->with('total', 0)
                                                                 ->with('postId', $post->postId);
        
        if (Input::has('json')) return $post;
        
        return View::make('user.post.viewpost')->with('item', $post)
                                               ->with('commentsView', $commentsView)
                                               ->with('selectedLocation', $postLocation)
                                               ->with('selectedGroup', $group);
    }


    /**
     * @return \Illuminate\View\View
     */
    public function createVideo(){
        return View::make('user.post.attachment.video');
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(){
        $response = new ResponseClass();

        try {

            $relativePath = strtolower( '/users/' . Auth::user()->username . '/gallery/' );
            $path = public_path() . $relativePath;

            if (!file_exists($path)) {
                File::makeDirectory($path,0755,true,true);
            }

            $file = Input::file('file');
            $original = $file->getClientOriginalName();
            $filename =  time(). '_' . $original;

            $file->move($path, $filename);

            $image = new SimpleImage( $path . $filename );
            if($image->get_width() > 558 )
            {
                $image->fit_to_width(558)->save();
            }

            //$response['filename'] = $relativePath . $filename ;
            $response->filename = $relativePath . $filename ;
            return $response->successful();

        } catch (Exception $e) {
            return $response->failedException($e);
        }

    }

    /**
     * @param $url
     *
     * @return null
     */
    protected function youtubeIdFromUrl($url) {
//      $pattern =
//          '%^# Match any youtube URL
//         (?:https?://)?  # Optional scheme. Either http or https
//         (?:www\.)?      # Optional www subdomain
//         (?:             # Group host alternatives
//          youtu\.be/    # Either youtu.be,
//         | youtube\.com  # or youtube.com
//          (?:           # Group path alternatives
//             /embed/     # Either /embed/
//          | /v/         # or /v/
//          | /watch\?v=  # or /watch\?v=
//          )             # End path alternatives.
//         )               # End host alternatives.
//         ([\w-]{10,12})  # Allow 10-12 for 11 char youtube id.
//         $%x'
//      ;
//      $result = preg_match($pattern, $url, $matches);
//      if (false !== $result) {
//          return $matches[1];
//      }
//      return null;


        if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match)) {
            $id = $match[1];
            return $id;
        }

        return null;
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteUploadedFile(){
        $response = Config::get('site.response');

        $file =Input::get('filename');

        $array = explode('/',$file);
        if($array[2] == Auth::user()->username)
        {
            if (File::delete(public_path() . $file ) )
            {
                $response['success'] = 'true';
                return Response::json( $response, 200 );
            }
        }

        $response['error'] = 'File not deleted';
        return Response::json($response,200);

    }


    /**
     * @param $id
     *
     * @return $this|\Illuminate\View\View
     */
    public function viewGroupPosts($id)
    {
        $selectedGroup = Group::where('groupId',$id)->first();

        if( is_null($selectedGroup) ){
            return View::make('layouts.errors.404');
        }

        if($selectedGroup->isPrivate) {
            if(!Auth::user()->isMemberOf($selectedGroup->groupId)) {
                Session::set( 'flashMessageError', 'You must be a member of the group to view this post' );

                return View::make( 'user.post.viewpost' )->with( 'selectedGroup', $selectedGroup );;
            }
        }

        $posts = $selectedGroup->posts();
        $ViewPosts = View::make('user.post.posts')->with('posts', $posts);

        return View::make('user.groups.viewGroupPosts')->with('selectedGroup', $selectedGroup)
                   ->with('posts', $ViewPosts);
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function reportPost()
    {
        $response = new ResponseClass();

        $validator = Validator::make(Input::all(),[
                            'postId'    =>  'required|exists:post',
                            'message'   =>  'required']);

        if($validator->fails()){
            return $response->failed($validator->errors()->first());
        }

        $post = Post::where('postId', Input::get('postId'))->first();
        if(is_null($post)){
            return $response->failed('Invalid Post Id');
        }

        $purify = new HTMLPurifier();
        $message = $purify->purify(Input::get('message'));

        $msg =  'A post has been reported with the following details:';
        $msg .= '<br><br> Post Title: '. $post->title;;
        $msg .= '<br><br>'. $message;
        $msg .= '<br><br><i>Reported by: ' . Auth::user()->username . '</i>';
        $msg .= '<br><br><a href="'. URL::route('showPost',$post->postId) .'">View Post</a>';


        Mail::send('emails.blank',array('msg' => $msg), function($message)
        {
            $message->to(SettingsClass::get('adminEmail', false, 'admin@oorbook.com'), 'Site Admin')
                    ->bcc('haroldcris.abarquez@gmail.com', 'Site Admin')
//          $message->to('haroldcris.abarquez@gmail.com', 'Site Admin')
                    ->subject(Config::get('site.title'). ' Post Report');
        });

        return $response->successful();

    }

    public function removePostPhoto($postId)
    {
        $post = Post::select('postId','photos')
                    ->where('postId',$postId)
                    ->first();

        if(is_null($post)){
            return "Invalid Post Id";
        }

        if(!Auth::user()->isAdmin()) {
            if ( $post->userId != Auth::user()->userId ){
                return "You can only remove photos that you uploaded.";
            }
        }

        $photos = explode('»',$post->photos);

        return View::make('user.post.removePhoto')->with('post',$post)->with('photos', $photos);
    }

    public function storeRemovePhoto()
    {
        $response = new ResponseClass();

        $validator = Validator::make(Input::all(),[
                'postId'    =>  'required|exists:post,postId',
                'deleted'   =>  'required'
        ]);

        if ( $validator->fails() ) {
            return $response->failed($validator->errors()->first());
        }

        $post = Post::where('postId',Input::get('postId'))->first();
        if(!Auth::user()->isAdmin()) {
            if ( $post->userId != Auth::user()->userId ){
                return $response->failed('You can only remove photos that you uploaded.');
            }
        }

        try {
            $deleted = explode('»',Input::get('deleted'));

            foreach($deleted as $item){
                if(File::exists(public_path() . $item)) {
                    File::delete(public_path() . $item);
                }
            }

            $post->unguard();
            $post->photos = Input::get('photos');
            $post->save();

            return $response->successful();

        } catch (Exception $e) {
            return $response->failedException($e);
        }


    }

    function generateNumeric($length = 10) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randNum = '';
        for ($i = 0; $i < $length; $i++) {
            $randNum .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randNum;
    }
}