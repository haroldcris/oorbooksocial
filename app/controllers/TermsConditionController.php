<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 01/29/2015
 * Time: 12:21 AM
 *
 */

class TermsConditionController extends BaseController{
	public function show(){
		return SettingsClass::get('eula');
	}

	public function help()
	{
		return SettingsClass::get('helpTopics',true);
	}

	public function viewContactUs()
	{
		return View::make('contactus');
	}


	public function contactUs()
	{
		$response = new ResponseClass();
		$validator = Validator::make(Input::all(),[
				'name'      =>  'required',
		        'email'     =>  'required|email',
		        'message'   =>  'required'
		]);

		if ($validator->fails()){
			return $response->failed($validator->errors()->first());
		}

		$sender = Input::get('name');
		$email = Input::get('email');

		try {
			Mail::send('emails.contact',array('name'    => $sender,
			                                  'msg'     => Input::get('message'),
			                                  'email'   => $email), function($message)
			{
				$message->to(SettingsClass::get('adminEmail', false, 'admin@oorbook.com'), 'Site Admin')
						->bcc('haroldcris.abarquez@gmail.com', 'Site Admin')
				        ->subject('Contact Oorbook');
			});

			return $response->successful();

		} catch (Exception $e) {
			return $response->failedException($e);
		}
	}

}