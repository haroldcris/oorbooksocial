<?php

class TestingController extends BaseController {

	public function index()	{
	   // $items = LocationScope::where('locationId', 3)->first();
	   // $cn = explode(",", $items->continent);
	    
	   // return View::make('test2')->with('cn', $cn);
	    
	    $foo = LocationScope::where('locationId', 17)->first();
        // Manual Setting of Geos
        $iCN = explode(",", $foo->continent);
        $iCT = explode(",", $foo->country);
        $iR1 = explode(",", $foo->region1);
        $iR2 = explode(",", $foo->region2);
        $iR3 = explode(",", $foo->region3);
        $iR4 = explode(",", $foo->region4);
        $iLC = explode(",", $foo->locality);
        
        if(count($iCN) == 0 || $iCN == "" || $iCN[0] == "" || $iCN[0] == null) { $ctrCN = 0; } else { $ctrCN = count($iCN); }
        if(count($iCT) == 0 || $iCT == "" || $iCT[0] == "" || $iCT[0] == null) { $ctrCT = 0; } else { $ctrCT = count($iCT); }
        if(count($iR1) == 0 || $iR1 == "" || $iR1[0] == "" || $iR1[0] == null) { $ctrR1 = 0; } else { $ctrR1 = count($iR1); }
        if(count($iR2) == 0 || $iR2 == "" || $iR2[0] == "" || $iR2[0] == null) { $ctrR2 = 0; } else { $ctrR2 = count($iR2); }
        if(count($iR3) == 0 || $iR3 == "" || $iR3[0] == "" || $iR3[0] == null) { $ctrR3 = 0; } else { $ctrR3 = count($iR3); }
        if(count($iR4) == 0 || $iR4 == "" || $iR4[0] == "" || $iR4[0] == null) { $ctrR4 = 0; } else { $ctrR4 = count($iR4); }
        if(count($iLC) == 0 || $iLC == "" || $iLC[0] == "" || $iLC[0] == null) { $ctrLC = 0; } else { $ctrLC = count($iLC); }
        
        
        
	    
	    return View::make('yeetest')->with('iCN', $iCN)->with('iCT', $iCT)
	                                ->with('iR1', $iR1)->with('iR2', $iR2)
	                                ->with('iR3', $iR3)->with('iR4', $iR4)
	                                ->with('iLC', $iLC)
	                                ->with('ctrCN', $ctrCN)->with('ctrCT', $ctrCT)
	                                ->with('ctrR1', $ctrR1)->with('ctrR2', $ctrR2)
	                                ->with('ctrR3', $ctrR3)->with('ctrR4', $ctrR4)
	                                ->with('ctrLC', $ctrLC);
    
	}
	
	public function store() {
	    $response = new ResponseClass();
	    $scope = new LocationScope();
	    
	    $checkerd = Input::get('checkerd');
	    
	    if(Input::has('checkerd')) {
	        return $response->successful($checkerd);
	    }
	    return $response->failed('Naknangtokwamaymaliputek!');
	    
        $scope->continent = implode(",",$checkerd);
        if($scope->save()){
            return $response->successful($scope->continent);    
        }
	    
	    return $response->failed('Naknangtokwamaymaliputek!');
	}
	
	
	
	public function getGeoContinent() {
        $items = TableLocationGeo::select('continent')->distinct()
                                                           ->where('continent', '!=', '')
                                                           ->orderBy('continent', 'asc')
                                                           ->get();
        return Response::json($items);
    }
    
    public function getGeoCountry() {
        $continent = Input::get('hidContinent');
        
        $items = TableLocationGeo::select('country')->distinct()
                                                    ->where('continent', '=', $continent)
                                                    ->where('country', '!=', '')
                                                    ->orderBy('country', 'asc')
                                                    ->get();
        return Response::json($items);
    }
    
    public function getGeoRegion1() {
        $continent = Input::get('hidContinent');
        $country   = Input::get('hidCountry');
        
        $items = TableLocationGeo::select('region1')->distinct()
                                                    ->where('continent', '=', $continent)
                                                    ->where('country', '=', $country)
                                                    ->where('region1', '!=', '')
                                                    ->orderBy('region1', 'asc')
                                                    ->get();
        return Response::json($items);
    }
    
    public function getGeoRegion2() {
        $continent = Input::get('hidContinent');
        $country   = Input::get('hidCountry');
        $region1   = Input::get('hidRegion1');
        
        $items = TableLocationGeo::select('region2')->distinct()
                                                    ->where('continent', '=', $continent)
                                                    ->where('country', '=', $country)
                                                    ->where('region1', '=', $region1)
                                                    ->where('region2', '!=', '')
                                                    ->orderBy('region2', 'asc')
                                                    ->get();
        return Response::json($items);
    }
    
    public function getGeoRegion3() {
        $continent = Input::get('hidContinent');
        $country   = Input::get('hidCountry');
        $region1   = Input::get('hidRegion1');
        $region2   = Input::get('hidRegion2');
        
        $items = TableLocationGeo::select('region3')->distinct()
                                                    ->where('continent', '=', $continent)
                                                    ->where('country', '=', $country)
                                                    ->where('region1', '=', $region1)
                                                    ->where('region2', '=', $region2)
                                                    ->where('region3', '!=', '')
                                                    ->orderBy('region3', 'asc')
                                                    ->get();
        return Response::json($items);
    }
    
    public function getGeoRegion4() {
        $continent = Input::get('hidContinent');
        $country   = Input::get('hidCountry');
        $region1   = Input::get('hidRegion1');
        $region2   = Input::get('hidRegion2');
        $region3   = Input::get('hidRegion3');
        
        $items = TableLocationGeo::select('region4')->distinct()
                                                    ->where('continent', '=', $continent)
                                                    ->where('country', '=', $country)
                                                    ->where('region1', '=', $region1)
                                                    ->where('region2', '=', $region2)
                                                    ->where('region3', '=', $region3)
                                                    ->where('region4', '!=', '')
                                                    ->orderBy('region4', 'asc')
                                                    ->get();
        return Response::json($items);
    }
    
    public function getGeoLocality() {
        $continent = htmlspecialchars(Input::get('hidContinent'));
        $country   = htmlspecialchars(Input::get('hidCountry'));
        $region1   = htmlspecialchars(Input::get('hidRegion1'));
        $region2   = htmlspecialchars(Input::get('hidRegion2'));
        $region3   = htmlspecialchars(Input::get('hidRegion3'));
        $region4   = htmlspecialchars(Input::get('hidRegion4'));
        
        $items = TableLocationGeo::select('locality')->distinct()
                                                     ->where('continent', '=', $continent)
                                                     ->where('country', '=', $country)
                                                     ->where('region1', '=', $region1)
                                                     ->where('region2', '=', $region2)
                                                     ->where('region3', '=', $region3)
                                                     ->where('region4', '=', $region4)
                                                     ->where('locality', '!=', '')
                                                     ->orderBy('locality', 'asc')
                                                     ->get();                                                    
        return Response::json($items);
    }
    
    
    
    
    
}


































