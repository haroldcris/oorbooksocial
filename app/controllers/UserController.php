<?php

class UserController extends BaseController {

	public function manage() {
		$user = User::select('userId','username','email','securityLevel')->where('deleted', '=', 0)
					->orderBy('username')
					->paginate(20);

		$xuser = User::select('userId','username','email','securityLevel')->where('deleted', '=', 1)->get();

		$roles = Role::select('roleId','name')->orderBy('name')->get();

		return View::make('admin.users.user_manage', ['users'  => $user,
		                                              'xusers' => $xuser ,
													  'roles'  => $roles]);
	}



	public function store() {
		$response = new ResponseClass();

		if(Input::has('userId')) {
			$validation = Validator::make( Input::all(), [
				'userId'           => 'required|exists:user',
				'email'            => 'required|min:5|email|unique:user,email,'. Input::get('userId') . ',userId',
			    'securityLevel'    => 'required|exists:role,name'
			] );

		} else {
			$validation = Validator::make( Input::all(), [
				'email'           => 'required|min:5|email|unique:user,email',
				'username'        => 'required|min:5|unique:user,username',
				'password'        => 'required|min:5',
				'retype_password' => 'required|same:password',
				'securityLevel'    => 'required|exists:role,name'
			] );
		}

		if($validation->fails()){
			return $response->failed($validation->errors()->first());
		}


		$user = new User();
		if(Input::has('userId')) {
			$user = User::where('userId', Input::get('userId'))->first();

			if(is_null($user)){
				return $response->failed('Invalid UserId');
			}
		}

		$user->unguard();
		$user->email            = Input::get('email');
		$user->securityLevel    = Input::get('securityLevel');

		if(!Input::has('userId')) {
			$user->username      = Input::get( 'username' );
			$user->password      = Hash::make( Input::get( 'password' ) );
			$user->manuallyAdded = 1;
		}

		try {
			$user->save();
			return $response->successful();
		} catch (Exception $e) {
			return $response->failedException($e);
		}
	}



	public function deactivate() {
		$response   = new ResponseClass();
		
		$id         = Input::get('id');
		$setBoolean = User::select('userId', 'deleted')->where('userId', $id)->first();

		$setBoolean->deleted = 1;

		try {
			$setBoolean->save();
			return $response->successful();
		} catch (Exception $e) {
			return $response->failedException($e);
		}
	}



	public function restore() {
		$response   = new ResponseClass();
		
		$id         = Input::get('id');
		$setBoolean = User::select('userId', 'deleted')->where('userId', $id)->first();

		$setBoolean->deleted = 0;

		try {
			$setBoolean->save();
			return $response->successful();
		} catch (Exception $e) {
			return $response->failedException($e);
		}
	}



	public function delete() {
		$response   = new ResponseClass();

		$id         = Input::get('id');
		$setBoolean = User::select('userId', 'deleted')->where('userId', $id)->first();
		
		try {
			$setBoolean->delete();
			return $response->successful();
		} catch (Exception $e) {
			return $response->failedException($e);
		}
	}


	public function edit($userId)
	{
		$user = User::select('userId','username','email','securityLevel')
					->where('userId',$userId)
					->first();

		if(is_null($user)){
			return "Invalid UserId";
		}

		$roles = Role::select('roleId','name')->orderBy('name')->get();
		return View::make('admin.users.create')->with('user', $user)
											   ->with('roles',$roles);
	}


	public function create()
	{
		$roles = Role::select('roleId','name')->orderBy('name')->get();
		return View::make('admin.users.create')->with('roles',$roles);
	}
}
