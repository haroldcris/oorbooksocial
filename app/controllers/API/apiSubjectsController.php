<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/20/2015
 * Time: 4:17 PM
 *
 */

class apiSubjectsController extends BaseController{

	public function index(){
		$subjects = SubjectClass::select('subjectId','subjectOrder','description','icon')->orderBy('subjectOrder')->get();

		return $subjects;
	}
}