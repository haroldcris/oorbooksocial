<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/01/2015
 * Time: 6:28 PM
 *
 */

class apiNoticeController extends BaseController {

	public function index(){
		$api = ApiCredential::orderBy('country')
		                    ->orderBy('region1')
		                    ->orderBy('region2')
		                    ->orderBy('region3')
		                    ->orderBy('region4')
		                    ->orderBy('locality')->get();

		return View::make('api.noticeKey')->with('api', $api);
	}

	public function storeNoticeKey(){
//		$response = Config::get('site.response');

		/*******************************
		* Start validation
		********************************/
//		$fields = ['country','region1','region2','region3','region4','locality','scope'];
//		$fieldsFormal = ['Country', 'Region 1', 'Region 2', 'Region 3', 'Region 4', 'Locality','Scope'];

//		foreach($fields as $field){
//			if( is_null(Request::get($field)) ){
//				$response['error'] = $field . ' not specified';
//				return Response::json($response,406);
//			}
//			$response[$field] = Request::get($field);
//		}
//
//		$scope = Request::get('scope');
//
//		//Validate Scope
//		if ( ! in_array($scope ,['continent','country','region1','region2','region3','region4','locality']) ){
//			$response ['error'] = 'Invalid scope value';
//			return Response::json($response,200);
//		}

//		$scopeIndex = array_search(Input::get('scope'),$fields);
//		for($i = 0; $i <= $scopeIndex; $i++){
//			if ( Input::get($fields[$i]) == '' ){
//				$response['error'] = $fieldsFormal[$i] . ' is required';
//				return Response::json($response,200);
//			}
//		}

//		if(Input::get('country') == '' || Input::get('locality') == ''){
//			$response['error'] = 'Country and Locality is required.';
//			return Response::json($response,200);
//		}

		$response = new ResponseClass();

		$validator = Validator::make(Input::all(),[
								'scope'             => 'required|in:country,region1,region2,region3,region4,locality' ,
								'searchLocationTab' => 'required'] );

		if($validator->fails()) {
			return $response->failed($validator->errors()->first());
		}

		if(Input::get('searchLocationTab') == 'location') {
			if (!Input::has('tableLocation')) {
				return $response->failed('Select location to continue');
			}
		} else {
			if(!Input::has('tableLocationGeo')) {
				return $response->failed('Select location from database to continue');
			}
		}

		/*******************************
		* End of Validation
		********************************/
		if (Input::get('searchLocationTab') == 'locationGeo') {
			$locGeo = TableLocationGeo::where( 'id', Input::get( 'tableLocationGeo' ) )->first();
			if ( is_null($locGeo) ) {
				return $response->failed("Invalid Location Geo Id");
			}

			$country = is_null($locGeo->country) ? '' : $locGeo->country;
			$region1 = is_null($locGeo->region1) ? '' : $locGeo->region1;
			$region2 = is_null($locGeo->region2) ? '' : $locGeo->region2;
			$region3 = is_null($locGeo->region3) ? '' : $locGeo->region3;
			$region4 = is_null($locGeo->region4) ? '' : $locGeo->region4;
			$locality= is_null($locGeo->locality) ? '': $locGeo->locality;

		} else {
			$tableLocation = TableLocation::where('tableLocationId', Input::get('tableLocationId'))->first();

			if(is_null($tableLocation)){
				return $response->failed('Invalid Location Id');
			}

			$country 	= $tableLocation->Country;
			$region1    = $tableLocation->Province;
			$region2    = $tableLocation->District;
			$region3    = $tableLocation->Division;
			$region4	= '';
			$locality   = $tableLocation->Village;
		}

		$apiKey = str_random(60);
		while ( !is_null(ApiCredential::where('apiKey',$apiKey)->first()) ) {
			$apiKey = str_random(60);
		}

		$data = ['apiKey'       => $apiKey,
				 'scope'        => Input::get('scope'),
				 'country'      => $country,
				 'region1'      => $region1,
		         'region2'      => $region2,
		         'region3'      => $region3,
		         'region4'      => $region4,
		         'locality'     => $locality,
		         'created_at'   => new \Carbon\Carbon(),
		         'updated_at'   => new \Carbon\Carbon(),
		         'userId'       => Auth::user()->userId
		];

		DB::table('apiCredential')->insert($data);
		$response->apiKey     = $apiKey;
		return $response->successful();
	}


	public function createNoticeKey(){
		return View::make('api.apiCreateNoticeKey');
	}

	/*******************************
	* CREATE NOTICE
	********************************/
	public function createNotice(){
		return View::make('notice.create');
	}

	public function storeNotice(){
		//title, description, link
		$response = Config::get('site.response');

		$fields = ['title','description','apikey'];
		foreach($fields as $field){
			if( is_null( Request::get($field) ) || Request::get($field) =='' ){
				$response['error'] = $field . ' not specified';
				return Response::json($response,200);
			}
			$response[$field] = Request::get($field);
		}

		//Verify apiKey
		$apiKey = Input::get('apikey');
		$api = ApiCredential::where('apiKey', $apiKey)->first();
		if( is_null($api) ) {
			$response ['error'] = 'Invalid Api Key';
			return Response::json( $response, 200);
		}


		//Clean Data
		$config = HTMLPurifier_Config::createDefault();
		$config->set('HTML.SafeIframe', true);
		$config->set('URI.SafeIframeRegexp', '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%'); //allow YouTube and Vimeo
		$purifier = new HTMLPurifier($config);

		$title       = $purifier->purify(Input::get('title'));
		$link        =Input::has('link') ? Input::get('link') : '/';
		$description = $purifier->purify(Input::get('description'));

		try {
			$notice = new Notice;
			$notice->unguard();
			$notice->userId          = Auth::user()->userId;
			$notice->apiCredentialId = $api->apiCredentialId;
			$notice->title           = $title;
			$notice->description     = $description;
			$notice->link            = $link;


			$notice->save();

			$response ['success'] = true;

			return Response::json( $response, 200 );

		}catch (Exception $e){
			Log::error($e->getMessage());
			$response['error'] = 'Error occurred while processing request';
			return Response::json($response,400);
		}

	}


	public function deleteNoticeKey(){
		$response = Config::get('site.response');

		if( ! Input::has('apiId') ){
			$response['error'] = 'API id not specified';
			return Response::json($response,400);
		}

		try {
			$api = ApiCredential::where( 'apiCredentialId', Input::get( 'apiId' ) )
							    ->where( 'userId' , Auth::user()->userId)
								->first();

			if ( is_null( $api ) ) {
				$response['error'] = 'Invalid API Credential Id or not the creator';

				return Response::json( $response, 400 );
			}

			$api->delete();

			$response['success'] = 'true';
			return Response::json($response,200);

		} catch (Exception $e){
			$response['error'] = $e.getMessage();
			return Response::json($response,406);
		}
	}



	public function deleteNotice(){
		$response = Config::get('site.response');

		if( ! Input::has('noticeId') ){
			$response['error'] = 'id not specified';
			return Response::json($response,400);
		}

		try {
			$notice = Notice::where( 'noticeId', Input::get( 'noticeId' ) )
			                    ->where( 'userId' , Auth::user()->userId)
			                    ->first();

			if ( is_null( $notice ) ) {
				$response['error'] = 'Invalid Id or not the creator';
				return Response::json( $response, 400 );
			}

			$notice->delete();

			$response['success'] = 'true';
			return Response::json($response,200);

		} catch (Exception $e){
			$response['error'] = $e.getMessage();
			return Response::json($response,406);
		}
	}

	public function rssLocal()
	{
		return $this->rss(true);
	}

	/*******************************
	 * Render RSS
	 ********************************/
	public function rss($localOnly = false){
		$rss = <<<'EOF'
<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0">
    <channel>
        <title>%channelTitle%</title>
        <link>%channelLink%</link>
        <description>%channelDescription%</description>
        %items%
	</channel>
</rss>
EOF;
		$rssItem = <<<'EOF'
 <item>
	<title>%itemTitle%</title>
	<link>%itemLink%</link>
	<pubDate>%itemDate%</pubDate>
	<description>%itemDescription%</description>
</item>
EOF;
		$rss = str_replace( '%channelTitle%', Config::get('site.title'), $rss );
		$rss = str_replace( '%channelLink%', URL::to('/'), $rss );

		if(Request::has('locationId')){
			$location = Location::where('locationId', Request::get('locationId'))->first();

			if(is_null($location)){
				/*******************************
				 * Invalid Location id
				 ********************************/
				$rss = str_replace( '%items%', '', $rss);
				return Response::make( $rss, 200 )->header( 'Content-Type', 'text/xml' );
			}

			$notice = $location->notices($localOnly);

		} else{
			$notice = Notice::all();
		}


		if(is_null($notice)) {
			$rss = str_replace( '%items%', '', $rss);
			return Response::make( $rss, 200 )->header( 'Content-Type', 'text/xml' );
		}

		$items = '';
		foreach($notice as $item) {
			$element = $rssItem;

			$element = str_replace( '%itemTitle%'       , $item->title , $element  );
			$element = str_replace( '%itemLink%'        , $item->link , $element  );
			$element = str_replace( '%itemDescription%' , $item->description , $element  );
			$element = str_replace( '%itemDate%'        , \Carbon\Carbon::createFromTimeStamp(strtotime($item->created_at))->diffForHumans(), $element  );

			$items.= $element;
		}

		$rss = str_replace( '%items%', $items, $rss);
		return Response::make($rss,200)->header('Content-Type', 'text/xml');
	}



}