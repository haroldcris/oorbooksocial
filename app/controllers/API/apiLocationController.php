<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/04/2015
 * Time: 2:27 PM
 *
 */

class apiLocationController extends BaseController {

    public function search() {
        $response = new ResponseClass();

        $validator = Validator::make(Input::all(),['data'   => 'required' ]);
        if($validator->fails()){
            return $response->failed($validator->errors()->first());
        }

        if (Input::has('limit')) {
            $limit = Input::get('limit');
        } else {
            $limit = 10;
        }


        return TableLocation::search(Input::get('data'), $limit);
    }

    public function searchGeo() {
        if (Input::has('limit')) {
            $limit = Input::get('limit');
        } else {
            $limit = 20;
        }

        return TableLocationGeo::search(Input::get('data'), $limit, Input::get('country'));
    }


    public function enrolledLocations(){
        if(Auth::check()){
                return $enrolled = Auth::user()->enrolledLocations()
                                               ->orderBy('name')
                                               ->get();
        }
        return null;
    }
    
    

    public function viewEnroll(){
        return View::make('location.searchLocation');
    }

    public function enroll(){
        $response = ["success"=>"false","error"=>"Invalid Request"];

        if(!Input::has('tableLocationId') && !Input::has('MapData') && !Input::has('tableLocationGeoId')){
                return Response::json($response,400);
        }

        if(strtolower(Auth::user()->securityLevel) != 'admin') {
            $enrolledCount = Auth::user()->enrolledLocations()
                                 ->select( 'location.locationId' )
                                 ->count();

            if ( $enrolledCount > 2 ) {
                $response['error'] = "You can not add more than three (3) locations";

                return Response::json( $response, 200 );
            }
        }


        DB::begintransaction();

        try{

                //Find from Page 1
                if(Input::has('tableLocationId')){
                        $tableLocation = TableLocation::where('tableLocationId', Input::get('tableLocationId'))->first();

                        if(is_null($tableLocation)){
                                return Response::json($response,400);
                        }

                        $locationCode = $tableLocation->GNNum . '|' . $tableLocation->Village . '|' . $tableLocation->PlaceCode;	

                        $name = $tableLocation->Continent . ' » ' .
                                $tableLocation->Country . ' » ' .
                                $tableLocation->Province . ' » ' .
                                $tableLocation->District . ' » ' .
                                $tableLocation->Division . ' » ' .
                                $tableLocation->Village . ' » ' .
                                $tableLocation->PlaceCode;

                        $data['code'] 		= $locationCode;
                        $data['name'] 		= $name;
                        $data['continent'] 	= $tableLocation->Continent;
                        $data['country'] 	= $tableLocation->Country;
                        $data['region1'] 	= $tableLocation->Province;
                        $data['region2']	= $tableLocation->District;
                        $data['region3']	= $tableLocation->Division;
                        $data['region4']	= '';
                        $data['locality'] 	= $tableLocation->Village;
                        $data['postal'] 	= $tableLocation->PlaceCode;

                        $data['lat']		= '';
                        $data['lng']		= '';
                        $data['address']	= $tableLocation->Village ;
                } 

                if(Input::has('MapData')){
                        if(Input::get('MapData')['city'] == ''){
                                $response['error'] = 'Location is too big. Choose certain address.';
                                return Response::json($response,200);
                        }

                        $input = Input::get('MapData');
                        $name = '';
                        if($input['country']  != ''){ $name .= $input['country']; }
                        if($input['state'] 	  != ''){ $name .= ' » ' . $input['state']; }
                        if($input['district'] != ''){ $name .= ' » ' . $input['district']; }
                        if($input['city'] 	  != ''){ $name .= ' » ' . $input['city']; }
                        if($input['postal']   != ''){ $name .= ' » ' . $input['postal']; }

                        $locationCode = $name;

                        $data['code'] 		= $locationCode;
                        $data['name'] 		= $locationCode;
                        $data['continent'] 	= '';
                        $data['country'] 	= $input['countryLong'];
                        $data['region1'] 	= $input['state'];
                        $data['region2']	= $input['district'];
                        $data['region3']    = '';
                        $data['region4']    ='';
                        $data['locality'] 	= $input['city'];
                        $data['postal'] 	= $input['postal'];

                        $data['lat']		= $input['lat'];
                        $data['lng']		= $input['lng'];
                        $data['address']	= $input['address'];

                }

                //Find from Page 3 GEo Database
                if(Input::has('tableLocationGeoId')){
                    $tableLocation = TableLocationGeo::where('id', Input::get('tableLocationGeoId'))->first();

                    if(is_null($tableLocation)){
                        return Response::json($response,400);
                    }

                    $locationCode = $tableLocation->id;
                    $name = '';
                    if($tableLocation->country  != ''){ $name .= $tableLocation->country; }
                    if($tableLocation->region1  != ''){ $name .= ' » ' . $tableLocation->region1; }
                    if($tableLocation->region2  != ''){ $name .= ' » ' . $tableLocation->region2; }
                    if($tableLocation->region3  != ''){ $name .= ' » ' . $tableLocation->region3; }
                    if($tableLocation->region4  != ''){ $name .= ' » ' . $tableLocation->region4; }
                    if($tableLocation->locality != ''){ $name .= ' » ' . $tableLocation->locality; }
                    if($tableLocation->postcode != ''){ $name .= ' » ' . $tableLocation->postcode; }


                    $data['code'] 		= $locationCode;
                    $data['name'] 		= $name;
                    $data['continent'] 	= $tableLocation->continent;
                    $data['country'] 	= $tableLocation->country;
                    $data['region1'] 	= $tableLocation->region1;
                    $data['region2']	= $tableLocation->region2;
                    $data['region3']	= $tableLocation->region3;
                    $data['region4']	= $tableLocation->region4;
                    $data['locality'] 	= $tableLocation->locality;
                    $data['postal'] 	= $tableLocation->postcode;

                    $data['lat']		= $tableLocation->latitude;
                    $data['lng']		= $tableLocation->longitude;
                    $data['address']	= $tableLocation->locality . ' ' . $tableLocation->postcode  ;
                }


                /*******************************************
                * Start Checking if Location Already Exists
                ********************************************/
                $location = Location::select(['locationId','code'])->where('code', $locationCode)->first();

                if(is_null($location)) {
                        $location = new Location();
                        $location->unguard();
                        $location->code 		= $data['code'];
                        $location->name 		= $data['name'];
                        $location->continent 	= $data['continent'];
                        $location->country 		= $data['country'];
                        $location->region1 		= $data['region1'];
                        $location->region2 	    = $data['region2'];
                        $location->region3      = $data['region3'];
                        $location->region4      = $data['region4'];
                        $location->locality		= $data['locality'];
                        $location->postal 		= $data['postal'];
                        $location->lat          = $data['lat'];
                        $location->lng          = $data['lng'];
                        
                        $base = $location->locality;                    
                        $base = str_replace('North','',$base);
                        $base = str_replace('East','',$base);
                        $base = str_replace('South','',$base);
                        $base = str_replace('West','',$base);
                        $base = str_replace('Centre','',$base);

                        $location->baseLocality = $base;
                } else {
                        //Check if Already Enrolled
                        if( !is_null(Auth::user()->enrolledLocations()->select('location.locationId')->where('location.locationId', $location->locationId)->first()) ) {
                                $response['error']	= 'You are already enrolled in this location';
                                return Response::json($response, 200);
                        }
                }


                $isPrimary = Auth::user()->enrolledLocations()->count() ? 0 : 1;

                $ret = Auth::user()->enrolledLocations()->save($location,['isPrimary'	=> $isPrimary, 'address' 	=> $data['address'], ]);

                //Successful Operation
                if($ret) {
                        DB::commit();
                        Cache::forget('enrolledLocations');

                        if(!Session::has('selectedLocationId')){
                                if($isPrimary){	Session::set('selectedLocationId', $location->locationId); }
                        }

                        return Response::json( [ "success" => "true", "error" => "","id"=> $location->locationId], 200 );
                }

        } catch (Exception $e){
                Log::error($e);
                DB::rollback();
                return Response::json( [ "success" => "false", "error" => $e->getMessage()], 200 );
        }

    }

    public function delete(){
        $response = ["success" => "false", "error" => ""];

        if(!Auth::check()) { return Response::json($response,401); }
        if( !Input::has('userLocationId') ) { return Response::json($response,400); }

        $input = Input::get('userLocationId');

        foreach($input as $item){

                //DB::delete('delete from userLocation where userLocationId = :id',[':id'=> $item]);
                $ret = UserLocation::select('userLocationId','locationId')->where('userLocationId',$item)->first();

                if(Session::has('selectedLocationId')) {
                    if ( $ret->locationId == Session::get( 'selectedLocationId' ) ) {
                        Session::forget('selectedLocationId');
                    }
                }

                $ret->delete();

                if(Session::has('selectedUserLocationId')){
                        if (Session::get('selectedUserLocationId') == $item){
                                Session::forget('selectedUserLocationId');
                                Session::forget('selectedLocationId');
                        }
                }
        }

        Cache::forget('enrolledLocations');

        $response['success'] = "true";

        return Response::json($response,200);
    }


    public function setPrimarylocation(){
        $response = ["success" => "false", "error" => ""];

        if(!Auth::check()) { return Response::json($response,401); }
        if( !Input::has('userLocationId') ) { return Response::json($response,400); }

        $input = Input::get('userLocationId');

        DB::update('update userLocation 
                                        set isPrimary = case when userLocationId = :userLocationId then 1 else 0 end
                                        where userId = :userId',[':userLocationId'  => $input, 
                                                                 ':userId'          => Auth::user()->userId]);

        $response['success'] = "true";

        return Response::json($response,200);
    }


}