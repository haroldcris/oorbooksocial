<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/06/2015
 * Time: 10:19 AM
 *
 */

class apiUserController extends BaseController {

	public function searchUser (){
		if (!Request::has('username')){
			return App::abort(404);
		}

		$username = Request::get('username');
		$limit = Request::get('limit');
		if(is_null($limit)) $limit = 20;

		$result = [];

		$user = User::select('userId','username','email','imageAvatar')
						->where('username','like',"$username%")
						->where('activated',1)
						->orderby('username')
						->take($limit)
						->get();

		foreach($user as $index => $item){
			$user[$index]->imageAvatar = '/user/' . $item->userId . '/avatar';
			$item->type= 'user';
			$result[] = $item;
		}
		
		$group = Group::with([ 'owner' => function($query){ $query->select('userId','username'); }] )->select('groupId','name','creatorId')
						->where('name','like',"%$username%")
						->orderBy('name')
						->take($limit)
						->get();
		foreach($group as $item){
			$item->name = mb_strimwidth($item->name,0,15,'...');
			$item->owner->username = mb_strimwidth($item->owner->username,0,15,'...');
			$item->type = 'group';
			$result[] = $item;
		}


		$posts = Post::with(['creator' => function($query){$query->select('userId','username')->get();}])
					->select('userId','postId','title')
					->where('title' ,'like', "%$username%")
					->orWhere('matter','like',"%$username%")
					->orderBy('title')
					->take($limit)
					->get();

		foreach($posts as $postItem){
			$postItem->title = mb_strimwidth($postItem->title,0,30,'...');
			$postItem->creator->username = mb_strimwidth($postItem->creator->username,0,15,'...');
			$postItem->type = 'post';
			$result[] = $postItem;
		}
		
		$tags = Post::with(['creator' => function($query){$query->select('userId','username')->get();}])
					->select('userId','postId','matter')
					->Where('matter','like',"%$username%")
					->orderBy('matter')
					->take($limit)
					->get();

		foreach($tags as $tagItem){
			$tagItem->matter = mb_strimwidth($tagItem->matter,0,30,'...');
			$tagItem->creator->username = mb_strimwidth($tagItem->creator->username,0,15,'...');
			$tagItem->type = 'tags';
			$result[] = $tagItem;
		}


		return Response::json($result,200);
	}


	public function followUser(){
		$response = ["success" => "false", "error" => ""];

		if (!Input::has('userId')) {
			return Response::json($response,400);
		}

		//$user = User::where('userId', Input::get('userId'));
		$requestedBy = Auth::user()->userId;
		$requestedTo = Input::get('userId');

		$ret = DB::insert('insert into followerRequest set requestedById = :rb, requestedToId = :rt',[':rb' => $requestedBy, ':rt' => $requestedTo]);
		if($ret) { $response['success'] = "true"; }

		return Response::json($response,200);
	}


	public function unFollowUser(){
		$response = ["success" => "false", "error" => ""];

		if (!Input::has('userId')) {
			return Response::json($response,400);
		}

		//$user = User::where('userId', Input::get('userId'));
		$requestedBy = Auth::user()->userId;
		$requestedTo = Input::get('userId');

		$ret = DB::delete('delete from followerRequest where requestedById = :rb and requestedToId = :rt',[':rb' => $requestedBy, ':rt' => $requestedTo]);
		if($ret) { $response['success'] = "true"; }

		return Response::json($response,200);
	}



	public function getFolloweeUpdatesCount(){
		$response = ["success" => "false", "error" => "","updates" => "0"];

		$lastCheck = Auth::user()->getUserSettings('lastNotificationTimeCheck');
		if(is_null($lastCheck)){
			$lastCheck = \Carbon\Carbon::now();
			Auth::user()->setUserSettings('lastNotificationTimeCheck', $lastCheck);
		}

		$notification = DB::select('select count(*) as total from logBook where userId in (select requestedToId from followerRequest where requestedById = :userId) and created_at >= :lastCheck', [':userId' => Auth::user()->userId, ':lastCheck' => $lastCheck]);
		$response['lastCheck'] = $lastCheck;
		$response['updates'] = $notification[0]->total;
		$response['success'] = "true";

		return Response::json($response,200);

	}





	public function getFolloweeUpdates(){
		if(Request::has('limit')){
			$limit = (integer)Request::get('limit');
		} else {
			$limit = 100;
		}

		$notification = DB::select('select logBook.* from logBook where public = 0 and userId in (select requestedToId from followerRequest where requestedById = :userId) UNION select logBook.* from logBook where public = 1 order by created_at desc limit :limit', [':userId' => Auth::user()->userId, ':limit'=> $limit]);

		//Loop and Get User Information
		foreach($notification as $key => $item){
			$notification[$key]->created = \Carbon\Carbon::createFromTimeStamp(strtotime($item->created_at))->diffForHumans();

//			$user = User::select('username','userId','imageAvatar','sex')->where('userId',$item->userId)->first();
//
//			if(!$user->imageAvatar){
//				if($user->sex =='female') {
//					$user->imageAvatar = '/assets/images/avatar_female.png';
//				} else {
//					$user->imageAvatar = '/assets/images/avatar_male.jpg';
//				}
//			}
//
//			$notification[$key]->user = $user;
		}

		if(Request::has('updatetimecheck')){
			Auth::user()->setUserSettings('lastNotificationTimeCheck', \Carbon\Carbon::now());
		}

		return Response::json($notification, 200);
	}
}