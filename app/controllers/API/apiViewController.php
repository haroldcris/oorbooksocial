<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/10/2015
 * Time: 7:11 PM
 *
 */

class apiViewController  extends BaseController {

	public function weather(){
		$expiration = 60 * 60 * 3; //3 hours
		//$expiration = 10;

		$lastUpdate = time();
		if ( Session::has( 'weatherLastUpdate' ) ) {
			$lastUpdate = Session::get( 'weatherLastUpdate' );
		}

		$expired = false;
		if ( time() - $lastUpdate > $expiration ) {
			$expired = true;
		}

		if($expired) {
			Session::forget('currentWeather');
		}



		if( !Session::has('currentWeather') ) {
			$ip = Request::getClientIp();
			if(App::environment() == 'local'){
				$ip = file_get_contents("http://bot.whatismyipaddress.com/");
			}

			$url = 'http://api2.worldweatheronline.com/free/v2/weather.ashx?q=' . $ip . '&format=json&num_of_days=1&key=12670eeed5dfe6da4a559bd2603ca';

			$res  = Curl::make( $url );
			$json = json_decode( $res );


			Session::set( 'currentWeather', $json );
			Session::set( 'weatherLastUpdate', time() );

			return Response::json ($json, 200);
		}


		return Response::json (Session::get('currentWeather'),200);
	}


	public function searchYoutube(){
		$response = Config::get('site.response');

		if (!Input::has('q')){
			$response['error'] = 'Search parameter is required';
			return Response::json($response,200);
		}

		$q          = urlencode(Input::get('q'));
		$pageToken  = urlencode(Input::get('pageToken'));
		$api        = Config::get('site.apiKey.google');
		$maxResults = 3;

		$param = 'part=snippet&fields=items(id%2Ckind%2Csnippet)%2CnextPageToken%2CprevPageToken';
		if($pageToken) {
			$param .= '&pageToken=' . $pageToken;
		}

		$json = Curl::make('https://www.googleapis.com/youtube/v3/search?'.$param.'&q=' . $q . '&maxResults='. $maxResults.'&key=' . $api);


		return Response::make($json,200)->header('Content-Type', 'Application/json');

	}




}