<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/22/2015
 * Time: 9:22 PM
 *
 */

class CommentController extends BaseController {

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function store (){
		$response = Config::get('site.response');

		if(! Input::has('postId')) {
			$response ['error'] = 'Invalid post Id';
			return Response::json ($response, 200);
		}

		if( !Input::has('message')){
			$response ['error'] = 'Comment is required';
			return Response::json ($response, 200);
		}

		if(Input::get('message') == ''){
			$response ['error'] = 'Comment is required';
			return Response::json ($response, 200);
		}

		$purifier = new HTMLPurifier();

		$postId = (int)Input::get('postId');
		$message = trim($purifier->purify(Input::get('message')));

		if ( $message == ''){
			$response ['error'] = 'Comment is required';
			return Response::json ($response, 200);
		}

		$parent = Input::has('parentId') ? Input::get('parentId') : 0;
		$level = Input::has('level') ? Input::get('level') : 0;
		// End validation

		try {
			$comment = new CommentClass;
			$comment->unguard();
			$comment->userId  = Auth::user()->userId;
			$comment->postId  = $postId;
			$comment->message = $message;
			$comment->parentId = $parent;
//			$comment->level = $level;
			$comment->save();

			Event::fire('commentCreated', $comment);
			$response['success'] = 'true';
			$response['comment'] = $comment;
			$response['username'] = Auth::user()->username;
			return Response::json ($response, 200);

		} catch( Exception $e){
			$response ['error'] = 'Error occurred while saving';
			return Response::json ($response, 406);
		}

	}


	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function delete(){
		$response = Config::get('site.response');

		if(!Input::has('commentId')){
			$response['error'] = 'Comment Id is required';
			return Response::json($response, 200);
		}

		if(Auth::user()->isAdmin()) {
			$comment = CommentClass::where( 'commentId', Input::get( 'commentId' ) )
			                       ->first();
		} else {
			$comment = CommentClass::where( 'commentId', Input::get( 'commentId' ) )
			                       ->where( 'userId', Auth::user()->userId )
			                       ->first();
		}

		if(is_null($comment)){
			$response ['error'] = 'Invalid Comment Id or not the creator';
			return Response::json($response, 200);
		}

		try {
			$comment->delete();
			$response['success'] = 'true';
			return Response::json($response, 200);

		} catch(Exception $e){
			$response ['error'] = 'Error occurred while deleting record';
			return Response::json($response, 406);
		}

	}
}