<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/03/2015
 * Time: 4:36 AM
 *
 */

class ResponseClass
{
	public $description;

	/**
	 * @var string
	 */
	public $success;

	/**
	 * @var optional
	 */
	public $data;

	public function __construct()
	{
		$this->success = 'false';
		$this->data = null;
		$this->description = '';
	}


	/**
	 * @param null $data
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function successful($data = null)
	{
		$this->success = 'true';
		if(!is_null($data)){
			$this->data = $data;
		}
		return Response::json($this, 200);
	}

	/**
	 * @param $description
	 * @param int $statusCode
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function failed($description = 'An error occurred...', $statusCode = 200)
	{
		$this->success = 'false';
		$this->error = $description;

		return Response::json( $this, $statusCode);
	}


	/**
	 * @param Exception $e
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function failedException (Exception $e)
	{
		myLog::Error($e->getMessage());
		$description = 'An error occurred while performing the process. ';
		if( Config::get('app.debug') ) {
			$description .= $e->getMessage();
		}

		$this->success = 'false';
		$this->error = $description;
		return Response::json( $this, 200);
	}

	/**
	 * @param $jsonResponse
	 *
	 * @return Object
	 */
	private  static function decodeApiResponse($jsonResponse)
	{
		$decode = json_decode( $jsonResponse->getContent() );

		$decode->success = $decode->success == 'true' ? true : false;
		return $decode;
	}


//	public static function call($ControllerAction, $params = null)
//	{
//		$array = explode('@',$ControllerAction);
//		$controller = $array[0];
//		$action = $array[1];
//
//		if(is_null($params)) {
//			return static::decodeApiResponse( App::make( $controller )->{$action}() );
//		}
//		return static::decodeApiResponse( App::make( $controller )->{$action}( $params) );
//	}


	public static function create($action, $route, $params =null)
	{
		if(is_null($params)){
			$request = Request::create($route, $action);
		}else{
			$request = Request::create($route, $action, $params);
		}

		$response = Route::dispatch($request);

		return static::decodeApiResponse($response);
	}
}
