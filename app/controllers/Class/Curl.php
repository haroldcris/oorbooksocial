<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/27/2015
 * Time: 7:36 AM
 *
 */

class Curl {
	public static function make($URL){
		//$timeout=5;
		$c = curl_init();
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($c, CURLOPT_URL, $URL);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);

		//curl_setopt ($c, CURLOPT_CONNECTTIMEOUT, $timeout);

		$contents = curl_exec($c);
		curl_close($c);

		return $contents;
		// if ($contents) return $contents;
		//  	else return FALSE;
	}
}