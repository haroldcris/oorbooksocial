<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/02/2015
 * Time: 7:58 AM
 *
 */

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class myLog {
	static $logName = 'myLogger';
	public static function Error ($msg)
	{
		$log = new Logger('logger');
		$log->pushHandler(new StreamHandler(storage_path() .'/logs/debug.log', Logger::ERROR));

//		$arr = explode(':', $msg);
//		foreach($arr as $item) {
//			$log->addError($item);
//		}
		$log->addError($msg);

	}

}