<?php

class DivisionController extends BaseController {
    
    public function getSelectedLocation($id, $view) {
		$selectedLocation = Location::where('locationId', $id)->first();

		if(is_null($selectedLocation)){
			return 'Invalid Location Id';
		}
		return View::make($view, compact('selectedLocation'));
	}
	
	public function divisionLocality($id){
	    return $this->getSelectedLocation($id, 'division.locality');
	}
	public function divisionRegion4($id){
		return $this->getSelectedLocation($id, 'division.region4');
	}
	public function divisionRegion3($id){
		return $this->getSelectedLocation($id, 'division.region3');
	}
	public function divisionRegion2($id){
		return $this->getSelectedLocation($id, 'division.region2');
	}
	public function divisionRegion1($id){
		return $this->getSelectedLocation($id, 'division.region1');
	}
	public function divisionCountry($id){
		return $this->getSelectedLocation($id, 'division.country');
	}
}