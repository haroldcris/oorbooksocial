<?php 

class FollowerController extends BaseController {

	// public function index() {
	// 	return View::make('user.followers');
	// }

	// public function index() {
	// 	$followers = User::select('userId','username','email','securityLevel')->where('deleted', '=', 1)->get();
	// 	return View::make('user.followers', ['followers'  => $followers]);
	// }

	public function index( $username = null ) {

		$viewAsPublic = true;
		if ( Auth::check() ) {
			$viewAsPublic = false;
		}

		if ( is_null( $username ) ) {
			$user = Auth::user();
		} else {
			$field = 'username';
			if ( is_numeric( $username ) ) {
				$field = 'userId';
			}
			$user = User::where( $field, $username )->first();
			if ( is_null( $user ) ) {
				return App::abort( 404 );
			}
		}

		$iAmFollowingThisUser = false;
		if ( ! $viewAsPublic ) {
			// Checks if I follow the user
			if ( ! is_null( Auth::user()->followees()->select( 'userId' )->where( 'requestedToId', $user->userId )->first() ) ) {
				$iAmFollowingThisUser = true;
			}
		}

		$AuthUser = Auth::user();

		$follow   = '<i class="fa fa-thumb-tack"></i> <b>Follow this user</b>';
		$unfollow = '<i class="fa fa-thumb-tack"></i> <b>Stop following</b>';


		if ( ! $viewAsPublic ) {
			$followers = $user->followers()->select( 'userId', 'username', 'sex', 'imageAvatar')->get();
		}

		if ( $viewAsPublic ) {
			$site = Config::get( 'site.title' );
			Session::set( 'flashMessageSuccess', "<h4>$user->username is on $site</h4><div class='space-5'></div>
												To connect to $user->username, sign up for $site today
												<div class='space-5'></div>
												<a class='btn btn-sm btn-info btn-white btn-round' href='/' > Sign Up</a>&nbsp;&nbsp;
												<a class='btn btn-sm btn-success btn-white btn-round' href='/' > Log In</a>" );
		}


		$isOwner = false;
		$guestLocation = null;
		if(Auth::check()){
			if (Auth::user()->userId == $user->userId) {
				$isOwner = true;
			} else {
				$guestLocation = $user->enrolledLocations()->first();
			}
		}

		$selectedLocation = null;
		if( Session::has('selectedLocationId') ){
			$selectedLocation = Location::where('locationId', Session::get('selectedLocationId'))->first();
		}

		return View::make('user.followers', ['followers'=>$followers, 'AuthUser'=>$AuthUser, 'user'=>$user, 'viewAsPublic'=>$viewAsPublic, 'isOwner'=>$isOwner, 'guestLocation'=>$guestLocation, 'selectedLocation'=>$selectedLocation]);

		// return View::make( 'user.profile' )->with( 'AuthUser', $AuthUser )
		//            ->with( 'user', $user )
		//            ->with( 'iAmFollowingThisUser', $iAmFollowingThisUser )
		//            ->with( 'followersCount', $followersCount )
		//            ->with( 'viewAsPublic', $viewAsPublic )
		//            ->with( 'follow', $follow )
		//            ->with( 'unfollow', $unfollow )
		// 	       ->with('isOwner', $isOwner)
		// 		   ->with('guestLocation', $guestLocation)
		// 	       ->with('selectedLocation', $selectedLocation);
	}

	public function avatar( $userIdOrUsername ) {
		$field = 'username';
		if ( is_numeric( $userIdOrUsername ) ) {
			$field = 'userId';
		}

		$user = User::where( $field, $userIdOrUsername )->select( 'userId', 'sex', 'imageAvatar' )->first();

		if ( is_null( $user ) ) {
			return null;
		}

		$filename = $user->imageAvatar;
		if ( is_null( $filename ) ) {
			$sex = $user->sex;
			if ( is_null( $sex ) ) {
				$sex = 'male';
			}
			$filename = public_path() . '/assets/images/avatar_' . $sex . '.jpg';
		}

		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_URL, $filename );
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 20 );
		curl_setopt( $ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT'] );
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt( $ch, CURLOPT_HEADER, true );
		curl_setopt( $ch, CURLOPT_NOBODY, true );

		//$file = curl_exec ($ch);
		$file     = file_get_contents( $filename );
		$filetype = curl_getinfo( $ch, CURLINFO_CONTENT_TYPE );

		$response = Response::make( $file, 200 );
		$response->header( 'Content-Type', $filetype );

		return $response;
	}

}
