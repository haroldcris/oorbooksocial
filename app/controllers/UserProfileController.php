<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 01/29/2015
 * Time: 9:07 PM
 *
 */


class UserProfileController extends BaseController {

	public function profile( $username = null ) {

		$viewAsPublic = true;
		if ( Auth::check() ) {
			$viewAsPublic = false;
		}

		if ( is_null( $username ) ) {
			$user = Auth::user();
		} else {
			$field = 'username';
			if ( is_numeric( $username ) ) {
				$field = 'userId';
			}
			$user = User::where( $field, $username )->first();
			if ( is_null( $user ) ) {
				return App::abort( 404 );
			}
		}

		$iAmFollowingThisUser = false;
		if ( ! $viewAsPublic ) {
			if ( ! is_null( Auth::user()->followees()->select( 'userId' )->where( 'requestedToId', $user->userId )->first() ) ) {
				$iAmFollowingThisUser = true;
			}
		}

		$AuthUser = Auth::user();

		$follow   = '<i class="fa fa-thumb-tack"></i> <b>Follow this user</b>';
		$unfollow = '<i class="fa fa-thumb-tack"></i> <b>Stop following</b>';


		$followersCount = 0;
		$followingsCount = 0;
		if ( ! $viewAsPublic ) {
			$followersCount = $user->followers()->select( 'userId' )->get()->count();
			$followingsCount = $user->followees()->select( 'userId' )->get()->count();
		}

		if ( $viewAsPublic ) {
			$site = Config::get( 'site.title' );
			Session::set( 'flashMessageSuccess', "<h4>$user->username is on $site</h4><div class='space-5'></div>
												To connect to $user->username, sign up for $site today
												<div class='space-5'></div>
												<a class='btn btn-sm btn-info btn-white btn-round' href='/' > Sign Up</a>&nbsp;&nbsp;
												<a class='btn btn-sm btn-success btn-white btn-round' href='/' > Log In</a>" );
		}


		$isOwner = false;
		$guestLocation = null;
		if(Auth::check()){
			if (Auth::user()->userId == $user->userId) {
				$isOwner = true;
			} else {
				$guestLocation = $user->enrolledLocations()->first();
			}
		}

		$selectedLocation = null;
		if( Session::has('selectedLocationId') ){
			$selectedLocation = Location::where('locationId', Session::get('selectedLocationId'))->first();
		}


		return View::make( 'user.profile' )->with( 'AuthUser', $AuthUser )
		           ->with( 'user', $user )
		           ->with( 'iAmFollowingThisUser', $iAmFollowingThisUser )
		           ->with( 'followersCount', $followersCount )
		           ->with( 'followingsCount', $followingsCount )
		           ->with( 'viewAsPublic', $viewAsPublic )
		           ->with( 'follow', $follow )
		           ->with( 'unfollow', $unfollow )
			       ->with('isOwner', $isOwner)
				   ->with('guestLocation', $guestLocation)
			       ->with('selectedLocation', $selectedLocation);
	}


	public function edit() {
		$array = [ 'userId', 'activationCode', 'username', 'password', 'activated', 'securityLevel' ];

		if ( in_array( Input::get( 'name' ), $array ) ) {
			return Response::json( [ 'errors' => 'Invalid Input' ], 404 );
		}
		$attr = Input::get( 'name' );

		$user                  = Auth::user();
		$user->$attr           = Input::get( 'value' );
		$user->isFirstTimeUser = 0;
		$user->save();

		Cache::forget( 'AuthUser' );


		$array = [
			'country'        => 'country',
			'birthdate'      => 'birth day',
			'birthPlace'     => 'place of birth',
			'sex'            => '',
			'maritalStatus'  => 'marital status',
			'education'      => 'educational attainment',
			'occupation'     => 'occupation',
			'field'          => 'field of expertise',
			'childrenCount'  => 'number of children',
			'sexOrientation' => 'sexual orientation',
			'religion'       => 'religion'
		];

		if ( ! $array[ $attr ] == '' ) {
			$data = $array[ $attr ];
			LogBook::logAction( "%username% updated %sex% $data" );
		}

		return Response::json( [ 'errors' => '' ], 200 );
	}


	public function avatar( $userIdOrUsername ) {
		$field = 'username';
		if ( is_numeric( $userIdOrUsername ) ) {
			$field = 'userId';
		}

		$user = User::where( $field, $userIdOrUsername )->select( 'userId', 'sex', 'imageAvatar' )->first();

		if ( is_null( $user ) ) {
			return null;
		}

		$filename = $user->imageAvatar;
		if ( is_null( $filename ) ) {
			$sex = $user->sex;
			if ( is_null( $sex ) ) {
				$sex = 'male';
			}
			$filename = public_path() . '/assets/images/avatar_' . $sex . '.jpg';
		}

		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_URL, $filename );
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 20 );
		curl_setopt( $ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT'] );
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt( $ch, CURLOPT_HEADER, true );
		curl_setopt( $ch, CURLOPT_NOBODY, true );

		//$file = curl_exec ($ch);
		$file     = file_get_contents( $filename );
		$filetype = curl_getinfo( $ch, CURLINFO_CONTENT_TYPE );

		$response = Response::make( $file, 200 );
		$response->header( 'Content-Type', $filetype );

		return $response;
	}

	public function storeAvatar() {
		$response = Config::get( 'site.response' );
	
		$file_max = ini_get('upload_max_filesize');
		$response['max_file_size'] = $file_max;


		/*************************/
		/*** Added this section **/
		/*************************/
		if(Input::has('cropped-file')){
			$data = Input::get('cropped-file');
			$filename =  public_path() . '/users/' . strtolower( Auth::user()->username ). '/avatar.png';

			list($type, $data) = explode(';', $data);
			list(, $data)      = explode(',', $data);
			$data = base64_decode($data);
			file_put_contents( $filename, $data);

			if (!$this->updateUserAvatar($filename)) {
				$response['error'] = 'Something went wrong while updating user profile';
				return Response::json( $response, 200 );				
			}

			$response['success'] = 'true';
			return Response::json( $response, 200 );	
		}

		/***** CONTINUE SAVING FILE IF CROPPED-FILE WAS NOT FOUND    ****/

		if ( ! Input::hasFile( 'avatar-file' ) ) {
			$response['error'] = 'No file to upload or file size is too large';

			return Response::json( $response, 200 );
		}
		
		$file = Input::file( 'avatar-file' );

		$validator = Validator::make( Input::all(), [ 'avatar-file' => 'required|image' ] );

		if ( $validator->fails() ) {
			$response['error'] = 'file is required and must be an image.';

			return Response::json( $response, 200 );
		}
		
		$file = Input::get('hide-input');
		$filename = time() . $file->getClientOriginalName();

		$path     = public_path() . '/users/' . strtolower( Auth::user()->username );

		if ( ! file_exists( $path ) ) {
			File::makeDirectory( $path, 0755, true, true );
		}



		$file->move( $path, $filename );

		if (!$this->updateUserAvatar($path . '/' . $filename)) {
			$response['error'] = 'Something went wrong while updating user profile';
			return Response::json( $response, 200 );				
		}

		$response['success'] = 'true';
		return Response::json( $response, 200 );	
	}


	public function updateUserAvatar($filename)
	{
		$user = Auth::user();
		$user->unguard();
		$user->imageAvatar = $filename;
		$user->save();

		Event::fire( 'changeAvatar' );
		return true;
	}



	public function deleteAvatar() {
		$response = Config::get( 'site.response' );

		try {
			$user = Auth::user();
			$user->unguard();
			$user->imageAvatar = null;
			$user->save();

			$response['success'] = 'true';

			return Response::json( $response, 200 );

		} catch ( Exception $e ) {
			$response['error'] = 'Error occurred while deleting record';

			return Response::json( $response, 406 );
		}
	}

}