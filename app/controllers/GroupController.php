<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/03/2015
 * Time: 3:55 AM
 *
 */

class GroupController extends BaseController
{
	private function notifyAdd($group, $userId)
	{
		$action = '%username% added %user% to %sex% group <a href="%href%">%group%</a>';

		$user = User::select('userId','username')->where('userId', $userId)->first()->username;

		$action = str_replace('%user%', $user, $action);
		$action = str_replace('%href%', URL::route('manageSingleGroup',$group->groupId) , $action);
		$action = str_replace('%group%', $group->name, $action);

		LogBook::logAction($action);
	}

	private function notifyJoin ($group)
	{
		$action = '%username% joined <a href="%href%">%group%</a>';
		//$user = User::select('userId','username')->where('userId', $group->creatorId)->first()->username;

		$action = str_replace('%href%', URL::route('manageSingleGroup',$group->groupId) , $action);
		$action = str_replace('%group%', $group->name, $action);

		LogBook::logAction($action);
	}

	public function apiUserGroups()
	{
		$response = new ResponseClass();
		$response->remarks = 'list of Groups created by authenticated user';

		//return $response->failed();
		if(Auth::guest()){
			return App::abort(401);
		}

		$groups = Group::select(DB::raw('`group`.* , count(*) totalMembers'))
		               ->leftJoin('groupMember','group.groupId','=','groupMember.groupId')
					   ->where('group.creatorId',Auth::user()->userId)
					   ->groupBy('group.groupId')
					   ->orderBy('group.name')
		               ->get();


		return $response->successful($groups);
	}

	public function apiMembers($groupId)
	{
		$response = new ResponseClass();
		$response->description = 'Members of the specified group';

		$group = Group::with('owner')->where('groupId', $groupId)
						->first();

		if (is_null($group)) {
			return $response->failed('Invalid Group Id');
		}

		$data = [];
		$data['groupInfo']  = $group;
		$data['members']    = $group->members()->orderBy('username')->get();
		$data['currentUserMembership'] = null;
		if(Auth::check()){
			$data['currentUserMembership'] = Auth::user()->getGroupSubscriptionDate($groupId);
		}
		return $response->successful($data);
	}


	public function apiViewGroup($groupId)
	{
		$response = new ResponseClass();

		$group = Group::where('groupId', $groupId)->first();

		if(is_null($group)){
			return $response->failed('Invalid Group Id');
		}

		return $response->successful($group);
	}


	public function delete()
	{
		$response = new ResponseClass();

		$validation = Validator::make(Input::all(), ['id' => 'required|numeric']);
		if ($validation->fails()){
			return $response->failed($validation->errors()->first());
		}

		$group = Group::where('groupId', Input::get('id') )
						->where('creatorId', Auth::user()->userId )
						->first();

		if(is_null($group)){
			return $response->failed('Invalid Group Id or Not the creator');
		}
		
		try {

			$group->delete();
			Event::fire('groups.deleted', $group);
		    return $response->successful();

		} catch (Exception $e) {
		    return $response->failedException($e);
		}
	}

	public function store()
	{
		$response = new ResponseClass();

		$id = Input::get('id');
		if(is_null($id)) {
			//New Group
			$validation = Validator::make(Input::all(), ['name' => 'required|max:200']);
		} else {
			//Edit Group
			$validation = Validator::make(Input::all(), ['name' => 'required|max:200|unique:group,groupId,null,' . $id ]);
		}

		if($validation->fails()){
			return $response->failed($validation->errors()->first());
		}


		if(is_null($id)){
			$group = Group::select('groupId')
							->where('name',Input::get('name'))
							->where('creatorId', Auth::user()->userId)
							->first();
			if(!is_null($group)) {
				return $response->failed('You already have similar group name1');
			}
		} else {
			$group = Group::select('groupId')
						    ->where('groupId','<>', $id)
							->where('name',Input::get('name'))
			                ->where('creatorId', Auth::user()->userId)
							->first();

			if(!is_null($group)) {
				return $response->failed('You already have similar group name');
			}
		}

		$group = new Group();
		if(!is_null($id)){
			$group = Group::where('groupId', $id)->first();
			if(is_null($group)){
				return $response->failed('Invalid Id. Can not update.');
			}
		}

		$private = 0;
		if(Input::has('private')) $private = 1;

		$group->unguard();
		$group->name    = Input::get('name');
		$group->isPrivate = $private;
		$group->creatorId  = Auth::user()->userId;

		try {
			$group->save();

			if(is_null($id)) {
				DB::table( 'groupMember' )->insert( [
					'groupId'        => $group->groupId,
					'userId'         => $group->creatorId,
					'membershipdate' => \Carbon\Carbon::create()
				] );
				Event::fire('groups.created', $group);
			}

			return $response->successful($group);

		} catch (Exception $e) {
			return $response->failedException($e);
		}
	}

	public function viewGroupMembers($id)
	{

		//$posts = View::make('user.post.posts')->with('posts')
		//return View::make('user.groups.viewGroup')->with('posts',null);
	}


	public function create()
	{
		$group = null;
		if (Input::has('id')) {
			$group = Group::where('groupId', Input::get('id'))->first();
		}
		return View::make('user.groups.create')->with('group', $group);
	}


	public function apiJoin($groupId)
	{
		return $this->apiAddMember($groupId, true);
	}

	public function apiAddMember($groupId, $userJoinMode  = false)
	{
		$response = new ResponseClass();
		$response->description = 'Add user to specified group';

		$id = Input::get('userId');
		if ($userJoinMode ){
			$id = Auth::user()->userId;
		}

		$userId = $id;
		if(!is_numeric($id)) {
			$userId = User::select('userId')->where('username', $id)->first();
			if(is_null($userId)){
				return $response->failed('Invalid User Id');
			}
			$userId = $userId->userId;
		}

		$input = array_merge( Input::except('userId'), ['groupId' => $groupId, 'id' => $userId]);
		$validator = Validator::make( array_merge( Input::except('userId'), ['groupId' => $groupId, 'userId' => $userId]),[
						'groupId'   => 'exists:group',
						'userId'    => 'required|exists:user|unique:groupMember,userId,NULL,groupId,groupId,'. $groupId
			],['userId.unique' => 'User has already been added to this group']
		);

		if($validator->fails()){
			return $response->failed($validator->errors()->first());
		}


		if($userJoinMode) {
			$group = Group::where( 'groupId', $groupId )
			              ->first();

			if($group->isPrivate) {
				return $response->failed('Can not join in a private group');
			}
		} else {
			$group = Group::select('groupId','name')
			              ->where('groupId',$groupId)
			              ->where('creatorId', Auth::user()->userId)
			              ->first();

			if (is_null($group)) {
				return $response->failed( 'Can not proceed. You are not the owner of the specified group' );
			}
		}


		try {
			DB::insert('INSERT INTO groupMember(groupId, userId, membershipDate) values (?,?,?)',[$groupId, $userId, \Carbon\Carbon::create()]);

			if ($userJoinMode) {
				$this->notifyJoin ($group);
			}  else {
				$this->notifyAdd($group,$userId);
			}
		    return $response->successful();

		} catch (Exception $e) {
		    return $response->failedException($e);
		}


	}

	public function apiDeleteMember($groupId)
	{
		$response = new ResponseClass();

		$validator = Validator::make( array_merge(Input::all(),['groupId' => $groupId, 'creatorId' => Auth::user()->userId] ) ,[
						'groupId'   => 'required|exists:group',
						'userId'    => 'required|exists:groupMember,userId,groupId,' . $groupId
			],['userId.exists' => 'User is not a member of this group' ,]
		);

		if($validator->fails()){
			return $response->failed($validator->errors()->first());
		}

		if(Auth::user()->userId != Input::get('userId')) {
			$group = Group::select( 'groupId' )
			              ->where( 'groupId', $groupId )
			              ->where( 'creatorId', Auth::user()->userId )
			              ->first();
			if ( is_null( $group ) ) {
				return $response->failed( 'You are not the owner of the specified group' );
			}
		}

		try {
		    DB::delete('delete from groupMember where groupId =? and userId=?',[$groupId,Input::get('userId')]);
			Event::fire('groups.unsubscribed', []);
		    return $response->successful();
		} catch (Exception $e) {
		    return $response->failedException($e);
		}
	}

	public function subscriptions()
	{
		$response = new ResponseClass();
		if(Auth::guest()) {
			return $response->failed('You have been log out');
		}
			$subscriptions = Auth::user()->groupSubscriptions;
		return $response->successful($subscriptions);
	}


	public function viewUserGroups()
	{
		$response = ResponseClass::create('get', URL::route('apiUserGroups'));
		if (!$response->success) {
			return App::abort(400);
		}

		$groups = $response->data;
		return View::make( 'user.groups.index' )->with( 'groups', $groups );
	}

	public function viewSingleGroup($groupId)
	{
		$field = 'groupId';
		if(!is_numeric($groupId)){ $field = 'name'; }

		$group = Group::where($field , $groupId)
					  ->first();

		if(is_null($group)) {
			return App::abort(404);
		}

		$response = ResponseClass::create('get',URL::route('apiGroupMembers', $group->groupId));

		return View::make('user.groups.viewGroup')->with('data',$response->data)
												  ->with('selectedGroup', $group);
	}


	public function viewAddMember()
	{
		return View::make('user.groups.addMember');
	}
}