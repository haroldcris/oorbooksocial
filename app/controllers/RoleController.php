<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/27/2015
 * Time: 8:46 AM
 *
 */

class RoleController extends BaseController{
	public function index()
	{
		$roles = Role::all();
		return View::make('admin.roles.index')->with('roles', $roles);
	}


	public function create()
	{
		return View::make('admin.roles.create');
	}

	public function update($id)
	{
		$role = Role::where('roleId',$id)->first();

		if (is_null($role)){
			return 'Invalid Role Id';
		}

		return View::make('admin.roles.create')->with('role', $role);
	}


	public function store()
	{
		$response = new ResponseClass();

		if(Input::has('roleId')) {
			$validator = Validator::make( Input::all(), [
				'name'      => 'required',
			    'roleId'    => 'exists:role,roleId'
			] );
		} else {
			$validator = Validator::make( Input::all(), [
				'name' => 'required|unique:role,name'
			] );
		}


		if ( $validator->fails() ) {
		    return $response->failed($validator->errors()->first());
		}

		$manageRole = Input::has('manageRole')? 1 : 0;
		$manageUser = Input::has('manageUser')? 1 : 0;
		$managePoll = Input::has('managePoll')? 1 : 0;
		$manageInterest = Input::has('manageInterest')? 1 : 0;
		$manageAPI = Input::has('manageAPI')? 1 : 0;
		$manageSiteContent = Input::has('manageSiteContent')? 1 : 0;

		$role = new Role();
		if(Input::has('roleId')) {
			$role = Role::where('roleId', Input::get('roleId'))->first();

			if(Input::get('name') != $role->name) {
				switch ( strtolower( $role->name ) ) {
					case 'admin':
					case 'user':
					case 'super user':
						break;
					default:
						return $response->failed( 'Can not update built-in roles' );
				}
			}
		}

		$role->unguard();
		$role->name = Input::get('name');
		$role->manageRole           = $manageRole;
		$role->manageUser           = $manageUser;
		$role->managePoll           = $managePoll;
		$role->manageInterest       = $manageInterest;
		$role->manageAPI            = $manageAPI;
		$role->manageSiteContent    = $manageSiteContent;
		
		try {
		    $role->save();
		    return $response->successful($role);
		} catch (Exception $e) {
		    return $response->failedException($e);
		}
	}

	public function delete()
	{
		$response = new ResponseClass();
		
		$validator = Validator::make(Input::all(),['roleId' => 'required|exists:role,roleId']);
		if ( $validator->fails() ) {
		    return $response->failed($validator->errors()->first());
		}
		
		$role = Role::select('roleId','name')->where('roleId',Input::get('roleId'))->first();

		switch(strtolower($role->name)){
			case 'admin':
			case 'user':
			case 'super user':
				return $response->failed('Can not delete built-in Roles');
				break;

			default:

		}

		try {
		    $role->delete();
		    return $response->successful();
		} catch (Exception $e) {
		    return $response->failedException($e);
		}
	}

}