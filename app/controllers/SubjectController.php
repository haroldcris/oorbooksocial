<?php

class SubjectController extends BaseController {

	public function manage() {
		$subject = SubjectClass::orderBy('subjectOrder')->get();
		return View::make('admin.subjects.subject_manage', ['subjects'=>$subject]);
	}



	public function store() {
		$response = new ResponseClass();

		$subject = new SubjectClass;
		$id = Input::get('id');

		if(!empty($id)){
			$subject = SubjectClass::where('subjectId', $id)->first();
			if(is_null($subject)){
				return $response->failed('Invalid Id. Can not update.');
			}
		}


		$validation = Validator::make(Input::all(), [
			'subjectOrder' => 'required|numeric|unique:subject,subjectOrder,' . $id . ',subjectId',
			'description'  => 'required|unique:subject,description,' . $id . ',subjectId'
		]);

		if($validation->fails()){
			return $response->failed($validation->errors()->first());
		}


		$subject->unguard();
		$subject->subjectOrder = Input::get('subjectOrder');
		$subject->description  = Input::get('description');

		try {
			$subject->save();
			return $response->successful();
		} catch (Exception $e) {
			return $response->failedException($e);
		}
	}



	public function delete() {
		$response   = new ResponseClass();

		$id         = Input::get('id');
		$setBoolean = SubjectClass::select('subjectId')->where('subjectId', $id)->first();
		
		try {
			$setBoolean->delete();
			return $response->successful();
		} catch (Exception $e) {
			return $response->failedException($e);
		}
	}

}
