<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/05/2015
 * Time: 10:08 AM
 *
 */

class SettingsController extends BaseController {

	public function setSettings () {
		// params:
		//  - attribute   { if 'userTheme', save the User table}
		//  - value
		$ret = ['success' => false];

        $attribute = Input::get('attribute');
        $value     = Input::get('value');

		if($attribute == 'themeColor'){
			Auth::user()->themeColor = $value;
			Auth::user()->save();
			Cache::forget('AuthUser');

			$ret['success'] = true;
		}
		return Response::json($ret,200);
	}

	public function getSettings() {
		return View::make('settings.settings');
	}

	public function postSaveSettings() {
		$response = new ResponseClass();

		if(Input::has('attribute')) {
            $attr = Input::get('attribute');
            $val  = Input::get($attr);
			SettingsClass::set($attr, $val);
			return $response->successful();
		}


		if(Input::has('att')) {
			$att = Input::get( 'att' );
			$val = Input::get( 'data' );
			SettingsClass::set( $att, $val );

			if($att == 'eula') {
				//Notify User
				LogBook::logAction('Our terms of service has changed. By continuing to use Oorbook you accept the terms & conditions. <a href="/eula" target="_new">Read them here</a>', true);
			}
			return $response->successful();
		}

		if(Input::has('attColor')) {
			SettingsClass::set( Input::get( 'attColorNavbar' ), Input::get( 'colorNavbar' ) );
			SettingsClass::set( Input::get( 'attColorPanel' ), Input::get( 'colorPanel' ) );
			SettingsClass::set( Input::get( 'attColorPanelText' ), Input::get( 'colorPanelText' ) );
			return $response->successful();
		}

		if (Input::has('adsSettings')) {
			if ( Input::has( 'googleAds1' ) ) {
				SettingsClass::set( 'googleAds1', Input::get( 'googleAds1' ), true );
			}

			if ( Input::has( 'googleAds2' ) ) {
				SettingsClass::set( 'googleAds2', Input::get( 'googleAds2' ), true );
			}

			if ( Input::has( 'googleAds3' ) ) {
				SettingsClass::set( 'googleAds3', Input::get( 'googleAds3' ), true );
			}

			if ( Input::has( 'googleAnalytics' ) ) {
				SettingsClass::set( 'googleAnalytics', Input::get( 'googleAnalytics' ), true );
			}

			if ( Input::has( 'googleRemarketing' ) ) {
				SettingsClass::set( 'googleRemarketing', Input::get( 'googleRemarketing' ), true );
			}

			if ( Input::has( 'footerWidget' ) ) {
				SettingsClass::set( 'footerWidget', Input::get( 'footerWidget' ), true );
			}


			return $response->successful();
		}


		if(Input::has('helpTopics')){
			SettingsClass::set('helpTopics',Input::get('helpTopics'), true);
			return $response->successful();
		}
//			$attTopWidget = Input::get('attTopWidget');
//			$valTopWidget = Input::get('valTopWidget');
//			SettingsClass::set($attTopWidget, $valTopWidget);
//
//
//			$attWidget1 = Input::get('attWidget1');
//			$valWidget1 = Input::get('valWidget1');
//			SettingsClass::set($attWidget1, $valWidget1 ,  true);
//
//			$attWidget2 = Input::get('attWidget2');
//			$valWidget2 = Input::get('valWidget2');
//			SettingsClass::set($attWidget2, $valWidget2, true);
//
//			$attTwitter1 = Input::get('attTwitter1');
//			$valTwitter1 = Input::get('valTwitter1');
//			SettingsClass::set($attTwitter1, $valTwitter1, true);
//
//			$attTwitter2 = Input::get('attTwitter2');
//			$valTwitter2 = Input::get('valTwitter2');
//			SettingsClass::set($attTwitter2, $valTwitter2, true);
//
//			$attGoogleAnalytics = Input::get('attGoogleAnalytics');
//			$valGoogleAnalytics = Input::get('valGoogleAnalytics');
//			SettingsClass::set($attGoogleAnalytics, $valGoogleAnalytics,true);
//
//			$attGoogleRemarketing = Input::get('attGoogleRemarketing');
//			$valGoogleRemarketing = Input::get('valGoogleRemarketing');
//			SettingsClass::set($attGoogleRemarketing, $valGoogleRemarketing,true);
//		}

//		$response['success'] = 'true';
//		return Response::json($response, 200);

	}

	public function postSaveSettingsBanner() {
		$response = Config::get('site.response');

		$file = Input::file('banner');		
		$dest = public_path() . '/uploaded/';

		//Generate file name
		$filename = value(function() use ($file){
			$archivo = '/uploaded/' . Auth::user()->username . '_' . str_random(20) . '.' . $file->getClientOriginalExtension();
			return $archivo;
		});
		// $filename = public_path() . '/uploaded/mushroom.png');

		while (File::exists($dest . $filename)){
			$filename = value(function() use ($file){
		        $archivo = '/uploaded/' . Auth::user()->username . '_' . str_random(20) . '.' . $file->getClientOriginalExtension();
		        return $archivo;
		    });
		}


		if($file->move($dest, $filename)) {
			SettingsClass::set('Banner', $filename);

			$response['success'] = 'true';
			$response['filename'] = $filename;
			return Response::json($response, 200);
		}

		$response['error'] = 'Unable to move the file';
		return Response::json($response);	
	}

	public function upload(){
		$response = new ResponseClass();

		try {
			$relativePath = strtolower( '/assets/images/ads/' );
			$path = public_path() . $relativePath;

			if (!file_exists($path)) {
				File::makeDirectory($path,0755,true,true);
			}

			$file = Input::file('file');
			$original = $file->getClientOriginalName();
			$filename =  time() . '_' . $original;

			$file->move($path, $filename);

			$image = new SimpleImage( $path . $filename );
			if($image->get_width() > 300 )
			{
				$image->fit_to_width(300)->save();
			}

			//$response['filename'] = $relativePath . $filename ;
			$response->filename = $relativePath . $filename ;
			return $response->successful();

		} catch (Exception $e) {
			return $response->failedException($e);
		}

	}
}