<?php 

class MemberController extends BaseController {

	// public function index() {
	// 	return View::make('user.members');
	// }

	public function index($id) {
		$selectedLocation = Location::where('locationId', $id)->first();

		if(is_null($selectedLocation)){
			return 'Invalid Location Id';
		}
		return View::make('user.members', compact('selectedLocation'));
	}

	public function avatar( $userIdOrUsername ) {
		$field = 'username';
		if ( is_numeric( $userIdOrUsername ) ) {
			$field = 'userId';
		}

		$user = User::where( $field, $userIdOrUsername )->select( 'userId', 'sex', 'imageAvatar' )->first();

		if ( is_null( $user ) ) {
			return null;
		}

		$filename = $user->imageAvatar;
		if ( is_null( $filename ) ) {
			$sex = $user->sex;
			if ( is_null( $sex ) ) {
				$sex = 'male';
			}
			$filename = public_path() . '/assets/images/avatar_' . $sex . '.jpg';
		}

		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_URL, $filename );
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 20 );
		curl_setopt( $ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT'] );
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt( $ch, CURLOPT_HEADER, true );
		curl_setopt( $ch, CURLOPT_NOBODY, true );

		//$file = curl_exec ($ch);
		$file     = file_get_contents( $filename );
		$filetype = curl_getinfo( $ch, CURLINFO_CONTENT_TYPE );

		$response = Response::make( $file, 200 );
		$response->header( 'Content-Type', $filetype );

		return $response;
	}

}
