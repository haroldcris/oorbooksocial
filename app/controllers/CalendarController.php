<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/01/2015
 * Time: 5:12 PM
 *
 */

class CalendarController  extends BaseController {

	/**
	 * @return $this|\Illuminate\View\View|void
	 */
	public function index()
	{
		$locationId = Input::get('locationId');

		if ( is_null($locationId) ) {
			return View::make('calendar.index');
		}

		$location = Location::where('locationId', $locationId)->first();
		if ( is_null($location) ) {
			return App::abort(404);
		}
            
        $eventCount = CalendarEvent::where('userId',Auth::user()->userId)
                                        ->whereRaw('date(created_at) = curdate()')
                                        ->get()
                                        ->count();
                                    
		return View::make('calendar.index')->with('selectedLocation', $location)
                                    		->with('eventCount', $eventCount);

	}


	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function apiStore(){
		$response = Config::get('site.response');

		$validator = Validator::make(Input::all(), [
						'locationId'    => 'required|exists:location',
						'title'         => 'required',
						'eventDate'     => 'required',
						]);
		if( $validator->fails())
		{
			$response['error'] = $validator->errors()->first();
			return Response::json( $response, 200);
		}

		try {
			// You get all events where username = authenticated user and the event is equal to the
			// date of Event being saved
// 			$eventCount = CalendarEvent::where('userId',Auth::user()->userId)
// 									   ->whereRaw('eventDate = "'. date('Y-m-d', strtotime( Input::get('eventDate'))) . '"')
// 									   ->get()
// 									   ->count();
			
			$eventCount = CalendarEvent::where('userId',Auth::user()->userId)
                                        ->whereRaw('date(created_at) = curdate()')
                                        ->get()
                                        ->count();

			// if maxCalendarEventPerUser settings doesn't exists,
			// it sets the default to 3
			$maxEventPerUser = SettingsClass::get('maxCalendarEventPerUser',false,3);

			if($eventCount >= $maxEventPerUser ) {
				$response['error'] = 'You have exceeded the maximum number of Calendar Events per user.  Can not create the event.';
				return Response::json($response, 200);
			}

			$event = new CalendarEvent();
			$event->unguard();
			if (Input::has('calendarId'))
			{
				$event = CalendarEvent::where('calendarId', Input::get('calendarId'))->first();
			}

			$event->title = Input::get('title');
			$event->eventDate = date('Y-m-d', strtotime(Input::get('eventDate')));
			$event->details = Input::get('details');
			$event->locationId = Input::get('locationId');
			$event->userId = Auth::user()->userId;
			$event->save();
			
			
			$response['success'] = 'true';
			Event::fire('events.created', $event);
			return Response::json( $response, 200 );
	
		} catch (\Exception $e) {
			myLog::Error($e);
			$response['error']  = 'Error occurred while saving Calendar Event';
			return Response::json($response, 200);
		}


	}
	
	public function apiStore2(){
        $response = Config::get('site.response');
        
        $hasCN = Input::has('geoContinent');
        $hasCT = Input::has('geoCountry');
        $hasR1 = Input::has('geoRegion1');
        $hasR2 = Input::has('geoRegion2');
        $hasR3 = Input::has('geoRegion3');
        $hasR4 = Input::has('geoRegion4');
        $hasLC = Input::has('geoLocality');
        
        $location = new Location;
        
        $location->continent    = $hasCN ? implode(",", Input::get('geoContinent')) : '';
        $location->country      = $hasCT ? implode(",", Input::get('geoCountry')) : '';
        $location->region1      = $hasR1 ? implode(",", Input::get('geoRegion1')) : '';
        $location->region2      = $hasR2 ? implode(",", Input::get('geoRegion2')) : '';
        $location->region3      = $hasR3 ? implode(",", Input::get('geoRegion3')) : '';
        $location->region4      = $hasR4 ? implode(",", Input::get('geoRegion4')) : '';
        $location->locality     = $hasLC ? implode(",", Input::get('geoLocality')) : '';
        
        $checkLocation = Location::where('continent', $location->continent)
                                    ->where('country', $location->country)
                                    ->where('region1', $location->region1)
                                    ->where('region2', $location->region2)
                                    ->where('region3', $location->region3)
                                    ->where('region4', $location->region4)
                                    ->where('locality', $location->locality)
                                    ->first();
		if(count($checkLocation) == 1)
        {

    		try {
                    $event = new CalendarEvent();
            		$event->unguard();
            		if (Input::has('calendarId'))
            		{
            			$event = CalendarEvent::where('calendarId', Input::get('calendarId'))->first();
            		}
                    
            		$event->title = Input::get('title');
            		$event->eventDate = date('Y-m-d', strtotime(Input::get('eventDate')));
            		$event->details = Input::get('details');
            		$event->locationId = $checkLocation->locationId;
            		$event->userId = Auth::user()->userId;
            		$event->save();
            		
            		$response['success'] = 'true';
            		Event::fire('events.created', $event);
            		return Response::json( $response, 200 );		
    		} catch (\Exception $e) {
    			myLog::Error($e);
    			$response['error']  = 'Error occurred while saving Calendar Event';
    			return Response::json($response, 200);
    		}
        }
        else
        {
            // $response = new ResponseClass();
            $checkTableLocationGeo = TableLocationGeo::where('continent', $location->continent)
                                            ->where('country', $location->country)
                                            ->where('region1', $location->region1)
                                            ->where('region2', $location->region2)
                                            ->where('region3', $location->region3)
                                            ->where('region4', $location->region4)
                                            ->where('locality', $location->locality)
                                            ->first();
            if(count($checkTableLocationGeo) == 1)
            {
                $location->baseLocality = $location->locality;
                $location->code = $checkTableLocationGeo->id;
                $name = '';
                if($location->continent  != ''){ $name .= $location->continent; }
                if($location->country  != ''){ $name .= ' » ' . $location->country; }
                if($location->region1  != ''){ $name .= ' » ' . $location->region1; }
                if($location->region2  != ''){ $name .= ' » ' . $location->region2; }
                if($location->region3  != ''){ $name .= ' » ' . $location->region3; }
                if($location->region4  != ''){ $name .= ' » ' . $location->region4; }
                if($location->locality != ''){ $name .= ' » ' . $location->locality; }
                if($checkTableLocationGeo->postcode != ''){ $name .= ' » ' . $checkTableLocationGeo->postcode; }
                $location->name = $name;
                try 
                {
                    $location->save();
                    $lastLocationId = Location::select('locationId')->orderBy('locationId', 'desc')->first();
            		$event->locationId = $lastLocationId->locationId;
            		$event->save();
            		
            		$response['success'] = 'true';
            		Event::fire('events.created', $event);
            		return Response::json( $response, 200 );		
                } catch (\Exception $e) {
        			myLog::Error($e);
        			$response['error']  = 'Error occurred while adding Location';
        			return Response::json($response, 200);
        		}    
            }
            else
            {    
                $location->baseLocality = $location->locality;
                $location->code = $this->generateNumeric();
                $name = '';
                if($location->continent  != ''){ $name .= $location->continent; }
                if($location->country  != ''){ $name .= ' » ' . $location->country; }
                if($location->region1  != ''){ $name .= ' » ' . $location->region1; }
                if($location->region2  != ''){ $name .= ' » ' . $location->region2; }
                if($location->region3  != ''){ $name .= ' » ' . $location->region3; }
                if($location->region4  != ''){ $name .= ' » ' . $location->region4; }
                if($location->locality != ''){ $name .= ' » ' . $location->locality; }
                $location->name = $name;
                try 
                {
                    $location->save();
                    $lastLocationId = Location::select('locationId')->orderBy('locationId', 'desc')->first();
            		$event->locationId = $lastLocationId->locationId;
            		$event->save();
            		
            		$response['success'] = 'true';
            		Event::fire('events.created', $event);
            		return Response::json( $response, 200 );		
                } catch (\Exception $e) {
        			myLog::Error($e);
        			$response['error']  = 'Error occurred while adding Location';
        			return Response::json($response, 200);
        		}
            }
            
        }
	}


	/**
	 * @param $locationId
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function apiIndex($locationId)
	{
		$response = Config::get('site.response');

		//Validation
		$validator = Validator::make(Input::all(), [
						'start'     => 'required|date',
					    'end'       => 'required|date',
		]);

		if ($validator->fails() )
		{
			$response['error'] = $validator->errors()->first();
			return Response::json($response, 200);
		}

		$location = Location::select('locationId','baseLocality')->where('locationId',$locationId)->first();
		if(is_null($location)){
			$response['error'] = 'Invalid Location Id';
			return Response::json($response, 200);
		}

		$baseLocations = Location::select('locationId')->where('baseLocality',$location->baseLocality)->get()->toArray();
        // $hasCN = Input::has('geoContinent');
        // $hasCT = Input::has('geoCountry');
        // $hasR1 = Input::has('geoRegion1');
        // $hasR2 = Input::has('geoRegion2');
        // $hasR3 = Input::has('geoRegion3');
        // $hasR4 = Input::has('geoRegion4');
        // $hasLC = Input::has('geoLocality');
        
        // $location = new Location;
        
        // $location->continent    = $hasCN ? implode(",", Input::get('geoContinent')) : '';
        // $location->country      = $hasCT ? implode(",", Input::get('geoCountry')) : '';
        // $location->region1      = $hasR1 ? implode(",", Input::get('geoRegion1')) : '';
        // $location->region2      = $hasR2 ? implode(",", Input::get('geoRegion2')) : '';
        // $location->region3      = $hasR3 ? implode(",", Input::get('geoRegion3')) : '';
        // $location->region4      = $hasR4 ? implode(",", Input::get('geoRegion4')) : '';
        // $location->locality     = $hasLC ? implode(",", Input::get('geoLocality')) : '';
        
        // $baseLocationIds_GL = Location::select('locationId')
        //                     ->where('continent', 'Global')
        //                     ->get()->toArray();
    
        // $baseLocationIds_CN = Location::select('locationId')
        //                     ->where('continent', 'LIKE', '%'.$location->continent.'%')
        //                     ->where('country', '')
        //                     ->where('region1', '')
        //                     ->where('region2', '')
        //                     ->where('region3', '')
        //                     ->where('region4', '')
        //                     ->where('locality', '')->get()->toArray();
    
        // $baseLocationIds_CT = Location::select('locationId')
        //                     ->where('continent', 'LIKE', '%'.$location->continent.'%')
        //                     ->where('country', 'LIKE', '%'.$location->country.'%')
        //                     ->where('region1', '')
        //                     ->where('region2', '')
        //                     ->where('region3', '')
        //                     ->where('region4', '')
        //                     ->where('locality', '')->get()->toArray();
                            
        // $baseLocationIds_R1 = Location::select('locationId')
        //                     ->where('continent', 'LIKE', '%'.$location->continent.'%')
        //                     ->where('country', 'LIKE', '%'.$location->country.'%')
        //                     ->where('region1', 'LIKE', '%'.$location->region1.'%')
        //                     ->where('region2', '')
        //                     ->where('region3', '')
        //                     ->where('region4', '')
        //                     ->where('locality', '')->get()->toArray();
                            
        // $baseLocationIds_R2 = Location::select('locationId')
        //                     ->where('continent', 'LIKE', '%'.$location->continent.'%')
        //                     ->where('country', 'LIKE', '%'.$location->country.'%')
        //                     ->where('region1', 'LIKE', '%'.$location->region1.'%')
        //                     ->where('region2', 'LIKE', '%'.$location->region2.'%')
        //                     ->where('region3', '')
        //                     ->where('region4', '')
        //                     ->where('locality', '')->get()->toArray();
                            
        // $baseLocationIds_R3 = Location::select('locationId')
        //                     ->where('continent', 'LIKE', '%'.$location->continent.'%')
        //                     ->where('country', 'LIKE', '%'.$location->country.'%')
        //                     ->where('region1', 'LIKE', '%'.$location->region1.'%')
        //                     ->where('region2', 'LIKE', '%'.$location->region2.'%')
        //                     ->where('region3', 'LIKE', '%'.$location->region3.'%')
        //                     ->where('region4', '')
        //                     ->where('locality', '')->get()->toArray();
        
        // $baseLocationIds_R4 = Location::select('locationId')
        //                     ->where('continent', 'LIKE', '%'.$location->continent.'%')
        //                     ->where('country', 'LIKE', '%'.$location->country.'%')
        //                     ->where('region1', 'LIKE', '%'.$location->region1.'%')
        //                     ->where('region2', 'LIKE', '%'.$location->region2.'%')
        //                     ->where('region3', 'LIKE', '%'.$location->region3.'%')
        //                     ->where('region4', 'LIKE', '%'.$location->region4.'%')
        //                     ->where('locality', '')->get()->toArray();
        
        // $baseLocationIds_LC = Location::select('locationId')
        //                     ->where('continent', 'LIKE', '%'.$location->continent.'%')
        //                     ->where('country', 'LIKE', '%'.$location->country.'%')
        //                     ->where('region1', 'LIKE', '%'.$location->region1.'%')
        //                     ->where('region2', 'LIKE', '%'.$location->region2.'%')
        //                     ->where('region3', 'LIKE', '%'.$location->region3.'%')
        //                     ->where('region4', 'LIKE', '%'.$location->region4.'%')
        //                     ->where('locality', 'LIKE', '%'.$location->locality.'%')->get()->toArray();
                
        // $baseLocationIds_DF = Location::select('locationId')
        //                     ->where('country', $location->country)
        //                     ->where('region1', $location->region1)
        //                     ->where('region2', $location->region2)
        //                     ->where('region3', $location->region3)
        //                     ->where('region4', $location->region4)
        //                     ->where('baseLocality', $location->baseLocality)->get()->toArray();
        
        // $baseLocationIds_MERGED = array_merge($baseLocationIds_GL, $baseLocationIds_CN, $baseLocationIds_CT, $baseLocationIds_R1, $baseLocationIds_R2, $baseLocationIds_R3, $baseLocationIds_R4, $baseLocationIds_LC, $baseLocationIds_DF);
        // $baseLocationIds = $baseLocationIds_MERGED;
        
		$events = CalendarEvent::with(['creator' => function($query){ $query->select('userId','username');}])
							   ->where('eventDate','>=', Input::get('start'))
							   ->where('eventDate','<=', Input::get('end'))
							   ->whereIn('locationId', $baseLocations)
							   ->get();

		$json = array();
		if ( count( $events ) > 0 ) {
			foreach ( $events as $item )
			{
				$json[] = array(
					"eventid"       => $item->calendarId,
					"title"         => (strlen(htmlspecialchars($item->title)) > 11 ? substr(htmlspecialchars($item->title), 0, 11)."..." : substr(htmlspecialchars($item->title), 0, 11)), /* NOT HTMLENTITIES*/
					"title2"	=> htmlspecialchars($item->title),
					"start"         => $item->eventDate,
					"description"   => htmlspecialchars($item->details),
					"allDay"        => 'true',
					"createdby"     => $item->creator,
				);
			}
		}

		return Response::json($json, 200);
	}

	/**
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return View::make('calendar.create');
	}
	
	public function createMaster()
	{
	   // $response = new ResponseClass();
	   // return $response->successful('oks');
	    
	    $getMUser = UserLocation::where('userId', Auth::id())->where('isPrimary', 1)->first();
        $MUserLocation = Location::where('locationId', $getMUser->locationId)->first();
        
        $iCN = explode(",", $MUserLocation->continent);
        $iCT = explode(",", $MUserLocation->country);
        $iR1 = explode(",", $MUserLocation->region1);
        $iR2 = explode(",", $MUserLocation->region2);
        $iR3 = explode(",", $MUserLocation->region3);
        $iR4 = explode(",", $MUserLocation->region4);
        $iLC = explode(",", $MUserLocation->locality);
        
        if(count($iCN) == 0 || $iCN == "" || $iCN[0] == "" || $iCN[0] == null) { $ctrCN = 0; } else { $ctrCN = count($iCN); }
        if(count($iCT) == 0 || $iCT == "" || $iCT[0] == "" || $iCT[0] == null) { $ctrCT = 0; } else { $ctrCT = count($iCT); }
        if(count($iR1) == 0 || $iR1 == "" || $iR1[0] == "" || $iR1[0] == null) { $ctrR1 = 0; } else { $ctrR1 = count($iR1); }
        if(count($iR2) == 0 || $iR2 == "" || $iR2[0] == "" || $iR2[0] == null) { $ctrR2 = 0; } else { $ctrR2 = count($iR2); }
        if(count($iR3) == 0 || $iR3 == "" || $iR3[0] == "" || $iR3[0] == null) { $ctrR3 = 0; } else { $ctrR3 = count($iR3); }
        if(count($iR4) == 0 || $iR4 == "" || $iR4[0] == "" || $iR4[0] == null) { $ctrR4 = 0; } else { $ctrR4 = count($iR4); }
        if(count($iLC) == 0 || $iLC == "" || $iLC[0] == "" || $iLC[0] == null) { $ctrLC = 0; } else { $ctrLC = count($iLC); }
        
        return View::make('calendar.createMasterCalendarEvent')->with('iCN', $iCN)->with('iCT', $iCT)
                                                            ->with('iR1', $iR1)->with('iR2', $iR2)
                                                            ->with('iR3', $iR3)->with('iR4', $iR4)
                                                            ->with('iLC', $iLC)
                                                            ->with('ctrCN', $ctrCN)->with('ctrCT', $ctrCT)
                                                            ->with('ctrR1', $ctrR1)->with('ctrR2', $ctrR2)
                                                            ->with('ctrR3', $ctrR3)->with('ctrR4', $ctrR4)
                                                            ->with('ctrLC', $ctrLC);
        
// 		return View::make('calendar.createMasterCalendarEvent');
	}


	/**
	 * @return $this
	 */
	public function update()
	{
		$locationId = Input::get('locationId');
		$calendarId = Input::get('calendarId');

		$event = CalendarEvent::where('locationId',$locationId)
							->where('calendarId', $calendarId)
						    ->where('userId', Auth::user()->userId)
							->first();

//		dd($event);
		$locations = Auth::user()->enrolledLocations;
		return View::make('calendar.create')->with('locations', $locations)
											->with('event', $event);

	}


	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function apiDelete()
	{
		$response = Config::get('site.response');

		$calendarId = Input::get('calendarId');

		$event = CalendarEvent::select('calendarId')
							  ->where('calendarId', $calendarId)
		                      ->where('userId', Auth::user()->userId)
		                      ->first();

		if ( is_null($event)) {
			$response ['error'] = 'Invalid Calendar Event Id or not the creator';
			return Response::json($response, 200);
		}

		try {
			$event->delete();

			$response ['success'] = 'true';
			return Response::json($response, 200);

		} catch( \Exception $e){
			myLog::Error ($e);
			$response['error'] = 'Something went wrong while deleting Calendar Event Record';
			return Response::json($response, 200);
		}

	}
	
	function generateNumeric($length = 10) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randNum = '';
        for ($i = 0; $i < $length; $i++) {
            $randNum .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randNum;
    }
	

}