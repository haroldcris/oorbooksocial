<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/18/2015
 * Time: 2:24 PM
 *
 */

class PollController extends BaseController {

	public function index()
	{
		$polls = Poll::summary();
		return View::make('admin.poll.index')->with('polls', $polls);
	}

	public function update($pollId)
	{
		$poll = Poll::where('pollId', $pollId)->first();

		return View::make('admin.poll.create')->with('poll', $poll);
	}


	public function create()
	{
		return View::make('admin.poll.create');
	}

	public function createScope()
	{
		return View::make('admin.poll.setScope')->with('pollId', Input::get('pollId'));
	}


	public function storeScope()
	{
		$response = new ResponseClass();

		$validator = Validator::make(Input::all(),[
			'pollId'            =>  'required|exists:poll,pollId',
		    'scope'             =>  'required|in:country,region1,region2,region3,region4,locality',
		    'searchLocationTab' => 'required'
		]);

		if ( $validator->fails() ) {
		    return $response->failed($validator->errors()->first());
		}

		if(Input::get('searchLocationTab') == 'location') {
			if (!Input::has('tableLocation')) {
				return $response->failed('Select location to continue');
			}
		} else {
			if(!Input::has('tableLocationGeo')) {
				return $response->failed('Select location from database to continue');
			}
		}

		if(Input::get('searchLocationTab') == 'location') {
			//Sri Lanka Database
			// tableLocation
		} else {
			//tableLocationGeo
			$location = TableLocationGeo::where('id', Input::get('tableLocationGeo'))->first();

			if(is_null($location)) {
				return $response->failed('Invalid Location Id');
			}
		}

		try {

			$poll = Poll::where('pollId', Input::get('pollId'))->first();

			$poll->scope  = Input::get('scope');
			$poll->country = $location->country;
			$poll->region1 = $location->region1;
			$poll->region2 = $location->region2;
			$poll->region3 = $location->region3;
			$poll->region4 = $location->region4;
			$poll->locality = $location->locality;
			$poll->postcode = $location->postcode;
			$poll->save();

		    return $response->successful($poll);

		} catch (Exception $e) {
		    return $response->failedException($e);
		}
	}


	public function store()
	{
		$response = new ResponseClass();

		$validator = Validator::make(Input::all(),[
			'question'  =>  'required',
		    'option1'   =>  'required',
		    'option2'   =>  'required'
		]);

		$validator->sometimes('pollId' ,'exists:poll,pollId', function($input){
			return $input->has('pollId');
		});

		if($validator->fails()){
			return $response->failed($validator->errors()->first());
		}

		$question = Input::get('question');
		$option1 = Input::get('option1');
		$option2 = Input::get('option2');
		$option3 = Input::has('option3') ? Input::get('option3') : '';
		$option4 = Input::has('option4') ? Input::get('option4') : '';
		$option5 = Input::has('option5') ? Input::get('option5') : '';
		$disabled = Input::has('disabled') ? 1 : 0;

		try {
			$poll = new Poll();
			if(Input::has('pollId')) {
				$poll = Poll::where('pollId', Input::get('pollId'))->first();
			}

			$poll->unguard();
			$poll->question  =  $question;
			$poll->option1   =  $option1;
			$poll->option2   =  $option2;
			$poll->option3   =  $option3;
			$poll->option4   =  $option4;
			$poll->option5   =  $option5;
			$poll->disabled  =  $disabled;
			$poll->creatorId =  Auth::user()->userId;

			$poll->save();

			return $response->successful($poll);

		} catch (Exception $e) {
		    return $response->failedException($e);
		}

	}


	public function delete()
	{
		$response = new ResponseClass();

		$validator = Validator::make(Input::all(), [
						'pollId'    =>  'required|exists:poll,pollId'
		]);

		if($validator->fails()){
			return $response->failed($validator->errors()->first());
		}

		try {
		    $poll = Poll::where('pollId', Input::get('pollId'))->first();
			$poll->delete();
		    return $response->successful();
		} catch (Exception $e) {
		    return $response->failedException($e);
		}
	}


	public function storeAnswer($pollId)
	{
		$response = new ResponseClass();
		$validator = Validator::make(Input::all(),[
		    'answer'    =>  'required|in:1,2,3,4,5'
		],['answer.required' =>'You have to select at least one of the options']);

		if ( $validator->fails() ) {
		    return $response->failed($validator->errors()->first());
		}

		$validator = Validator::make([$pollId],['pollId'    =>  'exists:poll,pollId'] );
		if ( $validator->fails() ) {
		    return $response->failed($validator->errors()->first());
		}

		try {
		    $answer = [
			    'pollId'     => $pollId,
		        'answer'     => Input::get('answer'),
		        'userId'     => Auth::user()->userId,
		        'created_at' => New \Carbon\Carbon(),
		        'updated_at' => New \Carbon\Carbon()
		    ];

			DB::table('pollAnswer')->insert($answer);

		    return $response->successful();
		} catch (Exception $e) {
		    return $response->failedException($e);
		}
	}


	public function viewPoll()
	{
		$response = new ResponseClass();

		if (!Session::has('selectedLocationId')) {
			return $response->failed('no selected location');
		}

		$poll = Poll::getPoll(Session::get('selectedLocationId'), Auth::user()->userId);

		return $response->successful($poll);
	}
}