<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 01/29/2015
 * Time: 7:29 PM
 *
 */

class SocialMediaController extends BaseController {

	public function auth($action = "")
	{
		// check URL segment
		if ($action == "auth") {
			// process authentication
			try {
				Hybrid_Endpoint::process();
			}
			catch (Exception $e) {
				// redirect back to http://URL/social/
				return Redirect::route('hybridauth');
			}
			return 0;
		}

		try {
			// create a HybridAuth object
			$socialAuth = new Hybrid_Auth(app_path() . '/config/hybridauth.php');

			// authenticate with Google
			$provider = $socialAuth->authenticate(Session::get('platform'));
//			$provider = $socialAuth->authenticate('facebook');

			// fetch user profile
			$userProfile = $provider->getUserProfile();
		}
		catch(Exception $e) {
			// exception codes can be found on HybBridAuth's web site
			return $e->getMessage();
		}

		// logout
		$provider->logout();

		$email = $userProfile->email;

		if($email == '' && $userProfile->displayName != '') {
			$email = $userProfile->displayName;
		}

		if($email){
			$user = User::where('email', $email)->first();

			if(!is_null($user)){
				//Account Already Exists
				Auth::login($user);
				if($user->isFirstTimeUser){
					return Redirect::route('userProfile');
				}
				return Redirect::to('/dashboard');
			}

			$username = $userProfile->firstName;

			if($username == ''){
				$username = $email;
			}

			$username = str_replace(' ','',$username);

			//Check Username
			if(!is_null(User::where('username', $username)->first())) {
				$username = $email;
			}

			//Create the Account
			$user = new User();
			$user->email        = $email;
			$user->username     = $username;
			$user->sex          = strtolower($userProfile->gender);
			$user->country      = $userProfile->country;
			$user->imageAvatar  = $userProfile->photoURL;
			$user->signupDate   = new \Carbon\Carbon();
			$user->activated    = 1;

			if($userProfile->birthYear){
				$dFormat = $userProfile->birthMonth . '/'.
				           $userProfile->birthDay . '/' .
				           $userProfile->birthYear ;

				$dob = DateTime::createFromFormat('m/d/Y', $dFormat );
				$user->birthdate = $dob;
			}

			$user->save();

			Auth::login($user);
			Event::fire('accountCreated', Auth::user());
			return Redirect::route('userProfile');

		}

		// access user profile data
		echo "Connected with: <b>{$provider->id}</b><br />";
		echo "As: <b>{$userProfile->displayName}</b><br />";
		echo "<pre>" . print_r( $userProfile, true ) . "</pre><br />";

		return 0 ;



//		[identifier] => 117246901164521633039
//		[webSiteURL] =>
//		[profileURL] => https://plus.google.com/117246901164521633039
//		[photoURL] => https://lh4.googleusercontent.com/-QCJdOZRrUfU/AAAAAAAAAAI/AAAAAAAABY8/jZUAoTtd3JY/photo.jpg?sz=200
//		[displayName] => Harold Cris Abarquez
//		[description] =>
//		[firstName] => Harold Cris
//		[lastName] => Abarquez
//		[gender] => male
//		[language] =>
//		[age] => > 21
//		[birthDay] => 0
//		[birthMonth] => 0
//		[birthYear] => 0
//		[email] => haroldcris.abarquez@gmail.com
//		[emailVerified] =>
//		[phone] =>
//		[address] => San Fernando, Pampanga
//		[country] =>
//		[region] =>
//		[city] => San Fernando, Pampanga
//		[zip] =>


		//check if user
//		$email = $userProfile->email;
//
//		$newUser = new User();
//		$newUser->email = $email;
//		$newUser->username =

	}

}