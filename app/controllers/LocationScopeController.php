<?php 

class LocationScopeController extends BaseController {
    
	public function index() {
		$items = User::select('userId','username','email','securityLevel')->where('deleted', '=', 0)
		            ->where('securityLevel', '=', 'Master User')
					->orderBy('username')
					->paginate(20);

		$roles = Role::select('roleId','name')->orderBy('name')->get();

		return View::make('admin.locationScope.index', ['items' => $items,
													    'roles' => $roles]);
	}

	public function create(){
	    $iCN = ['']; $iCT = ['']; $iR1 = ['']; $iR2 = ['']; $iR3 = ['']; $iR4 = ['']; $iLC = [''];
	    $ctrCN = 0; $ctrCT = 0; $ctrR1 = 0; $ctrR2 = 0; $ctrR3 = 0; $ctrR4 = 0; $ctrLC = 0;
		
		return View::make('admin.locationScope.create')->with('iCN', $iCN)->with('iCT', $iCT)
                    	                               ->with('iR1', $iR1)->with('iR2', $iR2)
                    	                               ->with('iR3', $iR3)->with('iR4', $iR4)
                    	                               ->with('iLC', $iLC)
                    	                               ->with('ctrCN', $ctrCN)->with('ctrCT', $ctrCT)
                    	                               ->with('ctrR1', $ctrR1)->with('ctrR2', $ctrR2)
                    	                               ->with('ctrR3', $ctrR3)->with('ctrR4', $ctrR4)
                    	                               ->with('ctrLC', $ctrLC);
	}
	
	public function create2() {
	    Session::put('susi', 'laman');
	    return View::make('admin.locationScope.create2');
	}
	
	public function store() {
	    $response = new ResponseClass();
        $location = new Location;
        // $userLocation = new UserLocation;
        
        $userId = Input::get('userId');
        
        $userLocation = UserLocation::where('userId', '=', $userId)->where('isPrimary', '=', 1)->first();
        
        // HAS Checkbox Values instead of Hidden Values
        $hasCN = Input::has('geoContinent');
        $hasCT = Input::has('geoCountry');
        $hasR1 = Input::has('geoRegion1');
        $hasR2 = Input::has('geoRegion2');
        $hasR3 = Input::has('geoRegion3');
        $hasR4 = Input::has('geoRegion4');
        $hasLC = Input::has('geoLocality');
        
        // SET Location Fields
        $location->continent = $hasCN ? implode(",", Input::get('geoContinent')) : '';
        $location->country   = $hasCT ? implode(",", Input::get('geoCountry')) : '';
        $location->region1   = $hasR1 ? implode(",", Input::get('geoRegion1')) : '';
        $location->region2   = $hasR2 ? implode(",", Input::get('geoRegion2')) : '';
        $location->region3   = $hasR3 ? implode(",", Input::get('geoRegion3')) : '';
        $location->region4   = $hasR4 ? implode(",", Input::get('geoRegion4')) : '';
        $location->locality  = $hasLC ? implode(",", Input::get('geoLocality')) : '';
        
        
        // Check If "Selected Location" Exists on "Location Table"
        $checkLocation = Location::where('continent', $location->continent)
                                    ->where('country', $location->country)
                                    ->where('region1', $location->region1)
                                    ->where('region2', $location->region2)
                                    ->where('region3', $location->region3)
                                    ->where('region4', $location->region4)
                                    ->where('locality', $location->locality)
                                    ->first();
        
        if(count($checkLocation) == 1) { // If Selected Geos Exist on Location Table
            // return $response->successful('o');
            
            // Set Data to be Stored in "User Location Table"
            $userLocation->locationId = $checkLocation->locationId;
            $userLocation->address = $checkLocation->locality . ' ' . $checkLocation->postal;
            
            try { // Store Data in "User Location Table"
                $userLocation->save();
                return $response->successful();
            } catch (Exception $e) { return $response->failedException($e); }
        } 
        
        else { // If Selected Geos !Exist on Location Table
            
            // Check If Selected Geos Exist in "Table Geo Location" (CN - LC)
            $checkTableLocationGeo = TableLocationGeo::where('continent', $location->continent)
                                                        ->where('country', $location->country)
                                                        ->where('region1', $location->region1)
                                                        ->where('region2', $location->region2)
                                                        ->where('region3', $location->region3)
                                                        ->where('region4', $location->region4)
                                                        ->where('locality', $location->locality)->first();
            
            if(count($checkTableLocationGeo) == 1) {
            //   return $response->successful($checkTableLocationGeo->postcode); 
                $location->postal = $checkTableLocationGeo->postcode;
                $location->lat = $checkTableLocationGeo->latitude;
                $location->lng = $checkTableLocationGeo->longitude;
                
                // $location->name = $checkTableLocationGeo->latitude
                $location->baseLocality = $location->locality;
                $location->code = $this->generateNumeric(); // Generate Code
                
                // ---------------------------------------------------------------------
                // SET 'NAME' Field
                // ---------------------------------------------------------------------
                $name = '';
                if($location->continent  != ''){ $name .= $location->continent; }
                if($location->country  != ''){ $name .= ' » ' . $location->country; }
                if($location->region1  != ''){ $name .= ' » ' . $location->region1; }
                if($location->region2  != ''){ $name .= ' » ' . $location->region2; }
                if($location->region3  != ''){ $name .= ' » ' . $location->region3; }
                if($location->region4  != ''){ $name .= ' » ' . $location->region4; }
                if($location->locality != ''){ $name .= ' » ' . $location->locality; }
                if($checkTableLocationGeo->postcode != ''){ $name .= ' » ' . $checkTableLocationGeo->postcode; }
                $location->name = $name;
                
                // Regenerate Code if Generated Code Already Exists
                while(!$location->save()) { $location->code = $this->generateNumeric(); }
                
                try {
                    $location->save();
                    
                    // Store Data in "User Location Table"
                    $lastLocationId = Location::where('code', '=', $location->code)->first();
                    
                    $userLocation->address = $lastLocationId->locality . ' ' . $lastLocationId->postal;
                    $userLocation->locationId = $lastLocationId->locationId;
                    
                    try {
                        $userLocation->save();
                        return $response->successful();
                    } catch (Exception $e) { return $response->failedException($e); }
                    
                } catch (Exception $e) { return $response->failedException($e); }
            } 
            else {
                
                $location->code = $this->generateNumeric(); // Generate Code
                
                $name = '';
                if($location->continent  != ''){ $name .= $location->continent; }
                $xname = str_replace(',', '>', $name);
                $iname = str_replace(' ', '_', $xname);
                
                $location->name = $name;
                
                
                
                
                // Regenerate Code if Generated Code Already Exists
                while(!$location->save()) { $location->code = $this->generateNumeric(); }
                
                $location->slug = $iname;
                
                // return $response->successful($location->slug);
                
                $location->save();
                
                
                // Store Data in "User Location Table"
                $lastLocationId = Location::where('code', '=', $location->code)->first();
                    
                // Set Data to be Stored in "User Location Table"
                $userLocation->locationId = $lastLocationId->locationId;
                $userLocation->address = '';
                
                try { // Store Data in "User Location Table"
                    $userLocation->save();
                    return $response->successful();
                } catch (Exception $e) { return $response->failedException($e); }
            }
            
            
            
            
            
            
            
            // // If Selected Locality == 1
            // $location->baseLocality = $location->locality;
            // $location->code = $this->generateNumeric(); // Generate Code
            
            // // Regenerate Code if Generated Code Already Exists
            // while(!$location->save()) { $location->code = $this->generateNumeric(); }
            
            // // Store Location Fields
            // try { 
            //     $location->save(); 
                
            //     // Store Data in "User Location Table"
            //     $lastLocationId = Location::where('code', '=', $location->code)->first();
                
            //     $userLocation->locationId = $lastLocationId->locationId;
            //     $userLocation->address = $lastLocationId->locality . ' ' . $lastLocationId->postal;
            //     // return $response->successful($userLocation->userId . ' ' . $userLocation->locationId . ' ' . $userLocation->LocationId . ' ' . $userLocation->LocationId);
            //     try {
            //         $userLocation->save();
            //         return $response->successful();
            //     } catch (Exception $e) { return $response->failedException($e); }
            // } 
            // catch (Exception $e) { return $response->failedException($e); }
        }    
    }
    
	function generateNumeric($length = 10) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randNum = '';
        for ($i = 0; $i < $length; $i++) {
            $randNum .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randNum;
    }
}
