<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/22/2015
 * Time: 2:10 PM
 *
 */

class NoticeController extends BaseController {
	public function index() {
		$notices = Notice::select(DB::raw('*'))->join('apiCredential','notice.apiCredentialId','=','apiCredential.apiCredentialId')
						->where('notice.userId', Auth::user()->userId)
						->paginate(10);
		return View::make('notice.index')->with('notices', $notices);
	}
}