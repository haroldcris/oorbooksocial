<?php

return array(
	'fetch' => PDO::FETCH_CLASS,
	'default' => 'mysql',

	'connections' => array(
		'mysql' => array(
			'driver'    => 'mysql',
			'host'      => '192.168.1.50',
			'database'  => 'oorbook_social',
			'username'  => 'oorbook_userdb4',
			'password'  => 'w0rking4oorbook',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
		),
	),

	'migrations' => 'migrations',

);
