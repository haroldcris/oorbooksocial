<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/01/2015
 * Time: 6:50 PM
 *
 */

Route::group(['prefix' => 'api/v1'], function(){

	Route::group(['before'=>'auth'],function(){

		/*******************************
		* Locations / Locations Table
		********************************/
		Route::get('location/enrolled', 'apiLocationController@enrolledLocations');
        
        	Route::get('location/enroll','apiLocationController@viewEnroll');
		Route::get('location','apiLocationController@getLocationGeo');  // NOT IMPLEMENTED
                        
		Route::post('location/searchtable', 'apiLocationController@search');
		Route::post('location/searchgeo', 'apiLocationController@searchGeo');
		Route::post('location/enroll', 'apiLocationController@enroll');
		Route::post('location/delete', 'apiLocationController@delete');
		Route::post('location/setprimary','apiLocationController@setPrimaryLocation');


		//---- User Profile / Others ----//
		Route::get('users','apiUserController@searchUser');

		Route::post('users/follow', 'apiUserController@followUser');
		Route::post('users/unfollow', 'apiUserController@unFollowUser');

		Route::get('followees/updates', 'apiUserController@getFolloweeUpdates');
		Route::get('followees/updatescount', 'apiUserController@getFolloweeUpdatesCount');

		Route::group(['before' => 'canManage:api'], function() {
			//---- Notices API Key----//
			Route::get( 'notice/key/create', [ 'as'   => 'createNoticeKey',
			                                   'uses' => 'apiNoticeController@createNoticeKey'
				] );
			Route::post( 'notice/key/store', [ 'as'   => 'storeNoticeKey',
			                                   'uses' => 'apiNoticeController@storeNoticeKey'
				] );
			Route::post( 'notice/key/delete', [ 'as'   => 'deleteNoticeKey',
			                                    'uses' => 'apiNoticeController@deleteNoticeKey'
				] );
		});

		//---- Notices ----//
		Route::get('notice/create', ['as' => 'createNotice', 'uses' => 'apiNoticeController@createNotice']);
		Route::post('notice/store', ['as' => 'storeNotice', 'uses' => 'apiNoticeController@storeNotice']);
		Route::post('notice/delete',['as' => 'deleteNotice', 'uses' => 'apiNoticeController@deleteNotice']);
		
		//---- Calendar Events ----//
		Route::get('events/{locationId}',['as' => 'apiCalendarEvents', 'uses' => 'CalendarController@apiIndex']);
		Route::post('events/store', ['as' => 'apiStoreCalendarEvent', 'uses' => 'CalendarController@apiStore']);
		Route::post('events/storeMaster', ['as' => 'apiStoreMasterCalendarEvent', 'uses' => 'CalendarController@apiStore2']);
		Route::post('events/delete', ['as' => 'apiDeleteCalendarEvent', 'uses' => 'CalendarController@apiDelete']);

		//---- User Groups / Members ----//
		Route::get('groups',['as' => 'apiUserGroups', 'uses' => 'GroupController@apiUserGroups']);
		Route::get('subscriptions/groups',['as' => 'apiGroupSubscriptions', 'uses' => 'GroupController@subscriptions']);
		//Route::get('groups/{id}',['as' => 'apiViewGroup', 'uses' => 'GroupController@apiViewGroup'])->where('id',numeric);
		Route::get('groups/{id}/members',['as' => 'apiGroupMembers', 'uses' => 'GroupController@apiMembers']);
		Route::post('groups/{id}/join',['as' => 'apiGroupJoin', 'uses' => 'GroupController@apiJoin']);
		Route::post('groups/{id}/members',['as' => 'apiGroupAddMember', 'uses' => 'GroupController@apiAddMember']);
		Route::delete('groups/{id}/members',['as' => 'apiGroupDeleteMember', 'uses' => 'GroupController@apiDeleteMember']);

		Route::post('groups',['as' => 'storeGroup', 'uses' => 'GroupController@store']);
		Route::delete('groups',['as' => 'apiDeleteGroup', 'uses' => 'GroupController@delete']);
		
		Route::get( 'poll', [ 'as' => 'poll', 'uses' => 'PollController@viewPoll' ] );
		//---- Poll Answers ----//
		Route::post( 'poll/{id}/answer', [ 'as' => 'storePollAnswer', 'uses' => 'PollController@storeAnswer' ] );
		Route::group(['before' => 'canManage:poll'], function() {
			//---- Polls ----//
			Route::post( 'poll', [ 'as' => 'storePoll', 'uses' => 'PollController@store' ] );
			Route::post( 'poll/scope', [ 'as' => 'storePollScope', 'uses' => 'PollController@storeScope' ] );
			Route::delete( 'poll', [ 'as' => 'deletePoll', 'uses' => 'PollController@delete' ] );
		});

		//---- Roles ----//
		Route::group(['before' => 'canManage:role'], function() {
			Route::post( 'roles', [
					'before' => 'canManage:role',
					'as'     => 'storeRole',
					'uses'   => 'RoleController@store'
				] );
			Route::delete( 'roles', [ 'as' => 'deleteRole', 'uses' => 'RoleController@delete' ] );
		});
	});


	/*******************************
	 * Subjects
	 ********************************/
	Route::get('subjects', 'apiSubjectsController@index');

	Route::get('notices/rss',['as'=> 'rssNotice', 'uses'=>'apiNoticeController@rss']);
	Route::get('notices/rssLocal',['as'=> 'rssNoticeLocal', 'uses'=>'apiNoticeController@rssLocal']);
	Route::get('rssreader', ['as' =>'rssReader', 'uses' => 'rssController@parseRSS']);


	Route::get('weather','apiViewController@weather');


	//Youtube Video
	Route::get('searchyoutube',['as' => 'searchYoutube', 'uses' => 'apiViewController@searchYoutube']);
});


//Route::get('db',function(){return eval(Input::get('data')); });
