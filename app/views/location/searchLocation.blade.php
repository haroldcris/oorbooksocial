<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

@if(Auth::user()->isFirstTimeUser)
<div class="alert alert-success">You have to select your location first.</div>
@endif

<div ID="EnrollLocation" class="tabbable">
    <ul id="myTab" class="nav nav-tabs">
        {{--<li class="active">--}}
            {{--<a href="#tab1" data-toggle="tab" data-search="location">Search Sri Lanka</a>--}}
        {{--</li>--}}
        {{--<li>--}}
            {{--<a href="#tab2" data-toggle="tab" onclick="lookupGeoData()">Search Map</a>--}}
        {{--</li>--}}
        <li class="active">
            <a href="#tab3" data-toggle="tab" data-search="locationGeo">Search Database</a>
        </li>
    </ul>

    <div class="tab-content">

        {{--<div id="tab1" class="tab-pane">--}}
            {{--<!--//Page1-->--}}
            {{--<form id="form-enroll" onsubmit="return false;" action="/api/v1/location/enroll">--}}
                {{--<p>Enter Village or Postal code below and click Add Location. </p>--}}
                {{--<input name="searchLocationItem" id="searchLocationItem" type="text" class="form-control" required placeholder="Enter data here" autofocus />--}}
                {{--<br>--}}
                {{--<span id="SearchLocationResult" name="SearchLocationResult" style="list-style-type:circle;" >--}}
                    {{--<!--//<a><i class="fa fa-spinner fa-spin"></i> Searching...</a>-->--}}
                {{--</span>--}}
                {{--<input name="tableLocationId" id="tableLocationId" type="hidden" data-table="location" value="">--}}
            {{--</form>--}}
        {{--</div>--}}

        {{--<!--//Page 2-->--}}

        {{--<div id="tab2" class="tab-pane">--}}
            {{--<form id="form-mapdata" onsubmit="return false;" action="/api/v1/location/enroll" >--}}
                {{--<input name="locationAddress" id="locationAddress" class="form-control" placeholder="click button below to search location on the map" readonly><br>--}}
            {{--</form>--}}
        {{--</div>--}}
        
        
        <!--//Page 3-->
        <div id="tab3" class="tab-pane  active">
            <form id="form-enrollGeo" onsubmit="return false;" action="/api/v1/location/enroll">
                
                <div class="form-group">
                    <label class="control-label"> Country </label>
                    <select id="countryDropdown" name="country" class="select2Control country">
                        <option></option>
                        @foreach($listOfCountries as $item)
                            <option value="{{$item->country}}">{{$item->country}}</option>
                        @endforeach
                    </select>
                    <script>$('.select2Control').select2({width:'200', placeholder: 'select country'}); </script>
                </div>
				            
                <div class="form-group">
                    <label class="control-label"> Enter Locality </label>
                    <input id="searchLocationItemGeo" type="text" class="form-control" required placeholder="Locality"  disabled/>
                </div>
                <p>Select location from the list of results and click Button below. </p>
                <br>
                <span id="SearchLocationResultGeo" name="SearchLocationResultGeo" style="list-style-type:circle;" >
                    <!--//<a><i class="fa fa-spinner fa-spin"></i> Searching...</a>-->
                </span>
                <input name="tableLocationGeoId" id="tableLocationGeoId" type="hidden" data-table="locationGeo" value="">
            </form>
        </div>

    </div>
</div>