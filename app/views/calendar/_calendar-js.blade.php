<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/01/2015
 * Time: 6:f56 PM
 *
 */

?>

<script>

    function frmAddCalendarEvent_Validate()
    {
        var frm = $('#frmAddCalendarEvent');
        var field;
        var result = true;
        //clear all
        $('.field').removeClass('has-error');
        $('span.help-block').html('');

        field = frm.find('input[name=title]');
        if(field.val() == '')
        {
            field.closest('.field').addClass('has-error').find('span.help-block').html('Title is required');
            result = false;
        }

        field = frm.find('input[name=eventDate]');
        if(field.val() == '')
        {
            field.closest('.field').addClass('has-error').find('span.help-block').html('Date of Event is required');
            result = false;
        }
        return result;
    }

    function addEvent(eventObject)
    {
        // var securityLevel = arguments[0];
        // console.log(securityLevel);
        var url, okBtn;

        if (eventObject == undefined) {        
            okBtn = 'Save';
            // if(securityLevel != 'master user')
            // {
                url = '{{ URL::route('createCalendarEvent') }}';
                // console.log('not a master user');
            // }
            // else
            // {
            //     url = '{{ URL::route('createMasterCalendarEvent') }}';
            //     // console.log('a master user');
            // }
            
        } else {
            // console.log('not a master user');
            var param = '?calendarId=%1&locationId=%2';
            param = param.replace('%1', eventObject.eventid)
                         .replace('%2', $('#calendarEventLocation').val());
            okBtn = 'Update';
            url = '{{ URL::route('updateCalendarEvent') }}' + encodeURI(param);
        }

        eventDialog = messageBox('frmAddCalendarEventDialog', '{{ Config::get('site.loader') }}', 'Calendar Event', okBtn, 'Cancel',BootstrapDialog.TYPE_SUCCESS);
        $.get(url)
            .done(function(frm){
                eventDialog.getModalBody().html(frm);
            });

    }
    
    function addEventMaster(eventObject)
    {
        // var securityLevel = arguments[0];
        // console.log(securityLevel);
        var url, okBtn;

        if (eventObject == undefined) {        
            okBtn = 'Save';
                url = '{{ URL::route('createCalendarEvent') }}';
        } else {
            // console.log('not a master user');
            var param = '?calendarId=%1&locationId=%2';
            param = param.replace('%1', eventObject.eventid)
                         .replace('%2', $('#calendarEventLocation').val());
            okBtn = 'Update';
            url = '{{ URL::route('updateCalendarEvent') }}' + encodeURI(param);
        }

        eventDialog = messageBox('frmAddCalendarEventDialog', '{{ Config::get('site.loader') }}', 'Calendar Event', okBtn, 'Cancel',BootstrapDialog.TYPE_SUCCESS);
        $.get(url)
            .done(function(frm){
                eventDialog.getModalBody().html(frm);
                
            });

    }


    function frmAddCalendarEventDialog_onButtonClick(dialogRef)
    {
        if (! frmAddCalendarEvent_Validate() ) return false;

        postMyData('frmAddCalendarEvent', dialogRef).then (
            function()
            {
                refreshCalendarData();
                dialogRef.close();
                location.reload();
            }
        );
    }

    function deleteEvent(ctrl)
    {
        var  _html =  'You are about to delete this Calendar Event permanently.<br><br>';
             _html += 'Do you want to continue?';
             _html += '<form id="frmDeleteEvent" action="{{URL::route('apiDeleteCalendarEvent', Session::get('selectedCalendarEventLocationId'))}}"><input type="hidden" name="calendarId" value="' + $(ctrl).attr('data-id') + '" /> </form>';
        messageBox ('frmDeleteEventDialog', _html,'Confirmation','Delete','', BootstrapDialog.TYPE_DANGER);
    }

    function frmDeleteEventDialog_onButtonClick(dialogRef)
    {
        postMyData('frmDeleteEvent', dialogRef).then(
            function()
            {
                refreshCalendarData();
                dialogRef.close();
                eventDialog.close();
                location.reload();
            }
        )
    }

    $('#calendarEventLocation').on('change', function(selectedItem){
        console.log(selectedItem.target.value);
        location.href = "{{ URL::route('calendarEvents') }}?locationId=" +  selectedItem.target.value;
    })

    function refreshCalendarData(){
        if( $('.fullCalendar').length != 0 ) { $('.fullCalendar').fullCalendar( 'refetchEvents' ) };
    }
</script>