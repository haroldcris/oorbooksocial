<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/01/2015
 * Time: 9:01 PM
 *
 */
//$event

    $title ='';
    $date  = '';
    $details = '';

    if(isset($event))
    {
        $id = $event->calendarId;
        $title = $event->title;
        $date = date('m/d/Y', strtotime($event->eventDate));
        $details = $event->details;
    }
 ?>
<style type="text/css">
    div.modal-content{
        width: 120%;
    }
    div.scrollable {
        width: 100%;
        height: 200px;
        margin: 0;
        padding: 0;
        overflow: auto;
    }
    .boxfix {
        display: block;
        padding-left: 15px;
        text-indent: -15px;
        font-size: 10px;
        margin-bottom: -10px;
    }
    .boxfix input {
        padding: 0;
        margin:0;
        vertical-align: bottom;
        position: relative;
        top: -1px;
    }
    .spinnery {
        font-size: 50px;
        position: absolute;
        top: 75px;
        left: 310px;
        display: none;
        z-index: 999;
    }
    .geo-table {
        height: 200px; 
        max-height: 200px; 
        min-height: 200px; 
        width: 700px; 
        max-width: 700px; 
        min-width: 700px;
    }
</style>
@if (isset($event))
    <button type="button" class="btn btn-white btn-round btn-sm btn-danger pull-right" data-id="{{$event->calendarId}}" onclick="deleteEvent(this)" style="margin-right:20px;margin-bottom:20px;">
        <i class="fa fa-trash-o bigger-120"></i> Delete this Event
    </button><br/>
@endif
<script>
	function countChar(val){
            		var len = val.value.length;
            		if (len >= 121){
            			val.value = val.value.substring(0, 120);
            		} else {
            			$('#charNum').text(120 - len);
            		}
            	};
        function countChar2(val){
            		var len = val.value.length;
            		if (len >= 121){
            			val.value = val.value.substring(0, 120);
            		} else {
            			$('#charNum2').text(120 - len);
            		}
            	};
</script>
<div style="padding:10px 20px 10px 0;">
    <form id="frmAddCalendarEvent" submit="javascript:void(0)" action="{{URL::route('apiStoreMasterCalendarEvent')}}" class="form-inline form-horizontal" role="form" >
    {{ Form::token() }}
        @if(isset($event))
            <input type="hidden" name = "calendarId" value="{{ $id }}" />
        @endif

                <input  id="hidContinent" name="hidContinent" type="hidden">
                <input  id="hidCountry"   name="hidCountry"   type="hidden">
                <input  id="hidRegion1"   name="hidRegion1"   type="hidden">
                <input  id="hidRegion2"   name="hidRegion2"   type="hidden">
                <input  id="hidRegion3"   name="hidRegion3"   type="hidden">
                <input  id="hidRegion4"   name="hidRegion4"   type="hidden">
                <input  id="hidLocality"  name="hidLocality"  type="hidden">
                <input  id="userId"       name="userId"       type="hidden">
                <table border="0" style="geo-table">
                    <thead>
                        <tr>
                            <th id="thCN" style="width: 100px;">CONTINENT</th>
                            <th id="thCT" style="width: 100px;">COUNTRY</th>
                            <th id="thR1" style="width: 100px;">REGION 1</th>
                            <th id="thR2" style="width: 100px;">REGION 2</th>
                            <th id="thR3" style="width: 100px;">REGION 3</th>
                            <th id="thR4" style="width: 100px;">REGION 4</th>
                            <th id="thLC" style="width: 100px;">LOCALITY</th>
                        <tr>
                        
                    </thead>
                    <tbody>
                        <i id="spinnery" class="fa fa-spinner fa-spin spinnery"></i>
                        <tr>
                            <td style="vertical-align: top;"><div id="tdCN" class="scrollable"></td>
                            <td style="vertical-align: top;"><div id="tdCT" class="scrollable"></td>
                            <td style="vertical-align: top;"><div id="tdR1" class="scrollable"></td>
                            <td style="vertical-align: top;"><div id="tdR2" class="scrollable"></td>
                            <td style="vertical-align: top;"><div id="tdR3" class="scrollable"></td>
                            <td style="vertical-align: top;"><div id="tdR4" class="scrollable"></td>
                            <td style="vertical-align: top;"><div id="tdLC" class="scrollable"></td>
                        </tr>
                       
                    </tbody>
                </table>
        
        <div class="field">
            <label class="control-label col-md-3" for="title">Event Title</label>
            <div class="input-group col-md-9">
                <input class="form-control" type="text" name="title" onkeyup="countChar2(this)" maxlength="120" placeholder="Type your title" value="{{$title}}" required autofocus/>
            <div class="pull-right" id="charNum2">120</div>   
            </div>
            <span class="help-block col-md-offset-3"></span>
        </div>
        <div class="field">
            <label class="control-label col-md-3" for="eventDate">Date of Event</label>
            <div class="input-group col-md-9 date">
                <input type="text" class="form-control" name="eventDate" id="eventdate" value = "{{$date}}" />
            </div>
            <span class="help-block col-md-offset-3"></span>
        </div>
        <div class="field">
            <label class="control-label col-md-3" >Details</label>
            <div class="input-group col-md-9 date">
                <textarea id="details" name="details" type="text" onkeyup="countChar(this)" maxlength="120" placeholder="Details (Optional)" class="form-control">{{$details}}</textarea>
            </div>
            <div class="pull-right" id="charNum">120</div>
            <span class="help-block col-md-offset-3"></span>
        </div>
    </form>
</div>


<script>
    $('#eventLocation').select2({width: '100%', placeholder: 'Select Location'});
    $('#eventLocation').select2('val', selectedCalendarLocationId);
    $('#eventdate').datetimepicker({ format : "m/d/Y", timepicker : false, closeOnDateSelect : true});
</script>
@include('layouts.partials._geoCheckboxFunction-js')
@include('layouts.partials._geoCheckboxLoadMaster-js')
@include('layouts.partials._geoCheckboxChangeMaster-js')