<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/01/2015
 * Time: 5:07 PM
 *
 */?>

@extends ('layouts.main')

@section('styles')
    <link href='/controls/calendar/fullcalendar.css' rel='stylesheet' />
	<link href='/controls/calendar/fullcalendar.print.css' rel='stylesheet' media='print' />
	<style>
		#loading {
			display: none;
			position: absolute;
			top: 10px;
			right: 10px;
		}

		#calendar {
			width:100%;
			height:100%;
			margin: 40px auto;
			padding: 0 10px;
		}
	</style>
@stop


@section('pageContent')
    @include('layouts.partials.breadcrumbs')

    <div class="panel panel-body" style="margin-top:15px;">

        @if(isset($selectedLocation))
            <div class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Info: </strong> Click on the event items to modify or remove event items.
            </div>
        @endif
        
        
        
        <div style="display:none">
             <select id="calendarEventLocation" name="locationId" autofocus>
                <option></option>
                @foreach($enrolledLocations as $item)
                    <option value="{{$item->locationId}}" > {{ $item->name }} </option>
                @endforeach
            </select>

        </div>

        @if (isset($selectedLocation) )
            @if($eventCount < 3)
            <div style="padding:10px 0 0 0;">
                @if(strtolower(Auth::user()->securityLevel) != 'master user')
                    <button class="btn bnt-xs btn-round btn-success btn-white" onclick="addEvent()"><i class="fa fa-plus"></i> Create New</button>
                @else
                    <button class="btn bnt-xs btn-round btn-success btn-white" onclick="addEventMaster()"><i class="fa fa-plus"></i> Create New</button>
                @endif
            </div>
            @else
            <div style="padding:10px 0 0 0;">
                    <button class="btn bnt-xs btn-round btn-success btn-white">You have exceeded the maximum number of Calendar Events per day.  Can not create the event.</button>
            </div>
            @endif
        @endif
        <div id='loading'>loading...</div>
        <div id="calendar" class="fullCalendar"></div>
    </div>
@stop


@section('scripts')
    <script src='/controls/bootbox/bootbox.min.js'></script>
    {{--<script src='/controls/calendar/lib/moment.min.js'></script>--}}
    <script src="/packages/moment/moment.js"></script>

    <script src='/controls/calendar/fullcalendar.min.js'></script>
    <script src='/controls/calendar/lang-all.js'></script>

    <script>

        var selectedCalendarLocationId = {{isset($selectedLocation) ? $selectedLocation->locationId : 0  }};

        $(document).ready(function() {
            $('#calendarEventLocation').select2({width:'100%', placeholder: 'Select your location'});

            '@if (isset($selectedLocation) )'
                $('#calendarEventLocation').select2('val','{{ $selectedLocation->locationId }}');
            '@endif'

            var d = new Date(),
                startDate;

            startDate = getURLParameter('start');
            if (getURLParameter('start') == null) {
                startDate = d.getFullYear() + '/' + (d.getMonth()+1) + '/' + d.getDate();
            }

            $('#calendar').fullCalendar({
                header: {
                    left: 'prev',
                    center: 'title',
                    right: 'today, next'
                },
                defaultDate: startDate,
                editable: false,
                selectable:true,
                eventLimit: true, // allow "more" link when too many events
                eventColor: '#59c700',
                events: {
                    @if(isset($selectedLocation))
                        url : '{{URL::route('apiCalendarEvents', $selectedLocation->locationId)}}',
                        data :{
                          locationId : '{{$selectedLocation->locationId}}'
                        },
                        error: function() {}
                    @endif
                },
                loading: function(bool) {
                    $('#loading').toggle(bool);
                }

                ,eventRender: function(event, element) {
                    element.attr('href', 'javascript:void(0);');
                    element.popover({
                        title: event.title2,
                        content: event.description + '<br/><br/>' + '<hr style="margin:0;padding:0"/><img src="/user/'+ event.createdby.username +'/avatar" style="max-width:24px;"> ' + event.createdby.username ,
                        animation: true,
                        placement:'top',
                        html:true,
                        trigger:'hover',
                        container:'body'

                    });

                    if ( event.createdby.userId == '{{Auth::user()->userId}}' ) {
                        element.click(function(){addEvent(event);});
                    }

                }

                , dayClick: function(date, jsEvent, view) {
                        //alert('Clicked on: ' + date.format());
                        //alert('Current view: ' + view.name);
                        // change the day's background color just for fun
                        //$(this).css('background-color', 'red');
                }

            });
        });
    </script>



@stop