<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/01/2015
 * Time: 9:01 PM
 *
 */
//$event

    $title ='';
    $date  = '';
    $details = '';

    if(isset($event))
    {
        $id = $event->calendarId;
        $title = $event->title;
        $date = date('m/d/Y', strtotime($event->eventDate));
        $details = $event->details;
    }
 ?>

@if (isset($event))
    <button type="button" class="btn btn-white btn-round btn-sm btn-danger pull-right" data-id="{{$event->calendarId}}" onclick="deleteEvent(this)" style="margin-right:20px;margin-bottom:20px;">
        <i class="fa fa-trash-o bigger-120"></i> Delete this Event
    </button><br/>
@endif
<script>
	function countChar(val){
            		var len = val.value.length;
            		if (len >= 121){
            			val.value = val.value.substring(0, 120);
            		} else {
            			$('#charNum').text(120 - len);
            		}
            	};
        function countChar2(val){
            		var len = val.value.length;
            		if (len >= 121){
            			val.value = val.value.substring(0, 120);
            		} else {
            			$('#charNum2').text(120 - len);
            		}
            	};
</script>
<div style="padding:10px 20px 10px 0;">
    <form id="frmAddCalendarEvent" submit="javascript:void(0)" action="{{URL::route('apiStoreCalendarEvent')}}" class="form-inline form-horizontal" role="form" >

        @if(isset($event))
            <input type="hidden" name = "calendarId" value="{{ $id }}" />
        @endif

        <div >
            <select id="eventLocation" name="locationId">
                <option>Select Location</option>
                @foreach($enrolledLocations as $item)
                    <option value="{{$item->locationId}}" > {{ $item->name }} </option>
                @endforeach
            </select>
            <span class="help-block col-md-offset-3"></span>
        </div>
        
        <div class="field">
            <label class="control-label col-md-3" for="title">Event Title</label>
            <div class="input-group col-md-9">
                <input class="form-control" type="text" name="title" onkeyup="countChar2(this)" maxlength="120" placeholder="Type your title" value="{{$title}}" required autofocus/>
            <div class="pull-right" id="charNum2">120</div>   
            </div>
            <span class="help-block col-md-offset-3"></span>
        </div>
        <div class="field">
            <label class="control-label col-md-3" for="eventDate">Date of Event</label>
            <div class="input-group col-md-9 date">
                <input type="text" class="form-control" name="eventDate" id="eventdate" value = "{{$date}}" />
            </div>
            <span class="help-block col-md-offset-3"></span>
        </div>
        <div class="field">
            <label class="control-label col-md-3" >Details</label>
            <div class="input-group col-md-9 date">
                <textarea id="details" name="details" type="text" onkeyup="countChar(this)" maxlength="120" placeholder="Details (Optional)" class="form-control">{{$details}}</textarea>
            </div>
            <div class="pull-right" id="charNum">120</div>
            <span class="help-block col-md-offset-3"></span>
        </div>
    </form>
</div>


<script>
    $('#eventLocation').select2({width: '100%', placeholder: 'Select Location'});
    $('#eventLocation').select2('val', selectedCalendarLocationId);
    $('#eventdate').datetimepicker({ format : "m/d/Y", timepicker : false, closeOnDateSelect : true});
</script>