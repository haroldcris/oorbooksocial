<!DOCTYPE html>
<head><script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script></head>
<body>
<form id="formalin">
    {{Form::token()}}
    <input type="text"   id="key"     name="key">
    <input type="text"   id="value"   name="value">
    <input type="button" id="buttony" name="buttony" value="Submit">
</form>

<script>
    $( "#buttony" ).click(function() {
        $.post('{{URL::route("experimentPost")}}', $('#formalin').serialize()).done(
            function(data) {
                console.log(data);
           }
        ).fail(function(err) {
                console.log(err);
        });
    });
</script>
</body>
</html>