<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/18/2015
 * Time: 4:48 PM
 *
 */ ?>

 <script>

    function createPoll()
    {
        messageBoxRemote('frmCreatePollDialog', '{{URL::route('createPoll')}}','Poll Question', 'Create');
    }

    function frmCreatePollDialog_onButtonClick(dialogRef)
    {
        postMyData('frmCreatePoll', dialogRef).then(
            function(json){
                dialogRef.close();
                location.reload();
            }
        )
    }
 </script>

 {{--Edit Poll --}}
 <script>
    $('.btn-editPoll').click(function(){
        var id = $(this).closest('tr').attr('data-id');
        var _url = '/manage/polls/' + id;

        messageBoxRemote('frmCreatePollDialog', _url,'Poll Question', 'Update');
    });
 </script>


 {{--Delete Poll  --}}
 <script>
    $('.btn-deletePoll').click(function(){
        var _html,
            title,
            pollId = $(this).closest('tr').attr('data-id');

        $('.btn-deletePoll').closest('tr').removeClass('active');
        $(this).closest('tr').addClass('active');

        title = $(this).closest('tr').find('.pollQuestion').text();

        _html =  'You are about to delete the following Poll.<br><br>';
        _html += '<div><table class="table table-bordered "><tbody><tr><td>' + title + '</td></tr><tbody></table></div>';
        _html += 'Do you want to continue?';

        _html += '<form id="frmDeletePoll" action="{{URL::route('deletePoll')}}">';
        _html += '<input type="hidden" name="_method" value="delete" /> ';
        _html += '<input type="hidden" name="pollId" value="' + pollId + '" /> ';
        _html += '</form>';

        messageBox('deletePollDialog', _html ,'Confirmation','Delete Poll', '', BootstrapDialog.TYPE_DANGER ).setSize(BootstrapDialog.SIZE_SMALL);
    });

    function deletePollDialog_onButtonClick(dialogRef)
    {
        postMyData('frmDeletePoll', dialogRef).then (
            function()
            {
                $('tr.poll-item.active').remove();
                dialogRef.close();
            }
        );
    }
 </script>

{{--SET POLL Scope--}}
<script>
    $('.btn-setScopePoll').click(function(){
        var pollId = $(this).closest('tr').attr('data-id');
        var _url = encodeURI("{{URL::route('pollScope')}}" + '?pollId=' + pollId );

        messageBoxRemote ('frmSetPollScopeDialog', _url,'Scope', 'Set Scope' );

    });

    function frmSetPollScopeDialog_onButtonClick(dialogRef)
    {
        postMyData ('frmSetPollScope', dialogRef).then(
            function(){
                return location.reload();
            }
        );
    }
</script>