<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/22/2015
 * Time: 12:52 PM
 *
 */
 ?>

    <form class="form-horizontal" id='frmCreatePoll' action="{{URL::route('storePoll')}}">
        @if(isset($poll))
            <input type="hidden" name="pollId" value="{{$poll->pollId}}" >
        @endif
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right"> Poll Question* </label>

            <div class="col-sm-9">
                <textarea name="question" rows="3" class="form-control" placeholder="Type your poll question here" autofocus>{{isset($poll) ? $poll->question : ''}}</textarea>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right"> Option 1* </label>
            <div class="col-sm-9">
                <input type="text" class="form-control" placeholder="Type choice" name="option1" value="{{isset($poll) ? $poll->option1 : ''}}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right"> Option 2* </label>
            <div class="col-sm-9">
                <input type="text" class="form-control" placeholder="Type choice" name="option2" value="{{isset($poll) ? $poll->option2 : ''}}" >
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right"> Option 3 </label>
            <div class="col-sm-9">
                <input type="text" class="form-control" placeholder="Type choice [optional]" name="option3" value="{{isset($poll) ? $poll->option3 : ''}}" >
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right"> Option 4 </label>
            <div class="col-sm-9">
                <input type="text" class="form-control" placeholder="Type choice [optional]" name="option4" value="{{isset($poll) ? $poll->option4 : ''}}" >
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right"> Option 5 </label>
            <div class="col-sm-9">
                <input type="text" class="form-control" placeholder="Type choice [optional]" name="option5" value="{{isset($poll) ? $poll->option5 : ''}}" >
            </div>
        </div>

        <div class="checkbox">
           <label>
               <input type="checkbox" class="ace" name = "disabled"
                    @if(isset($poll))
                        @if($poll->disabled) {{'checked'}} @endif
                    @endif
               >
               <span class="lbl"> Disable this Poll</span>
           </label>
       </div>
    </form>


        {{--<div class="accordion-style1 panel-group">--}}
            {{--<div class="panel panel-default">--}}
                {{--<div class="panel-heading">--}}
                    {{--<h4 class="panel-title">--}}
                        {{--<a href="#collapseOne" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle" aria-expanded="true">--}}
                            {{--<i data-icon-show="ace-icon fa fa-angle-right" data-icon-hide="ace-icon fa fa-angle-down" class="bigger-110 ace-icon fa fa-angle-down"></i>--}}
                            {{--&nbsp;Poll Content--}}
                        {{--</a>--}}
                    {{--</h4>--}}
                {{--</div>--}}

                {{--<div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" >--}}
                    {{--<div class="panel-body">--}}


                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="panel panel-default">--}}
                {{--<div class="panel-heading">--}}
                    {{--<h4 class="panel-title">--}}
                        {{--<a href="#collapseTwo" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle" aria-expanded="true">--}}
                            {{--<i data-icon-show="ace-icon fa fa-angle-right" data-icon-hide="ace-icon fa fa-angle-down" class="bigger-110 ace-icon fa fa-angle-right"></i>--}}
                            {{--&nbsp;Poll Scope--}}
                        {{--</a>--}}
                    {{--</h4>--}}
                {{--</div>--}}

                {{--<div id="collapseTwo" class="panel-collapse collapse in " aria-expanded="true" >--}}

                    {{--<div class="panel-body">--}}

                         {{--<div class="col-md-12 ">--}}

                             {{--<div class="form-group">--}}
                                 {{--<label class="control-label">Scope</label>--}}
                                 {{--<select name="scope" class="select2Control">--}}
                                     {{--<option>Select Scope</option>--}}
                                     {{--<option value="country">Country &nbsp;&nbsp;&nbsp;&nbsp;</option>--}}
                                     {{--<option value="region1">Region 1</option>--}}
                                     {{--<option value="region2">Region 2</option>--}}
                                     {{--<option value="region3">Region 3</option>--}}
                                     {{--<option value="region4">Region 4</option>--}}
                                     {{--<option value="locality">Locality</option>--}}
                                 {{--</select>--}}
                             {{--</div>--}}

                         {{--<input type="hidden" name="tableLocation">--}}
                         {{--<input type="hidden" name="tableLocationGeo">--}}
                         {{--<input type="hidden" name="searchLocationTab" value="location">--}}

                        {{--<div >--}}
                            {{--@include('location.searchLocation')--}}
                        {{--</div>--}}

                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

        {{--</div>--}}

    {{--</form>--}}
 {{--<script>--}}
     {{--$('body').on('change', '[data-table="location"]' , function(){--}}
         {{--$('input[name="tableLocation"]').val($(this).val());--}}
     {{--});--}}

     {{--$('body').on('change', '[data-table="locationGeo"]' , function(){--}}
         {{--$('input[name="tableLocationGeo"]').val($(this).val());--}}
     {{--});--}}

     {{--$('[data-search="location"]').bind('click' , function(){--}}
         {{--$('[name="searchLocationTab"]').val('location');--}}
     {{--});--}}

     {{--$('[data-search="locationGeo"]').bind('click' , function(){--}}
         {{--$('[name="searchLocationTab"]').val('locationGeo');--}}
     {{--});--}}
 {{--</script>--}}


