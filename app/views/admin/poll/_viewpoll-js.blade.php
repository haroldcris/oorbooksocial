<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/22/2015
 * Time: 4:03 AM
 *
 */ ?>


 @if(Auth::check() && Auth::user()->securityLevel == 'User')

    {{--GET POLL--}}
    <script>
        $(function(){
		showPollQuestionManual();
        });
        
        function disableUrl(hyperlink, panel, icon) {
		$(hyperlink).removeAttr('href');
		$(panel).css('border', 'none').hover(function(){$(this).css('cursor', 'context-menu');});
		$(icon).css('color', '#BDBDBD');
	}
	function disablePollButton() {
		disableUrl(".url-poll-disable", ".menuPanel-poll", ".url-poll-disable i");
	}
        
        function showPollQuestionManual() {
        	$.get('{{URL::route("poll")}}')
        	.done(function(json){
                    if(json.success == 'true') {
                        if (json.data == null) return disablePollButton();
                        for (var i=0; i< json.data.length; i++){
                            showPollQuestion(json.data[i]);
                        }
                    }
                });
        }

        function showPollQuestion(json)
        {
            if (typeof json != 'object') return Error ('Invalid Object');
            var _choice = '<div class="checkbox"> \
                               <label> \
                                   <input type="radio" class="ace" name = "answer"> \
                                   <span class="lbl"></span> \
                               </label> \
                           </div>',
                _frm = $('<form id="frmPollAnswer'+ json.pollId +'" action="">'),
                _url = '{{URL::route('storePollAnswer')}}';

                _frm.append( json.question.replace(/\r\n|\n|\r/g, '<br />') );

                for (var i=1;i<5;i++){
                    var $field = 'option' + i;

                    if (json[$field].length != 0){
                        var c = $(_choice);
                        $(c).find('input').val(i);
                        $(c).find('.lbl').html(' ' + json[$field]);
                        _url = _url.replace('%7Bid%7D', json.pollId);
                        _frm.append(c);
                        _frm.attr('action', _url);
                    }
                }
                var dialog = messageBox ('pollDialog', _frm,'Poll Question');

                dialog.setType(BootstrapDialog.TYPE_WARNING)
                      .setSize(BootstrapDialog.SIZE_SMALL)
                      .setData('pollId', json.pollId);

        }

        function pollDialog_onButtonClick(dialogRef)
        {
            postMyData ('frmPollAnswer' + dialogRef.getData('pollId'), dialogRef).then(
                function(){
                    dialogRef.close();
                    disablePollButton();
                }
            )
        }

    </script>

 @endif