<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/21/2015
 * Time: 12:08 AM
 *
 */ ?>

 <div class="row">
     <form id='frmSetPollScope' action="{{URL::route('storePollScope')}}">
         <div class="col-md-12 ">

             <div class="form-group">
                 <label class="control-label">Scope</label>
                 <select name="scope" class="select2Control">
                     <option>Select Scope</option>
                     <option value="country">Country &nbsp;&nbsp;&nbsp;&nbsp;</option>
                     <option value="region1">Region 1</option>
                     <option value="region2">Region 2</option>
                     <option value="region3">Region 3</option>
                     <option value="region4">Region 4</option>
                     <option value="locality">Locality</option>
                 </select>
             </div>

             <input type="hidden" name="tableLocation">
             <input type="hidden" name="tableLocationGeo">
             <input type="hidden" name="searchLocationTab" value="locationGeo">
             <input type="hidden"  name="pollId" value="{{$pollId}}" >
         </div>
     </form>

     <div class="col-md-12">
         @include('location.searchLocation')
     </div>

 </div>


<script>
    $('body').on('change', '[data-table="location"]' , function(){
        $('input[name="tableLocation"]').val($(this).val());
    });

    $('body').on('change', '[data-table="locationGeo"]' , function(){
        $('input[name="tableLocationGeo"]').val($(this).val());
    });

    $('[data-search="location"]').bind('click' , function(){
        $('[name="searchLocationTab"]').val('location');
    });

    $('[data-search="locationGeo"]').bind('click' , function(){
        $('[name="searchLocationTab"]').val('locationGeo');
    });
</script>