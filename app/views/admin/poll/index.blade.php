<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/10/2015
 * Time: 7:13 PM
 *
 */
?>

@extends('layouts.main')

@section('pageContent')
    <div class="widget-box">
        <div class="widget-header">
            <h5 class="widget-title bigger lighter">
                <i class="glyphicon glyphicon-tasks"></i>
                Polls
            </h5>
            <div class="widget-toolbar no-border">
                <button class="btn btn-round btn-white btn-success btn-xs " onclick="createPoll()" >&nbsp;&nbsp;<i class="fa fa-plus"></i> New Poll&nbsp;&nbsp;</button>
            </div>

        </div>

        <div class="widget-body">
            <div class="widget-main no-padding">
                <table class="table table-striped table-bordered table-hover">
                    <tbody>
                        @foreach($polls as $item)
                            <tr class="poll-item" data-id="{{$item->pollId}}">
                                <td class="" >
                                    <span class="pollQuestion">
                                        <strong>{{ $item->question }}</strong>
                                        {{$item->disabled ? '<span class="label label-danger arrowed">disabled</span>' : ''  }}
                                    </span><br>

                                    <div style="margin-top:20px;">
                                    @for($i=1; $i<=5; $i++)
                                        <?php
                                            $option = 'option'.$i;
                                            $summary = 'summary'.$i;
                                         ?>
                                        <div>
                                            <div class="col-md-6">#{{$i}}<i class="ace-icon fa fa-angle-right bigger-110"></i> {{$item->$option}}</div>
                                            <div class="col-md-6">
                                                <div data-percent="{{number_format($item->$summary)}} ({{round($item->$summary / $item->total * 100,2)}}%)" class="progress pos-rel">
                                                    <div style="width:{{ round($item->$summary / $item->total * 100,2) }}%;" class="progress-bar"></div>
                                                </div>
                                            </div>
                                        </div>
                                    @endfor
                                    </div>

                                    <span class="location label label-sm label-info arrowed-right">{{$item->scope}}</span>
                                    <span>
                                        @if($item->country      ){{$item->country}}@endif
                                        @if($item->region1      )» {{$item->region1}}@endif
                                        @if($item->region2      )» {{$item->region2}}@endif
                                        @if($item->region3      )» {{$item->region3}}@endif
                                        @if($item->region4      )» {{$item->region4}}@endif
                                        @if($item->locality     )» {{$item->locality}}@endif
                                        @if($item->postcode       )» {{$item->postcode}}@endif
                                    </span>

                                </td>

                                <td class="" width="20%">
                                    <div style="line-height: 3">
                                        <button class="btn btn-xs btn-success btn-white btn-round btn-editPoll" >
                                            <i class="ace-icon fa fa-edit green"></i>
                                            Modify
                                        </button>

                                        <button class="btn btn-xs btn-success btn-white btn-round btn-setScopePoll" >
                                            <i class="ace-icon fa fa-edit green"></i>
                                            Set Scope
                                        </button>

                                        <button class="btn btn-xs btn-danger btn-white btn-round btn-deletePoll"  >
                                            <i class="ace-icon fa fa-times bigger-110 red2"></i>
                                            Delete
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        </div>

@stop

@section('scripts')
    @include('admin.poll._poll-js')
@stop