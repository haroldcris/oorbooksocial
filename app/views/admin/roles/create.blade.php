<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/28/2015
 * Time: 3:57 AM
 *
 */ ?>

    <form class="form-horizontal" id='frmCreateRole' action="{{URL::route('storeRole')}}">
        @if(isset($role))
            <input type="hidden" name="roleId" value="{{$role->roleId}}" >
        @endif
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right"> Role</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" placeholder="" name="name" value="{{isset($role) ? $role->name : ''}}">
            </div>
        </div>
        <div style="padding-left:20px;">
            <div><label><input type="checkbox" class="ace" name = "manageRole" {{isset($role) ? $role->manageRole == 1 ? 'checked': '' : ''  }} ><span class="lbl"> Manage Roles</span></label></div>
            <div><label><input type="checkbox" class="ace" name = "manageUser" {{isset($role) ? $role->manageUser == 1 ? 'checked': '' : ''  }} ><span class="lbl"> Manage Users</span></label></div>
            <div><label><input type="checkbox" class="ace" name = "managePoll" {{isset($role) ? $role->managePoll == 1 ? 'checked': '' : ''  }} ><span class="lbl"> Manage Polls</span></label></div>
            <div><label><input type="checkbox" class="ace" name = "manageInterest" {{isset($role) ? $role->manageInterest == 1 ? 'checked': '' : ''  }} ><span class="lbl"> Manage Interests</span></label></div>
            <div><label><input type="checkbox" class="ace" name = "manageAPI" {{isset($role) ? $role->manageAPI == 1 ? 'checked': '' : ''  }} ><span class="lbl"> Manage API Keys</span></label></div>
            <div><label><input type="checkbox" class="ace" name = "manageSiteContent" {{isset($role) ? $role->manageSiteContent == 1 ? 'checked': '' : ''  }} ><span class="lbl"> Manage Site Content</span></label></div>
        </div>
    </form>