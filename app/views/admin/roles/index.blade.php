@extends('layouts.main')
@section('styles')
@stop

@section('pageContent')
<style>
	#roles thead tr th,
	#roles tbody tr td.check {
		text-align: center;
	}
</style>


<div class="page-content col-md-12" style="padding:8px 5px 24px;margin-bottom:20px;">
	<div class="widget-box">
		<div class="widget-header">
			<h5 class="widget-title bigger lighter">
				<i class="fa fa-users"></i>
				Roles
			</h5>
			<div class="widget-toolbar no-border">
				<button class="btn btn-round btn-white btn-success btn-xs " onclick="createRole()" >&nbsp;&nbsp;<i class="fa fa-plus"></i> New Role&nbsp;&nbsp;</button>
			</div>
		</div>

		<div class="widget-body">
			<div class="widget-main no-padding">
				<table colspan="10" class="table table-striped table-bordered table-hover" id="roles">
					<thead>

						<tr>
							<th rowspan="2" colspan="2">Role</th>
							@foreach($roles as $item)
						    <th colspan="2" data-id="{{$item->roleId}}" class="role-item">
							    {{$item->name}}
							</th>
							@endforeach
						</tr>
						
					</thead>
					<tbody>
						
						<tr>
							<th colspan="2" style="text-align:center; margin-top: 5px;">Role</th>
							
							@foreach($roles as $item)
							<th colspan="2" style="text-align:center" width="10%" class="check" ><div class="editRole"><label><input type="checkbox" class="ace disabled" name = "manageRole" {{$item->manageRole == 1? 'checked': ''}} ><span class="lbl"></span></label></div></th>
							
							@endforeach
						</tr>
						<tr>
							<th colspan="2" style="text-align:center">User</th>
							
							@foreach($roles as $item)
							<th colspan="2" style="text-align:center" width="10%" class="check" ><div class="editRole"><label><input type="checkbox" class="ace disabled" name = "manageUser" {{$item->manageUser == 1? 'checked': ''}} ><span class="lbl"></span></label></div></th>
							
							@endforeach
						</tr>
						<tr>
							<th colspan="2" style="text-align:center">Poll</th>
							
							@foreach($roles as $item)
							<th colspan="2" style="text-align:center" width="10%" class="check" ><div class="editRole"><label><input type="checkbox" class="ace disabled" name = "managePol" {{$item->managePoll == 1? 'checked': ''}} ><span class="lbl"></span></label></div></th>
							
							@endforeach
						</tr>
						<tr>
							<th colspan="2" style="text-align:center">Interest</th>
							
							@foreach($roles as $item)
							<th colspan="2" style="text-align:center" width="10%" class="check" ><div class="editRole"><label><input type="checkbox" class="ace disabled" name = "manageInterest" {{$item->manageInterest == 1? 'checked': ''}} ><span class="lbl"></span></label></div></th>
							
							@endforeach
						</tr>
						<tr>
							<th colspan="2" style="text-align:center">API</th>
							
							@foreach($roles as $item)
							<th colspan="2" style="text-align:center" width="10%" class="check" ><div class="editRole"><label><input type="checkbox" class="ace disabled" name = "manageAPI" {{$item->manageAPI == 1? 'checked': ''}} ><span class="lbl"></span></label></div></th>
							
							@endforeach
						</tr>
					    <tr>
							<th colspan="2" style="text-align:center">Site Content</th>
							
							@foreach($roles as $item)
							<th colspan="2" style="text-align:center" width="10%" class="check" ><div class="editRole"><label><input type="checkbox" class="ace disabled" name = "manageSiteContent" {{$item->manageSiteContent == 1? 'checked': ''}} ><span class="lbl"></span></label></div></th>
							
							@endforeach

						</tr>
						<tr>
						    <th colspan="2" style="text-align:center">Action</th>
						    @foreach($roles as $item)
						    <th colspan="2" data-id="{{$item->roleId}}" class="role-item">
                              
                                <button class="btn btn-round btn-white btn-success btn-minier btnEditRole " onclick="editRole(this)" >&nbsp;<i class="fa fa-edit green"></i> Modify&nbsp;&nbsp;&nbsp;&nbsp</button>
                                <button class="btn btn-round btn-white btn-danger btn-minier btnDeleteRole " onclick="deleteRole(this)" >&nbsp;<i class="fa fa-ban"></i> Delete&nbsp;&nbsp;&nbsp;&nbsp</button>
                              
                            </th>
                            @endforeach
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div><!-- /.row id:content -->
@stop

@section('scripts')
	@include('admin.roles._roles-js')
@stop

