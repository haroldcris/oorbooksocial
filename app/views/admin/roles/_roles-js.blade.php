<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/28/2015
 * Time: 5:42 AM
 *
 */
?>
    <script>
		{{--Create Role--}}
		var createRole = function(){
			var _url = "{{URL::route('createRole')}}"
			messageBoxRemote('frmRoles',_url).setSize(BootstrapDialog.SIZE_SMALL);
		}

		{{--Edit Role --}}
		var editRole = function (object){
			var _role = $(object).find('input').attr('name');
			var id = $(object).closest('th').attr('data-id');
			var _url = '/manage/roles/' + id;
			messageBoxRemote ('frmRoles', _url).setSize(BootstrapDialog.SIZE_SMALL);
		}

		var frmRoles_onButtonClick = function(dialogRef){
			postMyData('frmCreateRole',dialogRef).then(
				function(){
					dialogRef.close();
					location.reload();
				}
			);
		}
	</script>

	<script>
		$(function(){
			$('.editRole').find('input').removeClass('disabled');
			$('.editRole').on('click','input',function(){ return false;});
		});
	</script>


     {{--Delete Poll  --}}
     <script>
        var deleteRole = function(btn){
            var _title = $(btn).closest('th').find('.role-name').text(),
                _fieldName = 'roleId',
                _id = $(btn).closest('th').attr('data-id'),
                _formId = 'frmDeleteRole';
                _targetUrl = '{{URL::route('deleteRole')}}';

            $('.btnDeleteRole').closest('th').removeClass('active');
            $(btn).closest('tr').addClass('active');

            var _html =  'You are about to delete the following Role:<br><br>';
            _html += '<div><table class="table table-bordered "><tbody><tr><td>' + _title + '</td></tr><tbody></table></div>';
            _html += 'Do you want to continue?';
            _html += '<form id="'+ _formId +'" action="'+ _targetUrl +'">';
            _html += '<input type="hidden" name="_method" value="delete" /> ';
            _html += '<input type="hidden" name="' + _fieldName + '" value="' + _id + '" /> ';
            _html += '</form>';

            return messageBox(_formId + 'Dialog', _html ,'Confirmation','Delete Role', '', BootstrapDialog.TYPE_DANGER ).setSize(BootstrapDialog.SIZE_SMALL);
        };


        var frmDeleteRoleDialog_onButtonClick = function (dialogRef)
        {

            postMyData('frmDeleteRole', dialogRef).then (
                function()
                {
                    $('tr.role-item.active').remove();
                    dialogRef.close();
                    location.reload();
                }
            );
        }
     </script>

