 <script>

    function createLocationGroup()
    {
        messageBox('frmCreateLocationGroupDialog', 'Content','New Location Group', 'Create');
    }

    // function frmCreateLocationGroupDialog_onButtonClick(dialogRef)
    // {
        // postMyData('frmCreateLocationGroup', dialogRef).then(
        //     function(json){
        //         dialogRef.close();
        //         location.reload();
        //     }
        // )
    // }
    
    function addLocationInGroup()
    {
        //messageBox('frmAddLocationDialog', 'Content', 'Add Location', 'Save');
        search_string = '';
        search_country = '';

        var dialog = messageBox('frmAddLocationInGroupDialog', '{{Config::get('site.loader')}}' , 'Add Location');
        
        $.get('/api/v1/location/enroll').done(function(d){
            dialog.getModalBody().html(d);

        }).error(function(d){
            dialog.getModalBody().html('<span class="label label-danger">' + d.responseText + '</span>');
        });
    }
    
    function deleteLocationInGroup()
    {
        {{--messageBox('frmDeleteLocationDialog', 'Content', 'Delete Location', 'Delete');--}}
        var dialog = messageBox('frmDeleteLocationInGroupDialog','Are you sure you want to delete the location?','Delete Location','Delete','Cancel',BootstrapDialog.TYPE_DANGER);
    }
 </script>