<?php

?>

@extends('layouts.main')

@section('pageContent')
    <div class="widget-box">
        <div class="widget-header">
            <h5 class="widget-title bigger lighter">
                <i class="fa fa-globe"></i>
                Location Groups
            </h5>
            <div class="widget-toolbar no-border">
                <button class="btn btn-round btn-white btn-success btn-xs " onclick="createLocationGroup()" >&nbsp;&nbsp;<i class="fa fa-plus"></i> New Location Group&nbsp;&nbsp;</button>
            </div>
        </div>

        <div class="widget-body">
            <div class="widget-main no-padding">
                <div class="row">
                    <div class="col-md-12" style="margin: 10px 15px;">
                        <table class="table table-bordered table-hover" style="width: 670px; margin-bottom: 15px;">
                            <thead>
                                <th style="border-right: none;">Location Group Name</th>
                                <th style="border-left: none; text-align: right; width: 115px;">
                                    <button class="btn btn-xs btn-success btn-white btn-round" onclick="addLocationInGroup()">
                                        <i class="ace-icon fa fa-plus bigger-110 green"></i> Add Location
                                    </button>
                                </th>
                            </thead>
                            <tbody style="border: 1px solid #e5e5e5;">
                            <tr>
                                <td style="font-size: 11px; border-right: none;">Philippines » Central Luzon » Pampanga » San Fernando » Sindalan » 2000</td>
                                <td style="text-align: right; border-left: none;">
                                    <button class="btn btn-xs btn-danger btn-white btn-round" onclick="deleteLocationInGroup()">
                                        <i class="ace-icon fa fa-times bigger-110 red2"></i> Delete
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 11px; border-right: none;">Philippines » Central Luzon » Bataan » Dinalupihan » Santo Niño » 2110</td>
                                <td style="text-align: right; border-left: none;">
                                    <button class="btn btn-xs btn-danger btn-white btn-round">
                                        <i class="ace-icon fa fa-times bigger-110 red2"></i> Delete
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 11px; border-right: none;">Sri Lanka » Northern » Jaffna » Jaffna » Passaiyoor West » J/65</td>
                                <td style="text-align: right; border-left: none;">
                                    <button class="btn btn-xs btn-danger btn-white btn-round">
                                        <i class="ace-icon fa fa-times bigger-110 red2"></i> Delete
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        
                        <table class="table table-bordered table-hover" style="width: 670px; margin-bottom: 15px;">
                            <thead>
                                <th style="border-right: none;">Location Group Name</th>
                                <th style="border-left: none; text-align: right; width: 115px;">
                                    <button class="btn btn-xs btn-success btn-white btn-round" onclick="addLocationInGroup()">
                                        <i class="ace-icon fa fa-plus bigger-110 green"></i> Add Location
                                    </button>
                                </th>
                            </thead>
                            <tbody style="border: 1px solid #e5e5e5;">
                            <tr>
                                <td style="font-size: 11px; border-right: none;">Philippines » Central Luzon » Pampanga » San Fernando » Sindalan » 2000</td>
                                <td style="text-align: right; border-left: none;">
                                    <button class="btn btn-xs btn-danger btn-white btn-round" onclick="deleteLocationInGroup()">
                                        <i class="ace-icon fa fa-times bigger-110 red2"></i> Delete
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 11px; border-right: none;">Philippines » Central Luzon » Bataan » Dinalupihan » Santo Niño » 2110</td>
                                <td style="text-align: right; border-left: none;">
                                    <button class="btn btn-xs btn-danger btn-white btn-round">
                                        <i class="ace-icon fa fa-times bigger-110 red2"></i> Delete
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 11px; border-right: none;">Sri Lanka » Northern » Jaffna » Jaffna » Passaiyoor West » J/65</td>
                                <td style="text-align: right; border-left: none;">
                                    <button class="btn btn-xs btn-danger btn-white btn-round">
                                        <i class="ace-icon fa fa-times bigger-110 red2"></i> Delete
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    @include('admin.locationGroups._locationGroups-js')
@stop