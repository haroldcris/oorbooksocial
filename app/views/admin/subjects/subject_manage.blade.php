@extends('layouts.mainNoAds')
@section('styles')
@stop

@section('pageContent')
<div class="row">
<div class="col-sm-12 col-xs-12">
	<div class="widget-box" style="width: 700px;">
        <div class="widget-header" style="width: 686px;">
            <h5 class="widget-title bigger lighter">
                <i class="glyphicon glyphicon-book"></i>
				Subjects
            </h5>
            <div class="widget-toolbar no-border">
                <button class="btn btn-round btn-white btn-success btn-xs " onclick="createSubject()" >&nbsp;&nbsp;<i class="fa fa-plus"></i> New Subject&nbsp;&nbsp;</button>
            </div>
        </div>

        <div class="widget-body" style="width: 700px;">
            <div class="widget-main no-padding" style="width: 698px;">
                <table class="table table-striped table-bordered table-hover" style="width: 698px;">
                	<thead>
                		<tr>
                			<th style="width:50px;">Subject Order</th>
                			<th>Description</th>
                			<th style="width:160px;">Action</th>
                		</tr>
                	</thead>
                    <tbody>
                    	@foreach ($subjects as $subject)
                        <tr>
                            <td>{{ $subject->subjectOrder }}</td>
                            <td>{{ $subject->description }}</td>
                            <td align="center">
                            	<button class="btn btn-round btn-white btn-primary btn-xs " onclick="createSubject({{ $subject->subjectId }}, {{ $subject->subjectOrder }}, '{{ $subject->description }}')" >&nbsp;&nbsp;<i class="fa fa-pencil"></i> Edit&nbsp;&nbsp;</button>
                            	<button class="btn btn-round btn-white btn-danger btn-xs " onclick="removeSubject({{ $subject->subjectId }}, '{{ $subject->description }}')" >&nbsp;&nbsp;<i class="fa fa-trash"></i> Delete&nbsp;&nbsp;</button>
                            </td>
                        </tr>
                       	@endforeach 
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>		
</div><!-- /.row -->
@stop


@section('scripts')
	@include('admin.subjects._subject_manage-js')
@stop

