<script>

function createSubject() {
    var id           = ((typeof arguments[0] == 'undefined') ? '':arguments[0]);
    var subjectOrder = ((typeof arguments[1] == 'undefined') ? '':arguments[1]);
    var description  = ((typeof arguments[2] == 'undefined') ? '':arguments[2]);

    var _html = '<form id="frmCreateSubject" action="{{ URL::route("storeSubject") }}" class="form-horizontal"> \
                    <div class="form-group"> \
                        <label for="subjectOrder" class="col-sm-4 control-label">Subject Order</label> \
                        <div class="col-sm-6"> \
                            <input type="text" class="form-control" id="subjectOrder" name="subjectOrder" placeholder="Subject Order" value="' + subjectOrder + '"> \
                        </div> \
                    </div> \
                    <div class="form-group"> \
                        <label for="description" class="col-sm-4 control-label">Description</label> \
                        <div class="col-sm-6"> \
                            <input type="text" class="form-control" id="description" name="description" placeholder="Description" value="' + description + '"> \
                        </div> \
                    </div> \
                    <input type="hidden" id="id" name="id" value="' + id + '"> \
                </form>';

    var dialog = messageBox('createSubjectDialog', _html, 'Create Subject', 'Save');
    dialog.setSize(BootstrapDialog.SIZE_MEDIUM);
}

function createSubjectDialog_onButtonClick(dialogRef){
    postMyData('frmCreateSubject', dialogRef).then(function(data){
        dialogRef.close();
        location.reload();
    }, function(error){
        console.log(error);
    });
}



function removeSubject() {
    var id          = arguments[0];
    var description = arguments[1];

    var _html ='<div class="row"> \
                    <div class="col-xs-12" style="margin-top:15px;"> \
                        <p>Do you want to delete <strong>' + description + '</strong>?</p> \
                    </div> \
                    <form id="frmRemoveUser" action="{{ URL::route("deleteSubject") }}" class="form-horizontal"> \
                        <input type="hidden" name="id" value="' + id + '"> \
                    </form> \
                </div>';

    var dialog = messageBox('removeUserDialog', _html, 'Delete User Confirmation', 'Delete');
    dialog.setSize(BootstrapDialog.SIZE_SMALL);
}

function removeUserDialog_onButtonClick(dialogRef){
    postMyData('frmRemoveUser', dialogRef).then(function(data){
        dialogRef.close();
        location.reload();
    }, function(error){
        console.log(error);
    });
}

</script>