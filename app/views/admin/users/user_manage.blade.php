@extends('layouts.main')
@section('styles')
@stop

@section('pageContent')
<div class="page-content col-md-12" style="padding:8px 5px 24px;margin-bottom:20px;">

		<!--Tab Notifications-->
		<div class="tabbable">
			<ul class="nav nav-tabs" id="myTab">
				<li class="active"><a data-toggle="tab" href="#tab-ActiveUsers"><i class="green"></i>Active Users</a></li>
				<li class=""><a data-toggle="tab" href="#tab-InactiveUsers"><i class="green"></i>Inactive Users</a></li>
			</ul>

			<div class="tab-content">

				<div id="tab-ActiveUsers" class="tab-pane active">
					{{$users->links()}}
					<div class="widget-box">
				        <div class="widget-header">
				            <h5 class="widget-title bigger lighter">
				                <i class="fa fa-users"></i>
								Active Users List
				            </h5>
				            <div class="widget-toolbar no-border">
				                <button class="btn btn-round btn-white btn-success btn-xs " onclick="createUser()" >&nbsp;&nbsp;<i class="fa fa-plus"></i> New User&nbsp;&nbsp;</button>
				            </div>
				        </div>

				        <div class="widget-body">
				            <div class="widget-main no-padding">
				                <table class="table table-striped table-bordered table-hover">
				                	<thead>
				                		<tr>
				                			<th>Username</th>
				                			<th>Email</th>
				                			<th>Action</th>
				                		</tr>
				                	</thead>
				                    <tbody>
				                    	@foreach ($users as $user)
				                        <tr data-id="{{$user->userId}}">
				                            <td><a href="{{ URL::route('guestUserProfile', $user->username) }}"><span class="clearfix">{{ $user->username }}</span></a> <small class="">{{$user->securityLevel}}</small></td>
				                            <td>{{ $user->email }}</td>
				                            <td width="20%" style="line-height: 2">
				                            	<button class="btn btn-round btn-white btn-info btn-minier btnEditUser" >&nbsp;&nbsp;<i class="fa fa-edit"></i> Modify&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
				                            	<button class="btn btn-round btn-white btn-danger btn-minier " onclick="deactivateUser({{ $user->userId }})" >&nbsp;<i class="fa fa-ban"></i> Deactivate&nbsp;&nbsp;</button>
				                            </td>
				                        </tr>
				                       	@endforeach 
				                    </tbody>
				                </table>
				            </div>
				        </div>
				    </div>
				</div><!-- /.tab-ActiveUsers -->



				<div id="tab-InactiveUsers" class="tab-pane">
					<div class="widget-box">
				        <div class="widget-header">
				            <h5 class="widget-title bigger lighter">
				                <i class="fa fa-users"></i>
								Inactive Users List
				            </h5>
				        </div>

				        <div class="widget-body">
				            <div class="widget-main no-padding">
				                <table class="table table-striped table-bordered table-hover">
				                	<thead>
				                		<tr>
				                			<th>Username</th>
				                			<th>Email</th>
				                			<th>Created at</th>
				                			<th>Action</th>
				                		</tr>
				                	</thead>
				                    <tbody>
				                    	@foreach ($xusers as $xuser)
				                        <tr>
				                            <td>{{ $xuser->username }}</td>
				                            <td>{{ $xuser->email }}</td>
				                            <td>{{ $xuser->created_at }}</td>
				                            <td>
				                            	<button class="btn btn-round btn-white btn-success btn-xs " onclick="restoreUser({{ $xuser->userId }})" >&nbsp;&nbsp;<i class="fa fa-refresh"></i> Restore&nbsp;&nbsp;</button>
				                            	<button class="btn btn-round btn-white btn-danger btn-xs " onclick="removeUser({{ $xuser->userId}}, '{{ $xuser->username }}')" >&nbsp;&nbsp;<i class="fa fa-trash"></i> Delete&nbsp;&nbsp;</button>
				                            </td>
				                        </tr>
				                       	@endforeach 
				                    </tbody>
				                </table>
				            </div>
				        </div>
				    </div>
				</div><!-- /.tab-InactiveUsers -->

			</div><!-- /.tab-content -->
		</div><!-- /.tabbbable -->

</div><!-- /.row id:content -->
@stop


@section('scripts')
	@include('admin.users._user_manage-js')
@stop

