<?php /**
 * Created by Harold Cris D. Abarquez
 * Date: 03/25/2015
 * Time: 1:24 AM
 *
 */ ?>

<form id="frmCreateUser" action="{{ URL::route("storeUser") }}" class="form-horizontal frmCreateUser" >
    @if(isset($user))
        <input type="hidden" name="userId" value="{{$user->userId}}" >
    @endif
    <div class="form-group"> 
        <label for="securityLevel" class="col-sm-4 control-label">Security Level</label> 
        <div class="col-sm-6"> 
            <select id="securityLevel" name="securityLevel" class="" autofocus>
                <option value="">Choose Role</option>
                @foreach ($roles as $role)
                <option value="{{$role->name}}" {{isset($user)? $user->securityLevel == $role->name ? 'selected' : ''   :'' }} >{{$role->name}}</option>
                @endforeach
            </select> 
        </div> 
    </div> 
    <div class="form-group"> 
        <label for="email" class="col-sm-4 control-label">Email</label> 
        <div class="col-sm-6"> 
            <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{isset($user) ? $user->email : ''}}">
        </div> 
    </div>

    @if(!isset($user))
        <div class="form-group"> 
            <label for="username" class="col-sm-4 control-label">Username</label> 
            <div class="col-sm-6"> 
                <input type="text" class="form-control" id="username" name="username" placeholder="Username"> 
            </div> 
        </div> 
        <div class="form-group"> 
            <label for="password" class="col-sm-4 control-label">Password</label> 
            <div class="col-sm-6"> 
                <input type="password" class="form-control" id="password" name="password" placeholder="Password"> 
            </div> 
        </div> 
        <div class="form-group"> 
            <label for="retype_password" class="col-sm-4 control-label">Retype Password</label> 
            <div class="col-sm-6"> 
                <input type="password" class="form-control" id="retype_password" name="retype_password" placeholder="Retype Password"> 
            </div> 
        </div>
    @endif
</form>

<script>
    $('#securityLevel').select2({width:'100%'});
</script>