<script>
function createUser() {
    var dialog = messageBoxRemote('createUserDialog', '{{URL::route('createUser')}}','User Management', 'Create')
}

function createUserDialog_onButtonClick(dialogRef){
    postMyData('frmCreateUser', dialogRef).then(function(data){
        dialogRef.close();
        location.reload();
    }, function(error){
        console.log(error);
    });
}



function deactivateUser($id) {
	var _html ='<div class="row"> \
                    <div class="col-xs-12" style="margin-top:15px;"> \
                        <p>Do you want to deactivate <strong>' + $id + '</strong>?</p> \
                    </div> \
                    <form id="frmDeactivateUser" action="{{ URL::route("deactivateUser") }}" class="form-horizontal"> \
                        <input type="hidden" name="id" value="' + $id + '"> \
                    </form> \
                </div>';

    var dialog = messageBox('deactivateUserDialog', _html, 'Deactivate User Confirmation', 'Deactivate');
    dialog.setSize(BootstrapDialog.SIZE_SMALL);
}

function deactivateUserDialog_onButtonClick(dialogRef){
    postMyData('frmDeactivateUser', dialogRef).then(function(data){
        dialogRef.close();
        location.reload();
    }, function(error){
    	alert(error);
        // console.log(error);
    });
}



function restoreUser($id) {
	var _html ='<div class="row"> \
                    <div class="col-xs-12" style="margin-top:15px;"> \
                        <p>Do you want to restore <strong>' + $id + '</strong>?</p> \
                    </div> \
                    <form id="frmRestoreUser" action="{{ URL::route("restoreUser") }}" class="form-horizontal"> \
                        <input type="hidden" name="id" value="' + $id + '"> \
                    </form> \
                </div>';

    var dialog = messageBox('restoreUserDialog', _html, 'Restore User Confirmation', 'Restore');
    dialog.setSize(BootstrapDialog.SIZE_SMALL);
}

function restoreUserDialog_onButtonClick(dialogRef){
    postMyData('frmRestoreUser', dialogRef).then(function(data){
        dialogRef.close();
        location.reload();
    }, function(error){
    	alert(error);
    });
}



function removeUser($id, username) {
	var _html ='<div class="row"> \
                    <div class="col-xs-12" style="margin-top:15px;"> \
                        <p>Do you want to remove <strong>' + username + '</strong>?</p> \
                    </div> \
                    <form id="frmRemoveUser" action="{{ URL::route("deleteUser") }}" class="form-horizontal"> \
                        <input type="hidden" name="id" value="' + $id + '"> \
                    </form> \
                </div>';

    var dialog = messageBox('removeUserDialog', _html, 'Delete User Confirmation', 'Delete');
    dialog.setType(BootstrapDialog.TYPE_DANGER)
          .setSize(BootstrapDialog.SIZE_SMALL);
}

function removeUserDialog_onButtonClick(dialogRef){
    postMyData('frmRemoveUser', dialogRef).then(function(data){
        dialogRef.close();
        location.reload();
    }, function(error){
    	alert(error);
    });
}

</script>

{{--Edit User--}}
<script>
    $('.btnEditUser').on('click',function(){
        var _id = $(this).closest('tr').attr('data-id');
        var _url = '{{URL::route('editUser')}}'.replace('%7Bid%7D', _id);

        messageBoxRemote('frmEditUserDialog',_url,'User', 'Update');

    });

    function frmEditUserDialog_onButtonClick(dialogRef)
    {
        postMyData('frmCreateUser', dialogRef).then(
            function(){
                location.reload();
            }
        )
    }
</script>