<?php

?>

<style type="text/css">
    div.modal-content{
        width: 120%;
    }
    div.scrollable {
        width: 100%;
        height: 200px;
        margin: 0;
        padding: 0;
        overflow: auto;
    }
    .boxfix {
        display: block;
        padding-left: 15px;
        text-indent: -15px;
        font-size: 10px;
        margin-bottom: -10px;
    }
    .boxfix input {
        padding: 0;
        margin:0;
        vertical-align: bottom;
        position: relative;
        top: -1px;
    }
    .spinnery {
        font-size: 50px;
        position: absolute;
        top: 75px;
        left: 310px;
        display: none;
        z-index: 999;
    }
    .geo-table {
        height: 200px; 
        max-height: 200px; 
        min-height: 200px; 
        width: 700px; 
        max-width: 700px; 
        min-width: 700px;
    }
</style>

<div class="panel panel-body" style="padding:0">
    <form id="frmCreatePost" action="{{URL::route('storeLocationScope')}}" enctype="multipart/form-data" >
	    <div class="tab-content tab-color-blue">
    	    <div class="form-group">
                {{ Form::token() }}
                
                <input  id="hidContinent" name="hidContinent" type="hidden">
                <input  id="hidCountry"   name="hidCountry"   type="hidden">
                <input  id="hidRegion1"   name="hidRegion1"   type="hidden">
                <input  id="hidRegion2"   name="hidRegion2"   type="hidden">
                <input  id="hidRegion3"   name="hidRegion3"   type="hidden">
                <input  id="hidRegion4"   name="hidRegion4"   type="hidden">
                <input  id="hidLocality"  name="hidLocality"  type="hidden">
                <input  id="userId"       name="userId"       type="hidden">
                <table border="0" style="geo-table">
                    <thead>
                        <tr>
                            <th id="thCN" style="width: 100px;">CONTINENT</th>
                            <th id="thCT" style="width: 100px;">COUNTRY</th>
                            <th id="thR1" style="width: 100px;">REGION 1</th>
                            <th id="thR2" style="width: 100px;">REGION 2</th>
                            <th id="thR3" style="width: 100px;">REGION 3</th>
                            <th id="thR4" style="width: 100px;">REGION 4</th>
                            <th id="thLC" style="width: 100px;">LOCALITY</th>
                        <tr>
                        
                    </thead>
                    <tbody>
                        <i id="spinnery" class="fa fa-spinner fa-spin spinnery"></i>
                        <tr>
                            <td style="vertical-align: top;"><div id="tdCN" class="scrollable"></td>
                            <td style="vertical-align: top;"><div id="tdCT" class="scrollable"></td>
                            <td style="vertical-align: top;"><div id="tdR1" class="scrollable"></td>
                            <td style="vertical-align: top;"><div id="tdR2" class="scrollable"></td>
                            <td style="vertical-align: top;"><div id="tdR3" class="scrollable"></td>
                            <td style="vertical-align: top;"><div id="tdR4" class="scrollable"></td>
                            <td style="vertical-align: top;"><div id="tdLC" class="scrollable"></td>
                        </tr>
                       
                    </tbody>
                </table>
                
            </div>
        </div>
    </form>
</div>


@include('layouts.partials._geoCheckboxFunction-js')
@include('layouts.partials._geoCheckboxLoad-js')
@include('layouts.partials._geoCheckboxChange-js')