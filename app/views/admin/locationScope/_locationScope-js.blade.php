<script>
function setScopeMasterUser() {
    search_string = '';
    search_country = '';

    var dialog = messageBox('setScopeMasterUserDialog', '{{Config::get('site.loader')}}' , 'Add Location');
    
    $.get('/api/v1/location/enroll').done(function(d){
        dialog.getModalBody().html(d);

    }).error(function(d){
        dialog.getModalBody().html('<span class="label label-danger">' + d.responseText + '</span>');
    });
}
function createLocationScope(){
    var id = arguments[0];
    var username = arguments[1];
    console.log(id + ' ' + username);
    
    var dialog = messageBox('createLocationScope', '{{Config::get('site.loader')}}', 'Set Location Scope for ' + username, 'Set Scope','',BootstrapDialog.TYPE_SUCCESS);
    
    $.get('{{URL::route('createLocationScope')}}')
    .error(function(error){
        dialog.getModalBody().html(error.responseText); })
    .done(function(frmData){
        dialog.getModalBody().html(frmData);
        $('#userId').val(id);
    });
}

function createLocationScope_onButtonClick(dialogRef){
        
    postMyData('frmCreatePost', dialogRef).then (function(done){
        console.log(done);
        // location.reload();
    },function(err){});
    
}
</script>