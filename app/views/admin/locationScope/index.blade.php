@extends('layouts.main')
@section('styles')
@stop

@section('pageContent')
<div class="page-content col-md-12" style="padding:8px 5px 24px;margin-bottom:20px;">

		<!--Tab Notifications-->
		<div class="tabbable">
			<ul class="nav nav-tabs" id="myTab">
				<li class="active"><a data-toggle="tab" href="#tab-ActiveUsers"><i class="green"></i>Location Scope</a></li>
			</ul>

			<div class="tab-content">

				<div id="tab-ActiveUsers" class="tab-pane active">
					{{$items->links()}}
					<div class="widget-box">
				        <div class="widget-header">
				            <h5 class="widget-title bigger lighter">
				                <i class="fa fa-user"></i>
								Master Users List
				            </h5>
				        </div>

				        <div class="widget-body">
				            <div class="widget-main no-padding">
				                <table class="table table-striped table-bordered table-hover">
				                	<thead>
				                		<tr>
				                			<th>Username</th>
				                			<th>Email</th>
				                			<th>Action</th>
				                		</tr>
				                	</thead>
				                    <tbody>
				                    	@foreach ($items as $item)
				                        <tr data-id="{{$item->userId}}">
				                            <td><a href="{{ URL::route('guestUserProfile', $item->username) }}"><span class="clearfix">{{$item->username}}</span></a> <small class="">{{$item->securityLevel}}</small></td>
				                            <td>{{$item->email}}</td>
				                            <td width="20%" style="line-height: 2">
				                            	<button class="btn btn-round btn-white btn-info btn-minier" onclick="createLocationScope('{{$item->userId}}', '{{$item->username}}')" >&nbsp;<i class="fa fa-edit"></i> Set Scope&nbsp;&nbsp;</button>
				                            </td>
				                        </tr>
				                       	@endforeach
				                    </tbody>
				                </table>
				            </div>
				        </div>
				    </div>
				</div><!-- /.tab-ActiveUsers -->


			</div><!-- /.tab-content -->
		</div><!-- /.tabbbable -->

</div><!-- /.row id:content -->
@stop


@section('scripts')
	@include('admin.locationScope._locationScope-js')
@stop

