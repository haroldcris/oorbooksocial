@extends('layouts.default')
@section('styles')
    <style type="text/css">
        div.scrollable {
            width: 100%;
            height: 200px;
            margin: 0;
            padding: 0;
            overflow: auto;
        }
        .boxfix {
            display: block;
            padding-left: 15px;
            text-indent: -15px;
            font-size: 10px;
            margin-bottom: -10px;
        }
        .boxfix-input {
            padding: 0;
            margin:0;
            vertical-align: bottom;
            position: relative;
            top: -1px;
        }
        .spinnery {
            font-size: 50px;
            position: absolute;
            top: 75px;
            left: 420px;
            display: none;
            z-index: 999;
        }
        .geo-table {
            height: 200px; 
            max-height: 200px; 
            min-height: 200px; 
            width: 700px; 
            max-width: 700px; 
            min-width: 700px;
        }
    </style>
@stop
@section('pageContent')
    <form id="frmCreatePost">
        {{ Form::token() }}
        <input  id="hidContinent" name="hidContinent" type="hidden">
        <input  id="hidCountry"   name="hidCountry"   type="hidden">
        <input  id="hidRegion1"   name="hidRegion1"   type="hidden">
        <input  id="hidRegion2"   name="hidRegion2"   type="hidden">
        <input  id="hidRegion3"   name="hidRegion3"   type="hidden">
        <input  id="hidRegion4"   name="hidRegion4"   type="hidden">
        <input  id="hidLocality"  name="hidLocality"  type="hidden">
        
        <table border="0" id="geoTable">
            <thead>
                <tr>
                    <th id="thCN" style="width: 100px;">CONTINENT</th>
                    <th id="thCT" style="width: 100px;">COUNTRY</th>
                    <th id="thR1" style="width: 100px;">REGION 1</th>
                    <th id="thR2" style="width: 100px;">REGION 2</th>
                    <th id="thR3" style="width: 100px;">REGION 3</th>
                    <th id="thR4" style="width: 100px;">REGION 4</th>
                    <th id="thLC" style="width: 100px;">LOCALITY</th>
                <tr>
                
            </thead>
            <tbody>
                <i id="spinnery" class="fa fa-spinner fa-spin"></i>
                <tr id="geoRow">
                    <td style="vertical-align: top;"><div id="tdCN" class="scrollable"></td>
                    <td style="vertical-align: top;"><div id="tdCT" class="scrollable"></td>
                    <td style="vertical-align: top;"><div id="tdR1" class="scrollable"></td>
                    <td style="vertical-align: top;"><div id="tdR2" class="scrollable"></td>
                    <td style="vertical-align: top;"><div id="tdR3" class="scrollable"></td>
                    <td style="vertical-align: top;"><div id="tdR4" class="scrollable"></td>
                    <td style="vertical-align: top;"><div id="tdLC" class="scrollable"></td>
                </tr>
               
            </tbody>
        </table>
          
        
    </form>
@stop

@section('scripts')
<script>

var ctrCN = '{{ $ctrCN }}';
var ctrCT = '{{ $ctrCT }}';
var ctrR1 = '{{ $ctrR1 }}';
var ctrR2 = '{{ $ctrR2 }}';
var ctrR3 = '{{ $ctrR3 }}';
var ctrR4 = '{{ $ctrR4 }}';
var ctrLC = '{{ $ctrLC }}';

// Loop through geos then append
function loopend(data, geo, element, spinnery){
    for(var i=0; i<data.length; i++) {
        var geos;
        var name;
        switch(geo) {
            case 'continent':
                geos = data[i].continent;
                name = "Continent";
                break;
            case 'country':
                geos = data[i].country;
                name = "Country";
                break;
            case 'region1':
                geos = data[i].region1;
                name = "Region1";
                break;
            case 'region2':
                geos = data[i].region2;
                name = "Region2";
                break;
            case 'region3':
                geos = data[i].region3;
                name = "Region3";
                break;
            case 'region4':
                geos = data[i].region4;
                name = "Region4";
                break;
            case 'locality':
                geos = data[i].locality;
                name = "Locality";
                break;
        }
        var newOption = $('<label class="boxfix"><input type="checkbox" class="chk'+ htmlspecialchars(name) +'" name="geo' + htmlspecialchars(name) + '[]" value="' + htmlspecialchars(geos) + '">' + htmlspecialchars(geos) + '</label><br>');
        $(element).append(newOption);
    }
    ipakita(element);
    ipakita(spinnery, false);
}

// Set to empty then destroy
function emptroy(){
    if(arguments.length == 0) return;
    for(var i=0; i<arguments.length; i++) {
        $(arguments[i]).empty().css('display', 'none');
    }
}

// Check if data has a value
function hasValue(data) {
    if(typeof data !== 'undefined' && data.length > 0){
        return true;
    }
}

// Escape string
function htmlspecialchars(str) {
    if (typeof(str) == "string") {
        str = str.replace(/&/g, "&amp;"); /* must do &amp; first */
        str = str.replace(/"/g, "&quot;");
        str = str.replace(/'/g, "&#039;");
        str = str.replace(/</g, "&lt;");
        str = str.replace(/>/g, "&gt;");
    }
    return str;
}

// Set visibility [ default: visible ]
function ipakita(element, bool){
    // If bool is not set
    if(typeof(bool)==='undefined') {
        $(element).css('display', 'inline-block');
    } else {
        $(element).css('display', 'none');
    }
}

// Set fields to empty values
function emptiness() {
    if(arguments.length == 0) return;
    for(var i=0; i<arguments.length; i++) {
        $(arguments[i]).val('');
    }
}

function getLastSelectedGeo() {
    // Returns the last selected geo, starts with the locality(last).
    if(ctrLC != 0) {
        return 'Locality';
    } else if(ctrR4 != 0) {
        return 'Region 4';
        
    } else if(ctrR3 != 0) {
        return 'Region 3';
        
    } else if(ctrR2 != 0) {
        return 'Region 2';
        
    } else if(ctrR1 != 0) {
        return 'Region 1';
        
    } else if(ctrCT != 0) {
        return 'Country';
        
    } else if(ctrCN != 0) {
        return 'Continent';
    }
}

function setValue(element, data) {
    $(element).empty().append(data).css('font-size', '10px');
}

function copyCheckedValToHiddenVal(classOfCheckedCheckbox, idOfHiddenInput) {
    $("input:checkbox[class=" + classOfCheckedCheckbox + "]:checked").each(function() {
        $(idOfHiddenInput).val($(this).val());
    });
}
</script>
<script>

// ------------------------------------------------------------------------    
// O N   L O A D    
// ------------------------------------------------------------------------

$(function(){
    ipakita('#spinnery');
    
    console.log('ctrCN: '+ctrCN+'\t ctrCT: '+ctrCT+'\t ctrR1: '+ctrR1+'\t ctrR2: '+ctrR2+'\t ctrR3: '+ctrR3+'\t ctrR4: '+ctrR4+'\t ctrLC: '+ctrLC)
    
        
// C O N T I N E N T
    // Show Selected Scope Geos
    if(ctrCN != 0) {
        console.log('Admin o sel CN: ' + ctrCN + '\t *** Loop a Label ***');
        '@foreach($iCN as $geo)'
            $('#tdCN').append($('<div class="scopeCN"><input type="checkbox" class="chkContinent" style="display: none;" name="geoContinent[]" value="{{ $geo }}" checked ><label class="boxfix" style="margin-bottom: -3px;">' + '{{ $geo }}' + '</label></div>'));
        '@endforeach'
    } else {
        console.log('Admin x sel CN: ' + ctrCN + '\t *** Loop Checkboxes ***');
        $.post('{{ URL::route("getGeoContinent") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
            $('#tdCN').empty().append($('<label class="boxfix"><input type="checkbox" class="chkGlobal" name="geoContinent[]" value="Global">Global</label><br>'));
            loopend(data, 'continent', '#tdCN', '#spinnery');
        });
    }
    
// C O U N T R Y
    // Show Selected Scope Geos
    if(ctrCT != 0) {
        console.log('Admin o sel CT: ' + ctrCT + '\t *** Loop a Label ***');
        '@foreach($iCT as $geo)'
            $('#tdCT').append($('<div class="scopeCT"><input type="checkbox" class="chkCountry" style="display: none;" name="geoCountry[]" value="{{ $geo }}" checked ><label class="boxfix" style="margin-bottom: -3px;">' + '{{ $geo }}' + '</label></div>'));
        '@endforeach'
    }

// R E G I O N   1
    // Show Selected Scope Geos
    if(ctrR1 != 0) {
        console.log('Admin o sel R1: ' + ctrR1 + '\t *** Loop a Label ***');
        '@foreach($iR1 as $geo)'
            $('#tdR1').append($('<div class="scopeR1"><input type="checkbox" class="chkRegion1" style="display: none;" name="geoRegion1[]" value="{{ $geo }}" checked ><label class="boxfix" style="margin-bottom: -3px;">' + '{{ $geo }}' + '</label></div>'));
        '@endforeach'
    }

// R E G I O N   2
    // Show Selected Scope Geos
    if(ctrR2 != 0) {
        console.log('Admin o sel R2: ' + ctrR2 + '\t *** Loop a Label ***');
        '@foreach($iR2 as $geo)'
            $('#tdR2').append($('<div class="scopeR2"><input type="checkbox" class="chkRegion2" style="display: none;" name="geoRegion2[]" value="{{ $geo }}" checked ><label class="boxfix" style="margin-bottom: -3px;">' + '{{ $geo }}' + '</label></div>'));
        '@endforeach'
    }
    
// R E G I O N   3
    // Show Selected Scope Geos
    if(ctrR3 != 0) {
        console.log('Admin o sel R3: ' + ctrR3 + '\t *** Loop a Label ***');
        '@foreach($iR3 as $geo)'
            $('#tdR3').append($('<div class="scopeR3"><input type="checkbox" class="chkRegion3" style="display: none;" name="geoRegion3[]" value="{{ $geo }}" checked ><label class="boxfix" style="margin-bottom: -3px;">' + '{{ $geo }}' + '</label></div>'));
        '@endforeach'
    }

// R E G I O N   4
    // Show Selected Scope Geos
    if(ctrR4 != 0) {
        console.log('Admin o sel R4: ' + ctrR4 + '\t *** Loop a Label ***');
        '@foreach($iR4 as $geo)'
            $('#tdR4').append($('<div class="scopeR4"><input type="checkbox" class="chkRegion4" style="display: none;" name="geoRegion4[]" value="{{ $geo }}" checked ><label class="boxfix" style="margin-bottom: -3px;">' + '{{ $geo }}' + '</label></div>'));
        '@endforeach'
    }

// L O C A L I T Y
    // Show Selected Scope Geos
    if(ctrLC != 0) {
        console.log('Admin o sel LC: ' + ctrLC + '\t *** Loop a Label ***');
        '@foreach($iLC as $geo)'
            $('#tdLC').append($('<div class="scopeLC"><input type="checkbox" class="chkLocality" style="display: none;" name="geoLocality[]" value="{{ $geo }}" checked ><label class="boxfix" style="margin-bottom: -3px;">' + '{{ $geo }}' + '</label></div>'));
        '@endforeach'
    }
    
    
    
    console.log('getLastSelectedGeo() == ' + getLastSelectedGeo());


    
    if(getLastSelectedGeo() == 'Continent') {
    
        '@if($iCN[0] != "Global")'
            if(ctrCN == 1) {
                console.log('CN: Loop CT');
                emptroy('#tdCT', '#tdR1', '#tdR2', '#tdR3', '#tdR4', '#tdLC');
                copyCheckedValToHiddenVal('chkContinent', '#hidContinent');
                // Loop
                $.post('{{ URL::route("getGeoCountry") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                    loopend(data, 'country', '#tdCT', '#spinnery');
                });
            }
        '@else'
            copyCheckedValToHiddenVal('chkContinent', '#hidContinent');
            console.log('Put Global Label');
        '@endif'
        ipakita('#spinnery', false);
    } 
    
    else if(getLastSelectedGeo() == 'Country') {
        
        // Hidden Field Value Setting of Geos Upper Than CT
        copyCheckedValToHiddenVal('chkContinent', '#hidContinent');
    
        if(ctrCT == 1) {
            console.log('CT: Loop R1 - LC');
            emptroy('#tdR1', '#tdR2', '#tdR3', '#tdR4', '#tdLC');
            copyCheckedValToHiddenVal('chkCountry', '#hidCountry');
            // Loop
            $.post('{{ URL::route("getGeoRegion1") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                if(hasValue(data)){
                    console.log('Found');
                    loopend(data, 'region1', '#tdR1', '#spinnery');
                } else {
                    console.log('Not Found');
                    $.post('{{ URL::route("getGeoRegion2") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                        if(hasValue(data)){
                            console.log('Found');
                            loopend(data, 'region2', '#tdR2', '#spinnery');
                        } else {
                            console.log('Not Found');
                            $.post('{{ URL::route("getGeoRegion3") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                                if(hasValue(data)){
                                    console.log('Found');
                                    loopend(data, 'region3', '#tdR3', '#spinnery');
                                } else {
                                    console.log('Not Found');
                                    $.post('{{ URL::route("getGeoRegion4") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                                        if(hasValue(data)){
                                            console.log('Found');
                                            loopend(data, 'region4', '#tdR4', '#spinnery');
                                        } else {
                                            console.log('Not Found');
                                            $.post('{{ URL::route("getGeoLocality") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                                                if(hasValue(data)){
                                                    console.log('Found');
                                                    loopend(data, 'locality', '#tdLC', '#spinnery');
                                                } else {
                                                    console.log('Not Found');
                                                    emptroy('#tdLC');
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
        ipakita('#spinnery', false);
    } 
    
    else if(getLastSelectedGeo() == 'Region 1') {
        
        // Hidden Field Value Setting of Geos Upper Than R1
        copyCheckedValToHiddenVal('chkContinent', '#hidContinent');
        copyCheckedValToHiddenVal('chkCountry', '#hidCountry');
    
        if(ctrR1 == 1) {
            console.log('R1: Loop R2 - LC');
            emptroy('#tdR2', '#tdR3', '#tdR4', '#tdLC');
            copyCheckedValToHiddenVal('chkRegion1', '#hidRegion1');
            // Loop
            $.post('{{ URL::route("getGeoRegion2") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                if(hasValue(data)){
                    console.log('Found');
                    loopend(data, 'region2', '#tdR2', '#spinnery');
                } else {
                    console.log('Not Found');
                    $.post('{{ URL::route("getGeoRegion3") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                        if(hasValue(data)){
                            console.log('Found');
                            loopend(data, 'region3', '#tdR3', '#spinnery');
                        } else {
                            console.log('Not Found');
                            $.post('{{ URL::route("getGeoRegion4") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                                if(hasValue(data)){
                                    console.log('Found');
                                    loopend(data, 'region4', '#tdR4', '#spinnery');
                                } else {
                                    console.log('Not Found');
                                    $.post('{{ URL::route("getGeoLocality") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                                        if(hasValue(data)){
                                            console.log('Found');
                                            loopend(data, 'locality', '#tdLC', '#spinnery');
                                        } else {
                                            console.log('Not Found');
                                            emptroy('#tdLC');
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
        ipakita('#spinnery', false);
    } 
    
    else if(getLastSelectedGeo() == 'Region 2') {
        
        // Hidden Field Value Setting of Geos Upper Than R2
        copyCheckedValToHiddenVal('chkContinent', '#hidContinent');
        copyCheckedValToHiddenVal('chkCountry', '#hidCountry');
        copyCheckedValToHiddenVal('chkRegion1', '#hidRegion1');
    
        if(ctrR2 == 1) {
            console.log('R2: Loop R3 - LC');
            emptroy('#tdR3', '#tdR4', '#tdLC');
            copyCheckedValToHiddenVal('chkRegion2', '#hidRegion2');
            
            // Loop
            $.post('{{ URL::route("getGeoRegion3") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                if(hasValue(data)){
                    console.log('Found');
                    loopend(data, 'region3', '#tdR3', '#spinnery');
                } else {
                    console.log('Not Found');
                    $.post('{{ URL::route("getGeoRegion4") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                        if(hasValue(data)){
                            console.log('Found');
                            loopend(data, 'region4', '#tdR4', '#spinnery');
                        } else {
                            console.log('Not Found');
                            $.post('{{ URL::route("getGeoLocality") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                                if(hasValue(data)){
                                    console.log('Found');
                                    loopend(data, 'locality', '#tdLC', '#spinnery');
                                } else {
                                    console.log('Not Found');
                                    emptroy('#tdLC');
                                }
                            });
                        }
                    });
                }
            });
        }
        ipakita('#spinnery', false);
    } 
    
    else if(getLastSelectedGeo() == 'Region 3') {
        
        // Hidden Field Value Setting of Geos Upper Than R3
        copyCheckedValToHiddenVal('chkContinent', '#hidContinent');
        copyCheckedValToHiddenVal('chkCountry', '#hidCountry');
        copyCheckedValToHiddenVal('chkRegion1', '#hidRegion1');
        copyCheckedValToHiddenVal('chkRegion2', '#hidRegion2');
        
        if(ctrR3 == 1) {
            console.log('R3: Loop R4 - LC');
            emptroy('#tdR4', '#tdLC');
            copyCheckedValToHiddenVal('chkRegion3', '#hidRegion3');
            // Loop
            $.post('{{ URL::route("getGeoRegion4") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                if(hasValue(data)){
                    console.log('Found');
                    loopend(data, 'region4', '#tdR4', '#spinnery');
                } else {
                    console.log('Not Found');
                    $.post('{{ URL::route("getGeoLocality") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                        if(hasValue(data)){
                            console.log('Found');
                            loopend(data, 'locality', '#tdLC', '#spinnery');
                        } else {
                            console.log('Not Found');
                            emptroy('#tdLC');
                        }
                    });
                }
            });
        }
        ipakita('#spinnery', false);
    } 
    
    else if(getLastSelectedGeo() == 'Region 4') {
        
        // Hidden Field Value Setting of Geos Upper Than R4
        copyCheckedValToHiddenVal('chkContinent', '#hidContinent');
        copyCheckedValToHiddenVal('chkCountry', '#hidCountry');
        copyCheckedValToHiddenVal('chkRegion1', '#hidRegion1');
        copyCheckedValToHiddenVal('chkRegion2', '#hidRegion2');
        copyCheckedValToHiddenVal('chkRegion3', '#hidRegion3');
        
        if(ctrR4 == 1) {
            console.log('R4: Loop LC');
            emptroy('#tdLC');
            copyCheckedValToHiddenVal('chkRegion4', '#hidRegion4');
            // Loop
            $.post('{{ URL::route("getGeoLocality") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                if(hasValue(data)){
                    console.log('Found');
                    loopend(data, 'locality', '#tdLC', '#spinnery');
                } else {
                    console.log('Not Found');
                    emptroy('#tdLC');
                }
            });
        }
        ipakita('#spinnery', false);
    } 
    
    else if(getLastSelectedGeo() == 'Locality') {
        
        // Hidden Field Value Setting of Geos Upper Than LC
        copyCheckedValToHiddenVal('chkContinent', '#hidContinent');
        copyCheckedValToHiddenVal('chkCountry', '#hidCountry');
        copyCheckedValToHiddenVal('chkRegion1', '#hidRegion1');
        copyCheckedValToHiddenVal('chkRegion2', '#hidRegion2');
        copyCheckedValToHiddenVal('chkRegion3', '#hidRegion3');
        copyCheckedValToHiddenVal('chkRegion4', '#hidRegion4');
        
        if(ctrLC == 1) {
            console.log('Labeled CN - LC');
            copyCheckedValToHiddenVal('chkLocality', '#hidLocality');
        }
        ipakita('#spinnery', false);
    } 
    
    console.log('CN: '+$('#hidContinent').val()+'\t'+'CT: '+$('#hidCountry').val()+'\t'+'R1: '+$('#hidRegion1').val()+'\t'+'R2: '+$('#hidRegion2').val()+'\t'+'R3: '+$('#hidRegion3').val()+'\t'+'R4: '+$('#hidRegion4').val()+'\t'+'LC: '+$('#hidLocality').val());
    
});
</script>
<script>

// ------------------------------------------------------------------------    
// O N   C H A N G E    
// ------------------------------------------------------------------------
    
$(document).ready(function(){

    // On Load: Set Checkbox counter to zero
    var CN_checkCounter=0;var CT_checkCounter=0;var R1_checkCounter=0;var R2_checkCounter=0;var R3_checkCounter=0;var R4_checkCounter=0;var LC_checkCounter=0;
    
    $(document.body).on("change", ".chkGlobal", function(){
        ipakita('#spinnery');
        if($(this).is(":checked")) {
            $('.chkContinent').prop('checked', false);
            CN_checkCounter=0;CT_checkCounter=0;R1_checkCounter=0;R2_checkCounter=0;R3_checkCounter=0;R4_checkCounter=0;LC_checkCounter=0;
            emptroy('#tdCT', '#tdR1', '#tdR2', '#tdR3', '#tdR4', '#tdLC');
            emptiness('#hidContinent','#hidCountry', '#hidRegion1', '#hidRegion2', '#hidRegion3', '#hidRegion4', '#hidLocality');
            ipakita('#spinnery', false);
            
            console.log('CN: '+CN_checkCounter+'\t'+'CT: '+CT_checkCounter+'\t'+'R1: '+R1_checkCounter+'\t'+'R2: '+R2_checkCounter+'\t'+'R3: '+R3_checkCounter+'\t'+'R4: '+R4_checkCounter+'\t'+'LC: '+LC_checkCounter);
            console.log('CN: '+$('#hidContinent').val()+'\t'+'CT: '+$('#hidCountry').val()+'\t'+'R1: '+$('#hidRegion1').val()+'\t'+'R2: '+$('#hidRegion2').val()+'\t'+'R3: '+$('#hidRegion3').val()+'\t'+'R4: '+$('#hidRegion4').val()+'\t'+'LC: '+$('#hidLocality').val());
        }
    });
    
    // Check Continent
    $(document.body).on("change", ".chkContinent", function(){
        $(this).is(":checked") ? CN_checkCounter++ : CN_checkCounter--;
        
        $('.chkGlobal').prop('checked', false);
        // Set Checkbox counter to zero
        CT_checkCounter=0;R1_checkCounter=0;R2_checkCounter=0;R3_checkCounter=0;R4_checkCounter=0;LC_checkCounter=0;

        if(CN_checkCounter==0) {
            //destroy all, empty data
            console.log('0 CN_checkCounter');
            emptroy('#tdCT', '#tdR1', '#tdR2', '#tdR3', '#tdR4', '#tdLC');
        } else if(CN_checkCounter>1) {
            //destroy all child
            console.log('1+ CN_checkCounter: ' + CN_checkCounter);
            emptroy('#tdCT', '#tdR1', '#tdR2', '#tdR3', '#tdR4', '#tdLC');
        } else if(CN_checkCounter==1) {
            //loop country to locality
            $('#hidContinent').val($('input[name="geoContinent[]"]:checked').val());
            emptiness('#hidCountry', '#hidRegion1', '#hidRegion2', '#hidRegion3', '#hidRegion4', '#hidLocality');

            console.log('CN: '+CN_checkCounter+'\t'+'CT: '+CT_checkCounter+'\t'+'R1: '+R1_checkCounter+'\t'+'R2: '+R2_checkCounter+'\t'+'R3: '+R3_checkCounter+'\t'+'R4: '+R4_checkCounter+'\t'+'LC: '+LC_checkCounter);
            console.log('CN: '+$('#hidContinent').val()+'\t'+'CT: '+$('#hidCountry').val()+'\t'+'R1: '+$('#hidRegion1').val()+'\t'+'R2: '+$('#hidRegion2').val()+'\t'+'R3: '+$('#hidRegion3').val()+'\t'+'R4: '+$('#hidRegion4').val()+'\t'+'LC: '+$('#hidLocality').val());
            
            ipakita('#spinnery');

            $.post('{{ URL::route("getGeoCountry") }}', $('#frmCreatePost').serialize())
            .fail(function(err) { console.log(err); })
            .done(function(data) {
                loopend(data, 'country', '#tdCT', '#spinnery');
            });
        }
        
        // if($(this).is(":checked") == 'Global') {
        //     console.log('Global');
        // } else {
        //     console.log('!global');
        // }
    });
            
    
    
    // Check Country
    $(document.body).on("change", ".chkCountry", function(){
        $(this).is(":checked") ? CT_checkCounter++ : CT_checkCounter--;
        
        // Set Checkbox counter to zero
        R1_checkCounter=0;R2_checkCounter=0;R3_checkCounter=0;R4_checkCounter=0;LC_checkCounter=0;
        
        if(CT_checkCounter==0) {
            //destroy all, empty data
            console.log('0 CT_checkCounter');
            emptroy('#tdR1', '#tdR2', '#tdR3', '#tdR4', '#tdLC');
        } else if(CT_checkCounter>1) {
            //destroy all child
            console.log('1+ CT_checkCounter: ' + CT_checkCounter);
            emptroy('#tdR1', '#tdR2', '#tdR3', '#tdR4', '#tdLC');
            
        } else if(CT_checkCounter==1) {
            //loop region1 to locality
            $('#hidCountry').val($('input[name="geoCountry[]"]:checked').val());
            emptiness('#hidRegion1', '#hidRegion2', '#hidRegion3', '#hidRegion4', '#hidLocality');

            console.log('CN: '+CN_checkCounter+'\t'+'CT: '+CT_checkCounter+'\t'+'R1: '+R1_checkCounter+'\t'+'R2: '+R2_checkCounter+'\t'+'R3: '+R3_checkCounter+'\t'+'R4: '+R4_checkCounter+'\t'+'LC: '+LC_checkCounter);
            console.log('CN: '+$('#hidContinent').val()+'\t'+'CT: '+$('#hidCountry').val()+'\t'+'R1: '+$('#hidRegion1').val()+'\t'+'R2: '+$('#hidRegion2').val()+'\t'+'R3: '+$('#hidRegion3').val()+'\t'+'R4: '+$('#hidRegion4').val()+'\t'+'LC: '+$('#hidLocality').val());
            
            ipakita('#spinnery');
            
            $.post('{{ URL::route("getGeoRegion1") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                if(hasValue(data)){
                    console.log('Found');
                    loopend(data, 'region1', '#tdR1', '#spinnery');
                } else {
                    console.log('Not Found');
                    $.post('{{ URL::route("getGeoRegion2") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                        if(hasValue(data)){
                            console.log('Found');
                            loopend(data, 'region2', '#tdR2', '#spinnery');
                        } else {
                            console.log('Not Found');
                            $.post('{{ URL::route("getGeoRegion3") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                                if(hasValue(data)){
                                    console.log('Found');
                                    loopend(data, 'region3', '#tdR3', '#spinnery');
                                } else {
                                    console.log('Not Found');
                                    $.post('{{ URL::route("getGeoRegion4") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                                        if(hasValue(data)){
                                            console.log('Found');
                                            loopend(data, 'region4', '#tdR4', '#spinnery');
                                        } else {
                                            console.log('Not Found');
                                            $.post('{{ URL::route("getGeoLocality") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                                                if(hasValue(data)){
                                                    console.log('Found');
                                                    loopend(data, 'locality', '#tdLC', '#spinnery');
                                                } else {
                                                    console.log('Not Found');
                                                    emptroy('#tdLC');
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
    
    
    
    
    // Check Region1
    $(document.body).on("change", ".chkRegion1", function(){
        $(this).is(":checked") ? R1_checkCounter++ : R1_checkCounter--;
        
        // Set Checkbox counter to zero
        R2_checkCounter=0;R3_checkCounter=0;R4_checkCounter=0;LC_checkCounter=0;
        
        if(R1_checkCounter==0) {
            //destroy all
            console.log('0 R1_checkCounter');
            emptroy('#tdR2', '#tdR3', '#tdR4', '#tdLC');
        } else if(R1_checkCounter>1) {
            //destroy all child
            console.log('1+ R1_checkCounter: ' + R1_checkCounter);
            emptroy('#tdR2', '#tdR3', '#tdR4', '#tdLC');
            
        } else if(R1_checkCounter==1) {
            //loop region2 to locality
            $('#hidRegion1').val($('input[name="geoRegion1[]"]:checked').val());
            emptiness('#hidRegion2', '#hidRegion3', '#hidRegion4', '#hidLocality');

            console.log('CN: '+CN_checkCounter+'\t'+'CT: '+CT_checkCounter+'\t'+'R1: '+R1_checkCounter+'\t'+'R2: '+R2_checkCounter+'\t'+'R3: '+R3_checkCounter+'\t'+'R4: '+R4_checkCounter+'\t'+'LC: '+LC_checkCounter);
            console.log('CN: '+$('#hidContinent').val()+'\t'+'CT: '+$('#hidCountry').val()+'\t'+'R1: '+$('#hidRegion1').val()+'\t'+'R2: '+$('#hidRegion2').val()+'\t'+'R3: '+$('#hidRegion3').val()+'\t'+'R4: '+$('#hidRegion4').val()+'\t'+'LC: '+$('#hidLocality').val());
            
            ipakita('#spinnery');
            
            $.post('{{ URL::route("getGeoRegion2") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                if(hasValue(data)){
                    console.log('Found');
                    loopend(data, 'region2', '#tdR2', '#spinnery');
                } else {
                    console.log('Not Found');
                    $.post('{{ URL::route("getGeoRegion3") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                        if(hasValue(data)){
                            console.log('Found');
                            loopend(data, 'region3', '#tdR3', '#spinnery');
                        } else {
                            console.log('Not Found');
                            $.post('{{ URL::route("getGeoRegion4") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                                if(hasValue(data)){
                                    console.log('Found');
                                    loopend(data, 'region4', '#tdR4', '#spinnery');
                                } else {
                                    console.log('Not Found');
                                    $.post('{{ URL::route("getGeoLocality") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                                        if(hasValue(data)){
                                            console.log('Found');
                                            loopend(data, 'locality', '#tdLC', '#spinnery');
                                        } else {
                                            console.log('Not Found');
                                            emptroy('#tdLC');
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
    
    
    
    // Check Region2
    $(document.body).on("change", ".chkRegion2", function(){
        $(this).is(":checked") ? R2_checkCounter++ : R2_checkCounter--;
        
        // Set Checkbox counter to zero
        R3_checkCounter=0;R4_checkCounter=0;LC_checkCounter=0;
        
        if(R2_checkCounter==0) {
            //destroy all
            console.log('0 R2_checkCounter');
            emptroy('#tdR3', '#tdR4', '#tdLC');
        } else if(R2_checkCounter>1) {
            //destroy all child
            console.log('1+ R2_checkCounter: ' + R2_checkCounter);
            emptroy('#tdR3', '#tdR4', '#tdLC');
            
        } else if(R2_checkCounter==1) {
            //loop region3 to locality
            $('#hidRegion2').val($('input[name="geoRegion2[]"]:checked').val());
            emptiness('#hidRegion3', '#hidRegion4', '#hidLocality');

            console.log('CN: '+CN_checkCounter+'\t'+'CT: '+CT_checkCounter+'\t'+'R1: '+R1_checkCounter+'\t'+'R2: '+R2_checkCounter+'\t'+'R3: '+R3_checkCounter+'\t'+'R4: '+R4_checkCounter+'\t'+'LC: '+LC_checkCounter);
            console.log('CN: '+$('#hidContinent').val()+'\t'+'CT: '+$('#hidCountry').val()+'\t'+'R1: '+$('#hidRegion1').val()+'\t'+'R2: '+$('#hidRegion2').val()+'\t'+'R3: '+$('#hidRegion3').val()+'\t'+'R4: '+$('#hidRegion4').val()+'\t'+'LC: '+$('#hidLocality').val());
            
            ipakita('#spinnery');
            
            $.post('{{ URL::route("getGeoRegion3") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                if(hasValue(data)){
                    console.log('Found');
                    loopend(data, 'region3', '#tdR3', '#spinnery');
                } else {
                    console.log('Not Found');
                    $.post('{{ URL::route("getGeoRegion4") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                        if(hasValue(data)){
                            console.log('Found');
                            loopend(data, 'region4', '#tdR4', '#spinnery');
                        } else {
                            console.log('Not Found');
                            $.post('{{ URL::route("getGeoLocality") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                                if(hasValue(data)){
                                    console.log('Found');
                                    loopend(data, 'locality', '#tdLC', '#spinnery');
                                } else {
                                    console.log('Not Found');
                                    emptroy('#tdLC');
                                }
                            });
                        }
                    });
                }
            });
        }
    });



    // Check Region3
    $(document.body).on("change", ".chkRegion3", function(){
        $(this).is(":checked") ? R3_checkCounter++ : R3_checkCounter--;
        
        // Set Checkbox counter to zero
        R4_checkCounter=0;LC_checkCounter=0;
        
        if(R3_checkCounter==0) {
            //destroy all
            console.log('0 R3_checkCounter');
            emptroy('#tdR4', '#tdLC');
        } else if(R3_checkCounter>1) {
            //destroy all child
            console.log('1+ R3_checkCounter: ' + R3_checkCounter);
            emptroy('#tdR4', '#tdLC');
            
        } else if(R3_checkCounter==1) {
            //loop region4 to locality
            $('#hidRegion3').val($('input[name="geoRegion3[]"]:checked').val());
            emptiness('#hidRegion4', '#hidLocality');

            console.log('CN: '+CN_checkCounter+'\t'+'CT: '+CT_checkCounter+'\t'+'R1: '+R1_checkCounter+'\t'+'R2: '+R2_checkCounter+'\t'+'R3: '+R3_checkCounter+'\t'+'R4: '+R4_checkCounter+'\t'+'LC: '+LC_checkCounter);
            console.log('CN: '+$('#hidContinent').val()+'\t'+'CT: '+$('#hidCountry').val()+'\t'+'R1: '+$('#hidRegion1').val()+'\t'+'R2: '+$('#hidRegion2').val()+'\t'+'R3: '+$('#hidRegion3').val()+'\t'+'R4: '+$('#hidRegion4').val()+'\t'+'LC: '+$('#hidLocality').val());
            
            ipakita('#spinnery');
            
            $.post('{{ URL::route("getGeoRegion4") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                if(hasValue(data)){
                    console.log('Found');
                    loopend(data, 'region4', '#tdR4', '#spinnery');
                } else {
                    console.log('Not Found');
                    $.post('{{ URL::route("getGeoLocality") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                        if(hasValue(data)){
                            console.log('Found');
                            loopend(data, 'locality', '#tdLC', '#spinnery');
                        } else {
                            console.log('Not Found');
                            emptroy('#tdLC');
                        }
                    });
                }
            });
        }
    });



    // Check Region4
    $(document.body).on("change", ".chkRegion4", function(){
        $(this).is(":checked") ? R4_checkCounter++ : R4_checkCounter--;
        
        // Set Checkbox counter to zero
        LC_checkCounter=0;
        
        if(R4_checkCounter==0) {
            //destroy all
            console.log('0 R4_checkCounter');
            emptroy('#tdLC');
        } else if(R4_checkCounter>1) {
            //destroy all child
            console.log('1+ R4_checkCounter: ' + R4_checkCounter);
            emptroy('#tdLC');
            
        } else if(R4_checkCounter==1) {
            //loop locality
            $('#hidRegion4').val($('input[name="geoRegion4[]"]:checked').val());
            emptiness('#hidLocality');

            console.log('CN: '+CN_checkCounter+'\t'+'CT: '+CT_checkCounter+'\t'+'R1: '+R1_checkCounter+'\t'+'R2: '+R2_checkCounter+'\t'+'R3: '+R3_checkCounter+'\t'+'R4: '+R4_checkCounter+'\t'+'LC: '+LC_checkCounter);
            console.log('CN: '+$('#hidContinent').val()+'\t'+'CT: '+$('#hidCountry').val()+'\t'+'R1: '+$('#hidRegion1').val()+'\t'+'R2: '+$('#hidRegion2').val()+'\t'+'R3: '+$('#hidRegion3').val()+'\t'+'R4: '+$('#hidRegion4').val()+'\t'+'LC: '+$('#hidLocality').val());
            
            ipakita('#spinnery');
            
            $.post('{{ URL::route("getGeoLocality") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                if(hasValue(data)){
                    console.log('Found');
                    loopend(data, 'locality', '#tdLC', '#spinnery');
                } else {
                    console.log('Not Found');
                    emptroy('#tdLC');
                }
            });
        }
    });



    // Check Locality
    $(document.body).on("change", ".chkLocality", function(){
        $(this).is(":checked") ? LC_checkCounter++ : LC_checkCounter--;
        
        if(LC_checkCounter==0) {
            //destroy all
            console.log('0 LC_checkCounter');
        } else if(LC_checkCounter>1) {
            //destroy all child
            console.log('1+ LC_checkCounter: ' + LC_checkCounter);
        } else if(LC_checkCounter==1) {
            //loop locality
            $('#hidLocality').val($('input[name="geoLocality[]"]:checked').val());

            console.log('CN: '+CN_checkCounter+'\t'+'CT: '+CT_checkCounter+'\t'+'R1: '+R1_checkCounter+'\t'+'R2: '+R2_checkCounter+'\t'+'R3: '+R3_checkCounter+'\t'+'R4: '+R4_checkCounter+'\t'+'LC: '+LC_checkCounter);
            console.log('CN: '+$('#hidContinent').val()+'\t'+'CT: '+$('#hidCountry').val()+'\t'+'R1: '+$('#hidRegion1').val()+'\t'+'R2: '+$('#hidRegion2').val()+'\t'+'R3: '+$('#hidRegion3').val()+'\t'+'R4: '+$('#hidRegion4').val()+'\t'+'LC: '+$('#hidLocality').val());
        }
    });
});
</script>
@stop


