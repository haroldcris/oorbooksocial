<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/13/2015
 * Time: 3:49 AM
 *
 */ ?>

 <form id="frmContactUs" action="/contact-us">
    <div class="form-group">
        <label class="control-label col-md-3" >Name</label>
        <div class="input-group col-md-9 date">
            <input type="text" class="form-control" name="name"  />
        </div>
        <span class="help-block col-md-offset-3"></span>
    </div>

    <div class="form-group">
        <label class="control-label col-md-3" >Email Address</label>
        <div class="input-group col-md-9 date">
            <input type="email" class="form-control" name="email"  />
        </div>
        <span class="help-block col-md-offset-3"></span>
    </div>

    <div class="form-group">
        <label class="control-label col-md-3" >Message</label>
        <div class="input-group col-md-9 date">
            <textarea class="form-control" rows="3" name="message"></textarea>
        </div>
        <span class="help-block col-md-offset-3"></span>
    </div>

 </form>