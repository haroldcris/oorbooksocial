<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 01/29/2015
 * Time: 12:11 AM
 *
 */
 ?>

<script>
    function checkLogin(){
        if($('#loginEmail').val() == '' || $('#loginPassword').val() == ''){
            $('#loginStatus').removeClass('hidden').text('You must enter your username or email address and your password to login.');
            return false;
        }

        var _ajax = $.ajax({
                         url: '/accounts/login',
                         type: 'post',format:'jsonp',
                         data: $('#loginform').serialize(),
                         beforeSend: function(){
                             $('#loginBtn').attr('disabled','disabled');
                             $('#loginStatus').removeClass('hidden badge-danger')
                                              .addClass('badge badge-info')
                                              .text('Checking credentials. Please wait...');
                         }});

        _ajax.error (function(res){
            console.log('error encountered');
            console.log(res);

            //d = JSON.parse(res.responseText);
            if(res.status < 500 ){
                error = JSON.parse(res.responseText).message;
            } else{
                error = res.statusText
            }

            $('#loginBtn').removeAttr('disabled');
            $('#loginStatus').removeClass('hidden')
                              .addClass('badge badge-danger')
                              .text(error);
        });

        _ajax.done(function(data){
            d = JSON.parse(data);

            if (d.success){
                location.href = d.url;
                return false;
            }
             $('#loginBtn').removeAttr('disabled');
             $('#loginStatus').removeClass('hidden')
                              .addClass('badge badge-danger')
                              .text(d.message);
        });

        return false;
    }
</script>

