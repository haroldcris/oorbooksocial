<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 01/30/2015
 * Time: 8:21 AM
 *
 */
 ?>

<script>

    function forgotPassword(){
        dialogForgot = new BootstrapDialog({
                                        id: 'dialogForgot',
                                        type: BootstrapDialog.TYPE_PRIMARY,
                                        size: BootstrapDialog.SIZE_NORMAL,
                                        closable:false,
                                        animate:false,
                                        title: 'Forgot Password',
                                        message:'loading...',
                                        buttons:[{label:'Reset My Password', id:'btn-1', cssClass: 'btn btn-info btn3d', action: function(){this.spin().disable(); tryForgot() }},
                                                 {label:'Cancel', cssClass: 'btn btn-danger pull-left btn3d', action: function(dialog){ dialog.close();}}]
                                    });

        $.get("{{URL::route('accountForgot')}}").done(function(data){
            dialogForgot.open();
            dialogForgot.getModalBody().html(data);

            $('#dialogForgot').find('form:first *:input[type!=hidden]:first').focus();
        });
    }

    function tryForgot(){
        //Remove Error Bar
        dialogForgot.getModalBody().find('span:last').html('');

        $.post('{{URL::route('accountForgot')}}', $('#dialogForgot').find('form').serialize())
            .done(function(json){
                data = JSON.parse(json);

                if (data.success != true){
                    $('#dialogForgot').find('form:first *:input[type!=hidden]:first').focus();
                    dialogForgot.getModalBody().find('span:last').html('<div style="margin-top:20px;" class="alert alert-danger">'+data.error+'</div>');
                    dialogForgot.getButton('btn-1').stopSpin().enable();

                    return false
                }

                dialogForgot.close();
                BootstrapDialog.alert('Check your email to reset your password.','Password Reset');
            })

    }
</script>