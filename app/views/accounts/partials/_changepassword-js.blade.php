<script>
    function changePasswordPost() {
        var formContent =

        '<div class="row">' +
	        '<form id="frmChangePassword" action="{{ URL::route("account-change-password-post") }}">' +
	            '<div class="col-xs-12" style="margin-bottom:20px;">' +
	                '<label style="margin-bottom:5px;" for="old_password">Old Password</label><input type="password" id="old_password" name="old_password" class="form-control">' +
	                    '<span class="help-block">' +
	                        '<small id="errPwdOld"></small>' +
	                    '</span>' +
	            '</div>' +

	            '<div class="col-xs-6" style="margin-bottom:20px;">' +
	                '<label style="margin-bottom:5px;" for="new_password">New Password</label><input type="password" id="new_password" name="new_password" class="form-control">' +
	                    '<span class="help-block">' +
	                        '<small id="errPwdNew"></small>' +
	                    '</span>' +
	            '</div>' +

	            '<div class="col-xs-6" style="margin-bottom:15px;">' +
	            '<label style="margin-bottom:5px;" for="retype_password">Retype Password</label><input type="password" id="retype_password" name="retype_password" class="form-control">' +
	                '<span class="help-block">' +
	                    '<small id="errPwdRt"></small>' +
	                '</span>' +  
	            '</div>' +
	        '</form>' +
        '</div>';


        messageBox('dlgChangePassword', formContent, 'Change Password','Change');
    }

    function dlgChangePassword_onButtonClick(dialogRef) {
    }
    
</script>