<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 01/29/2015
 * Time: 1:52 AM
 *
 */
 ?>
 <script>
    var updateSignUpButtonState = function(){
        if( $("#chkTerms").is(':checked') && $("#chkOver18").is(':checked') ){
            $("#signupBtn").removeAttr('disabled');
        } else {
            $("#signupBtn").attr('disabled',true);
        }
    }
     $(function(){
         $("#chkTerms:checkbox").on('change',updateSignUpButtonState );
         $("#chkOver18:checkbox").on('change',updateSignUpButtonState );
     });

     function checkSignup(){
        if (!isValidInputs()){return false}

        $('#signupBtn i').addClass ('fa fa-spin fa-spinner');
        $.ajax({
            type : 'Post',
            url  : '/accounts/create',
            data : $('#signupForm').serialize(),
            error:  function(){
                        grecaptcha.reset(); //Reload Captcha
                        $('#ErrMsg').removeClass('hidden').html('Error:<br>' + data['responseText']);
                    },
            success: function(data){
                    $('#signupBtn i').removeClass ();

                    var d = $.parseJSON(data);
                    if(d.error){
                        grecaptcha.reset(); //Reload Captcha
                        $('#ErrMsg').removeClass('hidden').html('Error:<br>' + d.error).fadeIn('slow');
                    }else{
                        signupComplete($('#signupEmail').val());
                    }
            }
        });

     }

     function isValidInputs(){
        var _error = false;

        var ctrl;
        ctrl = $('#signupUsername');
//        if(!$(ctrl).next('span').hasClass('alert-success')){
//            updateControl(ctrl)
            if ($(ctrl).val()==''){
                _error = true;
                updateControl(ctrl,"Username is required");
            }
//        }

        //email
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        ctrl = $('#signupEmail');
        updateControl(ctrl)
        if ($(ctrl).val()==''){
            _error = true;
            updateControl(ctrl,"Email is required");
        } else if (!re.test($(ctrl).val())){
            _error = true;
            return updateControl(ctrl,"Invalid email address");
        }

        //password
        ctrl = $('#signupPassword');
        updateControl(ctrl)
        if ($(ctrl).val()==''){
            _error = true;
            updateControl(ctrl,"Password is required");
        } else if ($(ctrl).val().length <3){
            _error = true;
            updateControl(ctrl, 'Password must be at least 3 characters');
        }

        //password2
        ctrl = $('#signupPassword2');
        updateControl(ctrl)
        if ($(ctrl).val()!== $('#signupPassword').val()){
            _error = true;
            updateControl(ctrl,"Password do not match");
        }

        return !_error;
     }

     //Use to update error controls
     function updateControl(_ctrl, msg){
        if(msg != undefined){
            $(_ctrl).closest('div.form-group').addClass('has-error');
            $(_ctrl).next('span').html(msg);
        } else  {
            $(_ctrl).closest('div.form-group').removeClass('has-error');
            $(_ctrl).next('span').text('');
        }
        return false
     }

     function signupComplete(e){
     	var	msg = "Kindly check your <strong> email inbox or spam folder </strong> for the activation link from Oorbook.";
     	    msg += "<h5><strong>" + e + "</strong></h5>";
     		msg += "You will not be able to do anything on the site until you successfully activate your account.";

     	$("#SignupPanel #Body").html(msg);

     	$("#SignupPanel").removeClass("panel-primary")
     					 .addClass("panel-success");

     	$("#SignupPanel #Header").html("Signup Completed!<span class='glyphicon glyphicon-ok pull-right' ></span>");

     }

     function checkForDuplicate(ctrl, field){
        //var ctrl = $('#signupUsername');
        if($(ctrl).val() == ''){ return }

        $(ctrl).next('span').removeClass('alert-success').addClass('alert-danger').text('checking...');

        _ajax = $.ajax({url: "{{URL::route('checkForDuplicateRecord')}}?"+ field +"=" + $(ctrl).val()});
        _ajax.done(function(data){
            d = $.parseJSON(data);
            if(d.duplicate == true){
                console.log('duplicate');
                return $(ctrl).next('span').removeClass('alert-success').addClass('alert-danger').text(field +' is already taken');
            }else{
                return $(ctrl).next('span').removeClass('alert-danger').addClass('alert-success').text($(ctrl).val() + ' is available for use');
            }

        });
     }
</script>