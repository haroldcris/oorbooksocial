<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 01/30/2015
 * Time: 11:10 AM
 *
 */
?>
@extends('layouts.default')


@section('pageContent')

 <div class="container">
     <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
         <div class="panel panel-info" >
             <div class="panel-heading">
                 <div class="panel-title">Reset Password</div>
             </div>

             <div class="panel-body" >
                 {{ $errors->first('error', '<span class="help-block">:message</span>') }}
                 <p>Enter your new password</p>
                 <form method="POST" action="{{ URL::route('accountReset')}}" class="form-horizontal" >
                     {{Form::token()}}
                     <input type="hidden" name="resetCode" value="{{$user->activationCode}}" >

                     <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                         <label for="password" class="col-md-4 control-label">Create New Password</label>
                         <div class="col-md-8">
                             <input type="password" name="password" class="form-control" placeholder="Password" autofocus/>
                             {{ $errors->first('password', '<span class="help-block">:message</span>') }}
                         </div>
                     </div>

                     <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                         <label for="password_confirmation" class="col-md-4 control-label">Confirm New Password</label>
                         <div class="col-md-8">
                             <input type="password" name="password_confirmation" class="form-control" placeholder="Re-type Password" />
                             {{ $errors->first('password_confirmation', '<span class="help-block">:message</span>') }}
                         </div>
                     </div>
                     <div class="text-center">
                         <button id="btn-signup" type="submit" class="btn btn-primary">Reset My Password<i class="glyphicon glyphicon-chevron-right"></i></button>
                     </div>
                 </form>
             </div>
         </div>
     </div>
 </div>

@stop
