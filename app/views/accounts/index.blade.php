<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 01/28/2015
 * Time: 5:18 PM
 *
 */

    $colorPanel = SettingsClass::get('colorPanel');

?>

@extends('layouts.default')

@section('styles')
    <style>
    .panel{
        border-color: #{{$colorPanel}};
    }
    .panel-heading{
        background:#{{$colorPanel}};
        color:#000;
    }

    .social-login a{
        border-radius: 100%;
        width: 42px;
        height: 42px;
        line-height: 46px;
        padding: 0;
        margin: 0 1px;
        border-width: 0;
    }

    .btn-mobile {
        width: 150px;
        margin-top:100px;
        margin-left:20px;
    }
    </style>

    <script src='https://www.google.com/recaptcha/api.js'></script>
@stop

@section('pageHeader')
@if(Session::has('message'))
        <div class="alert alert-info">{{Session::get('message')}}</div>
    @endif
	<div class="navbar navbar-default" role="navigation" style="padding: 10px 0px 0px;margin-bottom:5px; background-color:#{{SettingsClass::get('colorNavbar')}}">
		<div class="">
		    <div class="container">
                <div class="navbar-header">
                    {{SettingsClass::get('frontPageTitle')}}
                </div>

                <div class="navbar-collapse collapsed">
                    <form class="navbar-form navbar-right" role="form" id="loginform" method="post" onsubmit="return checkLogin()" novalidate>
                        {{Form::token()}}

                        <div class="form-group">
                            <input id="loginEmail" name="loginEmail" placeholder="Email or Username" class="form-control" type="text" style="min-width:250px" required autofocus autocomplete/>
                            <div class="">&nbsp</div>
                        </div>
                        <div class="form-group">
                            <input id="loginPassword" name="loginPassword" placeholder="Password" class="form-control" type="password" required autocomplete="false">
                            <div><a id="forgotPassword" onclick="return forgotPassword()" style="text-decoration:none;color:white; cursor: pointer;">I forgot my password</a></div>

                        </div>

                        <div class="form-group">
                            <button id="loginBtn" type="submit" class="btn btn-success" >Log in</button>
                            <div>&nbsp;</div>
                        </div>

                        <div>
                            <span id="loginStatus" class="badge badge-danger"></span>
                        </div>

                    </form>

                </div>
            </div>
		</div>
    </div>
@stop

@section('pageContent')    
    <div class="container" style="margin-top: 20px" >
        <div class="row">
            <div class="col-md-8" >
                <div class="panel panel-default">
                    <div class="panel-body" style="padding:5px;background-color:lightyellow">
                        {{ SettingsClass::get('frontPage',true,null,true) }}
                    </div>
                </div>

                <div class="row center">
                    <button class="btn btn-warning btn-mobile" placeholder="Available soon!"><i class="fa fa-apple fa-2x"></i>&nbsp;</button>
                    <button class="btn btn-info btn-mobile" placeholder="Available soon!"><i class="fa fa-android fa-2x"></i>&nbsp;</button>
                </div>
            </div>

            <div class="col-md-4 col-xs-12 container">
                <div class="row">
                    <div class="panel" id="SignupPanel" >
                        <div class="panel-heading" id="Header" ><strong>SIGN UP</strong></div>

                        <div class="panel-body" id="Body">
                            <div class="alert alert-danger hidden" id="ErrMsg"></div>

                            <form id="signupForm" class="form" onsubmit="return false" role="form" novalidate="true">
                                {{Form::token()}}

                                <div class="form-group">
                                    {{--<label for="UserName" class="form-label">Username:</label>--}}{{--Username--}}
                                    <input type="text" id="signupUsername" name="username" class="form-control" placeholder="Username" onblur="checkForDuplicate(this,'username')"
                                        data-toggle="tooltip"
                                        title="Username">
                                    <span class="alert-danger" id="usernameStatus"></span>
                                </div>

                                <div class="form-group">
                                    {{--<!--<label for="Email">Email Address:</label>-->Email Address--}}
                                    <input id="signupEmail" name="email" type="email" class="form-control" placeholder="Email Address" onblur="checkForDuplicate(this,'email')"
                                        data-toggle="tooltip"
                                        title="Email Address">
                                    <span  class="alert-danger" id="signupEmailStatus"></span>
                                </div>

                                <div class="form-group">
                                    {{--<!--<label for="Pwd1">Create Password:</label>-->Password--}}
                                    <input id="signupPassword" name="password" type="password" class="form-control" placeholder="Create Password"
                                        data-toggle="tooltip"
                                        title="Password">
                                    <span  class="alert-danger" id="signupPasswordStatus"></span>
                                </div>

                                <div class="form-group">
                                    {{--<!--<label for="Pwd1">Re-enter Password:</label>-->Re-enter Password--}}
                                    <input id="signupPassword2" name="password_confirmation" type="password" class="form-control" placeholder="Re-enter Password"
                                        data-toggle="tooltip"
                                        title="Re-enter Password">
                                    <span  class="alert-danger" id="password2Status"></span>
                                </div>

                                <div class="form-group">
                                    <label style="cursor: pointer;font-weight: normal"><input type="checkbox" name="checkbox" id="chkTerms"> Accept Terms and Conditions</label>  <button class="btn btn-xs btn-info" onclick="return pop_up('/eula', 'Pop Up')">View Terms</button>
                                    <label style="cursor: pointer;font-weight: normal"><input type="checkbox" name="checkbox" id="chkOver18"> I'm over 13 years old</label>
                                </div>

                                <div class="form-group">
                                    <div class="g-recaptcha" data-sitekey="6LcQrf8SAAAAAFHAH4RgDBo3chQ2GMDNAc-6uZbg"></div>
                                </div>

                                <div class="form-group">

                                    <button id="signupBtn" class="btn btn-success btn-block btn-lg" onclick="checkSignup()" disabled="true"><i></i> Create Account </button>

                                </div>

                            </form>

                            <div class="social-or-login center">
                                <hr>
                                <span><b>Or Login Using</b></span>
                            </div>

                            <div class="social-login center">
                                <a class="btn btn-primary" href="/social/facebook/auth">
                                    <i class="ace-icon fa fa-facebook fa-2x"></i>
                                </a>

                                <a class="btn btn-info" href="/social/twitter/auth">
                                    <i class="ace-icon fa fa-twitter fa-2x"></i>
                                </a>

                                <a class="btn btn-danger" href="/social/google/auth">
                                    <i class="ace-icon fa fa-google-plus fa-2x"></i>
                                </a>
                            </div>

                        </div>
                    </div>


                        <div>
                            <button class="btn btn-info btn-sm" onclick="contactUs()">Contact Oorbook</button>
                            <button class="btn btn-info btn-sm pull-right" onclick="return pop_up('/help', 'Pop Up')">Help</button>
                        </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('scripts')
    @include ('layouts.partials._bootstrapDialog-js')
    @include ('layouts.partials._postData-js')

    @include ('accounts.partials._login-js')
    @include ('accounts.partials._signup-js')
    @include ('accounts.partials._forgotpassword-js')


    <script src='/controls/bootbox/bootbox.min.js'></script>

    <script>
        $(function(){
            $('[data-toggle="tooltip"]').tooltip({
                'placement': 'left'
            });

            $('.btn-mobile').popover({
                 title: '',
                 content: 'Available Soon!',
                 animation: true,
                 placement:'top',
                 html:true,
                 trigger:'hover',
                 container:'body'
             });

            //$('[data-toggle="tooltip"]').tooltip('show');
        });
    </script>

    <script>
        function contactUs()
        {
            messageBoxRemote('contactDialog','/contact-us','Contact Us','Send');
        }

        function contactDialog_onButtonClick(dialogRef){
            postMyData ('frmContactUs', dialogRef).then(function(){
                dialogRef.close();
            });
        }
    </script>



@stop