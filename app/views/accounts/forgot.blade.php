<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 01/30/2015
 * Time: 9:24 AM
 *
 */?>

<form id='formForgot' method="post" action="{{URL::route('accountForgot')}}" onsubmit="return false">
    {{Form::token()}}
    <div class="form-group">
        <label for="email" class="form-label">Enter your email address to reset your password:</label>
        <span class="block input-icon input-icon-right">
        <input type="text" name="email" class="form-control">
        <input type="hidden" name="dummy" value="dummy" />
        </span>
    </div>
</form>
<span></span>

