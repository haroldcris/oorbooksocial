<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/22/2015
 * Time: 12:52 PM
 *
 */

 ?>

 <div class="panel">
    <form id="frmCreateNotice" action="{{URL::route('storeNotice')}}">

    <div class="form-group">
        <input type="text" class="form-control" placeholder="Enter API Key" name="apikey">
    </div>
    <div class="form-group">
        <input type="text" class="form-control" placeholder = "Title" name="title">
    </div>
    <div class="form-group">
        <input type="text" class="form-control" placeholder = "URL Link for this Announcement (optional)" name="link">
    </div>
    <div class="form-group">
        <textarea class="form-control" rows="5" placeholder="Enter your announcement" name="description"></textarea>
    </div>
    </form>
 </div>
