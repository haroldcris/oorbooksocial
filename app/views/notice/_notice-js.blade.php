<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/22/2015
 * Time: 2:30 PM
 *
 */ ?>


 <script>
    function createAnnouncement(){
        var dialog = messageBox ( 'noticeDialog', '{{ Config::get('site.loader'); }}' ,'Announcement', 'Create' );

        $.get("{{URL::route('createNotice')}}")
            .error(function(error){ console.log(error); alert('Error occurred while fetching Announcement Form'); })
            .done(function(frm){
                dialog.getModalBody().html(frm);
            });
    }

    function noticeDialog_onButtonClick(dialogRef){
        postMyData('frmCreateNotice', dialogRef).then(function(){
            dialogRef.close();
            location.reload();
        });
    }

 </script>

 <script>
    //Delete
    $('.deleteNoticeBtn').click(function(){
        var _html,
            title,
            noticeId = $(this).closest('tr').attr('data-id');

        $('.deleteNoticeBtn').closest('tr').removeClass('active');
        $(this).closest('tr').addClass('active');

        title = $(this).closest('tr').find('.notice-title').text();

        _html =  'You are about to delete the following API Key.<br><br>';
        _html += '<div><table class="table table-bordered "><tbody><tr><td>' + title + '</td></tr><tbody></table></div>';
        _html += 'Do you want to continue?';

        _html += '<form id="frmDeleteNotice" action="{{URL::route('deleteNotice')}}"><input type="hidden" name="noticeId" value="' + noticeId + '" /> </form>';

        messageBox('deleteNotice', _html ,'Confirmation','Delete Notice', '', BootstrapDialog.TYPE_DANGER );
    });

    function deleteNotice_onButtonClick(dialogRef)
    {
        postMyData('frmDeleteNotice', dialogRef).then (
            function()
            {
                $('#noticeTable tr.active').remove();
                dialogRef.close();
            }
        );
    }
 </script>
