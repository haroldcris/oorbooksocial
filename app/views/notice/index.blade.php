<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/10/2015
 * Time: 7:13 PM
 *
 */
?>

@extends('layouts.main')

@section('pageContent')
    <div class="widget-box">
        <div class="widget-header">
            <h5 class="widget-title bigger lighter">
                <i class="ace-icon fa fa-table"></i>
                Manage Announcements
            </h5>
            <div class="widget-toolbar no-border">
                <button class="btn btn-round btn-white btn-success btn-xs " onclick="createAnnouncement()" >&nbsp;&nbsp;<i class="fa fa-plus"></i> Create New&nbsp;&nbsp;</button>
            </div>
        </div>

        <div class="widget-body">
            <div class="widget-main no-padding">
                <table id="noticeTable" class="table table-striped table-bordered table-hover">
                    <tbody>
                        @foreach($notices as $item)
                            <tr data-id="{{$item->noticeId}}">
                                <td >
                                    <div>
                                        <span class="notice-title" style="font-weight: bold">{{$item->title}}</span>
                                    </div>
                                    <div>
                                        <span class="location label label-sm label-info arrowed-right" >{{$item->link}}</span> <small>Level: {{$item->scope}}</small>
                                    </div>
                                    <div>
                                        {{$item->description}}
                                    </div>

                                </td >

                                <td class="">
                                    <div class="btn-group">
                                        {{--<button class="btn btn-xs btn-success btn-white btn-round editBtn" onclick="editApiCredential({{$item->apiCredentialId}})" >--}}
                                            {{--<i class="ace-icon fa fa-edit green"></i>--}}
                                            {{--Modify--}}
                                        {{--</button>--}}

                                        <button class="btn btn-xs btn-danger btn-white btn-round deleteNoticeBtn">
                                            <i class="ace-icon fa fa-times bigger-110 red2"></i>
                                            Delete
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    <div>
        {{$notices->links()}}
    </div>
@stop
