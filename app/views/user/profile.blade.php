<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 01/29/2015
 * Time: 9:08 PM
 *
 */
    $followStatus = $follow;
        if(isset($iAmFollowingThisUser)){
            if($iAmFollowingThisUser){
                $followStatus = $unfollow;
            }
        }
?>

@extends('layouts.main')

@section('title')
- Profile
@stop

@section('styles')

    <style>
        .pageProfile{
            background-color:white;
            padding:10px;
            margin:2px;
        }

        .profile-info-value{
            min-height: 32px;
        }

        .profile-contact-info{
            background-color:#d73d32 !important;
            padding:5px;
        }

        .profile-contact-info a {
            color: #fff !important;
        }

        .profile-picture:hover a{
            display: block !important;
            position: absolute;
            cursor: pointer;
        }



        .xcontainer {
            /*position: relative;
            top: 10%; left: 10%; right: 0; bottom: 0;*/
            margin: 0 87px;
        }
        .xaction {
            width: 400px;
            height: 30px;
            margin: 10px 0;
        }
        .xcropped>img {
            margin-right: 10px;
        }
        .ximageBox {
            position: relative; 
            height: 400px; 
            width: 400px; 
            border: 1px solid #aaa; 
            background: #fff; 
            overflow: hidden; 
            background-repeat: no-repeat; 
            cursor: move;
        }
        .ximageBox .xthumbBox {
            position: absolute; 
            top: 50%; 
            left: 50%; 
            width: 200px; 
            height: 200px; 
            margin-top: -100px; 
            margin-left: -100px; 
            box-sizing: border-box; 
            border: 1px solid rgb(102, 102, 102); 
            box-shadow: 0 0 0 1000px rgba(0, 0, 0, 0.5); 
            background: none repeat scroll 0% 0% transparent;
        }
        .ximageBox .xspinner {
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            text-align: center;
            line-height: 400px;
            background: rgba(0,0,0,0.7);
        }
    </style>
    
@stop

@section('pageContent')
    <script>
    function getCropian() {
            var options =
            {
                imageBox: '.ximageBox',
                thumbBox: '.xthumbBox',
                spinner: '.xspinner',
                imgSrc: 'avatar.png'
            }
            var cropper;
            //document.querySelector('#file-input').addEventListener('change', function(){
            //Convert above Code to Jquery Style
            $('body').on('change','#file-input', function() {
                var reader = new FileReader();
                reader.onload = function(e) {
                    options.imgSrc = e.target.result;
                    cropper = new cropbox(options);
                }
                reader.readAsDataURL(this.files[0]);
                this.files = [];
            });

            //document.querySelector('#btnCrop').addEventListener('click', function(){
            //Convert above Code to Jquery Style
            $('body').on('click','#btnCrop', function() {
                var img = cropper.getDataURL()
                // document.querySelector('.xcropped').innerHTML += '<img src="' + img + '">';
                document.querySelector('#msg-crop').innerHTML = 'Your profile picture was successfully cropped';
                $("#msg-crop").fadeOut("fast");
                $("#msg-crop").fadeIn();
                document.querySelector('#cropped-file').value = img;
            });


            //document.querySelector('#btnZoomIn').addEventListener('click', function(){
            //Convert above Code to Jquery Style
            $('body').on('click','#btnZoomIn', function() { 
                cropper.zoomIn();
            });


            //document.querySelector('#btnZoomOut').addEventListener('click', function(){
            //Convert above Code to Jquery Style
            $('body').on('click','#btnZoomOut', function() {
                cropper.zoomOut();
            })
        
        'use strict';
        var cropbox = function(options){
            var el = document.querySelector(options.imageBox),
            obj =
            {
                state : {},
                ratio : 1,
                options : options,
                imageBox : el,
                thumbBox : el.querySelector(options.thumbBox),
                spinner : el.querySelector(options.spinner),
                image : new Image(),
                getDataURL: function ()
                {
                    var width = this.thumbBox.clientWidth,
                        height = this.thumbBox.clientHeight,
                        canvas = document.createElement("canvas"),
                        dim = el.style.backgroundPosition.split(' '),
                        size = el.style.backgroundSize.split(' '),
                        dx = parseInt(dim[0]) - el.clientWidth/2 + width/2,
                        dy = parseInt(dim[1]) - el.clientHeight/2 + height/2,
                        dw = parseInt(size[0]),
                        dh = parseInt(size[1]),
                        sh = parseInt(this.image.height),
                        sw = parseInt(this.image.width);

                    canvas.width = width;
                    canvas.height = height;
                    var context = canvas.getContext("2d");
                    context.drawImage(this.image, 0, 0, sw, sh, dx, dy, dw, dh);
                    var imageData = canvas.toDataURL('image/png');
                    return imageData;
                },
                getBlob: function()
                {
                    var imageData = this.getDataURL();
                    var b64 = imageData.replace('data:image/png;base64,','');
                    var binary = atob(b64);
                    var array = [];
                    for (var i = 0; i < binary.length; i++) {
                        array.push(binary.charCodeAt(i));
                    }
                    return  new Blob([new Uint8Array(array)], {type: 'image/png'});
                },
                zoomIn: function ()
                {
                    this.ratio*=1.1;
                    setBackground();
                },
                zoomOut: function ()
                {
                    this.ratio*=0.9;
                    setBackground();
                }
            },
            attachEvent = function(node, event, cb)
            {
                if (node.attachEvent)
                    node.attachEvent('on'+event, cb);
                else if (node.addEventListener)
                    node.addEventListener(event, cb);
            },
            detachEvent = function(node, event, cb)
            {
                if(node.detachEvent) {
                    node.detachEvent('on'+event, cb);
                }
                else if(node.removeEventListener) {
                    node.removeEventListener(event, render);
                }
            },
            stopEvent = function (e) {
                if(window.event) e.cancelBubble = true;
                else e.stopImmediatePropagation();
            },
            setBackground = function()
            {
                var w =  parseInt(obj.image.width)*obj.ratio;
                var h =  parseInt(obj.image.height)*obj.ratio;

                var pw = (el.clientWidth - w) / 2;
                var ph = (el.clientHeight - h) / 2;

                el.setAttribute('style',
                        'background-image: url(' + obj.image.src + '); ' +
                        'background-size: ' + w +'px ' + h + 'px; ' +
                        'background-position: ' + pw + 'px ' + ph + 'px; ' +
                        'background-repeat: no-repeat');
            },
            imgMouseDown = function(e)
            {
                stopEvent(e);

                obj.state.dragable = true;
                obj.state.mouseX = e.clientX;
                obj.state.mouseY = e.clientY;
            },
            imgMouseMove = function(e)
            {
                stopEvent(e);

                if (obj.state.dragable)
                {
                    var x = e.clientX - obj.state.mouseX;
                    var y = e.clientY - obj.state.mouseY;

                    var bg = el.style.backgroundPosition.split(' ');

                    var bgX = x + parseInt(bg[0]);
                    var bgY = y + parseInt(bg[1]);

                    el.style.backgroundPosition = bgX +'px ' + bgY + 'px';

                    obj.state.mouseX = e.clientX;
                    obj.state.mouseY = e.clientY;
                }
            },
            imgMouseUp = function(e)
            {
                stopEvent(e);
                obj.state.dragable = false;
            },
            zoomImage = function(e)
            {
                var evt=window.event || e;
                var delta=evt.detail? evt.detail*(-120) : evt.wheelDelta;
                delta > -120 ? obj.ratio*=1.1 : obj.ratio*=0.9;
                setBackground();
            }

            obj.spinner.style.display = 'block';
            obj.image.onload = function() {
                obj.spinner.style.display = 'none';
                setBackground();

                attachEvent(el, 'mousedown', imgMouseDown);
                attachEvent(el, 'mousemove', imgMouseMove);
                attachEvent(document.body, 'mouseup', imgMouseUp);
                var mousewheel = (/Firefox/i.test(navigator.userAgent))? 'DOMMouseScroll' : 'mousewheel';
                attachEvent(el, mousewheel, zoomImage);
            };
            obj.image.src = options.imgSrc;
            attachEvent(el, 'DOMNodeRemoved', function(){detachEvent(document.body, 'DOMNodeRemoved', imgMouseUp)});

            return obj;
        };
    }

    $(function(){
        getCropian();
    });
    </script>



    @if (!is_null($selectedLocation))
        @include('layouts.partials.breadcrumbs')
    @endif

    <div class="row pageProfile" style="margin-top: 30px">

        <div id="user-profile-1" class="user-profile row">

            <div class="col-xs-12">

                <div class="tabbable">
                    <ul id="myTab" class="nav nav-tabs">
                            <li class="active"><a href="#profile-tab1" data-toggle="tab"><i class="ace-icon fa fa-user bigger-120"></i> User Profile</a></li>
                            {{--<li><a href="#profile-tab2" data-toggle="tab">Messages<span class="badge badge-danger">4</span></a></li>--}}
                    </ul>

                    <div class="tab-content">

                        <div class="tab-pane fade in active" id="profile-tab1">
                            <div class="row">
                                 @if( $isOwner )
                                    <div class="alert alert-warning" style="margin-left:20px;margin-right:20px">
                                        <button data-dismiss="alert" class="close" type="button">
                                            <i class="ace-icon fa fa-times"></i>
                                        </button>
                                        <i>Click on the image below or on profile fields to edit them</i>
                                    </div>
                                @endif
                                <!--Profile Picture-->
                                <div class="col-md-3 center">
                                    <div class="profile-picture">
                                        <img id="avatar" src="{{ URL::route('userAvatar', $user->userId) }}" class="avatar img-responsive editable-click " alt="Avatar" style="width: 130px; height: 130px;">
                                        @if( $isOwner )
                                            <a href="javascript:void(0)" onclick="updateAvatar()" style="display:none;top: 5%; left: 20%;" class="btn btn-minier btn-white"><i class="green fa fa-edit bigger-150"></i></a>
                                            <a href="javascript:void(0)" onclick="deleteAvatar()" style="display:none;top: 5%; right: 20%;" class="btn btn-minier btn-white"><i class="red fa fa-trash-o bigger-150"></i></a>
                                        @endif
                                    </div>

                                    <div class="space-4"></div>

                                    <!--Profile Name Label-->
                                    <div class="width-100 label label-info label-xlg ">
                                        <div class="inline position-relative">
                                            <a href="#" class="user-title-label dropdown-toggle" data-toggle="dropdown">
                                                <span class="white">
                                                   {{$user->username}}
                                                </span>&nbsp;&nbsp;
                                                {{--<i class="caret white"></i>--}}
                                            </a>
                                        </div>
                                    </div>

                                    <div class="space-10"></div>

                                    <a href="/followers/{{$user->username}}"><div class=" width-100 btn btn-xs btn-info no-hover" style="margin: 1px 0px;"><b>{{ $followersCount . ($followersCount > 1 ? ' followers': ' follower') }} </b></div></a>

                                    {{-- /followings/{{$user->username}} --}}
                                    <a href="/followings/{{$user->username}}"><div class=" width-100 btn btn-xs btn-info no-hover" style="margin: 1px 0px;"><b>{{ $followingsCount . ($followingsCount > 1 ? ' followings': ' following') }} </b></div></a>
                                    
                                    <div class="space-5"></div>

                                    @if ( !$viewAsPublic  && !$isOwner )
                                        <div class="profile-contact-info" style="margin: 1px 0px;">

                                                <a id="followUserBtn" href="javascript:void(0)" onclick="return followUser();" data-id="{{$user->userId}}">{{$followStatus}}</a>

                                                {{--<a class="btn btn-link" href="#">--}}
                                                    {{--<i class="ace-icon fa fa-envelope bigger-120 pink"></i>--}}
                                                    {{--Send a message--}}
                                                {{--</a>--}}
                                        </div>
                                    @endif

                                    {{--<div class="profile-contact-info">--}}
                                        {{--<div class="profile-contact-links align-left">--}}
                                            {{--<ul class="nav nav-list">--}}
                                                {{--<li> <a id="ProfilePic" href="javascript:void(0)"><i class="fa fa-plus"></i> Upload Avatar</a></li>--}}
                                                {{--<li><a id="ProfilePicDelete" href="javascript:void(0)" ><i class="fa fa-trash-o"></i> Remove Avatar</a></li>--}}
                                                {{--<li></li>--}}
                                                {{--<li><a id="Wallpaper" href="javascript:void(0)"><i class="fa fa-plus"></i> Upload Wallpaper</a></li>--}}
                                                {{--<li><a id="WallpaperDelete" href="javascript:void(0)"><i class="fa fa-trash-o"></i> Remove Wallpaper</a></li>--}}
                                            {{--</ul>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                </div>

                                <div class="col-md-9">

                                    <div class="profile-user-info profile-user-info-striped">
                                        <div class="profile-info-row">
                                            <div class="profile-info-name">Username</div>

                                            <div class="profile-info-value">
                                                <span class="" id="username">
                                                    <strong>{{$user->username}}</strong>
                                                </span>
                                            </div>
                                        </div>

                                        <!--Date of Birth-->
                                        @if($isOwner)
                                            <div class="profile-info-row">
                                                <div class="profile-info-name">
                                                    Date of Birth
                                                </div>

                                                <div class="profile-info-value">
                                                    <span class="editable editable-click" id="birthdate">
                                                        {{date("M d, Y",strtotime($user->birthdate)) }}
                                                    </span>
                                                </div>
                                            </div>
                                        @else
                                            <div class="profile-info-row">
                                                <div class="profile-info-name">
                                                    Location
                                                </div>

                                                <div class="profile-info-value">
                                                    <span class="editable editable-click" id="birthdate">
                                                        @if(!is_null($guestLocation))
                                                            {{$guestLocation->name}}
                                                        @endif
                                                    </span>
                                                </div>
                                            </div>
                                        @endif





                                            <!--Sex-->
                                            <div class="profile-info-row">
                                                <div class="profile-info-name">
                                                    Sex
                                                </div>

                                                <div class="profile-info-value">
                                                    <span class="editable editable-click" id="sex">
                                                        {{$user->sex}}
                                                    </span>
                                                </div>
                                            </div>

                                            <!--Education -->
                                            <div class="profile-info-row">
                                                <div class="profile-info-name">
                                                    Education
                                                </div>

                                                <div class="profile-info-value">
                                                    <span class="editable editable-click" id="education">
                                                        {{$user->education}}
                                                    </span>
                                                </div>
                                            </div>

                                            <!--Occupation-->
                                            <div class="profile-info-row">
                                                <div class="profile-info-name">
                                                    Occupation
                                                </div>

                                                <div class="profile-info-value">
                                                    <span class="editable editable-click" id="occupation">
                                                        {{$user->occupation}}
                                                    </span>
                                                </div>
                                            </div>

                                            <!--Field-->
                                            <div class="profile-info-row">
                                                <div class="profile-info-name">
                                                    Field
                                                </div>

                                                <div class="profile-info-value">
                                                    <span class="editable editable-click" id="field">
                                                        {{$user->field}}
                                                    </span>
                                                </div>
                                            </div>

                                            <!--Place of Birth-->
                                            <div class="profile-info-row">
                                                <div class="profile-info-name">Place of Birth</div>

                                                <div class="profile-info-value">
                                                    <i class="fa fa-map-marker light-orange bigger-110"></i>
                                                    <span class="editable editable-click" id="birthPlace">
                                                        {{$user->birthPlace}}
                                                    </span>
                                                </div>
                                            </div>

                                            <!--Country-->
                                            <div class="profile-info-row">
                                                <div class="profile-info-name">Country Living</div>

                                                <div class="profile-info-value">
                                                    <i class="fa fa-map-marker light-orange bigger-110"></i>
                                                    <span class="editable editable-click" id="country">
                                                        {{$user->country}}
                                                    </span>
                                                </div>
                                            </div>


                                            <!--Marital Status-->
                                            <div class="profile-info-row">
                                                <div class="profile-info-name">
                                                    Marital Status
                                                </div>

                                                <div class="profile-info-value">
                                                    <span class="editable editable-click" id="maritalStatus">
                                                        {{$user->maritalStatus}}
                                                    </span>
                                                </div>
                                            </div>

                                            <!--Sexual Orientation-->
                                            <div class="profile-info-row">
                                                <div class="profile-info-name">
                                                    Sexual Orientation
                                                </div>

                                                <div class="profile-info-value">
                                                    <span class="editable editable-click" id="sexOrientation">
                                                        {{$user->sexOrientation}}
                                                    </span>
                                                </div>
                                            </div>

                                            <!--Number of Children-->
                                            <div class="profile-info-row">
                                                <div class="profile-info-name">
                                                    Children
                                                </div>

                                                <div class="profile-info-value">
                                                    <span class="editable editable-click" id="childrenCount">
                                                        {{$user->childrenCount}}
                                                    </span>
                                                </div>
                                            </div>


                                            <!--Religion-->
                                            <div class="profile-info-row">
                                                <div class="profile-info-name">
                                                    Religion
                                                </div>

                                                <div class="profile-info-value">
                                                    <span class="editable editable-click" id="religion">
                                                        {{$user->religion}}
                                                    </span>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="profile-tab2">
                            <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid.</p>
                        </div>
                    </div>
            </div>

            <div class="space-20"></div>

        </div>
        </div>
    </div>
@stop


@section('scripts')

    @include('layouts.partials._editable-js')


    @if( $isOwner )
        @include('layouts.partials._postFile-js')

        <script src="/packages/moment/moment.js"></script>

        <script>

            $.fn.editableform.buttons = '<button type="submit" class="btn btn-round btn-success editable-submit"><i class="ace-icon fa fa-check"></i></button>'+
                                        '<button type="button" class="btn btn-round editable-cancel"><i class="ace-icon fa fa-times"></i></button>';

            var _token = '{{csrf_token()}}';

            $('#birthdate').editable({
                pk:1,
                type: 'combodate',
                placement: 'bottom',
                title: 'Enter your Date of Birth',
                combodate: {minYear: 1900},
                format: 'YYYY-MM-DD',
                viewformat: 'MMMM DD, YYYY',
                template: 'MMMM / DD / YYYY',
                value: '{{$user->birthdate}}',
                url: '/profile',
                params: function(params) {
                    params._token = _token;
                    return params;
                  }
            });

            $('#sex').editable({
                pk:1,
                type: 'select',
                placement: 'bottom',
                title: 'Select your Sex',
                source: [
                    {value: 'male', text: 'Male'},
                    {value: 'female', text: 'Female'},
                    {value: '3rd gender', text: '3rd gender'}
                ],
                value: '{{$user->sex}}',
                url:'/profile',
                params: function(params) {
                    params._token = _token;
                    return params;
                  }
            });

            $('#education').editable({
                pk:1,
                type: 'text',
                placement: 'bottom',
                title: 'Enter your education',
                url:'/profile',
                params: function(params) {
                    params._token = _token;
                    return params;
                  }
            });

            $('#occupation').editable({
                pk:1,
                type: 'text',
                placement: 'bottom',
                title: 'Enter your occupation',
                url:'/profile',
                params: function(params) {
                    params._token = _token;
                    return params;
                  }
            });

            $('#field').editable({
                pk:1,
                type: 'text',
                placement: 'bottom',
                title: 'Enter your field',
                url:'/profile',
                params: function(params) {
                    params._token = _token;
                    return params;
                  }
            });

            $('#birthPlace').editable({
                pk:1,
                type: 'text',
                placement: 'bottom',
                title: 'Enter your field',
                url:'/profile',
                params: function(params) {
                    params._token = _token;
                    return params;
                  }
            });

            //$countries = [{"id":"Afganistan", "text": "Afganistan"}, {"id":"Albania", "text": "Albania"}, {"id":"Algeria", "text": "Algeria"}, {"id":"American Samoa", "text": "American Samoa"}, {"id":"Andorra", "text": "Andorra"}, {"id":"Angola", "text": "Angola"}, {"id":"Anguilla", "text": "Anguilla"}, {"id":"Antigua & Barbuda", "text": "Antigua & Barbuda"}, {"id":"Argentina", "text": "Argentina"}, {"id":"Armenia", "text": "Armenia"}, {"id":"Aruba", "text": "Aruba"}, {"id":"Australia", "text": "Australia"}, {"id":"Austria", "text": "Austria"}, {"id":"Azerbaijan", "text": "Azerbaijan"}, {"id":"Bahamas", "text": "Bahamas"}, {"id":"Bahrain", "text": "Bahrain"}, {"id":"Bangladesh", "text": "Bangladesh"}, {"id":"Barbados", "text": "Barbados"}, {"id":"Belarus", "text": "Belarus"}, {"id":"Belgium", "text": "Belgium"}, {"id":"Belize", "text": "Belize"}, {"id":"Benin", "text": "Benin"}, {"id":"Bermuda", "text": "Bermuda"}, {"id":"Bhutan", "text": "Bhutan"}, {"id":"Bolivia", "text": "Bolivia"}, {"id":"Bonaire", "text": "Bonaire"}, {"id":"Bosnia & Herzegovina", "text": "Bosnia & Herzegovina"}, {"id":"Botswana", "text": "Botswana"}, {"id":"Brazil", "text": "Brazil"}, {"id":"British Indian Ocean Ter", "text": "British Indian Ocean Ter"}, {"id":"Brunei", "text": "Brunei"}, {"id":"Bulgaria", "text": "Bulgaria"}, {"id":"Burkina Faso", "text": "Burkina Faso"}, {"id":"Burundi", "text": "Burundi"}, {"id":"Cambodia", "text": "Cambodia"}, {"id":"Cameroon", "text": "Cameroon"}, {"id":"Canada", "text": "Canada"}, {"id":"Canary Islands", "text": "Canary Islands"}, {"id":"Cape Verde", "text": "Cape Verde"}, {"id":"Cayman Islands", "text": "Cayman Islands"}, {"id":"Central African Republic", "text": "Central African Republic"}, {"id":"Chad", "text": "Chad"}, {"id":"Channel Islands", "text": "Channel Islands"}, {"id":"Chile", "text": "Chile"}, {"id":"China", "text": "China"}, {"id":"Christmas Island", "text": "Christmas Island"}, {"id":"Cocos Island", "text": "Cocos Island"}, {"id":"Colombia", "text": "Colombia"}, {"id":"Comoros", "text": "Comoros"}, {"id":"Congo", "text": "Congo"}, {"id":"Cook Islands", "text": "Cook Islands"}, {"id":"Costa Rica", "text": "Costa Rica"}, {"id":"Cote DIvoire", "text": "Cote DIvoire"}, {"id":"Croatia", "text": "Croatia"}, {"id":"Cuba", "text": "Cuba"}, {"id":"Curaco", "text": "Curaco"}, {"id":"Cyprus", "text": "Cyprus"}, {"id":"Czech Republic", "text": "Czech Republic"}, {"id":"Denmark", "text": "Denmark"}, {"id":"Djibouti", "text": "Djibouti"}, {"id":"Dominica", "text": "Dominica"}, {"id":"Dominican Republic", "text": "Dominican Republic"}, {"id":"East Timor", "text": "East Timor"}, {"id":"Ecuador", "text": "Ecuador"}, {"id":"Egypt", "text": "Egypt"}, {"id":"El Salvador", "text": "El Salvador"}, {"id":"Equatorial Guinea", "text": "Equatorial Guinea"}, {"id":"Eritrea", "text": "Eritrea"}, {"id":"Estonia", "text": "Estonia"}, {"id":"Ethiopia", "text": "Ethiopia"}, {"id":"Falkland Islands", "text": "Falkland Islands"}, {"id":"Faroe Islands", "text": "Faroe Islands"}, {"id":"Fiji", "text": "Fiji"}, {"id":"Finland", "text": "Finland"}, {"id":"France", "text": "France"}, {"id":"French Guiana", "text": "French Guiana"}, {"id":"French Polynesia", "text": "French Polynesia"}, {"id":"French Southern Ter", "text": "French Southern Ter"}, {"id":"Gabon", "text": "Gabon"}, {"id":"Gambia", "text": "Gambia"}, {"id":"Georgia", "text": "Georgia"}, {"id":"Germany", "text": "Germany"}, {"id":"Ghana", "text": "Ghana"}, {"id":"Gibraltar", "text": "Gibraltar"}, {"id":"Great Britain", "text": "Great Britain"}, {"id":"Greece", "text": "Greece"}, {"id":"Greenland", "text": "Greenland"}, {"id":"Grenada", "text": "Grenada"}, {"id":"Guadeloupe", "text": "Guadeloupe"}, {"id":"Guam", "text": "Guam"}, {"id":"Guatemala", "text": "Guatemala"}, {"id":"Guinea", "text": "Guinea"}, {"id":"Guyana", "text": "Guyana"}, {"id":"Haiti", "text": "Haiti"}, {"id":"Hawaii", "text": "Hawaii"}, {"id":"Honduras", "text": "Honduras"}, {"id":"Hong Kong", "text": "Hong Kong"}, {"id":"Hungary", "text": "Hungary"}, {"id":"Iceland", "text": "Iceland"}, {"id":"India", "text": "India"}, {"id":"Indonesia", "text": "Indonesia"}, {"id":"Iran", "text": "Iran"}, {"id":"Iraq", "text": "Iraq"}, {"id":"Ireland", "text": "Ireland"}, {"id":"Isle of Man", "text": "Isle of Man"}, {"id":"Israel", "text": "Israel"}, {"id":"Italy", "text": "Italy"}, {"id":"Jamaica", "text": "Jamaica"}, {"id":"Japan", "text": "Japan"}, {"id":"Jordan", "text": "Jordan"}, {"id":"Kazakhstan", "text": "Kazakhstan"}, {"id":"Kenya", "text": "Kenya"}, {"id":"Kiribati", "text": "Kiribati"}, {"id":"Korea North", "text": "Korea North"}, {"id":"Korea Sout", "text": "Korea Sout"}, {"id":"Kuwait", "text": "Kuwait"}, {"id":"Kyrgyzstan", "text": "Kyrgyzstan"}, {"id":"Laos", "text": "Laos"}, {"id":"Latvia", "text": "Latvia"}, {"id":"Lebanon", "text": "Lebanon"}, {"id":"Lesotho", "text": "Lesotho"}, {"id":"Liberia", "text": "Liberia"}, {"id":"Libya", "text": "Libya"}, {"id":"Liechtenstein", "text": "Liechtenstein"}, {"id":"Lithuania", "text": "Lithuania"}, {"id":"Luxembourg", "text": "Luxembourg"}, {"id":"Macau", "text": "Macau"}, {"id":"Macedonia", "text": "Macedonia"}, {"id":"Madagascar", "text": "Madagascar"}, {"id":"Malaysia", "text": "Malaysia"}, {"id":"Malawi", "text": "Malawi"}, {"id":"Maldives", "text": "Maldives"}, {"id":"Mali", "text": "Mali"}, {"id":"Malta", "text": "Malta"}, {"id":"Marshall Islands", "text": "Marshall Islands"}, {"id":"Martinique", "text": "Martinique"}, {"id":"Mauritania", "text": "Mauritania"}, {"id":"Mauritius", "text": "Mauritius"}, {"id":"Mayotte", "text": "Mayotte"}, {"id":"Mexico", "text": "Mexico"}, {"id":"Midway Islands", "text": "Midway Islands"}, {"id":"Moldova", "text": "Moldova"}, {"id":"Monaco", "text": "Monaco"}, {"id":"Mongolia", "text": "Mongolia"}, {"id":"Montserrat", "text": "Montserrat"}, {"id":"Morocco", "text": "Morocco"}, {"id":"Mozambique", "text": "Mozambique"}, {"id":"Myanmar", "text": "Myanmar"}, {"id":"Nambia", "text": "Nambia"}, {"id":"Nauru", "text": "Nauru"}, {"id":"Nepal", "text": "Nepal"}, {"id":"Netherland Antilles", "text": "Netherland Antilles"}, {"id":"Netherlands", "text": "Netherlands"}, {"id":"Nevis", "text": "Nevis"}, {"id":"New Caledonia", "text": "New Caledonia"}, {"id":"New Zealand", "text": "New Zealand"}, {"id":"Nicaragua", "text": "Nicaragua"}, {"id":"Niger", "text": "Niger"}, {"id":"Nigeria", "text": "Nigeria"}, {"id":"Niue", "text": "Niue"}, {"id":"Norfolk Island", "text": "Norfolk Island"}, {"id":"Norway", "text": "Norway"}, {"id":"Oman", "text": "Oman"}, {"id":"Pakistan", "text": "Pakistan"}, {"id":"Palau Island", "text": "Palau Island"}, {"id":"Palestine", "text": "Palestine"}, {"id":"Panama", "text": "Panama"}, {"id":"Papua New Guinea", "text": "Papua New Guinea"}, {"id":"Paraguay", "text": "Paraguay"}, {"id":"Peru", "text": "Peru"}, {"id":"Philippines", "text": "Philippines"}, {"id":"Pitcairn Island", "text": "Pitcairn Island"}, {"id":"Poland", "text": "Poland"}, {"id":"Portugal", "text": "Portugal"}, {"id":"Puerto Rico", "text": "Puerto Rico"}, {"id":"Qatar", "text": "Qatar"}, {"id":"Republic of Montenegro", "text": "Republic of Montenegro"}, {"id":"Republic of Serbia", "text": "Republic of Serbia"}, {"id":"Reunion", "text": "Reunion"}, {"id":"Romania", "text": "Romania"}, {"id":"Russia", "text": "Russia"}, {"id":"Rwanda", "text": "Rwanda"}, {"id":"St Barthelemy", "text": "St Barthelemy"}, {"id":"St Eustatius", "text": "St Eustatius"}, {"id":"St Helena", "text": "St Helena"}, {"id":"St Kitts-Nevis", "text": "St Kitts-Nevis"}, {"id":"St Lucia", "text": "St Lucia"}, {"id":"St Maarten", "text": "St Maarten"}, {"id":"St Pierre & Miquelon", "text": "St Pierre & Miquelon"}, {"id":"St Vincent & Grenadines", "text": "St Vincent & Grenadines"}, {"id":"Saipan", "text": "Saipan"}, {"id":"Samoa", "text": "Samoa"}, {"id":"Samoa American", "text": "Samoa American"}, {"id":"San Marino", "text": "San Marino"}, {"id":"Sao Tome & Principe", "text": "Sao Tome & Principe"}, {"id":"Saudi Arabia", "text": "Saudi Arabia"}, {"id":"Senegal", "text": "Senegal"}, {"id":"Serbia", "text": "Serbia"}, {"id":"Seychelles", "text": "Seychelles"}, {"id":"Sierra Leone", "text": "Sierra Leone"}, {"id":"Singapore", "text": "Singapore"}, {"id":"Slovakia", "text": "Slovakia"}, {"id":"Slovenia", "text": "Slovenia"}, {"id":"Solomon Islands", "text": "Solomon Islands"}, {"id":"Somalia", "text": "Somalia"}, {"id":"South Africa", "text": "South Africa"}, {"id":"Spain", "text": "Spain"}, {"id":"Sri Lanka", "text": "Sri Lanka"}, {"id":"Sudan", "text": "Sudan"}, {"id":"Suriname", "text": "Suriname"}, {"id":"Swaziland", "text": "Swaziland"}, {"id":"Sweden", "text": "Sweden"}, {"id":"Switzerland", "text": "Switzerland"}, {"id":"Syria", "text": "Syria"}, {"id":"Tahiti", "text": "Tahiti"}, {"id":"Taiwan", "text": "Taiwan"}, {"id":"Tajikistan", "text": "Tajikistan"}, {"id":"Tanzania", "text": "Tanzania"}, {"id":"Thailand", "text": "Thailand"}, {"id":"Togo", "text": "Togo"}, {"id":"Tokelau", "text": "Tokelau"}, {"id":"Tonga", "text": "Tonga"}, {"id":"Trinidad & Tobago", "text": "Trinidad & Tobago"}, {"id":"Tunisia", "text": "Tunisia"}, {"id":"Turkey", "text": "Turkey"}, {"id":"Turkmenistan", "text": "Turkmenistan"}, {"id":"Turks & Caicos Is", "text": "Turks & Caicos Is"}, {"id":"Tuvalu", "text": "Tuvalu"}, {"id":"Uganda", "text": "Uganda"}, {"id":"Ukraine", "text": "Ukraine"}, {"id":"United Arab Erimates", "text": "United Arab Erimates"}, {"id":"United Kingdom", "text": "United Kingdom"}, {"id":"United States of America", "text": "United States of America"}, {"id":"Uraguay", "text": "Uraguay"}, {"id":"Uzbekistan", "text": "Uzbekistan"}, {"id":"Vanuatu", "text": "Vanuatu"}, {"id":"Vatican City State", "text": "Vatican City State"}, {"id":"Venezuela", "text": "Venezuela"}, {"id":"Vietnam", "text": "Vietnam"}, {"id":"Virgin Islands (Brit)", "text": "Virgin Islands (Brit)"}, {"id":"Virgin Islands (USA)", "text": "Virgin Islands (USA)"}, {"id":"Wake Island", "text": "Wake Island"}, {"id":"Wallis & Futana Is", "text": "Wallis & Futana Is"}, {"id":"Yemen", "text": "Yemen"}, {"id":"Zaire", "text": "Zaire"}, {"id":"Zambia", "text": "Zambia"}, {"id":"Zimbabwe", "text": "Zimbabwe"}];
            $countries = [ @foreach($listOfCountries as $item){"id": "{{$item->country}}" , "text": "{{$item->country}}" }, @endforeach ];

            $('#country').editable({
                pk:1,
                type: 'select2',
                placement: 'bottom',
                title: 'Select your Country',
                source: $countries,
                select2: {
                    width: 200,
                    placeholder : 'Select Country',
                    allowClear : true
                } ,
                url:'/profile',
                params: function(params) {
                    params._token = _token;
                    return params;
                  }
            });

            $('#maritalStatus').editable({
                pk:1,
                type: 'select',
                placement: 'bottom',
                title: 'Select your Marital Status',
                source: [
                   {value: 'Married', text: 'Married'},
                   {value: 'Living Common Law', text: 'Living Common Law'},
                   {value: 'Widowed', text: 'Widowed'},
                   {value: 'Separated', text: 'Separated'},
                   {value: 'Divorced', text: 'Divorced'},
                   {value: 'Single', text: 'Single'}
                ],
                value: '{{$user->maritalStatus}}',
                url:'/profile',
                params: function(params) {
                   params._token = _token;
                   return params;
                 }
            });

            $('#sexOrientation').editable({
                pk:1,
                type: 'select',
                placement: 'bottom',
                title: 'Select your Sexual Orientation',
                source: [
                    {value: 'Heterosexual', text: 'Heterosexual'},
                    {value: 'Bisexual', text: 'Bisexual'},
                    {value: 'Homosexual', text: 'Homosexual'}
                ],
                value: '{{$user->sexOrientation}}',
                url:'/profile',
                params: function(params) {
                    params._token = _token;
                    return params;
                  }
            });

            $('#childrenCount').editable({
                pk:1,
                type: 'number',
                placement: 'bottom',
                title: 'Enter number of your children',
                min: 0,
                max: 100,
                clear: false,
                url:'/profile',
                params: function(params) {
                    params._token = _token;
                    return params;
                  }
            });

            $('#religion').editable({
                pk:1,
                type: 'text',
                placement: 'bottom',
                title: 'Enter your Religion',
                url:'/profile',
                params: function(params) {
                    params._token = _token;
                    return params;
                  }
            });

        </script>

        <script>
        
            function updateAvatar(){
                var _html ='<form id="frmAvatar" action = "{{URL::route('storeAvatar')}}" > \
                                <div class="space-4"></div> \
                                <div class="row">\
                                <div class="col-xs-12">\
                                    <div class="xcontainer">\
                                        <div class="ximageBox">\
                                            <div class="xthumbBox"></div>\
                                            <div class="xspinner" style="display: none">Loading...</div>\
                                        </div>\
                                        <div class="xaction">\
                                            <input id="file-input" type="file" name="avatar-file" style="float:left; width: 250px"/>\
                                            <input type="button" id="btnCrop" value="Crop" style="float: right">\
                                            <input type="button" id="btnZoomIn" value="+" style="float: right">\
                                            <input type="button" id="btnZoomOut" value="-" style="float: right">\
                                        </div> \
                                        <input type="hidden" id="cropped-file" name="cropped-file" > \
                                        <div class="progress"> \
                                            <div class="bar" style="width: 0%;"></div> \
                                            <div class="percent">0%</div> \
                                        </div> \
                                        <div class="xcropped"><p><span id="msg-crop"></span></p></div>\
                                    </div>\
                                </div>\
                                </div>\
                            </form>';


                var dialog = messageBox('avatarDialog', _html, 'Profile Picture', 'Set Profile');
                dialog.setSize(BootstrapDialog.SIZE_MEDIUM);
            }

            function deleteAvatar(){
                var _html  = '<form id="frmAvatar" action="{{URL::route('deleteAvatar')}}"></form> \
                              Are you sure you want delete your profile picture?';

                var dialog = messageBox('avatarDialog', _html,'Profile Picture', 'Delete','',BootstrapDialog.TYPE_DANGER);
                dialog.setSize(BootstrapDialog.SIZE_SMALL);
            }

            function avatarDialog_onButtonClick(dialogRef){
                postMyFile('frmAvatar', dialogRef).then(function(){
                    var path = $('img.avatar').attr('src').split('?')[0];
                    $('img.avatar').attr('src', path + '?rnd=' + Math.random());
                    dialogRef.close();
                    location.reload();
                    
                }, function(error){
                    console.log(error.responseJSON);
                });
                postMyData('frmAvatar', dialogRef).then(function(){
                    dialogRef.close();
                }, function(error){
                    console.log(error.responseJSON);
                });
            }


            $('body').on('change','#file-input',function(){
                var size = this.files[0].size;

                $('.limit-error').remove();
                if (size > 2000000) {
                    $('#file-input').val('');
                    var _span = $('<div class="limit-error label label-danger" style="width:100%">').html('You can not upload files larger than 2mb');
                    $('.modal-body').append(_span);
                    console.log('file limit reached');
                }

            });
    
    
        </script>
       
    @endif

    @if(Auth::check())
        <script>
            function followUser(){
                var _follow = '{{$follow}}';
                var _unfollow = '{{$unfollow}}';

                var btn = $('#followUserBtn');
                var _caption = $(btn).html() ;

                var _url = '/api/v1/users/';
                var _finalCaption = '';

                 if (_caption == _follow) {
                    _finalCaption = _unfollow;
                    _url += 'follow';
                 } else{
                    _finalCaption = _follow;
                    _url += 'unfollow';
                 }

                $.post(_url, {'userId': $(btn).attr('data-id'), '_token' : '{{csrf_token()}}' })
                    .done(function(d){
                        if (d.success == 'true'){
                            console.log('true');
                            $('#followUserBtn').html(_finalCaption);
                        }
                    });
            }
        </script>


        {{--Choose Location on first login --}}
        <script>
            @if( Auth::user()->enrolledLocations()->count() == 0 )
                $(function(){
                    enrollLocation();
                });
            @endif
        </script>
    @endif

@stop