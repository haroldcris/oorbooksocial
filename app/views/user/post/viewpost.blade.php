<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/21/2015
 * Time: 8:39 AM
 *
 */

    $thumb = Config::get('app.url').'/assets/images/logo.png';
    if(isset($item)) {
        if(!is_null($item->photos)) {
            $thumb = Config::get('app.url').$item->photos;
            $photos = explode('»',$item->photos );
            if(count($photos) > 1) {
                $thumb = Config::get('app.url').$photos[0];
            }
        }

        if(!is_null($item->youtubeImage) && $item->youtubeImage != ''){
            $thumb = $item->youtubeImage;
        }

        $thumb = str_replace(' ', '%20', $thumb);
    }
?>

@extends('layouts.main')

@if(!isset($item))
    @section ('pageContent')
        @include('layouts.partials.breadcrumbs')
        @include('layouts.partials.flashmessage')
    @stop

@else
    @section('meta')
    <!-- 241621169219185 -->
    <meta property="fb:app_id" content="{{Config::get('hybridauth.providers.Facebook.keys.id')}}">
    <meta property="og:url" content="{{URL::route('showPost',$item->postId)}}" />
    <meta property="og:site_name" content="{{Config::get('site.title')}}" />

    {{--<meta property="og:type"               content="article" />--}}
    <meta property="og:title"              content="{{$item->title}}" />
    <meta property="og:description"        content="{{$item->description}}"  />
    {{--<meta property="article:author"        content="{{$item->creator->username}}"  />--}}
    {{--<meta property="article:publisher"     content="Publisher" />--}}
    <meta property="article:published"     content="{{$item->created_at}}"  />
    {{--<meta property="article:section"       content="International Arts" />--}}
    <meta property="og:image"              content="{{ $thumb . '?t=' . time()}}" />
    @if(!is_null($item->youtube) && $item->youtube != '')
    <meta property="og:video"              content="https://www.youtube-nocookie.com/embed/{{ $item->youtube}}" />
    @endif
    @stop

    @section('styles')
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-54e8596c4a93a12e" async="async"></script>
    @stop

    @section ('pageContent')
        @include('layouts.partials.breadcrumbs')
        
	<div style="display: block;padding-top: 30px;"></div>
	
        <div class="panel post" style="margin-bottom:20px;" >
            <div class="panel-heading post-body">
                @if(Auth::check())
                    <div class="inline position-relative pull-right">
                        <button data-position="auto" data-toggle="dropdown" class="btn btn-white btn-minier btn-no-border dropdown-toggle" aria-expanded="false">
                            <i class="ace-icon fa fa-angle-down icon-only bigger-120"></i>
                        </button>

                        <ul class="dropdown-menu dropdown-only-icon dropdown-yellow2 icon-only dropdown-caret dropdown-close pull-right">

                            @if(Auth::user()->userId == $item->userId || Auth::user()->isAdmin() )
                                {{--<li> <a class="tooltip-success editPost" href="javascript:void(0)" > <span class=""> <i class="ace-icon fa fa-pencil-square-o bigger-110"></i> Edit&nbsp;&nbsp; </span> </a> </li>--}}
                                <li> <a class="tooltip-warning deletePost" href="javascript:void(0)" data-id="{{$item->postId}}"><span class="red"><i class="ace-icon fa fa-times bigger-110"></i> Delete Post</span></a> </li>
                                @if(!is_null($item->photos) && $item->photos !='')
                                <li> <a class="tooltip-warning deletePhoto" href="javascript:void(0)" data-id="{{$item->postId}}"><span class="red"><i class="ace-icon fa fa-photo bigger-110"></i> Remove Image</span></a> </li>
                                @endif
                            @else
                                <li> <a class="tooltip-warning reportPost" href="javascript:void(0)" data-id="{{$item->postId}}"> <span class="red"> <i class="ace-icon fa  fa-bolt  bigger-110"></i> Report </span> </a> </li>
                            @endif
                        </ul>
                    </div>
                @endif


                <span class="post-title single" >{{$item->title}}</span>
                <span class="post-description" itemprop="description" >

                    {{$item->description}}

                    @if ($item->photos != null)
                       <div class="cycle-slideshow photo"
                           data-cycle-loader=true
                           data-cycle-fx="scrollHorz"
                           data-cycle-timeout=5000
                           data-cycle-pause-on-hover="true"
                           data-cycle-manual-fx="scrollHorz"
                           data-cycle-manual-speed="200"
                           >
                            <?php  $photos = explode('»',$item->photos )  ?>
                            @foreach( $photos as $index => $photoItem)
                                <img src="{{$photoItem}}" {{$index !=0 ? 'style="display:none"' : '' }} >
                            @endforeach

                            @if(count($photos) > 1)
                               <div class="photo-prev"></div>
                               <div class="photo-next"></div>
                            @endif

                        </div>
                    @endif

                    @if ($item->youtube != null)
                        <iframe width="558" height="314" src="https://www.youtube-nocookie.com/embed/{{$item->youtube}}?html5=1" frameborder="0" allowfullscreen></iframe>
                    @endif

                </span>


                <div class="post-subject">
                    <span class="label label-light arrowed-in-right">{{$item->subject}}</span>
                </div>

                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="pull-right addthis_sharing_toolbox"></div>

            </div>

            <div class="panel-body">
                <span class="post-matter single" >
                    <?php $xtag = explode(',',$item->matter); ?>
                    <small>Tags: </small>
                    @foreach($xtag as $tag)
                        {{-- <span class="label label-sm label-white label-info arrowed">{{$tag}}</span> --}}
                        <span class="label label-sm arrowed" style="background: #880085 !important;">{{$tag}}</span>
                    @endforeach
                    
                </span>

                @if($item->creator)
                <div class="post-avatar avatar-creator">
                    <img src="{{URL::route('userAvatar',$item->creator->userId)}}" alt="{{$item->creator->username}}">
                </div>

                <div class="post-avatar-body">
                    <div class="name">
                        <a href="{{URL::route('guestUserProfile',$item->creator->username)}}">{{$item->creator->username}}</a>
                    </div>

                    <div class="time">
                        <i class="ace-icon fa fa-clock-o"></i>
                        <span class="green">{{ \Carbon\Carbon::createFromTimestamp(strtotime($item->created_at))->diffForHumans() }}</span>
                    </div>


                </div>
                @endif
            </div>

            <div class="panel-footer post-footer">
                <div><i class="ace-icon fa fa-comments-o fa-2x green middle"></i> Comments </div>
                <div class="space-10"></div>

                {{ $commentsView }}
            </div>
        </div>
    @stop


    @section('scripts')
        @include ('user.post.comments._comment-js')

        {{--Add Comment--}}
        <script>
            $('body').on('submit','.commentForm',function(e){
                e.preventDefault();

                var commentDiv = $(this).closest('.post-footer').find('.post-comments');
                var newComment = $('#commentTemplate').find('.itemdiv').clone(true,true);

                var form = $(this);
                form.find('i.submit').removeClass('fa-share').addClass('fa-refresh fa-spin');

                $.post("{{URL::route('storeComment')}}", $(this).serialize())
                    .always(function(){
                        //ace-icon fa fa-share
                        form.find('i.submit').removeClass('fa-refresh fa-spin').addClass('fa-share');
                    })
                    .done(function(json){
                        if(json.success == 'true') {
                            form[0].reset();

                            newComment.attr('data-commentId', json.comment.commentId).attr('data-parentId',json.comment.parentId);
                            newComment.find('.user img').attr('src', '/user/'+json.comment.userId + '/avatar');
                            newComment.find('.name a').html(json.username);
                            newComment.find('.time').html('just now');
                            newComment.find('.text').html(json.comment.message);
                            newComment.find('.deleteComment').attr('data-id',json.comment.commentId);
                            newComment.find('.replyComment').attr('data-id', json.comment.commentId);

                            if(json.comment.parentId == 0) return newComment.appendTo(commentDiv);

                            var _parent = $('[data-commentId="'+ json.comment.parentId +'"]');
                            if($(_parent).length != 0) {

                                $(form).closest('.commentdiv.post-comment').find('.commentReply').remove();

                                var _margin = Number(_parent.css('margin-left').replace('px','')) + 10;
                                newComment.appendTo(_parent);
                                newComment.css('margin-left', _margin + 'px');
                                return;
                            }

                            return newComment.appendTo(commentDiv);

                        }
                    });
             });
        </script>
    @stop
@endif