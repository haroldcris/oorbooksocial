<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/20/2015
 * Time: 3:37 PM
 *
 */
 ?>

    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-54e8596c4a93a12e" async="async"></script>
    <style type="text/css">
    .balls-container {
        height: 95px; 
        border-top: 2px solid #3A87AD; 
        border-bottom: 2px solid #3A87AD; 
        background: rgba(255,255,255,.8);
    }
    .balls-row {
        width: 694px; 
        margin-left: -6px;
    }
    .balls-title { 
        line-height: 15px; 
        font-weight: 600;
        letter-spacing: 2px;
    }
    .balls-content {
        padding: 0 0 0 30px; 
        height: 70px; 
        margin-top: 10px;
    }
    .balls-news-border { border-right: 1px solid #D8D8D8; }
    .balls-list ul { margin-left: 15px; }
    .balls-list ul li {
        list-style-position: inside; 
        list-style-type: square; 
        line-height: 17px; 
        color: #1C6081; 
        font-size: 15px;
        font-weight: bold;
    }
    .balls-list ul li span a {
        color: #06F; 
        font-size: 11px;
    }
    .ico-remove {
    color: #FF4F4F;
    cursor: pointer;
    position: absolute;
    right: 3px;
    top: 4px;
    font-size: 15px;
    }
    .ico-remove:hover {
        color: #f00;
    }
    .pad-adjust {
        display: block; 
        padding-top: 95px;
    }
    </style>

@if(!is_null($posts))

    {{--<div class="breadcrumbs balls-container">
        <div class="row balls-row">
            <i class="glyphicon glyphicon-remove ico-remove"></i>
            <div class="col-md-6 balls-content balls-news-border">
                <div class="balls-title">LATEST NEWS</div>
                <div class="balls-list">
                    <ul>
                        <li><span><a href="#">The Location Book</a></span></li>
                        <li><span><a href="#">About Oorbook</a></span></li>
                        <li><span><a href="#">Oorbook Updates</a></span></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 balls-content">
                <div class="balls-title">TRENDS</div>
                <div class="balls-list">
                    <ul>
                        <li><span><a href="#">#Oorbook</a></span></li>
                        <li><span><a href="#">#WelcomeOorbook</a></span></li>
                        <li><span><a href="#">#ILoveOorbook</a></span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>--}}
    
    {{--<div class="pad-adjust"></div>--}}

    <div style="display:none">
        {{$posts->appends(Request::except('page'))->links()}}
    </div>
    
    <div id="post-scroller">
        @foreach($posts as $item)
            @if ( $item->isPrivate != 1 ||  $item->creator->userId == Auth::user()->userId || Request::is('interest/*') )
                <div class="panel post" style="margin-bottom:20px; padding:10px;">
                    <div class="panel-heading">
                        @if($item->creator)
                            <div class="post-avatar avatar-creator">
                                <img src="{{URL::route('userAvatar',$item->creator->userId)}}" alt="avatar">
                            </div>
    
                            <div class="post-body">
                                @if(Auth::check())
                                    <div class="inline position-relative pull-right">
                                        <button data-position="auto" data-toggle="dropdown" class="btn btn-white btn-minier btn-no-border dropdown-toggle" aria-expanded="false">
                                            <i class="ace-icon fa fa-angle-down icon-only bigger-120"></i>
                                        </button>
    
                                        <ul class="dropdown-menu dropdown-only-icon dropdown-yellow2 icon-only dropdown-caret dropdown-close pull-right">
    
                                            @if(Auth::user()->userId == $item->userId || Auth::user()->isAdmin())
                                                {{--<li style="line-height: 10px;"><a href="#"> <a class="tooltip-success editPost" href="javascript:void(0)" > <span class=""> <i class="ace-icon fa fa-pencil-square-o bigger-110"></i> Edit&nbsp;&nbsp; </span> </a> </a></li>--}}
                                                <li style="line-height: 10px;"><a href="#"> <a class="tooltip-warning deletePost" href="javascript:void(0)" data-id="{{$item->postId}}"><span class="red"><i class="ace-icon fa fa-times bigger-110"></i> Delete Post</span></a> </a></li>
                                                @if(!is_null($item->photos) && $item->photos !='')
                                                <li style="line-height: 10px;"><a href="#"> <a class="tooltip-warning deletePhoto" href="javascript:void(0)" data-id="{{$item->postId}}"><span class="red"><i class="ace-icon fa fa-photo bigger-110"></i> Remove Image</span></a> </a></li>
                                                @endif
                                            @else
                                                <li style="line-height: 10px;"><a href="#"> <a class="tooltip-warning reportPost" href="javascript:void(0)" data-id="{{$item->postId}}"> <span class="red"> <i class="ace-icon fa  fa-bolt  bigger-110"></i> Report </span> </a> </a></li>
                                            @endif
                                        </ul>
                                    </div>
                                @endif
    
                                <div class="name">
                                    <a href="{{URL::route('guestUserProfile',$item->creator->username)}}">{{$item->creator->username}}</a>
                                </div>
    
                                <div class="time">
                                    <i class="ace-icon fa fa-clock-o"></i>
                                    <span class="green">{{ \Carbon\Carbon::createFromTimestamp(strtotime($item->created_at))->diffForHumans() }}</span>
                                </div>
    
                            </div>
                        @endif
                    </div>
                    <div class="panel-body post-body">
                        {{--tweaks removed target attr which opens the post in new tab--}}
                        <span class="post-title"><a href="{{URL::route('showPost',$item->postId)}}">{{$item->title}}</a></span>
    
                        <span style="display: inline-block; width: 666px;">
    
                            @if(strlen($item->description) > 280 )
                                {{substr($item->description,0,280)}}<br>
                                {{--tweaks removed target attr which opens the post in new tab--}}
                                <a class="btn btn-minier btn-round btn-yellow" href="{{URL::route('showPost',$item->postId)}}">Read more...</a>
                            @else
                                {{$item->description}}
                            @endif
    
                            @if ($item->photos != null)
                               <div class="cycle-slideshow photo"
                                   data-cycle-loader=true
                                   data-cycle-fx="scrollHorz"
                                   data-cycle-timeout=5000
                                   data-cycle-pause-on-hover="true"
    
                                   data-cycle-manual-fx="scrollHorz"
                                   data-cycle-manual-speed="200"
    
                                   style="border: 1px solid #bbb;"
                                   >
                                   <!-- prev/next links -->
                                   <?php  $photos = explode('»',$item->photos )  ?>
                                    @foreach( $photos as $index => $photoItem)
                                        <img src="{{$photoItem}}" {{$index !=0 ? 'style="display:none"' : '' }} >
                                    @endforeach
                                    
                                    @if(count($photos) > 1)
                                       <div class="photo-prev"></div>
                                       <div class="photo-next"></div>
                                    @endif
    
                                </div>
    
                            @endif
    
                            @if ($item->youtube != null)
                            {{--<div style="width: 300px;">--}}
                                <br>
                                <iframe style="width: 640px; height: 360px; margin-left: 15px;" src="https://www.youtube-nocookie.com/embed/{{$item->youtube}}?html5=1" frameborder="1" allowfullscreen></iframe>
                            {{--<div>--}}
                            @endif
                        </span>
    
                        <div class="post-subject">
                            <span class="label label-light arrowed-in-right">{{$item->subject}}</span>
                        </div>
    
                        <div class="pull-right addthis_sharing_toolbox" data-url="{{URL::route('showPost',$item->postId)}}" data-title="{{$item->title}}" ></div>
    
                        <div class="space-10" style="margin-bottom: 20px;"></div>
                    </div>
    
                    <div class="panel-footer post-footer">
                        {{ View::make('user.post.comments.comments')->with('comments', $item->comments)
                                                               ->with('total', $item->totalcomments)
                                                               ->with('postId', $item->postId) }}
                    </div>
    
                </div>
            @endif
        @endforeach
    </div>
@endif



<script src="/controls/infinite-scroll/jquery.infinitescroll.js"></script>
<script>
     $(function() {
         $('#post-scroller').infinitescroll({
             navSelector     : ".pagination",
             nextSelector    : ".pagination a:last",
             itemSelector    : "#post-scroller",
             debug           : false,
             dataType        : 'html',
             path: function(index) {
                 return "?{{ Request::getQueryString()}}&page=" + index;
             }
         }, function(newElements, data, url){

            try{
                addthis.layers.refresh();
                $('.cycle-slideshow.photo').cycle();

            } catch (e){

            }

         });
     });
</script>

{{--Add Comment on Posts--}}
<script>
    $(function(){
        $('body').on('submit','.commentForm',function(e){
            e.preventDefault();

            var commentDiv = $(this).closest('.post-footer').find('.post-comments');
            var newComment = $('#commentTemplate').find('.itemdiv').clone(true,true);
            console.log(newComment);

            var form = $(this);
            form.find('i.submit').removeClass('fa-share').addClass('fa-refresh fa-spin');

            $.post("{{URL::route('storeComment')}}", $(this).serialize())
                .always(function(){
                    //ace-icon fa fa-share
                    form.find('i.submit').removeClass('fa-refresh fa-spin').addClass('fa-share');
                })
                .done(function(json){
                    if(json.success == 'true') {
                        form[0].reset();

                        newComment.attr('data-commentId', json.comment.commentId).attr('data-parentId',json.comment.parentId);
                        newComment.find('.user img').attr('src', '/user/'+json.comment.userId + '/avatar');
                        newComment.find('.name a').html(json.username);
                        newComment.find('.time').html('just now');
                        newComment.find('.text').html(json.comment.message);
                        newComment.find('.deleteComment').attr('data-id',json.comment.commentId);
                        newComment.find('.replyComment').attr('data-id', json.comment.commentId);

                        if(json.comment.parentId == 0) return newComment.appendTo(commentDiv);

                        var _parent = $('[data-commentId="'+ json.comment.parentId +'"]');
                        if($(_parent).length != 0) {

                            $(form).closest('.commentdiv.post-comment').find('.commentReply').remove();

                            var _margin = Number(_parent.css('margin-left').replace('px','')) + 10;
                            newComment.appendTo(_parent);
                            newComment.css('margin-left', _margin + 'px');
                            return;
                        }

                        return newComment.appendTo(commentDiv);



                    }
                });
         });
    });
</script>
<script>
    $(".ico-remove").click(function(){
        $(".balls-container").hide(500);
        $(".pad-adjust").hide();
    });
</script>