<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/29/2015
 * Time: 9:43 AM
 *
 */

 ?>

    <div>
        <ul class="ace-thumbnails clearfix" data-id="{{$post->postId}}" >
            @foreach($photos as $item)
            <li>
                <a data-rel="colorbox" title="Photo Title" href="javascript:void(0)" class="cboxElement">
                    <img class="postPhoto" width="100px" height="100px" src="{{$item}}" alt="150x150">
                </a>

                <div class="tags">
                    <span class="label-holder">
                        <span class="btn btn-xs btn-white btn-danger removePostPhoto">Remove</span>
                    </span>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
    <form id="frmRemovePhoto" action="{{URL::route('storeRemovePhoto',$post->postId)}}">
        <input type="hidden" style="width:100%" name="postId" value="{{$post->postId}}" >
        <input type="hidden" style="width:100%"  name="photos" value="{{$post->photos}}" >
        <input type="hidden"  style="width:100%"  name="deleted" value="" >
    </form>


    <script>
        var updatePhoto = function(){
            var _photos = '';
            $('img.postPhoto').each(function(){
                _photos += $(this).attr('src') + '»';
            });
            _photos = _photos.substr(0,_photos.length-1);
            $('[name="photos"]').val(_photos);
        }

        $('.removePostPhoto').click(function(){
            var _li = $(this).closest('li'),
                _src = _li.find('img.postPhoto').attr('src'),
                _deleted = $('[name="deleted"]').val();

            _deleted += _src + '»';
            $('[name="deleted"]').val(_deleted);
            $(this).closest('li').remove();
            updatePhoto();
        });

    </script>