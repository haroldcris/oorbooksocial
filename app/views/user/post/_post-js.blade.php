{{-- CREATE POST --}}
<script>
    function createPost(){
        var securityLevel = arguments[0];
        console.log(securityLevel + ' {{ Auth::id() }}');
        
        if(securityLevel!="master user") {
            var dialog = messageBox('createPostDialog', '{{Config::get('site.loader')}}', $('#breadcrumbLocation').text(), 'Share','',BootstrapDialog.TYPE_SUCCESS);

            $.get('{{URL::route('createPost')}}')
                .error(function(error){
                    dialog.getModalBody().html(error.responseText); })
    
                .done(function(frmData){
    
                    dialog.getModalBody().html(frmData);
    
                    var _select = $("#frmCreatePost").find("select");
                    $(_select).each(function(){
                        $(this).select2({ width: $(this).attr('data-select2-width') } );
                    });
    
                    $('#create-post-location').select2('val','{{ Session::get('selectedLocationId')  }}');
    
    
            });
        } else {
            var dialog = messageBox('createPostDialog', '{{Config::get('site.loader')}}', $('#breadcrumbLocation').text(), 'Share Master','',BootstrapDialog.TYPE_SUCCESS);

            $.get('{{URL::route('createPost')}}')
                .error(function(error){
                    dialog.getModalBody().html(error.responseText); })
    
                .done(function(frmData){
                    dialog.getModalBody().html(frmData);
                    $('#create-post-location').select2('val','{{ Session::get('selectedLocationId')  }}');
    
    
            });
        }
    }

    function createPostDialog_onButtonClick(dialogRef){
        if(typeof dropzoneFiles !== 'undefined')
        {
            var photos='';
            for(var i = 0 ; i< dropzoneFiles.length; i++){
                photos +=  '»' + dropzoneFiles[i];
            }
            if(photos !== '')
            {
                $('#frmCreatePost').append('<input type="hidden" name="photos" value = "' + photos.substr(1) + '">')
            }

        }

        if ( $('.tab-pane.active').find('#create-post-location').length == 0 ) {
            $('#create-post-location').select2('val','');
        } else {
            $('#create-post-group').select2('val','');
        }

        postMyData('frmCreatePost', dialogRef).then (function(done){
            console.log(done);
            dialogRef.close();

            var _url = window.location.pathname;

            if( _url.search('/location') != -1 ||
                _url.search('/interest') != -1 ||
                _url.search('/posts') != -1 )
            {
                window.location.reload();
            }

        },function(err){});
    }
</script>

{{-- DELETE POST --}}
<script>
    $('body').on('click', '.deletePost', function(){
        $('.panel.post').removeClass('active');
        $(this).closest('.panel.post').addClass('active');

        var _html = 'Are you sure you want to delete this?';
         _html += '<form id="deletePostForm" action="{{URL::route('deletePost')}}"><input type="hidden" name="postId" value="' + $(this).attr('data-id') + '" > </form>';

        var dialog = messageBox ('deletePostDialog', _html ,'Delete Post','Delete Post');
        
        dialog.setSize (BootstrapDialog.SIZE_SMALL);
    });
    
    function deletePostDialog_onButtonClick(dialogRef){

        postMyData('deletePostForm',dialogRef).then(function(){
            $('.panel.post.active').remove();
            dialogRef.close();
        })

    }

</script>


 {{--ATTACH DROPZONE / PICTURE / YOUTUBE VIDEOS --}}
<script>
    function showHideField(field)
    {
        $('#selected-youtube-id').val('');
        $('.attachment').addClass('hidden');

        switch(field){
            case 'dropzone':
                $('.dropzonePost').toggleClass('hidden');
                break;

            case 'youtube':
                var videoDialog = messageBox('youtubeVideoDialog', '{{Config::get('site.loader')}}','Video', 'Add Video' );

                $.get('{{URL::route('createVideo')}}')
                    .done(function(d){
                        videoDialog.getModalBody().html(d);

                        setTimeout(function(){
                            $('#q').focus();
//                            console.log('focus ++++++++++++++');
                        },200);
                    });
                break;

            default: break;
        }

    }


    function youtubeVideoDialog_onButtonClick(dialogRef){
        var _youtubeId = $('#youtube-video-id');
        if( _youtubeId.val() == '') { return false; }

        $('.youtube').removeClass('hidden');

        var id = _youtubeId.val();
        var tn = _youtubeId.attr('data-thumbnail');

        $('.youtube-selected-video').html('<img src="' + tn + '" alt="YouTube Video">');
        $('#selected-youtube-id').val(id);
        $('#selected-youtube-id-image').val(tn);
        dialogRef.close();
    }
</script>


 {{--REPORT POST --}}
<script>
    $('body').on('click', '.reportPost', function(){
        var id = $(this).attr('data-id');

        var frm = $('<form id="frmReportPost" action="{{URL::route('reportPost')}}" >');
        var txtId = $('<input type="hidden" name="postId">').val(id);
        var txt = $('<textarea rows=3 class="form-control" placeholder="Write your report" name="message"></textarea>');
        frm.append(txtId).append(txt);

        messageBox('frmReporPostDialog',frm,'Report','Report').setSize(BootstrapDialog.SIZE_SMALL);
    });

    function frmReporPostDialog_onButtonClick(dialogRef)
    {
        postMyData('frmReportPost',dialogRef).then(
            function(json){
                messageBoxOk('Your report has been sent to the Site Administrator','Report Post',null,BootstrapDialog.TYPE_SUCCESS).setSize(BootstrapDialog.SIZE_SMALL);
                dialogRef.close();
            }
        );
    }
</script>


 {{--Highligh Post on FOCUS --}}
<script>
    $('.panel.post').on('click',function()
    {
        $('.panel.post').css('border-color','transparent');
        $(this).css('border-color','green');
    });
</script>


{{--Delete Photos--}}
<script>
    $('body').on('click', '.deletePhoto', function(){
       var _html = "{{URL::route('removePostPhoto')}}".replace('%7Bid%7D',$(this).attr('data-id'));

        messageBoxRemote('deletePhotoDialog', _html,'Remove Image','Update Post').setSize(BootstrapDialog.SIZE_WIDE);
    });

    function deletePhotoDialog_onButtonClick(dialogRef)
    {
        postMyData('frmRemovePhoto',dialogRef).then(
            function(){
                dialogRef.close();
                location.reload();
            }
        )
    }
</script>