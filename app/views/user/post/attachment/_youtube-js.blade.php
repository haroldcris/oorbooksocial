<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/27/2015
 * Time: 11:48 AM
 *
 */
 ?>

 <script>
     $('body').on('submit','#searchYoutube', function(e){
         e.preventDefault();

         if( $('#q').val().trim() == '' ) { return false; }

         $('#youtube-video-id').val('');

         $('#video-search-result').html('<span>{{Config::get('site.loader')}}&nbsp;</span>');

         $.get('{{URL::route('searchYoutube')}}', $('#searchYoutube').serialize())
                 .done(function(json){
                     //console.log(json);
                     buildVideoResult(json)
                 })
     });



     function buildVideoResult(jsonData){

         var items = jsonData.items;

         if(typeof items == 'object')
         {

             var result = '<table>';
             for( var i = 0 ; i < items.length; i++)
             {
                 if( items[i].id.kind == 'youtube#video'  )
                 {
                     result += '<tr class="youtube-search-result" data-id="'+ items[i].id.videoId +'"  data-thumbnail="'+ items[i].snippet.thumbnails.medium.url +'"  > ';
                     result += ' <td><img src="'+ items[i].snippet.thumbnails.default.url +'"></img></td>';
                     result += ' <td>';
                     result += '     <span class="title">' + items[i].snippet.title + '</span>';
                     result += '     <span class="desc">' + items[i].snippet.description + '</span>';
                     result += ' <td>';
                     result += '</tr>';
                 }
             }
             result +='</table>';
             result +='<div>';

            if (items.length > 0)
            {
                 if( jsonData.prevPageToken !== undefined)
                 {
                     result +='  <span class="youtube-btn btn btn-white btn-xs" data-token="'+ jsonData.prevPageToken +'" ><i class="fa fa-caret-left fa-2x"></i></span>';
                 }

                 result +='  <span class="youtube-btn btn btn-white btn-xs" data-token="'+ jsonData.nextPageToken +'" ><i class="fa fa-caret-right fa-2x"></i></span>';
                 result +='</div>';

                 $('#video-search-result').html(result);
            }

         }
     }
 </script>

 <script>
     $('body').on('click', '.youtube-btn', function(){
        console.log('youtube button clicked');
         $('#pageToken').val($(this).attr('data-token'));
         $('#searchYoutube').submit();
     });

     $('body').on('click', 'tr.youtube-search-result',function(){
         $('tr.youtube-search-result').removeClass('active');
         $(this).addClass('active');
         var id = $(this).attr('data-id');
         var thumbnail = $(this).attr('data-thumbnail');

         $('#youtube-video-id').val(id).attr('data-thumbnail', thumbnail);
     });

 </script>