<?php /**
 * Created by Harold Cris D. Abarquez
 * Date: 02/27/2015
 * Time: 7:54 AM
 *
 */ ?>

<style>
    tr.youtube-search-result {
        display: block;
        cursor: pointer;
        padding:5px;
        vertical-align: top;
    }

    tr.youtube-search-result.active {
        border: 2px solid #000080;
    }

    .youtube-search-result img {
        max-width: 64px;
        margin-right: 10px;
    }

    .youtube-search-result .title {
        display: block;
        font-weight: bold;
    }

    .youtube-btn {
        margin-top:10px;
    }
</style>

<div class="widget-box">
    <div class="widget-header">
        <h4 class="widget-title lighter smaller" >
            <span style="width:70px;background: url('/assets/images/youtube.png') no-repeat;border: none;margin:5px 0;float:left">&nbsp </span>
        </h4>
        <div class="widget-toolbar no-border">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#search-tab" data-toggle="tab" aria-expanded="false">Search</a></li>
            </ul>
        </div>
    </div>
    <div class="widget-body">
        <div class="tab-content">

            {{--//Search Youtube --}}

            <div class="tab-pane active" id="search-tab">
                <form id="searchYoutube">
                    <div style="margin-bottom: 10px;font-weight: bold">Type your search or YouTube Url in the box to find videos and click the <i class="fa fa-search bigger-150"></i> icon.</div>
                    <div class="input-group">
                        <input type="text" id="q" name="q" class="form-control" placeholder="Search YouTube or YouTube Url" autofocus>
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-white  btn-info">
                                <i class="ace-icon fa fa-search bigger-120"></i>
                            </button>
                        </span>

                    </div>

                    <input type="hidden" id="pageToken" name="pageToken" >
                </form>
                    <div id="video-search-result" style="margin-top:20px">
                    </div>
            </div>

        </div>
    </div>
</div>

<input type="hidden" id="youtube-video-id" >
