<script>
    console.log('++++++++ Creating Dropzone ++++++++');
    var dropzoneUploading;
    var dropzoneFiles =[];

    $(".dropzonePost").dropzone({
        init: function () {
            dropzoneUploading = false;
            dropzoneFiles = [];
            console.log('Dropzone Initialize....');

            this.on("complete", function () {
                dropzoneUploading = true;
                if (this.getUploadingFiles().length === 0)
                {
                    if(this.getQueuedFiles().length === 0)
                    {
                        dropzoneUploading = false;
                    }
                }

            });
        },
        acceptedFiles: "image/*",
        url: '{{URL::route('uploadFile')}}',
        maxFiles: 10, // Number of files at a time
        maxFilesize: 2, //in MB
        maxfilesexceeded: function(file)
        {
          //  alert('You have uploaded more than 10 Image. Only the first 10 files will be uploaded!');
        },
        success: function (response, ajax) {
            //console.log('success');
            //console.log(response);
            //console.log(ajax);
            //console.log(ajax.filename);
            dropzoneFiles.push(ajax.filename);
        },
        error : function(file,ajax){
            console.log('error file');
            console.log(file);
            console.log(ajax);
            $('#dropzoneError').html(ajax).removeClass('hidden');
            var _ref; // Remove file on clicking the 'Remove file' button
            return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
        },

        addRemoveLinks: true,

        removedfile: function(file) {
            console.log('removing File');

            var item = JSON.parse(file.xhr.responseText);
            console.log(item.filename);

            $.post("{{URL::route('deleteFile')}}" , {_method: 'delete', _token : '{{csrf_token()}}', filename : item.filename } )
                .done(function(json){
                    if ( json.success == 'true')
                    {
                        var index = dropzoneFiles.indexOf(item.filename)
                        dropzoneFiles.splice(index,1);


                        var _ref; // Remove file on clicking the 'Remove file' button
                        return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                    }

                });


        },
        sending: function(file, xhr, formData) {
                console.log('dropzone sending');
                dropzoneUploading = true;
                $('#dropzoneError').addClass('hidden').html('');
                formData.append("_token", '{{csrf_token()}}'  );
            }

    });


</script>
