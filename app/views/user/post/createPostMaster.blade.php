<?php

?>

<style type="text/css">
    div.modal-content{
        width: 120%;
    }
    div.scrollable {
        width: 100%;
        height: 200px;
        margin: 0;
        padding: 0;
        overflow: auto;
    }
    .boxfix {
        display: block;
        padding-left: 15px;
        text-indent: -15px;
        font-size: 10px;
        margin-bottom: -10px;
    }
    .boxfix input {
        padding: 0;
        margin:0;
        vertical-align: bottom;
        position: relative;
        top: -1px;
    }
    .spinnery {
        font-size: 50px;
        position: absolute;
        top: 75px;
        left: 310px;
        display: none;
        z-index: 999;
    }
    .geo-table {
        height: 200px; 
        max-height: 200px; 
        min-height: 200px; 
        width: 700px; 
        max-width: 700px; 
        min-width: 700px;
    }
</style>

{{--For Testing Only--}}
{{--<p><b>{{$tezt}}</b></p>--}}

<div class="panel panel-body" style="padding:0">
    <form id="frmCreatePost" action="{{URL::route('storePostMaster')}}" enctype="multipart/form-data" >
        <ul class="nav nav-tabs">
            <li class="active"><a href="#share-location-tab" data-toggle="tab">Share Something</a></li>
            
            {{--<li><a href="#share-group-tab" data-toggle="tab" > Share within Group</a></li>--}}
        </ul>
        <div class="tab-content tab-color-blue">

            <div class="tab-pane active" id="share-location-tab">
        
                <div class="form-group">
                    {{ Form::token() }}
                    
                    <input  id="hidContinent" name="hidContinent" type="hidden">
                    <input  id="hidCountry"   name="hidCountry"   type="hidden">
                    <input  id="hidRegion1"   name="hidRegion1"   type="hidden">
                    <input  id="hidRegion2"   name="hidRegion2"   type="hidden">
                    <input  id="hidRegion3"   name="hidRegion3"   type="hidden">
                    <input  id="hidRegion4"   name="hidRegion4"   type="hidden">
                    <input  id="hidLocality"  name="hidLocality"  type="hidden">
                    
                    <table border="0" style="geo-table">
                        <thead>
                            <tr>
                                <th id="thCN" style="width: 100px;">CONTINENT</th>
                                <th id="thCT" style="width: 100px;">COUNTRY</th>
                                <th id="thR1" style="width: 100px;">REGION 1</th>
                                <th id="thR2" style="width: 100px;">REGION 2</th>
                                <th id="thR3" style="width: 100px;">REGION 3</th>
                                <th id="thR4" style="width: 100px;">REGION 4</th>
                                <th id="thLC" style="width: 100px;">LOCALITY</th>
                            <tr>
                            
                        </thead>
                        <tbody>
                            <i id="spinnery" class="fa fa-spinner fa-spin spinnery"></i>
                            <tr>
                                {{--<td style="vertical-align: top;"><div id="tdCN" class="scrollable"><label id="lblCN"></label></div></td>
                                <td style="vertical-align: top;"><div id="tdCT" class="scrollable"><label id="lblCT"></label></div></td>
                                <td style="vertical-align: top;"><div id="tdR1" class="scrollable"><label id="lblR1"></label></div></td>
                                <td style="vertical-align: top;"><div id="tdR2" class="scrollable"><label id="lblR2"></label></div></td>
                                <td style="vertical-align: top;"><div id="tdR3" class="scrollable"><label id="lblR3"></label></div></td>
                                <td style="vertical-align: top;"><div id="tdR4" class="scrollable"><label id="lblR4"></label></div></td>
                                <td style="vertical-align: top;"><div id="tdLC" class="scrollable"><label id="lblLC"></label></div></td>--}}
                                
                                <td style="vertical-align: top;"><div id="tdCN" class="scrollable"></td>
                                <td style="vertical-align: top;"><div id="tdCT" class="scrollable"></td>
                                <td style="vertical-align: top;"><div id="tdR1" class="scrollable"></td>
                                <td style="vertical-align: top;"><div id="tdR2" class="scrollable"></td>
                                <td style="vertical-align: top;"><div id="tdR3" class="scrollable"></td>
                                <td style="vertical-align: top;"><div id="tdR4" class="scrollable"></td>
                                <td style="vertical-align: top;"><div id="tdLC" class="scrollable"></td>
                            </tr>
                           
                        </tbody>
                    </table>
                    
                </div>
            </div>
            
            <div class="form-group subject">
                <!--<label class="control-label">Select Interest</label>-->
                <select name='subjectId' data-select2-width="200" class="">
                    <option value='' >Select Interest</option>
                    @foreach($subjects as $item)
                        <option value="{{$item->subjectId}}" >{{$item->description}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <!--<label class="control-label">Enter Matter</label>-->
                <input id="tags" type="text" class="form-control" placeholder="Enter Tags separated by comma" name="tags">
            </div>
            <div class="form-group">
                {{--<label class="control-label">Title</label>--}}
                <input type="text" class="form-control" placeholder = "Title" name="title">
            </div>
            <div class="form-group">
                {{--<!--<label class="control-label">Enter Matter</label>-->--}}
                <textarea class="form-control" rows="3" placeholder="Write something to share" name="description"></textarea>
            </div>
            <div class="form-group">
                <input type="checkbox" id="isPrivate" name="isPrivate"><label for="hide-post"><span style="display: inline-block; position: relative; top: -1px; left: 2px;">Hide this post to news feed</span></label>
            </div>


            {{--Youtube Videos--}}
            {{--******************** --}}
            <div class="form-group attachment youtube hidden">
                <!--<label class="control-label">Title</label>-->
                <input type="hidden" class="form-control" name="youtubeId" id="selected-youtube-id">
                <input type="hidden" class="form-control" name="youtubeImage" id="selected-youtube-id-image">
                <div class="youtube-selected-video center" style="display: block;height:180px;">
                </div>
            </div>


            {{--DROP ZONE--}}
            {{--******************** --}}
            <div class="dropzone dropzonePost attachment hidden">
                <div id="dropzoneError" class="alert alert-danger hidden"></div>
                <div class="dz-default dz-message">
                    <span>
                        <span class="bigger-150 bolder"><i class="ace-icon fa fa-caret-right red"></i> Drop files</span> to upload
                        <span class="smaller-80 grey">(or click here)</span> <br>
                        <i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>
                    </span>
                </div>

            </div>

        </div>
    </form>

    <div style="margin-top:20px;margin-bottom:0">
        <span style="font-weight: bold;padding-right:10px;">Attach</span>
        <span>
            <button class="btn btn-xs btn-white btn-round btn-info2" onclick="showHideField('dropzone')"><i class="blue fa fa-camera" ></i> Photos</button>
            <button class="btn btn-xs btn-white btn-round btn-info2" onclick="showHideField('youtube')"><i class="red fa fa-youtube-square bigger-150" ></i> Youtube Video</button>
        </span>
    </div>

</div>


@include('user.post._dropzonePost-js')
@include('user.post._post-js')

<script>
    $('#tags').selectize({
        delimiter: ',',
        persist: false,
        create: function(input) {
            return {
                value: input,
                text: input
            }
        }
    });
    
    $('#create-post-location').on('change',function(){
        var _visible = !isNaN($(this).val());
        if(_visible) {
            $('.form-group.subject').removeClass('hidden');
        } else{
            $('.form-group.subject').addClass('hidden');
        }
    });
</script>

@include('layouts.partials._geoCheckboxFunction-js')
@include('layouts.partials._geoCheckboxLoad-js')
@include('layouts.partials._geoCheckboxChange-js')