<?php

?>
<div class="panel panel-body" style="padding:0">
    <form id="frmCreatePost" action="{{URL::route('storePost')}}" enctype="multipart/form-data" >
        <ul class="nav nav-tabs">
            <li class="active"><a href="#share-location-tab" data-toggle="tab">Share Something</a></li>
            
            {{--<li><a href="#share-group-tab" data-toggle="tab" > Share within Group</a></li>--}}
        </ul>
        <div class="tab-content tab-color-blue">

            <div class="tab-pane active" id="share-location-tab">
        
                <div class="form-group">
                    <!--<label class="control-label">Select Interest</label>-->
                    <select id="create-post-location" name='locationId' data-select2-width="100%" data-selected-id="{{Session::get('selectedLocationId')}}" class="select2-location">
                        <option value='' >Select Location</option>
                        <optgroup label="Locations">
                            @foreach($enrolledLocations as $item)
                                <option value="{{$item->locationId}}" >{{$item->name}}</option>
                            @endforeach
                        </optgroup>
    
                        <optgroup label="Groups">
                             @foreach($groups as $item)
                                 <option value="{{$item->groupId}}" >{{$item->name}}</option>
                             @endforeach
                        </optgroup>
                    </select>
                </div>
            
            
                <div class="form-group subject">
                    <!--<label class="control-label">Select Interest</label>-->
                    <select name='subjectId' data-select2-width="200" class="">
                        <option value='' >Select Interest</option>
                        @foreach($subjects as $item)
                            <option value="{{$item->subjectId}}" >{{$item->description}}</option>
                        @endforeach
                    </select>
                </div>
            </div>  

            <div class="form-group">
                <!--<label class="control-label">Enter Matter</label>-->
                <input id="tags" type="text" class="form-control" placeholder="Enter Tags separated by comma" name="tags">
            </div>
            <div class="form-group">
                {{--<label class="control-label">Title</label>--}}
                <input type="text" class="form-control" placeholder = "Title" name="title">
            </div>
            <div class="form-group">
                {{--<!--<label class="control-label">Enter Matter</label>-->--}}
                <textarea class="form-control" rows="3" placeholder="Write something to share" name="description"></textarea>
            </div>
            <div class="form-group">
                <input type="checkbox" id="isPrivate" name="isPrivate"><label for="hide-post"><span style="display: inline-block; position: relative; top: -1px; left: 2px;">Hide this post to news feed</span></label>
            </div>


            {{--Youtube Videos--}}
            {{--******************** --}}
            <div class="form-group attachment youtube hidden">
                <!--<label class="control-label">Title</label>-->
                <input type="hidden" class="form-control" name="youtubeId" id="selected-youtube-id">
                <input type="hidden" class="form-control" name="youtubeImage" id="selected-youtube-id-image">
                <div class="youtube-selected-video center" style="display: block;height:180px;">
                </div>
            </div>


            {{--DROP ZONE--}}
            {{--******************** --}}
            <div class="dropzone dropzonePost attachment hidden">
                <div id="dropzoneError" class="alert alert-danger hidden"></div>
                <div class="dz-default dz-message">
                    <span>
                        <span class="bigger-150 bolder"><i class="ace-icon fa fa-caret-right red"></i> Drop files</span> to upload
                        <span class="smaller-80 grey">(or click here)</span> <br>
                        <i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>
                    </span>
                </div>

            </div>

        </div>
    </form>

    <div style="margin-top:20px;margin-bottom:0">
        <span style="font-weight: bold;padding-right:10px;">Attach</span>
        <span>
            <button class="btn btn-xs btn-white btn-round btn-info2" onclick="showHideField('dropzone')"><i class="blue fa fa-camera" ></i> Photos</button>
            <button class="btn btn-xs btn-white btn-round btn-info2" onclick="showHideField('youtube')"><i class="red fa fa-youtube-square bigger-150" ></i> Youtube Video</button>
        </span>
    </div>

</div>



@include('user.post._dropzonePost-js')
@include('user.post._post-js')

<script>
    $('#tags').selectize({
        delimiter: ',',
        persist: false,
        create: function(input) {
            return {
                value: input,
                text: input
            }
        }
    });

    $('#create-post-location').on('change',function(){
        var _visible = !isNaN($(this).val());
        if(_visible) {
            $('.form-group.subject').removeClass('hidden');
        } else{
            $('.form-group.subject').addClass('hidden');
        }
    });
</script>
