<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/23/2015
 * Time: 12:32 AM
 *
 */

 // Required variable :  $total , $postId, $comments
 ?>

    <div id="commentTemplate" class="hidden">
        <div class="itemdiv commentdiv post-comment" >
            <div class="user" >
                <img src="" alt="Avatar" style="border-radius: 0 !important; width:32px; height:32px">
            </div>

            <div class="body" style="margin-left:35px">

                <div class="name">
                    <a href="#">%username%</a>
                    <i class="ace-icon fa fa-clock-o"></i>
                    <span class="time green">%time%</span>
                </div>

                <div class="text">%message%</div>

                <div class="tools" style="top:0; padding-left:50px;" >
                    <div class="action-buttons bigger-125">
                        <a href="javascript:void(0)" class="replyComment" title="Reply Comment" data-postId="{{$postId}}" >
                            <i class="ace-icon fa fa-reply blue"></i>
                        </a>
                        <a href="javascript:void(0)" class="deleteComment" title="Delete Comment" data-id="%id%">
                            <i class="ace-icon fa fa-trash-o red"></i>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="comments post-comments" style="padding-left:15px;">
        @if($total > 1)
            <div style="margin-bottom:10px;">
                <i class="ace-icon fa fa-comments-o fa-2x green middle"></i>&nbsp;
                <a class="btn btn-minier btn-white btn-info" target="_blank" href="{{URL::route('showPost', $postId)}}">
                    {{$total - 1 . ' comments' }} &nbsp;
                    <i class="ace-icon fa fa-arrow-right"></i>
                </a>
            </div>
        @endif

        @foreach($comments as $item)

            <div class="itemdiv commentdiv post-comment{{$item->parentId != 0 ? ' child' : ''}}" data-commentId = "{{$item->commentId}}" data-parentId="{{$item->parentId}}" >

                <div class="user" >
                    <img src="{{URL::route('userAvatar',$item->userId)}}" alt="Avatar" style="border-radius: 0 !important; width:32px; height:32px">
                </div>
                <div class="body">
                    <div class="name">
                        <a href="#">{{ User::getUsername($item->userId) }}</a>
                        <i class="ace-icon fa fa-clock-o"></i>
                        <span class="time green">{{ \Carbon\Carbon::createFromTimestamp(strtotime($item->created_at))->diffForHumans() }}</span>
                    </div>
                    <div class="text">
                        {{ $item->message }}
                    </div>
                    <div class="tools" style="top:0; padding-left:50px; height:30px" >
                        <div class="action-buttons bigger-125">
                            @if(Auth::check())
                                <a href="javascript:void(0)" class="replyComment" title="Reply Comment" data-postId="{{$postId}}" data-id="{{$item->commentId}}" >
                                    <i class="ace-icon fa fa-reply blue"></i>
                                </a>

                                @if($item->userId == Auth::user()->userId)
                                    <a href="javascript:void(0)" class="deleteComment" title="Delete Comment" data-id="{{$item->commentId}}">
                                        <i class="ace-icon fa fa-trash-o red"></i>
                                    </a>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>

            </div>
        @endforeach

    </div>

    @if(!Auth::check())
        <div>
            <a class="btn btn-white btn-round btn-success btn-minier" href="/">Log in or Sign Up</a> to add comments.
        </div>
    @else
        <div >
            <div class="post-avatar avatar-user">
                <img src="{{ URL::route('userAvatar', Auth::user()->userId) }}" alt="avatar">
            </div>
            <div >
                <form class="commentForm" onsubmit="return false;" >
                    <div class="input-group" >
                        {{Form::token()}}
                        <input type="hidden" name="postId" value="{{$postId}}" >
                        <input type="text" name="message" class="form-control" placeholder="Write your comment here ..." autocomplete="off">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-white btn-purple">
                                <i class="submit ace-icon fa fa-share"></i>
                            </button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    @endif

    <script>
        {{-- Move all child comments to parent comment --}}
        $(function(){
            $('.child.post-comment').each(function(){
                var _parentId = $(this).attr('data-parentId');
                var _parent = $('[data-commentId="'+ _parentId +'"]');

                if ( $(_parent).length != 0 ) {
                    var _parentMargin = Number($(_parent).css('margin-left').replace('px','')) + 10 ;
                    _parent.append($(this));
                    $(this).css('margin-left', _parentMargin + 'px');
                }
                
            });
        });
    </script>