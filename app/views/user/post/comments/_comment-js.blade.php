<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/23/2015
 * Time: 8:54 AM
 *
 */ ?>

 <script>
    $('body').on('click','.deleteComment',function(){
        var id = ($(this).attr('data-id'));
        var _html = '<form id="frmDeleteComment" action="{{ URL::route('deleteComment') }}"><input type="hidden" name="commentId" value="' + id + '"> </form>' +
                    'Are you sure you want to delete this comment?';

        $('.post-comment').removeClass('active');
        $(this).closest('.post-comment').addClass('active');

        messageBox ('deleteCommentDialog', _html,'Delete Confirmation','Delete Comment','', BootstrapDialog.TYPE_DANGER).setSize(BootstrapDialog.SIZE_SMALL);
    });

    function deleteCommentDialog_onButtonClick(dialogRef){
        postMyData('frmDeleteComment', dialogRef).then(function(){
            $('.post-comment.active').remove();
            dialogRef.close();
        })
    }

 </script>

    {{--Reply Comment--}}
<script>
    $('body').on('click', '.replyComment', function(){
        var _body = $(this).closest('div.body').find('.text');

        if($(_body).find('[data-function="replycomment"]').length != 0) {
            console.log('reply comment already added');
            return;
        }

        var _div = $('<div class="commentReply">');
        var _frm = $('<form class="commentForm">');

            _frm.append('<input type="hidden" name="_token" value="{{csrf_token()}}">');
            _frm.append($('<input type="hidden" name="postId">').val($(this).attr('data-postId')) );
            _frm.append($('<input type="hidden" name="parentId">').val($(this).attr('data-id')) );
            _frm.append($('<div class="input-group">')
                    .append($('<input type="text" name="message" class="form-control" placeholder="Write your reply comment here" autocomplete="off" autofocus data-function="replycomment" >'))
                    .append($('<span class="input-group-btn">')
                        .append($('<button type="submit" class="btn btn-white btn-purple">')
                            .append('<i class="submit ace-icon fa fa-share"> ')
                        )
                    )
            );

        _div.append(_frm);
        _body.append(_div);
        console.log('reply click');
    });

</script>