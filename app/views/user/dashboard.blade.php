<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 01/31/2015
 * Time: 7:17 PM
 *
 */

?>

@extends('layouts.main')

@section('title')
    @if(isset($selectedLocation))
        - {{$selectedLocation->locality}}
    @endif
@stop

@section('pageContent')
    @include ('layouts.partials.breadcrumbs')
    <div style="display: block;padding-top: 30px;"></div>
    @if(isset($interest))
    <div style="display: block;padding-top: 39px;"></div>
    <div class="breadcrumbs" style="background: rgba(16, 16, 16, 0.8); margin-top: -43px;height: 38px;">
        <!-- <div class="page-header"> -->
            <h1 style="color: #FFFFFF; line-height: 0px; font-size: 25px; margin-top: 14px; font-family: Britannic Bold; font-weight: 600;">
                {{$interest}}
                <small style="color: #FFF;"><i class="ace-icon fa fa-angle-double-right"></i></small>
            </h1>
        <!-- </div> -->
    </div>
    @endif

    @include ('layouts.partials.flashmessage')

    @if(Auth::check())
        {{--<div class="panel" style="padding:5px 10px 5px 10px; background-color:#f5f5f5">--}}
            {{--<div>--}}
                {{--<div class="center" style="display:inline-block; width:100px;vertical-align: top;" >--}}
                    {{--<a href="javascript:void(0)" onclick="createPost()" style="text-decoration: none;">--}}
                        {{--<div class="btn btn-success btn-xs btn-white" style="border-radius: 100%" ><i class="ace-icon fa fa-pencil fa-3x"></i></div>--}}
                        {{--<div>Write New Post</div>--}}
                    {{--</a>--}}

                {{--</div>--}}
                {{--<div class="center" style="display:inline-block; width:100px;">--}}
                    {{--<a href="javascript:void(0)" onclick="createAnnouncement()" style="text-decoration: none;">--}}
                        {{--<div class="btn btn-info btn-xs btn-white" style="border-radius: 100%"><i class="glyphicon glyphicon-bullhorn bigger-270"></i></div>--}}
                        {{--<div>Announcement</div>--}}
                    {{--</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    @endif

    @if($posts)

        {{$posts}}

    @endif

@stop

@section('scripts')
    @if(Auth::check())
        @include('user.post.comments._comment-js')
    @endif
@stop