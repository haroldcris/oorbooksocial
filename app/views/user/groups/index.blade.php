<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/03/2015
 * Time: 11:00 AM
 *
 */

    $rowTemplate = <<<HEREDOC
<tr>
   <td><div><span class="group-name" style="font-weight: bold" id="item@id">@name</span></div>
       <div><span class="label label-sm label-info arrowed-right" >Members: @members</span></div>
   </td>
   <td width="25%">
       <div class="btn-group">
           <button class="btn btn-xs btn-success btn-white btn-round btn-control" onclick="editUserGroup(@id)" >
               <i class="ace-icon fa fa-edit green"></i>
               Modify
           </button>
           <button class="btn btn-xs btn-danger btn-white btn-round btn-control" onclick="deleteUserGroup(@id)" >
               <i class="ace-icon fa fa-times bigger-110 red2"></i>
               Delete
           </button>
       </div>
   </td>
</tr>
HEREDOC;

    // normalize newlines
    $rowTemplate = preg_replace('/(\r\n|\r|\n)+/', " ", $rowTemplate);
    // replace whitespace characters with a single space
    $rowTemplate = preg_replace('/\s+/', ' ', $rowTemplate);
?>

@extends('layouts.main')

@section('title')
- Groups
@stop


@section ('pageContent')
    <div class="widget-box" id="user-groups">
        <div class="widget-header">
            <h5 class="widget-title bigger lighter">
                <i class="ace-icon fa fa-group"></i>
                Manage Groups
            </h5>
            <div class="widget-toolbar no-border">
                <button class="btn btn-round btn-white btn-success btn-xs" onclick="addUserGroup()">&nbsp;&nbsp;<i class="fa fa-plus"></i> Create New&nbsp;&nbsp;</button>
            </div>
        </div>

        <div class="widget-body">
            <div class="widget-main no-padding">
                <table class="table table-striped table-bordered table-hover">
                    <tbody>
                        @foreach($groups as $item)
                           <tr>
                              <td>
                                    <div>
                                        <a href="{{URL::route('manageSingleGroup',$item->groupId)}}" class="group-name" style="font-weight: bold" id="item{{$item->groupId}}">{{$item->name}}</a>
                                        @if($item->isPrivate)
                                            <span><i>private</i></span>
                                        @endif
                                    </div>
                                  <div><span class="label label-sm label-info arrowed-right" >Members: {{$item->totalMembers}}</span></div>
                              </td>
                              <td width="40%">
                                  <div class="btn-group">
                                      <a href="{{URL::route('manageSingleGroup',$item->groupId)}}" class="btn btn-xs btn-info btn-white btn-round btn-control" >
                                            <i class="ace-icon fa fa-cog bigger-110 blue"></i>
                                            Manage
                                      </a>
                                      <button class="btn btn-xs btn-success btn-white btn-round btn-control" onclick="addUserGroup({{$item->groupId}})" >
                                          <i class="ace-icon fa fa-edit green"></i>
                                          Modify
                                      </button>
                                      <button class="btn btn-xs btn-danger btn-white btn-round btn-control" onclick="deleteUserGroup({{$item->groupId}})" >
                                          <i class="ace-icon fa fa-times bigger-110 red2"></i>
                                          Delete
                                      </button>
                                  </div>
                              </td>
                           </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@stop



@section('scripts')
    <script>
        var rowTemplate = '{{ $rowTemplate }}';

        function addUserGroup(id)
        {
           var dialog = messageBox('frmCreateUserGroupDialog','{{Config::get('site.loader')}}','Group','Create');
               dialog.setSize(BootstrapDialog.SIZE_SMALL);

           var url = '{{URL::route('createGroup')}}';
           if (id != undefined) { url += '?id=' + id; }
           $.get( url )
                .done(function(html){
                    dialog.getModalBody().html(html);
                    if(id != undefined) {
                        dialog.getButton('dialogOkBtn').text('Update');
                    }
                });
        }

        function frmCreateUserGroupDialog_onButtonClick(dialogRef)
        {
            postMyData('frmCreateUserGroup', dialogRef).then(
                function(json){
                    dialogRef.close();
                    location.reload();
                }
            )
        }

        $('#user-groups').on('click', '.btn-control', function(){
            $(this).closest('table').find('tr').removeClass('active');
            $(this).closest('tr').addClass('active');
        });
    </script>

    <script>
        //$('#user-groups').on('click', '.btn-delete', function(){
        function deleteUserGroup(id)
        {
            var  _html =  'You are about to delete this user group permanently.<br><br>';
                 _html += '<span style="font-weight:bold;">' + $('#item' + id).html() + '</span><br><br>';
                 _html += 'Do you want to continue?';
                 _html += '<form id="frmDeleteUserGroup" action="{{URL::route('apiDeleteGroup', Session::get('selectedCalendarEventLocationId'))}}"><input type="hidden" name="_method" value="delete" ><input type="hidden" name="id" value="' + id + '" /> </form>';

            var dialog = messageBox ('frmDeleteUserGroupDialog', _html,'Confirmation','Delete','', BootstrapDialog.TYPE_DANGER);
                dialog.setSize(BootstrapDialog.SIZE_SMALL);
        }

        function frmDeleteUserGroupDialog_onButtonClick(dialogRef)
        {
            postMyData('frmDeleteUserGroup', dialogRef).then(
                function() {
                    //$('#user-groups').find('tr.active').remove();
                    location.reload();
                    dialogRef.close();
                }
            );
        }
    </script>


@stop
