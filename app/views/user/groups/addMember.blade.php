<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/08/2015
 * Time: 7:03 AM
 *
 */ ?>




<div class="widget-box">
	<div class="widget-header">
		<h4 class="widget-title lighter smaller" >
			Add Member
		</h4>
		<div class="widget-toolbar no-border">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#search-tab" data-toggle="tab" aria-expanded="false">Search</a></li>
			</ul>
		</div>
	</div>
	<div class="widget-body">
		<div class="tab-content">

			{{--//Search Youtube --}}

			<div class="tab-pane active" id="search-tab">
				<form id="frmAddMemberSearch">
					{{--<div style="margin-bottom: 10px;font-weight: bold">Type your search or YouTube Url in the box to find videos and click the <i class="fa fa-search bigger-150"></i> icon.</div>--}}
					<div class="input-group">
						<input type="text" id="q" name="q" class="form-control" placeholder="Enter member username" autofocus autocomplete="off" onkeyup="searchForUser(this,'#add-member-search-result', searchGroupAddMember )">
						<input type="text" name='dummy' style="display: none" >
                        <span class="input-group-btn">
                            <span class="btn btn-white  btn-info">
	                            <i class="ace-icon fa fa-search bigger-120"></i>
                            </span>
                        </span>

					</div>
				</form>

				<div class="row">
					<div class="col-md-12 col-sm-12" id="add-member-search-result" style="margin-top:20px">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


