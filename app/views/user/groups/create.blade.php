<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/03/2015
 * Time: 1:32 PM
 *
 */
    $name = null;
    if (isset($group)) {
        $name = $group->name;
    }
 ?>

<form id="frmCreateUserGroup" action="{{URL::route('storeGroup')}}" class="form-inline form-horizontal" >
    {{--<div class="field">--}}
        <label class="control-label col-md-3" for="title">Name:</label>
        <div class="input-group col-md-9">
            <input class="form-control" type="text" name="name" placeholder="Type the name of your group" value="{{$name}}" required autofocus/>
            <input type="text" name="StackOverflow1370021" value="Fix IE bug" style="display:none" />

            @if(isset($group))
                <input type="hidden" name="id" value="{{$group->groupId}}">
            @endif
        </div>
        <div class="space-2"></div>
        <label class="control-label col-md-9">
            <input type="checkbox" name="private" @if(isset($group)) @if($group->isPrivate) checked @endif @endif  >
            This is a private group
        </label>



        <span class="help-block col-md-offset-3"></span>
    {{--</div>--}}
</form>