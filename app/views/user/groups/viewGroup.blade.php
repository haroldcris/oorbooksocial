<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 01/31/2015
 * Time: 7:17 PM
 *
 */

    //dd($data);
?>

@extends('layouts.main')

@section('title')
@stop

@section('pageContent')

    @include('layouts.partials.breadcrumbs')
    <div style="display: block;padding-top:30px;"></div>
    <div class="widget-box" id="user-groups">
        <div class="widget-header">
            <h5 class="widget-title bigger lighter">
                <i class="ace-icon fa fa-table"></i>
                {{$data->groupInfo->name}}
            </h5>
            <div class="widget-toolbar no-border">
                 @if($data->groupInfo->owner->userId == Auth::user()->userId)
                    <button class="btn btn-round btn-white btn-success btn-xs" onclick="addMember()" >&nbsp;&nbsp;<i class="fa fa-plus"></i> Add &nbsp;&nbsp;&nbsp; </button>
                 @else
                    @if($data->currentUserMembership == null)
                        @if($data->groupInfo->isPrivate == 0)
                            <button class="btn btn-round btn-white btn-success btn-xs btn-join-group" onclick="joinGroup(this)">&nbsp;&nbsp;<i class="fa fa-plus"></i> Join this Group &nbsp;&nbsp;&nbsp; </button>
                        @else
                            <i>Private Group</i>
                        @endif
                    @else
                        <button class="btn btn-round btn-white btn-danger btn-xs " onclick="leaveFromGroup({{$data->groupInfo->groupId}},true)">&nbsp;&nbsp;<i class="fa fa-minus"></i> Leave Group &nbsp;&nbsp;&nbsp; </button>
                    @endif
                 @endif


            </div>
        </div>

        <div class="widget-body">
            <div class="widget-main padding-10">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#member-tab">Members</a>
                        </li>
                    </ul>
                    <div class="tab-content padding-8">
                        <div id="member-tab" class="tab-pane active">
                            <div class="clearfix">

                                @foreach($data->members as $item)
                                    <div class="itemdiv memberdiv" id="groupMember{{$item->userId}}">
                                        <div class="user">
                                            <img alt="{{$item->userId}}" src="{{URL::route('userAvatar', $item->userId)}}" />
                                        </div>

                                        <div class="body">
                                            <div class="name">
                                                <a target="_blank" href="{{URL::route('guestUserProfile', $item->username)}}">{{$item->username}}</a>
                                            </div>

                                            <div class="time">
                                                <i class="ace-icon fa fa-clock-o"></i>
                                                <span class="green">{{ \Carbon\Carbon::createFromTimestamp( strtotime($item->membershipDate) )->diffForHumans() }}</span>
                                            </div>

                                            <div>
                                                {{--<span class="label label-warning label-sm">pending</span>--}}

                                                <div class="inline position-relative">
                                                    @if($data->groupInfo->owner->userId == Auth::user()->userId)
                                                        @if ($data->groupInfo->owner->userId != $item->userId)
                                                            <button class="btn btn-minier btn-yellow btn-no-border dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                                                <i class="ace-icon fa fa-angle-down icon-only bigger-120"></i>
                                                            </button>

                                                            <ul class="dropdown-menu dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                                            {{--<li>--}}
                                                                {{--<a href="#" class="tooltip-success" data-rel="tooltip" title="Approve">--}}
                                                                    {{--<span class="green">--}}
                                                                        {{--<i class="ace-icon fa fa-check bigger-110"></i>--}}
                                                                    {{--</span>--}}
                                                                {{--</a>--}}
                                                            {{--</li>--}}

                                                            {{--<li>--}}
                                                                {{--<a href="#" class="tooltip-warning" data-rel="tooltip" title="Reject">--}}
                                                                    {{--<span class="orange">--}}
                                                                        {{--<i class="ace-icon fa fa-times bigger-110"></i>--}}
                                                                    {{--</span>--}}
                                                                {{--</a>--}}
                                                            {{--</li>--}}

                                                            <li>
                                                                <a href="javascript:void(0);" class="tooltip-error" data-rel="tooltip" title="Remove from this Group" onclick="removeFromGroup({{$item->userId}})">
                                                                    <span class="red">
                                                                        <i class="ace-icon fa fa-trash-o bigger-110"></i> Remove from Group
                                                                    </span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                        @endif
                                                    @endif
                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                @endforeach
                            </div>

                            <div class="space-4"></div>

                            {{--<div class="center">--}}
                                {{--<i class="ace-icon fa fa-users fa-2x green middle"></i>--}}
                                {{--&nbsp;--}}
                                {{--<a href="#" class="btn btn-sm btn-white btn-info">--}}
                                    {{--See all members &nbsp;--}}
                                    {{--<i class="ace-icon fa fa-arrow-right"></i>--}}
                                {{--</a>--}}
                            {{--</div>--}}

                            <div class="hr hr-double hr8"></div>
                        </div><!-- /.#member-tab -->

                    </div>

            </div>
        </div>
    </div>



@stop

@section('scripts')

    {{--Add Member    --}}
    <script>
        function addMember()
        {
            messageBoxRemote('frmAddGroupMember','{{URL::route('addGroupMember')}}','','Done');
        }

        function frmAddGroupMember_onButtonClick(dialogRef)
        {
            dialogRef.close();
            location.reload();
        }

        function addToGroup(userId)
        {
            $('#btn'+userId).loaderIcon('show');
            $.post('{{URL::route('apiGroupAddMember', $data->groupInfo->groupId)}}',{
                    '_token' : '{{csrf_token()}}',
                    'userId': userId }).done(
                function(json){
                    $('#btn'+userId).loaderIcon('hide');
                    if (json.success == 'true') {
                        $('#btn' + userId).text('Added');
                    }
                }
            )

        }

         function searchGroupAddMember(ctrl)
        {
            var _url = "/api/v1/users?limit=10&username=" + $(ctrl).val();
            $.get(_url).done(function(json){
                $('#add-member-search-result').html('');
                for(var i =0 ; i < json.length; i++){
                    if(json[i].type =='user') {
                        var divContainer = $('<div class="itemdiv memberdiv" >'),
                                divHead = $('<div class="user">'),
                                divBody = $('<div class="body">');

                                divHead.append(
                                    $('<img alt="avatar">').attr('src', json[i].imageAvatar)
                                );

                                divBody.append(
                                    $('<div class="name">').append(
                                        $('<span>').append(json[i].username)
                                    )
                                );

                                var btn = $('<button class="btn btn-minier btn-success btn-white">')
                                            .attr('id', 'btn' + json[i].userId)
                                            .attr('data-id',json[i].userId)
                                            .append($('<i class="ace-icon fa fa-plus bigger-120">'))
                                            .append(' Add')
                                            .bind('click', function() {addToGroup($(this).attr('data-id'))} );

                                divBody.append(
                                    $('<div>').append(
                                        $('<div class="inline position-relative">').append(btn)
                                    )
                                );

                                divContainer.append(divHead);
                                divContainer.append(divBody);
                        $('#add-member-search-result').append(divContainer);
                    }
                }


            });
        }

    </script>

    {{--Remove from Group--}}
    <script>
        function removeFromGroup(userId)
        {
            $('.itemdiv.memberdiv').removeClass('active');
            $('#groupMember' + userId).addClass('active');

            var _html = '<form id="frmRemoveFromGroup" action="{{ URL::route('apiGroupDeleteMember',$data->groupInfo->groupId) }}"><input type="hidden" name="_method" value="delete" > <input type="hidden" name="userId" value="' + userId + '"> </form>' +
                        'Are you sure you want to remove this user from this group?';

            messageBox ('frmRemoveFromGroupDialog',_html,'Group','Remove','Cancel',BootstrapDialog.TYPE_DANGER).setSize(BootstrapDialog.SIZE_SMALL);
        }

        function frmRemoveFromGroupDialog_onButtonClick(dialogRef)
        {
            postMyData('frmRemoveFromGroup',dialogRef).then(
                function(json){
                     $('.itemdiv.memberdiv.active').remove();
                    dialogRef.close();
                }
            )
        }
    </script>

    @include('user.groups._groups-js')
@stop