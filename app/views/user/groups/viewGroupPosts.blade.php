<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/08/2015
 * Time: 10:35 AM
 *
 */
 ?>


@extends('layouts.main')

@section('title')
    - {{$selectedGroup->name}}
@stop

@section('pageContent')
    @include ('layouts.partials.flashmessage')
    @include('layouts.partials.breadcrumbs')
    <div style="display: block;padding-top:30px;"></div>
    <div class="page-header">
        <h1>
             @if($selectedGroup->creatorId != Auth::user()->userId)
                @if(is_null(Auth::user()->getGroupSubscriptionDate($selectedGroup->groupId)))
                    <button class="btn btn-round btn-white btn-success btn-xs btn-join-group pull-right" onclick="joinGroup(this)">&nbsp;&nbsp;<i class="fa fa-plus"></i> Join this Group &nbsp;&nbsp;&nbsp; </button>
                @endif
             @endif

            {{$selectedGroup->name}}
            <small><i class="ace-icon fa fa-angle-double-right"></i></small>
        </h1>

    </div>


    @if(Auth::check())
        {{--<div class="panel" style="padding:5px 10px 5px 10px; background-color:#f5f5f5">--}}
            {{--<div>--}}
                {{--<div class="center" style="display:inline-block; width:100px;vertical-align: top;" >--}}
                    {{--<a href="javascript:void(0)" onclick="createPost()" style="text-decoration: none;">--}}
                        {{--<div class="btn btn-success btn-xs btn-white" style="border-radius: 100%" ><i class="ace-icon fa fa-pencil fa-3x"></i></div>--}}
                        {{--<div>Write New Post</div>--}}
                    {{--</a>--}}

                {{--</div>--}}
                {{--<div class="center" style="display:inline-block; width:100px;">--}}
                    {{--<a href="javascript:void(0)" onclick="createAnnouncement()" style="text-decoration: none;">--}}
                        {{--<div class="btn btn-info btn-xs btn-white" style="border-radius: 100%"><i class="glyphicon glyphicon-bullhorn bigger-270"></i></div>--}}
                        {{--<div>Announcement</div>--}}
                    {{--</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    @endif

    @if($posts)

        {{$posts}}

    @endif

@stop

@section('scripts')
    @if(Auth::check())
        @include('user.post.comments._comment-js')

        @include('user.groups._groups-js')
    @endif
@stop