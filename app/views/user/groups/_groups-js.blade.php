<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/09/2015
 * Time: 12:33 AM
 *
 */ ?>

{{--Join Group --}}
<script>
    function joinGroup()
    {
        var _url;
        @if(isset($data))
            _url = "{{URL::route('apiGroupJoin', $data->groupInfo->groupId)}}";
        @else
            _url = "{{URL::route('apiGroupJoin', $selectedGroup->groupId)}}";
        @endif

        $('.btn-join-group').loaderIcon('show');

        $.post(_url)
            .done(function(data){
                if(data.success == 'true'){
                    location.reload();
                } else{
                    alert(data.error);
                }
            })
            .always(function(){
                $('.btn-join-group').loaderIcon('hide');
            });
    }
</script>
