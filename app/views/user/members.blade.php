@extends('layouts.main')

@section('title')
- Members
@stop

@section('pageContent')

    @if (!is_null($selectedLocation))
        @include('layouts.partials.breadcrumbs')
    @endif

    <div class="row" style="width: 700px; margin: 30px 0px 0px 0px; background: white; padding-bottom: 40px;">
        <div class="col-md-12">
            <h2 style="font-weight: 600; color: #5E5E5E; border-bottom: 1px solid #E5E5E5; font-size: 20px;">Members</h2>
        </div>
        <div class="col-md-12" style="padding: 15px;">
        @if(count($selectedLocation->members)==0)
            <div class="col-md-12">
                <div class="text-center">{{ 'No members yet.' }}</div>
            </div>
        @else
            @foreach($selectedLocation->members as $item)
                <div class="col-md-6" style="border: 1px solid rgb(226, 226, 226); padding: 10px;">
                    <div class="hide">{{ $item->userId }}</div>
                    <div class="col-md-3 text-center avatar" style="padding: 0px;"><img src="{{ URL::route('userAvatar', $item->userId) }}" style="width: 50px; height: 50px; border: 1px solid black;"></div>
                    <div class="col-md-9" style="padding-left: 0px; padding-top: 5px; font-weight: bolder; font-size: 13px;"><a href="/profile/{{$item->username}}">{{ $item->username }}</a></div>
                    <div class="col-md-9" style="padding-left: 0px; padding-top: 3px; letter-spacing: 1px; font-size: 11px;">{{ ucwords($item->sex) }}</div>
                </div>
            @endforeach
        @endif
        
        </div>
    </div>
@stop