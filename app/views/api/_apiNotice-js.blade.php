<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/10/2015
 * Time: 8:38 PM
 *
 */

 ?>

 <script>
    // Add API Credential
    function createApiCredential(){
        dialog = messageBox('addApi','Loading content... <i class="fa fa-spinner fa-spin"></i>','API Key', ' Create API Key ');

        $.get("{{URL::route('createNoticeKey')}}").done(function(d){
            dialog.getModalBody().html(d);
            //$('.select2Control').select2({width:'auto'});
        });
        
        
    }

    function addApi_onButtonClick(_dialog){
        postMyData('createAPI',_dialog).then(function(){
            _dialog.close();
            window.location.reload();
        })
    }

 </script>


 <script>
    // Delete
    function deleteApiCredential($id){
        var _html,
            apiKey = $('.apikeyid' + $id + ' .apikey').text();

        $('.apirow').removeClass('active');
        $('.apikeyid' + $id ).addClass('active');


        _html =  'You are about to delete the following API Key.<br><br>';
        _html += '<div><table class="table table-bordered "><tbody><tr><td>' + apiKey + '</td></tr><tbody></table></div>';
        _html += 'Do you want to continue?';

        _html += '<form id="deleteAPI" action="{{URL::route('deleteNoticeKey')}}"><input type="hidden" name="apiId" value="' + $id + '"></form>';

        messageBox('deleteApi',_html,'Confirmation','Delete API Key', '',BootstrapDialog.TYPE_DANGER );
    }

    function deleteApi_onButtonClick(_dialog){
        postMyData('deleteAPI',_dialog).then(function(){
            $('.apirow.active').closest('tr').remove();
            _dialog.close();
        },function(error){
            console.log(error.message);
        });

    }

 </script>