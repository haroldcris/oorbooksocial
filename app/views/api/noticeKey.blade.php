<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/10/2015
 * Time: 7:13 PM
 *
 */
?>

@extends('layouts.mainNoAds')

@section('pageContent')


    <div class="widget-box" style="width: 700px;">
        <div class="widget-header" style="width: 686px;">
            <h5 class="widget-title bigger lighter">
                <i class="glyphicon glyphicon-star"></i>
                API Keys
            </h5>
            <div class="widget-toolbar no-border">
                <button class="btn btn-round btn-white btn-success btn-xs " onclick="createApiCredential()" >&nbsp;&nbsp;<i class="fa fa-plus"></i> New API Key&nbsp;&nbsp;</button>
            </div>

        </div>

        <div class="widget-body" style="width: 700px;">
            <div class="widget-main no-padding" style="width: 698px;">
                <table class="table table-striped table-bordered table-hover" style="width: 698px;">
                    <tbody>
                        @foreach ($api as $item)
                            <tr>
                                <td class="apirow apikeyid{{$item->apiCredentialId}}" data-id="{{$item->apiCredentialId}}">
                                    <span class="apikey" style="font-size:14px;"><i class="fa fa-key"></i> {{$item->apiKey}}</span><br>

                                    <span class="location label label-sm label-info arrowed-right">
                                    @if(!is_null($item))
                                        @if($item->continent    )» {{$item->continent}}@endif
                                        @if($item->country      )» {{$item->country}}@endif
                                        @if($item->region1      )» {{$item->region1}}@endif
                                        @if($item->region2      )» {{$item->region2}}@endif
                                        @if($item->region3      )» {{$item->region3}}@endif
                                        @if($item->region4      )» {{$item->region4}}@endif
                                        @if($item->locality     )» {{$item->locality}}@endif
                                        @if($item->postal       )» {{$item->postal}}@endif
                                    @endif
                                    </span>
                                    <span class="scope ">{{ucfirst($item->scope)}}</span>
                                    <br>
                                    <br>
                                </td>


                                <td class="">
                                    <div class="btn-group">
                                        {{--<button class="btn btn-xs btn-success btn-white btn-round editBtn" onclick="editApiCredential({{$item->apiCredentialId}})" >--}}
                                            {{--<i class="ace-icon fa fa-edit green"></i>--}}
                                            {{--Modify--}}
                                        {{--</button>--}}

                                        <button class="btn btn-xs btn-danger btn-white btn-round deleteBtn" onclick="deleteApiCredential({{$item->apiCredentialId}})" >
                                            <i class="ace-icon fa fa-times bigger-110 red2"></i>
                                            Delete
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        </div>

@stop

@section('scripts')

    @include('api._apiNotice-js')
@stop