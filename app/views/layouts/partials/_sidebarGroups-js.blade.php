@if(Auth::check())
    <script>

    function leaveFromGroup(groupId, pageReload)
    {
        $('.sidebar-group').removeClass('active');
        $('#sidebarGroup' + groupId).addClass('active');

        var url = decodeURI('{{URL::route('apiGroupDeleteMember')}}');
        url = encodeURI(url.replace('{id}',groupId));

        var _form = $('<form>')
                    .attr('id','frmLeaveFromGroup')
                    .attr('action', url);

        _form.append ($('<input>')
            .attr('type','hidden')
            .attr('name','_method')
            .val('delete') );

        _form.append ($('<input>')
            .attr('type','hidden')
            .attr('name','userId')
            .val('{{Auth::user()->userId}}') );

        _form.append('Are you sure you want to leave this group?');

    //        var _html = '<form id="frmRemoveFromGroup" action="' + url + '"><input type="hidden" name="_method" value="delete" > <input type="hidden" name="userId" value="' + userId + '"> </form>' +
    //                    'Are you sure you want to remove this user from this group?';
        var dialog = messageBox ('frmLeaveFromGroupDialog', _form,'Group','Leave','Cancel',BootstrapDialog.TYPE_DANGER).setSize(BootstrapDialog.SIZE_SMALL);

        if (pageReload == undefined ) pageReload = false;
        dialog.setData('reload',pageReload);
    }

    function frmLeaveFromGroupDialog_onButtonClick(dialogRef)
    {
        postMyData('frmLeaveFromGroup',dialogRef).then(
            function(json){
                $('.sidebar-group.active').remove();
                console.log(dialogRef.getData('reload'));
                if(dialogRef.getData('reload') == true) location.reload();
                dialogRef.close();

            }
        )
    }
    </script>


@endif
