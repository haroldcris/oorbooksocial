
<script src="/controls/sticky/jquery.sticky.js"></script>


<script>
    $(document).ready(function(){
        $("#navHeader").sticky({topSpacing:40});

        @if(Auth::check())
            $('.sidebar-main').sticky({topSpacing:140, getWidthFrom: '.sidebar-main'});
        @else
            $('.sidebar-main').sticky({topSpacing:40});
        @endif

        $("#stickyRightAds").sticky();
        $(".primary.menu-bar").sticky({topSpacing:140});

    });

</script>