<script>
    function htmlspecialchars(str) {
        if (typeof(str) == "string") {
            str = str.replace(/&/g, "&amp;"); /* must do &amp; first */
            str = str.replace(/"/g, "&quot;");
            str = str.replace(/'/g, "&#039;");
            str = str.replace(/</g, "&lt;");
            str = str.replace(/>/g, "&gt;");
        }
        return str;
    }
    
    function burado() {
        var len = arguments.length;
        if(len == 0) return;
        $(arguments[0]).empty().append($('<option value="">' + $(arguments[0]).attr("title") + '</option>')).css('display', 'none');
        for(var i=1; i<len; i++) {
            $(arguments[i]).empty().css('display', 'none');
        }
    }
    
    function setGeoLocationsValues(field, geo, geoValue){
        $('#field').val(field);
        $('#geo').val(geo);
        $('#geoValue').val(geoValue);
    }
    
    // Set to empty then append
    function emppend(element){
        $(element).empty().append($('<option value="">' + $(element).attr("title") + '</option>')).css('display', 'none');
    }
    // Set to empty then destroy
    function emptroy(){
        var len = arguments.length;
        if(len == 0) return;
        for(var i=0; i<len; i++) {
            $(arguments[i]).empty().css('display', 'none');
        }
    }
    // Set visibility [ default: visible ]
    function ipakita(element, bool){
        // If bool is not set
        if(typeof(bool)==='undefined') {
            $(element).css('display', 'inline-block');
        } else {
            $(element).css('display', 'none');
        }
    }
    
    // Check if data has a value
    function hasValue(data) {
        if(typeof data !== 'undefined' && data.length > 0){
            return true;
        }
    }

    // Append data to geo dropdown, show dropdown, hide spinner
    function loopend(data, geo, dropdown, spinnery){
        for(var i=0; i<data.length; i++) {
            var geos;
            switch(geo) {
                case 'country':
                    geos = data[i].country;
                    break;
                case 'region1':
                    geos = data[i].region1;
                    break;
                case 'region2':
                    geos = data[i].region2;
                    break;
                case 'region3':
                    geos = data[i].region3;
                    break;
                case 'region4':
                    geos = data[i].region4;
                    break;
                case 'locality':
                    geos = data[i].locality;
                    break;
            }
            var newOption = $('<option value="' + htmlspecialchars(geos) + '">' + htmlspecialchars(geos) + '</option>');
            $(dropdown).append(newOption);
        }
        ipakita(dropdown);
        ipakita(spinnery, false);
    }
</script>