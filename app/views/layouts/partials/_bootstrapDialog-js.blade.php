<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 01/30/2015
 * Time: 8:25 AM
 *
 */?>

{{--Bootstrap Dialog--}}
<link rel="stylesheet" href="/controls/bootstrap-dialog/css/bootstrap-dialog.min.css">
<script src="/controls/bootstrap-dialog/js/bootstrap-dialog.min.js"></script>

<script>
    $('.bootstrapDialogFrame').click(function(e){
         e.preventDefault();
         var title = $(this).data('title');

         $.get(e.target).done(function(d){
            showInDialog(d,title);
         });
     });

    var bootstrapDialogFrame = new BootstrapDialog({
                            id: 'modalDialog',
                            animate: false,
                            closable : false,
                            draggable:true,
                            autospin: true,
                            size: BootstrapDialog.SIZE_WIDE,
                            message: 'loading...',
                            title: '',
                            buttons:[{label:'', cssClass:'', action:function(d){ d.close();}}]
                        });


    function showInDialog($body, $title){
        bootstrapDialogFrame.setTitle($title);
        bootstrapDialogFrame.setType(BootstrapDialog.TYPE_PRIMARY);
        bootstrapDialogFrame.open();
        bootstrapDialogFrame.getModalBody().html($body);

        $('#modalDialog').find($('form:first *:input[type!=hidden]:first')).focus();

        //get Form Callback
        _form = $('#modalDialog').find('form');

        _form.on('submit', function(){formSubmission(_form)});

        var _button = bootstrapDialogFrame.getModalBody().find('button');
        if (_button.length !== 0)
        {
         $(_button).css('display','none');
         bootstrapDialogFrame.setButtons([
                        {label:'Cancel', cssClass:'btn btn-danger pull-left', action:function(d){ d.close();}}
                       ,{label: _button.text(), id:'btn-1', icon: 'glyphicon glyphicon-ok',  cssClass: _button.attr('class'), action:function(dialogRef){ this.spin().disable(); bootstrapDialogFrame.getModalBody().find('form').submit();} }
                 ]);
        }else{
         bootstrapDialogFrame.setType(BootstrapDialog.TYPE_SUCCESS);
         bootstrapDialogFrame.setButtons([{id: 'btn-1',icon: 'glyphicon glyphicon-ok', label:' OK &nbsp;&nbsp;&nbsp;', cssClass:'btn btn-success text-center', action:function(d){ this.spin().disable(); location.reload();}}]);
        }

    }


    function formSubmission($form){
        console.log('form is submitted');
        console.log($form);

    }

    /**
      * Modal Message Box
      *
      * @param {type} dialogId
      * @param {type} content
      * @param {type} title
      * @param {type} okBtnCaption
      * @param {type} cancelBtnCaption
      * @returns {undefined}
      */
    function messageBox(dialogId, content, title, okBtnCaption, cancelBtnCaption, type, funcOnShown){
        if (title == '' || title == undefined) {title = '';}
        if (okBtnCaption == '' || okBtnCaption == undefined) {okBtnCaption = 'OK';}
        if (cancelBtnCaption == null || cancelBtnCaption == '' || cancelBtnCaption == undefined){ cancelBtnCaption ='Cancel'}
        if (type == null || type == '') {type = BootstrapDialog.TYPE_PRIMARY}



        // remove the newlines. Bootstrap-Dialog doesn't like them (turns them into <br>'s)
        //content = content.replace(/[\n]/g, '');
        if (typeof content == 'object') {
         //   console.log('object');
        } else {
            content = content.replace(/(?:\r\n|\r|\n)/g, '');
        }

        if(title != '') { title = title.replace(/[\n]/g, '') };

        var _modal = new BootstrapDialog({
                        id: dialogId,
                        title: title,
                        closable:false,
                        draggable:true,
                        type: type,
                        message: content,
                        spinicon: 'glyphicon glyphicon-refresh',
                        onshown: function(dialogRef){
                            // $(dialogRef.getModalBody()).find("form:not(.filter) :input:visible:enabled:first").focus();
                            // $("form :input:visible:enabled:first").focus();
				if (funcOnShown == null || funcOnShown == '' || funcOnShown == undefined){ 
					$(dialogRef.getModalBody()).find("form:not(.filter) :input:visible:enabled:first").focus();
				} else {
                            		funcOnShown();
                            	}
                        },
                        buttons:[{label: okBtnCaption,id:'dialogOkBtn',icon:'glyphicon glyphicon-ok', cssClass: 'btn btn-white btn-bold btn-purple btn-round btn-info',action: function(d){ window[dialogId + '_onButtonClick'](d)}},
                                 {label: cancelBtnCaption, icon: 'ace-icon fa fa-times red', cssClass: 'btn btn-white btn-danger btn-round pull-left', action: function(d){d.close()}}
                                ]
                    });
        _modal.open();
        return _modal;
    }

    function messageBoxRemote(dialogId, source, title, okBtnCaption, cancelBtnCaption, type)
    {
        var _dialog = messageBox(dialogId,'{{Config::get('site.loader')}}',title, okBtnCaption, cancelBtnCaption, type);

        $.get(source).done(
            function(_html){
                _dialog.getModalBody().html(_html);
                return _dialog;
            }
        )

        return _dialog;
    }

    function messageBoxOk(content, title, okBtnCaption,type){

             if (okBtnCaption == '' || okBtnCaption == undefined) {okBtnCaption = 'OK';}
             var _modal = new BootstrapDialog({
                            title: title,
                            type : type,
                            closable: false,
                            message: content,
                            buttons:[{label: okBtnCaption,id:'dialogOkBtn',icon:'glyphicon glyphicon-ok', cssClass: 'btn btn-success btn-white btn-round',action: function(d){ d.close(); }}]
                        });
             return _modal.open();
     }
    function messageBoxOkCancel(dialogId, content, title, okBtnCaption, type){
        if (okBtnCaption == '' ||okBtnCaption == undefined) {OkBtnCaption = 'OK';}
        var _modal = new BootstrapDialog({
                     title: title,
                     type : type,
                     closable: false,
                     message: content,
                     spinicon: 'glyphicon glyphicon-refresh',
                     buttons:[{ label: okBtnCaption,
                                id:'dialogOkBtn',
                                icon:'glyphicon glyphicon-ok',
                                cssClass: 'btn btn-success btn-white btn-round',
                                action: function(d){
                                    d.close();
                                    return true;
                                }
                              },
                              { label: 'Cancel' ,
                                icon:'ace-icon fa fa-times red',
                                cssClass: 'btn btn-success btn-white btn-round pull-left',
                                action: function(d){
                                    d.close();
                                    return false;
                                }
                              }
                             ]
                 });
        return _modal.open();
    }

</script>