<script>
    emppend('#drpCountry');
    emptroy('#drpRegion1', '#drpRegion2', '#drpRegion3', '#drpRegion4', '#drpLocality');
    ipakita('#spinnery', false);
    
    $('#drpContinent').change(function() {
        emppend('#drpCountry');
        emptroy('#drpRegion1', '#drpRegion2', '#drpRegion3', '#drpRegion4', '#drpLocality');
        var selectedGeoValue = $("#drpContinent option:selected").val();
        
        if(selectedGeoValue != '') {
            ipakita('#spinnery');
            $.post('{{ URL::route("getGeoCountry") }}', $('#frmCreatePost').serialize()).done(function(data) {
                loopend(data, 'country', '#drpCountry', '#spinnery');
            }).fail(function(err) {
                console.log(err);
            });
        }
    });
    
    $('#drpCountry').change(function() {
        emppend('#drpRegion1');
        emptroy('#drpRegion2', '#drpRegion3', '#drpRegion4', '#drpLocality');
        var selectedGeoValue = $("#drpCountry option:selected").val();
        
        if(selectedGeoValue != '') {
            ipakita('#spinnery');
            $.post('{{ URL::route("getGeoRegion1") }}', $('#frmCreatePost').serialize()).done(function(data) {
                loopend(data, 'region1', '#drpRegion1', '#spinnery');
            }).fail(function(err) {
                console.log(err);
            });
        }
        
    });
    
    $('#drpRegion1').change(function() {
        emppend('#drpRegion2');
        emptroy('#drpRegion3', '#drpRegion4', '#drpLocality');            
        var selectedGeoValue = $("#drpRegion1 option:selected").val();
        
        if(selectedGeoValue != '') {
            ipakita('#spinnery');
            $.post('{{ URL::route("getGeoRegion2") }}', $('#frmCreatePost').serialize()).done(function(data) {
                if(hasValue(data)){
                    loopend(data, 'region2', '#drpRegion2', '#spinnery');
                } else {
                    $.post('{{ URL::route("getGeoRegion3") }}', $('#frmCreatePost').serialize()).done(function(data) {
                        if(hasValue(data)){
                            loopend(data, 'region3', '#drpRegion3', '#spinnery');
                        } else {
                            $.post('{{ URL::route("getGeoRegion4") }}', $('#frmCreatePost').serialize()).done(function(data) {
                                if(hasValue(data)){
                                    loopend(data, 'region4', '#drpRegion4', '#spinnery');
                                } else {
                                    $.post('{{ URL::route("getGeoLocality") }}', $('#frmCreatePost').serialize()).done(function(data) {
                                        emppend('#drpLocality');
                                        loopend(data, 'locality', '#drpLocality', '#spinnery');
                                    }).fail(function(err) {
                                        console.log(err);
                                    });
                                }
                            }).fail(function(err) {
                                console.log(err);
                            });
                        }
                    }).fail(function(err) {
                        console.log(err);
                    });
                }
            }).fail(function(err) {
                console.log(err);
            });
        }
    });
    
    $('#drpRegion2').change(function() {
        emppend('#drpRegion3');
        emptroy('#drpRegion4', '#drpLocality');
        var selectedGeoValue = $("#drpRegion2 option:selected").val();
        
        if(selectedGeoValue != '') {
            ipakita('#spinnery');
            $.post('{{ URL::route("getGeoRegion3") }}', $('#frmCreatePost').serialize()).done(function(data) {
                if(hasValue(data)){
                    loopend(data, 'region3', '#drpRegion3', '#spinnery');
                } else {
                    $.post('{{ URL::route("getGeoRegion4") }}', $('#frmCreatePost').serialize()).done(function(data) {
                        if(hasValue(data)){
                            loopend(data, 'region4', '#drpRegion4', '#spinnery');
                        } else {
                            $.post('{{ URL::route("getGeoLocality") }}', $('#frmCreatePost').serialize()).done(function(data) {
                                emppend('#drpLocality');
                                loopend(data, 'locality', '#drpLocality', '#spinnery');
                            }).fail(function(err) {
                                console.log(err);
                            });
                        }
                    }).fail(function(err) {
                        console.log(err);
                    });
                }
            }).fail(function(err) {
                console.log(err);
            });
        }
    });
    
    $('#drpRegion3').change(function() {
        emppend('#drpRegion4');
        emptroy('#drpLocality');
        var selectedGeoValue = $("#drpRegion3 option:selected").val();
        
        if(selectedGeoValue != '') {
            ipakita('#spinnery');
            $.post('{{ URL::route("getGeoRegion4") }}', $('#frmCreatePost').serialize()).done(function(data) {
                if (hasValue(data)) {
                    loopend(data, 'region4', '#drpRegion4', '#spinnery');
                } else {
                    $.post('{{ URL::route("getGeoLocality") }}', $('#frmCreatePost').serialize()).done(function(data) {
                        emppend('#drpLocality');
                        loopend(data, 'locality', '#drpLocality', '#spinnery');
                    }).fail(function(err) {
                        console.log(err);
                    });
                }
            }).fail(function(err) {
                console.log(err);
            });
        }
    });
    
    $('#drpRegion4').change(function() {
        emptroy('#drpLocality');
        var selectedGeoValue = $("#drpRegion4 option:selected").val();
        
        if(selectedGeoValue != '') {
            ipakita('#spinnery');
            $.post('{{ URL::route("getGeoLocality") }}', $('#frmCreatePost').serialize()).done(function(data) {
                emppend('#drpLocality');
                loopend(data, 'locality', '#drpLocality', '#spinnery');
            }).fail(function(err) {
                console.log(err);
            });
        }
    });
</script>