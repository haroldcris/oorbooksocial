<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 01/30/2015
 * Time: 12:16 PM
 *
 */


 ?>

<style>
    .wrapper {
        white-space: nowrap;
        height: 100px;
        /*min-width: 1140px;*/
    }

    .side-by-side {
        display: inline-block;
        vertical-align: top;
        height:100px;
    }

    .header-left{
        background-color: #e4e6e9;
        width:190px;
        min-height:100%;
        padding-top: 5px;
        /*float:left*/
        text-align: left;
    }


    .header-center{
        margin-left:0;
        margin-right:10px;
        min-height:100%;
        /*width:665px;*/
        width: 590px;
        float:left;

    }

    /*.header-right{*/
        /*background-color: #fff;*/
        /*margin-left:0px;*/
        /*padding:2px 5px 2px 10px;*/
        /*min-height: 100%;*/
        /*max-width: 200px;;*/
        /*border: #000 solid  1px;*/
        /*vertical-align: top;*/
        /*float:right;*/
    /*}*/


    .avatar{
        padding: 5px;
    }

    .header-right  div > a > span > i{
        margin-top: 5px;
    }

    .menuPanel{
            /*background-color: #8eb3d0;*/
            height:30px;
            width: 35px;
            display:inline-block;
            cursor: pointer;
            overflow:hidden;
            float:left;
            vertical-align: middle;
            /*margin: 0 2px 0 2px;*/
    }

    .menuPanel:hover{
        border:blue solid 1px;
        /*background-color: #ff0*/
    }

    .notification-icon{
        font-size: 7pt;
        background-color: darkgreen;
        border-radius: 100px;
        padding: 2px;
        color: white;
        margin-left: 15px;
        position: absolute;
        z-index: 100;
        line-height: 10px;
    }

    .container {
        width:1024px;
    }
</style>

@if(Auth::check())

<div class="container">
    <div id="navHeader" style="z-index:999">
        {{--Left Side--}}
        <div class="header-left side-by-side">
            <div style="float:left" >
                <div><a href="javascript:void(0);" onclick="viewAvatar();"><img class="avatar"  src="{{ URL::route('userAvatar', $AuthUser->userId) }}"  height="70px" width="70px"/></a></div>
                <script>
                    function viewAvatar(){
                        BootstrapDialog.show({
                            title: '',
                            message: '<div class="row"><div class="col-md-12 text-center"><img class="avatar"  src="{{ URL::route('userAvatar', $AuthUser->userId) }}" style="width:500px; height:500px;"/></div></div>'
                        });
                    }
                </script>
            </div>


            {{--<div style="line-height: 10px; margin:5px 0px 0px 73px">--}}
                {{--<p><a href="#">haroldcris.abarquez@gmail.com</a></p>--}}
                {{--<p><a href="#">Profile</a></p>--}}
                {{--<p><a href="#">Log Out</a></p>--}}
            {{--</div>--}}

            <div style="margin-left:70px">

                <!--User Profile-->

                {{--<ul class="nav ace-nav" style="float:left; width:110px;padding-top: 5px">--}}
                    {{--<li style="height:auto">--}}
                        {{--<a style="text-align:left !important" class="dropdown-toggle" href="#" data-toggle="dropdown">--}}
                            {{--<span><b>thequickbrownfoxjumps{{$AuthUser->username}}</b> <i class="caret"></i></span>--}}
                        {{--</a>--}}

                        {{--<ul class="dropdown-navbar navbar-green dropdown-menu dropdown-caret dropdown-close">--}}
                            {{--<li><a id="changePassword" href="javascript:void()" class="popup" onclick="changePasswordPost()"><i class="fa fa-key"></i> Change Password</a></li>--}}
                            {{--<li style="margin:0px;padding:0px" class="divider"></li>--}}


                        {{--</ul>--}}

                    {{--</li>--}}
                {{--</ul>--}}
                
                <ul class="nav ace-nav">
                    <li>
                        <a id="userprofile" href="/profile"><small><i class="glyphicon glyphicon-user"></i> Profile &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small></a>
                    </li>
                    <li>
                        <div class="onoffswitch">
                            <input type="checkbox" checked="" id="myonoffswitch" class="onoffswitch-checkbox" name="onoffswitch">
                            <label for="myonoffswitch" class="onoffswitch-label">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                    </li>
                    <li>
                        <a href="/accounts/logout?_token={{csrf_token()}}"><small><i class="glyphicon glyphicon-log-out"></i> Log-out </small></a>
                    </li>
                </ul>
                
            </div>

            <div title="{{$AuthUser->username}}" style="text-align: center"><b><span>{{$AuthUser->username}}</span></b></div>
        </div>

        {{--Center Div--}}
        <div class="side-by-side">

            <div class="header-center" style="width: 700px;">
                <div style="max-height:100px;">
                    <div class="cycle-slideshow"
                        data-cycle-fx="fadeout"
                        data-cycle-timeout=5000
                        data-cycle-speed="1000"
                        data-cycle-loader="wait"
                        data-cycle-pause-on-hover="true"
                        {{--style="z-index: -999; height: 100px;" --}}
                        {{--scrollHorz--}}
                        >

                        @if(Auth::check())
                            <img class="wallpaperpic img-responsive" style="height:100px" src="{{$AuthUser->getImageWallpaperPath()}}" id="wallpaper" />
                        @endif

                        @if( !is_null( SettingsClass::get('banner') ) && SettingsClass::get('banner') != ''  )
                            <img class="banner img-responsive" style="height:100px; display:none" src="{{SettingsClass::get('banner')}}"  />
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endif