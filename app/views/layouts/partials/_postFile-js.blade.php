{{--<div class="progress">--}}
    {{--<div class="bar" style="width: 0%;"></div>--}}
    {{--<div class="percent">0%</div>--}}
{{--</div>--}}

<style>
    .progress { position:relative; width:100%; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
    .bar { background-color: #B4F5B4; width:0%; height:20px; border-radius: 3px; }
    .percent { position:absolute; display:inline-block; top:0px; left:45%;}
</style>

{{--<script src="http://malsup.github.com/jquery.form.js"></script>--}}
<script src="/controls/jquery-form/jquery.form.js"></script>

<script>
    function postMyFile(formName,dialogRef){
        return new RSVP.Promise (function(resolve, reject){
            var form = $('#' + formName);
            //add token
            if($(form).find('input[name="_token"]').length == 0){
                $(form).append('{{Form::token()}}');    
            }
            
            var action = $(form).attr('action');
            if (action == undefined ) { return reject(Error('No URL Action'));}
            //console.log('posting to ' + action);

            if(dialogRef != undefined && typeof dialogRef != 'object' ){
                //console.log('DialogRef must be an object!');
                return reject(Error('DialogRef must be an object!'));
            }

            if (dialogRef != undefined && dialogRef != ''){
                dialogRef.getButton('dialogOkBtn').spin().disable();
                //console.log('Posting inside a Modal Dialog Box');
            }

            $('.errorHeaderStatus').hide();

            var bar = $('.bar');
            var percent = $('.percent');
            var status = $('#status');

            $(form).ajaxForm({
               type :      'post',
               beforeSend: function() {
                   status.empty();
                   var percentVal = '0%';
                   bar.width(percentVal)
                   percent.html(percentVal);
               },
               uploadProgress: function(event, position, total, percentComplete) {
                   var percentVal = percentComplete + '%';
                   bar.width(percentVal)
                   percent.html(percentVal);
                   console.log(percentVal);
               },
               success: function() {
                   //var percentVal = '100%';
                   //bar.width(percentVal)
                   //percent.html(percentVal);

               },
                complete: function(data) {
                    //status.html(xhr.responseText);

                   if (dialogRef != undefined && dialogRef != ''){
                       dialogRef.getButton('dialogOkBtn').stopSpin().enable();
                       //console.log('Posting inside a Modal Dialog Box');
                   }

                   
                    if(typeof data != 'object'){
                        return reject(Error(data));
                    }

                    if(data.responseJSON.success == "true" || data.responseJSON.success == true){
                        //console.log('success posting to ' + action);
                        return resolve(data.responseJSON);

                    } else{
                        //console.log('success posting to ' + action + ' but with error');
                        showPostFileError(dialogRef, data.responseJSON.error);
                        //return reject(Error(data));
                        return reject(data);
                    }
                }
            }).submit();


        });
    }


    function showPostFileError(dialogRef,_error){
        if (dialogRef != undefined && dialogRef != ''){
            var _errStatus = dialogRef.getModalBody().find('.errorHeaderStatus');
            if(_errStatus.length == 0){
                dialogRef.getModalBody().append('<br><div class="errorHeaderStatus alert alert-danger" style="margin-bottom:0px !important">' + _error  + '</div>');
            }else{
                return $(_errStatus).html(_error).show();
            }
        }
    }

</script>


