<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 01/29/2015
 * Time: 9:40 PM
 *
 */?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Specific Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>{{Config::get('site.title')}} @yield('title') </title>

<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">

<meta name="keywords" content="@yield('keywords')" />
<meta name="author" content="@yield('author')" />


{{--<!-- Google will often use this as its description of your page/site. Make it good.--}}
<meta name="description" content="@yield('description')" />
{{--<!-- Speaking of Google, don't forget to set your site up: http://google.com/webmasters -->--}}
<meta name="google-site-verification" content="A8K-Cqvdx8RxklQ-KP1kpA93EpNnmYPsSWcrmCRWm_I" />

@yield('meta')