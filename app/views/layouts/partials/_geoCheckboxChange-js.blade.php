<script>

// ------------------------------------------------------------------------    
// O N   C H A N G E    
// ------------------------------------------------------------------------
    
$(document).ready(function(){

    // On Load: Set Checkbox counter to zero
    var CN_checkCounter=0;var CT_checkCounter=0;var R1_checkCounter=0;var R2_checkCounter=0;var R3_checkCounter=0;var R4_checkCounter=0;var LC_checkCounter=0;
    
    $(document.body).on("change", ".chkGlobal", function(){
        ipakita('#spinnery');
        if($(this).is(":checked")) {
            $('.chkContinent').prop('checked', false);
            CN_checkCounter=0;CT_checkCounter=0;R1_checkCounter=0;R2_checkCounter=0;R3_checkCounter=0;R4_checkCounter=0;LC_checkCounter=0;
            emptroy('#tdCT', '#tdR1', '#tdR2', '#tdR3', '#tdR4', '#tdLC');
            emptiness('#hidContinent','#hidCountry', '#hidRegion1', '#hidRegion2', '#hidRegion3', '#hidRegion4', '#hidLocality');
            ipakita('#spinnery', false);
            
            console.log('CN: '+CN_checkCounter+'\t'+'CT: '+CT_checkCounter+'\t'+'R1: '+R1_checkCounter+'\t'+'R2: '+R2_checkCounter+'\t'+'R3: '+R3_checkCounter+'\t'+'R4: '+R4_checkCounter+'\t'+'LC: '+LC_checkCounter);
            console.log('CN: '+$('#hidContinent').val()+'\t'+'CT: '+$('#hidCountry').val()+'\t'+'R1: '+$('#hidRegion1').val()+'\t'+'R2: '+$('#hidRegion2').val()+'\t'+'R3: '+$('#hidRegion3').val()+'\t'+'R4: '+$('#hidRegion4').val()+'\t'+'LC: '+$('#hidLocality').val());
        } else {
            ipakita('#spinnery', false);
        }
    });
    
    // Check Continent
    $(document.body).on("change", ".chkContinent", function(){
        $(this).is(":checked") ? CN_checkCounter++ : CN_checkCounter--;
        
        $('.chkGlobal').prop('checked', false);
        // Set Checkbox counter to zero
        CT_checkCounter=0;R1_checkCounter=0;R2_checkCounter=0;R3_checkCounter=0;R4_checkCounter=0;LC_checkCounter=0;

        if(CN_checkCounter==0) {
            //destroy all, empty data
            console.log('0 CN_checkCounter');
            emptroy('#tdCT', '#tdR1', '#tdR2', '#tdR3', '#tdR4', '#tdLC');
        } else if(CN_checkCounter>1) {
            //destroy all child
            console.log('1+ CN_checkCounter: ' + CN_checkCounter);
            emptroy('#tdCT', '#tdR1', '#tdR2', '#tdR3', '#tdR4', '#tdLC');
        } else if(CN_checkCounter==1) {
            //loop country to locality
            $('#hidContinent').val($('input[name="geoContinent[]"]:checked').val());
            emptiness('#hidCountry', '#hidRegion1', '#hidRegion2', '#hidRegion3', '#hidRegion4', '#hidLocality');

            console.log('CN: '+CN_checkCounter+'\t'+'CT: '+CT_checkCounter+'\t'+'R1: '+R1_checkCounter+'\t'+'R2: '+R2_checkCounter+'\t'+'R3: '+R3_checkCounter+'\t'+'R4: '+R4_checkCounter+'\t'+'LC: '+LC_checkCounter);
            console.log('CN: '+$('#hidContinent').val()+'\t'+'CT: '+$('#hidCountry').val()+'\t'+'R1: '+$('#hidRegion1').val()+'\t'+'R2: '+$('#hidRegion2').val()+'\t'+'R3: '+$('#hidRegion3').val()+'\t'+'R4: '+$('#hidRegion4').val()+'\t'+'LC: '+$('#hidLocality').val());
            
            ipakita('#spinnery');

            $.post('{{ URL::route("getGeoCountry") }}', $('#frmCreatePost').serialize())
            .fail(function(err) { console.log(err); })
            .done(function(data) {
                loopend(data, 'country', '#tdCT', '#spinnery');
            });
        }
        
        // if($(this).is(":checked") == 'Global') {
        //     console.log('Global');
        // } else {
        //     console.log('!global');
        // }
    });
            
    
    
    // Check Country
    $(document.body).on("change", ".chkCountry", function(){
        $(this).is(":checked") ? CT_checkCounter++ : CT_checkCounter--;
        
        // Set Checkbox counter to zero
        R1_checkCounter=0;R2_checkCounter=0;R3_checkCounter=0;R4_checkCounter=0;LC_checkCounter=0;
        
        if(CT_checkCounter==0) {
            //destroy all, empty data
            console.log('0 CT_checkCounter');
            emptroy('#tdR1', '#tdR2', '#tdR3', '#tdR4', '#tdLC');
        } else if(CT_checkCounter>1) {
            //destroy all child
            console.log('1+ CT_checkCounter: ' + CT_checkCounter);
            emptroy('#tdR1', '#tdR2', '#tdR3', '#tdR4', '#tdLC');
            
        } else if(CT_checkCounter==1) {
            //loop region1 to locality
            $('#hidCountry').val($('input[name="geoCountry[]"]:checked').val());
            emptiness('#hidRegion1', '#hidRegion2', '#hidRegion3', '#hidRegion4', '#hidLocality');

            console.log('CN: '+CN_checkCounter+'\t'+'CT: '+CT_checkCounter+'\t'+'R1: '+R1_checkCounter+'\t'+'R2: '+R2_checkCounter+'\t'+'R3: '+R3_checkCounter+'\t'+'R4: '+R4_checkCounter+'\t'+'LC: '+LC_checkCounter);
            console.log('CN: '+$('#hidContinent').val()+'\t'+'CT: '+$('#hidCountry').val()+'\t'+'R1: '+$('#hidRegion1').val()+'\t'+'R2: '+$('#hidRegion2').val()+'\t'+'R3: '+$('#hidRegion3').val()+'\t'+'R4: '+$('#hidRegion4').val()+'\t'+'LC: '+$('#hidLocality').val());
            
            ipakita('#spinnery');
            
            $.post('{{ URL::route("getGeoRegion1") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                if(hasValue(data)){
                    console.log('Found');
                    loopend(data, 'region1', '#tdR1', '#spinnery');
                } else {
                    console.log('Not Found');
                    $.post('{{ URL::route("getGeoRegion2") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                        if(hasValue(data)){
                            console.log('Found');
                            loopend(data, 'region2', '#tdR2', '#spinnery');
                        } else {
                            console.log('Not Found');
                            $.post('{{ URL::route("getGeoRegion3") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                                if(hasValue(data)){
                                    console.log('Found');
                                    loopend(data, 'region3', '#tdR3', '#spinnery');
                                } else {
                                    console.log('Not Found');
                                    $.post('{{ URL::route("getGeoRegion4") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                                        if(hasValue(data)){
                                            console.log('Found');
                                            loopend(data, 'region4', '#tdR4', '#spinnery');
                                        } else {
                                            console.log('Not Found');
                                            $.post('{{ URL::route("getGeoLocality") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                                                if(hasValue(data)){
                                                    console.log('Found');
                                                    loopend(data, 'locality', '#tdLC', '#spinnery');
                                                } else {
                                                    console.log('Not Found');
                                                    emptroy('#tdLC');
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
    
    
    
    
    // Check Region1
    $(document.body).on("change", ".chkRegion1", function(){
        $(this).is(":checked") ? R1_checkCounter++ : R1_checkCounter--;
        
        // Set Checkbox counter to zero
        R2_checkCounter=0;R3_checkCounter=0;R4_checkCounter=0;LC_checkCounter=0;
        
        if(R1_checkCounter==0) {
            //destroy all
            console.log('0 R1_checkCounter');
            emptroy('#tdR2', '#tdR3', '#tdR4', '#tdLC');
        } else if(R1_checkCounter>1) {
            //destroy all child
            console.log('1+ R1_checkCounter: ' + R1_checkCounter);
            emptroy('#tdR2', '#tdR3', '#tdR4', '#tdLC');
            
        } else if(R1_checkCounter==1) {
            //loop region2 to locality
            $('#hidRegion1').val($('input[name="geoRegion1[]"]:checked').val());
            emptiness('#hidRegion2', '#hidRegion3', '#hidRegion4', '#hidLocality');

            console.log('CN: '+CN_checkCounter+'\t'+'CT: '+CT_checkCounter+'\t'+'R1: '+R1_checkCounter+'\t'+'R2: '+R2_checkCounter+'\t'+'R3: '+R3_checkCounter+'\t'+'R4: '+R4_checkCounter+'\t'+'LC: '+LC_checkCounter);
            console.log('CN: '+$('#hidContinent').val()+'\t'+'CT: '+$('#hidCountry').val()+'\t'+'R1: '+$('#hidRegion1').val()+'\t'+'R2: '+$('#hidRegion2').val()+'\t'+'R3: '+$('#hidRegion3').val()+'\t'+'R4: '+$('#hidRegion4').val()+'\t'+'LC: '+$('#hidLocality').val());
            
            ipakita('#spinnery');
            
            $.post('{{ URL::route("getGeoRegion2") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                if(hasValue(data)){
                    console.log('Found');
                    loopend(data, 'region2', '#tdR2', '#spinnery');
                } else {
                    console.log('Not Found');
                    $.post('{{ URL::route("getGeoRegion3") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                        if(hasValue(data)){
                            console.log('Found');
                            loopend(data, 'region3', '#tdR3', '#spinnery');
                        } else {
                            console.log('Not Found');
                            $.post('{{ URL::route("getGeoRegion4") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                                if(hasValue(data)){
                                    console.log('Found');
                                    loopend(data, 'region4', '#tdR4', '#spinnery');
                                } else {
                                    console.log('Not Found');
                                    $.post('{{ URL::route("getGeoLocality") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                                        if(hasValue(data)){
                                            console.log('Found');
                                            loopend(data, 'locality', '#tdLC', '#spinnery');
                                        } else {
                                            console.log('Not Found');
                                            emptroy('#tdLC');
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
    
    
    
    // Check Region2
    $(document.body).on("change", ".chkRegion2", function(){
        $(this).is(":checked") ? R2_checkCounter++ : R2_checkCounter--;
        
        // Set Checkbox counter to zero
        R3_checkCounter=0;R4_checkCounter=0;LC_checkCounter=0;
        
        if(R2_checkCounter==0) {
            //destroy all
            console.log('0 R2_checkCounter');
            emptroy('#tdR3', '#tdR4', '#tdLC');
        } else if(R2_checkCounter>1) {
            //destroy all child
            console.log('1+ R2_checkCounter: ' + R2_checkCounter);
            emptroy('#tdR3', '#tdR4', '#tdLC');
            
        } else if(R2_checkCounter==1) {
            //loop region3 to locality
            $('#hidRegion2').val($('input[name="geoRegion2[]"]:checked').val());
            emptiness('#hidRegion3', '#hidRegion4', '#hidLocality');

            console.log('CN: '+CN_checkCounter+'\t'+'CT: '+CT_checkCounter+'\t'+'R1: '+R1_checkCounter+'\t'+'R2: '+R2_checkCounter+'\t'+'R3: '+R3_checkCounter+'\t'+'R4: '+R4_checkCounter+'\t'+'LC: '+LC_checkCounter);
            console.log('CN: '+$('#hidContinent').val()+'\t'+'CT: '+$('#hidCountry').val()+'\t'+'R1: '+$('#hidRegion1').val()+'\t'+'R2: '+$('#hidRegion2').val()+'\t'+'R3: '+$('#hidRegion3').val()+'\t'+'R4: '+$('#hidRegion4').val()+'\t'+'LC: '+$('#hidLocality').val());
            
            ipakita('#spinnery');
            
            $.post('{{ URL::route("getGeoRegion3") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                if(hasValue(data)){
                    console.log('Found');
                    loopend(data, 'region3', '#tdR3', '#spinnery');
                } else {
                    console.log('Not Found');
                    $.post('{{ URL::route("getGeoRegion4") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                        if(hasValue(data)){
                            console.log('Found');
                            loopend(data, 'region4', '#tdR4', '#spinnery');
                        } else {
                            console.log('Not Found');
                            $.post('{{ URL::route("getGeoLocality") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                                if(hasValue(data)){
                                    console.log('Found');
                                    loopend(data, 'locality', '#tdLC', '#spinnery');
                                } else {
                                    console.log('Not Found');
                                    emptroy('#tdLC');
                                }
                            });
                        }
                    });
                }
            });
        }
    });



    // Check Region3
    $(document.body).on("change", ".chkRegion3", function(){
        $(this).is(":checked") ? R3_checkCounter++ : R3_checkCounter--;
        
        // Set Checkbox counter to zero
        R4_checkCounter=0;LC_checkCounter=0;
        
        if(R3_checkCounter==0) {
            //destroy all
            console.log('0 R3_checkCounter');
            emptroy('#tdR4', '#tdLC');
        } else if(R3_checkCounter>1) {
            //destroy all child
            console.log('1+ R3_checkCounter: ' + R3_checkCounter);
            emptroy('#tdR4', '#tdLC');
            
        } else if(R3_checkCounter==1) {
            //loop region4 to locality
            $('#hidRegion3').val($('input[name="geoRegion3[]"]:checked').val());
            emptiness('#hidRegion4', '#hidLocality');

            console.log('CN: '+CN_checkCounter+'\t'+'CT: '+CT_checkCounter+'\t'+'R1: '+R1_checkCounter+'\t'+'R2: '+R2_checkCounter+'\t'+'R3: '+R3_checkCounter+'\t'+'R4: '+R4_checkCounter+'\t'+'LC: '+LC_checkCounter);
            console.log('CN: '+$('#hidContinent').val()+'\t'+'CT: '+$('#hidCountry').val()+'\t'+'R1: '+$('#hidRegion1').val()+'\t'+'R2: '+$('#hidRegion2').val()+'\t'+'R3: '+$('#hidRegion3').val()+'\t'+'R4: '+$('#hidRegion4').val()+'\t'+'LC: '+$('#hidLocality').val());
            
            ipakita('#spinnery');
            
            $.post('{{ URL::route("getGeoRegion4") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                if(hasValue(data)){
                    console.log('Found');
                    loopend(data, 'region4', '#tdR4', '#spinnery');
                } else {
                    console.log('Not Found');
                    $.post('{{ URL::route("getGeoLocality") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                        if(hasValue(data)){
                            console.log('Found');
                            loopend(data, 'locality', '#tdLC', '#spinnery');
                        } else {
                            console.log('Not Found');
                            emptroy('#tdLC');
                        }
                    });
                }
            });
        }
    });



    // Check Region4
    $(document.body).on("change", ".chkRegion4", function(){
        $(this).is(":checked") ? R4_checkCounter++ : R4_checkCounter--;
        
        // Set Checkbox counter to zero
        LC_checkCounter=0;
        
        if(R4_checkCounter==0) {
            //destroy all
            console.log('0 R4_checkCounter');
            emptroy('#tdLC');
        } else if(R4_checkCounter>1) {
            //destroy all child
            console.log('1+ R4_checkCounter: ' + R4_checkCounter);
            emptroy('#tdLC');
            
        } else if(R4_checkCounter==1) {
            //loop locality
            $('#hidRegion4').val($('input[name="geoRegion4[]"]:checked').val());
            emptiness('#hidLocality');

            console.log('CN: '+CN_checkCounter+'\t'+'CT: '+CT_checkCounter+'\t'+'R1: '+R1_checkCounter+'\t'+'R2: '+R2_checkCounter+'\t'+'R3: '+R3_checkCounter+'\t'+'R4: '+R4_checkCounter+'\t'+'LC: '+LC_checkCounter);
            console.log('CN: '+$('#hidContinent').val()+'\t'+'CT: '+$('#hidCountry').val()+'\t'+'R1: '+$('#hidRegion1').val()+'\t'+'R2: '+$('#hidRegion2').val()+'\t'+'R3: '+$('#hidRegion3').val()+'\t'+'R4: '+$('#hidRegion4').val()+'\t'+'LC: '+$('#hidLocality').val());
            
            ipakita('#spinnery');
            
            $.post('{{ URL::route("getGeoLocality") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                if(hasValue(data)){
                    console.log('Found');
                    loopend(data, 'locality', '#tdLC', '#spinnery');
                } else {
                    console.log('Not Found');
                    emptroy('#tdLC');
                }
            });
        }
    });



    // Check Locality
    $(document.body).on("change", ".chkLocality", function(){
        $(this).is(":checked") ? LC_checkCounter++ : LC_checkCounter--;
        
        if(LC_checkCounter==0) {
            //destroy all
            console.log('0 LC_checkCounter');
        } else if(LC_checkCounter>1) {
            //destroy all child
            console.log('1+ LC_checkCounter: ' + LC_checkCounter);
        } else if(LC_checkCounter==1) {
            //loop locality
            $('#hidLocality').val($('input[name="geoLocality[]"]:checked').val());

            console.log('CN: '+CN_checkCounter+'\t'+'CT: '+CT_checkCounter+'\t'+'R1: '+R1_checkCounter+'\t'+'R2: '+R2_checkCounter+'\t'+'R3: '+R3_checkCounter+'\t'+'R4: '+R4_checkCounter+'\t'+'LC: '+LC_checkCounter);
            console.log('CN: '+$('#hidContinent').val()+'\t'+'CT: '+$('#hidCountry').val()+'\t'+'R1: '+$('#hidRegion1').val()+'\t'+'R2: '+$('#hidRegion2').val()+'\t'+'R3: '+$('#hidRegion3').val()+'\t'+'R4: '+$('#hidRegion4').val()+'\t'+'LC: '+$('#hidLocality').val());
        }
    });
});
</script>