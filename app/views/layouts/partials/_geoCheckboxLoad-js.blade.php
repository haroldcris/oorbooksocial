<script>

// ------------------------------------------------------------------------    
// O N   L O A D    
// ------------------------------------------------------------------------

$(function(){
    ipakita('#spinnery');
    
    console.log('ctrCN: '+ctrCN+'\t ctrCT: '+ctrCT+'\t ctrR1: '+ctrR1+'\t ctrR2: '+ctrR2+'\t ctrR3: '+ctrR3+'\t ctrR4: '+ctrR4+'\t ctrLC: '+ctrLC)
    
        
// C O N T I N E N T
    // Show Selected Scope Geos
    if(ctrCN != 0) {
        console.log('Admin o sel CN: ' + ctrCN + '\t *** Loop a Label ***');
        
        '@if($iCN[0] == "Global")'
            $.post('{{ URL::route("getGeoContinent") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                $('#tdCN').empty().append($('<label class="boxfix"><input type="checkbox" class="chkGlobal" name="geoContinent[]" value="Global">Global</label><br>'));
                loopend(data, 'continent', '#tdCN', '#spinnery');
            });
        '@else'
            '@foreach($iCN as $geo)'
                $('#tdCN').append($('<div class="scopeCN"><input type="checkbox" class="chkContinent" style="display: none;" name="geoContinent[]" value="{{ $geo }}" checked ><label class="boxfix" style="margin-bottom: -3px;">' + '{{ $geo }}' + '</label></div>'));
            '@endforeach'
        '@endif'
        
    } else {
        console.log('Admin x sel CN: ' + ctrCN + '\t *** Loop Checkboxes ***');
        $.post('{{ URL::route("getGeoContinent") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
            $('#tdCN').empty().append($('<label class="boxfix"><input type="checkbox" class="chkGlobal" name="geoContinent[]" value="Global">Global</label><br>'));
            loopend(data, 'continent', '#tdCN', '#spinnery');
        });
    }
    
    
// C O U N T R Y
    // Show Selected Scope Geos
    if(ctrCT != 0) {
        console.log('Admin o sel CT: ' + ctrCT + '\t *** Loop a Label ***');
        '@foreach($iCT as $geo)'
            $('#tdCT').append($('<div class="scopeCT"><input type="checkbox" class="chkCountry" style="display: none;" name="geoCountry[]" value="{{ $geo }}" checked ><label class="boxfix" style="margin-bottom: -3px;">' + '{{ $geo }}' + '</label></div>'));
        '@endforeach'
    }

// R E G I O N   1
    // Show Selected Scope Geos
    if(ctrR1 != 0) {
        console.log('Admin o sel R1: ' + ctrR1 + '\t *** Loop a Label ***');
        '@foreach($iR1 as $geo)'
            $('#tdR1').append($('<div class="scopeR1"><input type="checkbox" class="chkRegion1" style="display: none;" name="geoRegion1[]" value="{{ $geo }}" checked ><label class="boxfix" style="margin-bottom: -3px;">' + '{{ $geo }}' + '</label></div>'));
        '@endforeach'
    }

// R E G I O N   2
    // Show Selected Scope Geos
    if(ctrR2 != 0) {
        console.log('Admin o sel R2: ' + ctrR2 + '\t *** Loop a Label ***');
        '@foreach($iR2 as $geo)'
            $('#tdR2').append($('<div class="scopeR2"><input type="checkbox" class="chkRegion2" style="display: none;" name="geoRegion2[]" value="{{ $geo }}" checked ><label class="boxfix" style="margin-bottom: -3px;">' + '{{ $geo }}' + '</label></div>'));
        '@endforeach'
    }
    
// R E G I O N   3
    // Show Selected Scope Geos
    if(ctrR3 != 0) {
        console.log('Admin o sel R3: ' + ctrR3 + '\t *** Loop a Label ***');
        '@foreach($iR3 as $geo)'
            $('#tdR3').append($('<div class="scopeR3"><input type="checkbox" class="chkRegion3" style="display: none;" name="geoRegion3[]" value="{{ $geo }}" checked ><label class="boxfix" style="margin-bottom: -3px;">' + '{{ $geo }}' + '</label></div>'));
        '@endforeach'
    }

// R E G I O N   4
    // Show Selected Scope Geos
    if(ctrR4 != 0) {
        console.log('Admin o sel R4: ' + ctrR4 + '\t *** Loop a Label ***');
        '@foreach($iR4 as $geo)'
            $('#tdR4').append($('<div class="scopeR4"><input type="checkbox" class="chkRegion4" style="display: none;" name="geoRegion4[]" value="{{ $geo }}" checked ><label class="boxfix" style="margin-bottom: -3px;">' + '{{ $geo }}' + '</label></div>'));
        '@endforeach'
    }

// L O C A L I T Y
    // Show Selected Scope Geos
    if(ctrLC != 0) {
        console.log('Admin o sel LC: ' + ctrLC + '\t *** Loop a Label ***');
        '@foreach($iLC as $geo)'
            $('#tdLC').append($('<div class="scopeLC"><input type="checkbox" class="chkLocality" style="display: none;" name="geoLocality[]" value="{{ $geo }}" checked ><label class="boxfix" style="margin-bottom: -3px;">' + '{{ $geo }}' + '</label></div>'));
        '@endforeach'
    }
    
    
    
    console.log('getLastSelectedGeo() == ' + getLastSelectedGeo());


    
    if(getLastSelectedGeo() == 'Continent') {
    
        '@if($iCN[0] != "Global")'
            if(ctrCN == 1) {
                console.log('CN: Loop CT');
                emptroy('#tdCT', '#tdR1', '#tdR2', '#tdR3', '#tdR4', '#tdLC');
                copyCheckedValToHiddenVal('chkContinent', '#hidContinent');
                // Loop
                $.post('{{ URL::route("getGeoCountry") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                    loopend(data, 'country', '#tdCT', '#spinnery');
                });
            }
        '@else'
            copyCheckedValToHiddenVal('chkContinent', '#hidContinent');
            console.log('Put Global Label');
        '@endif'
        ipakita('#spinnery', false);
    } 
    
    else if(getLastSelectedGeo() == 'Country') {
        
        // Hidden Field Value Setting of Geos Upper Than CT
        copyCheckedValToHiddenVal('chkContinent', '#hidContinent');
    
        if(ctrCT == 1) {
            console.log('CT: Loop R1 - LC');
            emptroy('#tdR1', '#tdR2', '#tdR3', '#tdR4', '#tdLC');
            copyCheckedValToHiddenVal('chkCountry', '#hidCountry');
            // Loop
            $.post('{{ URL::route("getGeoRegion1") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                if(hasValue(data)){
                    console.log('Found');
                    loopend(data, 'region1', '#tdR1', '#spinnery');
                } else {
                    console.log('Not Found');
                    $.post('{{ URL::route("getGeoRegion2") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                        if(hasValue(data)){
                            console.log('Found');
                            loopend(data, 'region2', '#tdR2', '#spinnery');
                        } else {
                            console.log('Not Found');
                            $.post('{{ URL::route("getGeoRegion3") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                                if(hasValue(data)){
                                    console.log('Found');
                                    loopend(data, 'region3', '#tdR3', '#spinnery');
                                } else {
                                    console.log('Not Found');
                                    $.post('{{ URL::route("getGeoRegion4") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                                        if(hasValue(data)){
                                            console.log('Found');
                                            loopend(data, 'region4', '#tdR4', '#spinnery');
                                        } else {
                                            console.log('Not Found');
                                            $.post('{{ URL::route("getGeoLocality") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                                                if(hasValue(data)){
                                                    console.log('Found');
                                                    loopend(data, 'locality', '#tdLC', '#spinnery');
                                                } else {
                                                    console.log('Not Found');
                                                    emptroy('#tdLC');
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
        ipakita('#spinnery', false);
    } 
    
    else if(getLastSelectedGeo() == 'Region 1') {
        
        // Hidden Field Value Setting of Geos Upper Than R1
        copyCheckedValToHiddenVal('chkContinent', '#hidContinent');
        copyCheckedValToHiddenVal('chkCountry', '#hidCountry');
    
        if(ctrR1 == 1) {
            console.log('R1: Loop R2 - LC');
            emptroy('#tdR2', '#tdR3', '#tdR4', '#tdLC');
            copyCheckedValToHiddenVal('chkRegion1', '#hidRegion1');
            // Loop
            $.post('{{ URL::route("getGeoRegion2") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                if(hasValue(data)){
                    console.log('Found');
                    loopend(data, 'region2', '#tdR2', '#spinnery');
                } else {
                    console.log('Not Found');
                    $.post('{{ URL::route("getGeoRegion3") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                        if(hasValue(data)){
                            console.log('Found');
                            loopend(data, 'region3', '#tdR3', '#spinnery');
                        } else {
                            console.log('Not Found');
                            $.post('{{ URL::route("getGeoRegion4") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                                if(hasValue(data)){
                                    console.log('Found');
                                    loopend(data, 'region4', '#tdR4', '#spinnery');
                                } else {
                                    console.log('Not Found');
                                    $.post('{{ URL::route("getGeoLocality") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                                        if(hasValue(data)){
                                            console.log('Found');
                                            loopend(data, 'locality', '#tdLC', '#spinnery');
                                        } else {
                                            console.log('Not Found');
                                            emptroy('#tdLC');
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
        ipakita('#spinnery', false);
    } 
    
    else if(getLastSelectedGeo() == 'Region 2') {
        
        // Hidden Field Value Setting of Geos Upper Than R2
        copyCheckedValToHiddenVal('chkContinent', '#hidContinent');
        copyCheckedValToHiddenVal('chkCountry', '#hidCountry');
        copyCheckedValToHiddenVal('chkRegion1', '#hidRegion1');
    
        if(ctrR2 == 1) {
            console.log('R2: Loop R3 - LC');
            emptroy('#tdR3', '#tdR4', '#tdLC');
            copyCheckedValToHiddenVal('chkRegion2', '#hidRegion2');
            
            // Loop
            $.post('{{ URL::route("getGeoRegion3") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                if(hasValue(data)){
                    console.log('Found');
                    loopend(data, 'region3', '#tdR3', '#spinnery');
                } else {
                    console.log('Not Found');
                    $.post('{{ URL::route("getGeoRegion4") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                        if(hasValue(data)){
                            console.log('Found');
                            loopend(data, 'region4', '#tdR4', '#spinnery');
                        } else {
                            console.log('Not Found');
                            $.post('{{ URL::route("getGeoLocality") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                                if(hasValue(data)){
                                    console.log('Found');
                                    loopend(data, 'locality', '#tdLC', '#spinnery');
                                } else {
                                    console.log('Not Found');
                                    emptroy('#tdLC');
                                }
                            });
                        }
                    });
                }
            });
        }
        ipakita('#spinnery', false);
    } 
    
    else if(getLastSelectedGeo() == 'Region 3') {
        
        // Hidden Field Value Setting of Geos Upper Than R3
        copyCheckedValToHiddenVal('chkContinent', '#hidContinent');
        copyCheckedValToHiddenVal('chkCountry', '#hidCountry');
        copyCheckedValToHiddenVal('chkRegion1', '#hidRegion1');
        copyCheckedValToHiddenVal('chkRegion2', '#hidRegion2');
        
        if(ctrR3 == 1) {
            console.log('R3: Loop R4 - LC');
            emptroy('#tdR4', '#tdLC');
            copyCheckedValToHiddenVal('chkRegion3', '#hidRegion3');
            // Loop
            $.post('{{ URL::route("getGeoRegion4") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                if(hasValue(data)){
                    console.log('Found');
                    loopend(data, 'region4', '#tdR4', '#spinnery');
                } else {
                    console.log('Not Found');
                    $.post('{{ URL::route("getGeoLocality") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                        if(hasValue(data)){
                            console.log('Found');
                            loopend(data, 'locality', '#tdLC', '#spinnery');
                        } else {
                            console.log('Not Found');
                            emptroy('#tdLC');
                        }
                    });
                }
            });
        }
        ipakita('#spinnery', false);
    } 
    
    else if(getLastSelectedGeo() == 'Region 4') {
        
        // Hidden Field Value Setting of Geos Upper Than R4
        copyCheckedValToHiddenVal('chkContinent', '#hidContinent');
        copyCheckedValToHiddenVal('chkCountry', '#hidCountry');
        copyCheckedValToHiddenVal('chkRegion1', '#hidRegion1');
        copyCheckedValToHiddenVal('chkRegion2', '#hidRegion2');
        copyCheckedValToHiddenVal('chkRegion3', '#hidRegion3');
        
        if(ctrR4 == 1) {
            console.log('R4: Loop LC');
            emptroy('#tdLC');
            copyCheckedValToHiddenVal('chkRegion4', '#hidRegion4');
            // Loop
            $.post('{{ URL::route("getGeoLocality") }}', $('#frmCreatePost').serialize()).fail(function(err) { console.log(err); }).done(function(data) {
                if(hasValue(data)){
                    console.log('Found');
                    loopend(data, 'locality', '#tdLC', '#spinnery');
                } else {
                    console.log('Not Found');
                    emptroy('#tdLC');
                }
            });
        }
        ipakita('#spinnery', false);
    } 
    
    else if(getLastSelectedGeo() == 'Locality') {
        
        // Hidden Field Value Setting of Geos Upper Than LC
        copyCheckedValToHiddenVal('chkContinent', '#hidContinent');
        copyCheckedValToHiddenVal('chkCountry', '#hidCountry');
        copyCheckedValToHiddenVal('chkRegion1', '#hidRegion1');
        copyCheckedValToHiddenVal('chkRegion2', '#hidRegion2');
        copyCheckedValToHiddenVal('chkRegion3', '#hidRegion3');
        copyCheckedValToHiddenVal('chkRegion4', '#hidRegion4');
        
        if(ctrLC == 1) {
            console.log('Labeled CN - LC');
            copyCheckedValToHiddenVal('chkLocality', '#hidLocality');
        }
        ipakita('#spinnery', false);
    } 
    
    console.log('CN: '+$('#hidContinent').val()+'\t'+'CT: '+$('#hidCountry').val()+'\t'+'R1: '+$('#hidRegion1').val()+'\t'+'R2: '+$('#hidRegion2').val()+'\t'+'R3: '+$('#hidRegion3').val()+'\t'+'R4: '+$('#hidRegion4').val()+'\t'+'LC: '+$('#hidLocality').val());
    
});
</script>