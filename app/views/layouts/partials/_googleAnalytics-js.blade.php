<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/24/2015
 * Time: 7:55 AM
 *
 */ ?>

@if( App::environment() != 'local' )

 <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-60043580-1', 'auto');
    ga('send', 'pageview');

    @if(Auth::check())
        ga('set', '&uid', '{{ Auth::user()->userId  }}' ); // Set the user ID using signed-in user_id.
    @endif
</script>

    {{SettingsClass::get('googleAnalytics')}}

@endif
