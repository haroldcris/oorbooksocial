<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 01/30/2015
 * Time: 7:45 PM
 *
 */

    $colorNavbar  = SettingsClass::get('colorNavbar');
?>

<style>
    .navbar-fixed-top {
        background: #{{$colorNavbar}};
        z-index: 1023;
        height: 40px;
        width:100%
    }

    .topBar > ul{
        padding-top:10px;
    }


    .topBar > ul > li > a:hover,
    .topBar > ul > li > a:focus,
    .topBar > ul > li > a:active,
    .topBar > ul > li > a {
        color: green !important;
        background-color: #{{$colorNavbar}} !important;
    }

    .topBar > ul > li {
        border: none;;
        width:36px;
    }


    ul.profile{
        margin:0;
    }
    .profile li{
        list-style: outside none none;
        list-style-type: none ;
    }
    .profile li>input,
    .profile > li>a{
        text-decoration: none;
        color: #000;
        display: block;
        line-height: inherit;
        text-align: center;
        height: 100%;
        /*width: auto;*/
        min-width: 50px;
        padding: 7px 5px;
        position: relative;
        margin-top:3px;
    }

    {{--.profile li>a:hover{--}}
        {{--background-color: #{{$colorNavbar}};--}}
        {{--box-shadow: 0px 0px 1px #000;--}}
    {{--}--}}

    .profile img{
        border-radius: 100%;
        width:35px;
    }

    #topbarSearchResult  ul {
        min-width:300px;
        width:320px;
    }
    #topbarSearchResult > ul > li {
        width:100%;
        height:auto;
    }
    #topbarSearchResultList {
        width:100% !important;
    }

</style>

<div class="navbar-fixed-top" id="breadcrumb" >

	<div class="container">
	    @if(Auth::guest())
	    <div class="pull-right">
            <ul class="profile">
            <li>
                <a href="/" style="">Log In</a>
            </li>
             </ul>
             </div>
	    @endif
	    @if(Auth::check())
            <div class="pull-right">
                <ul class="profile">
                    <li style="width:200px" class="">
                        <div class="input-group" style="margin-top:6px;">

                            <input id="searchOorbook" type="text" style="height:26px;" placeholder="Search Oorbook" class="form-control search-query" onkeyup="searchForUser(this)">

                            <span class="input-group-btn">
                                <span class="btn btn-purple btn-xs">
                                    <span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
                                </span>
                            </span>

                            <div  id="topbarSearchResult">
                               <ul class="navbar-green dropdown-navbar dropdown-menu pull-right">
                                   <li class="dropdown-header">
                                        <i class="fa fa-search"></i> Search Oorbook
                                        <a href="javascript:void(0);" class="pull-right" onclick="$('#searchOorbook').val(''); toggleSearchResult(false)" ><i class="ace-icon fa fa-times"></i></a>
                                   </li>
                                   <li class='dropdown-contents slimscroll'>
                                        <ul class="dropdown-navbar" id="topbarSearchResultList" >

                                        </ul>
                                   </li>
                                   <li><a href="#">&nbsp;</a></li>
                               </ul>
                            </div>
                        </div>
                    </li>

                    <!-- <li>
                        <a href="/dashboard" style=""><i class="fa fa-home fa-2x green"></i></a>
                    </li> -->

                    <!-- <li class="pull-right">
                        <a href="{{URL::route('userProfile')}}">
                            <img src="{{ URL::route('userAvatar',$AuthUser->userId) }}" >
                            {{ ucfirst($AuthUser->username)}}
                        </a>
                    </li> -->

                </ul>
            </div>
            <div class="pull-left">
                <a href="/dashboard"><img src="/assets/images/logo.png" style="height: 31px; width: 35px; margin-top: 4px; margin-left: 10px;"></a>
            </div>
        @endif


    </div>

 </div>


@if(Auth::check())
    <script>
        function searchForUser(ctrl, resultContainer, callbackFunction) {
            if (resultContainer == undefined ) {
                if($(ctrl).val()==''){
                    return toggleSearchResult(false);
                }
                toggleSearchResult(true);
            }

            clearTimeout($.data(ctrl, 'timer'));
            var search_string = $(ctrl).val();
            // Do Search
            if (search_string == '') {
                //$("#SearchLocationResult").fadeOut();
            }else{
                if (resultContainer == undefined ) {
                    $("#topbarSearchResultList").html('<li><i class="ace-icon fa fa-spinner fa-spin"></i></li>');
                } else{
                    $(resultContainer).html('{{Config::get('site.loader')}}');
                }

                if(callbackFunction == undefined) {
                    $(this).data('timer', setTimeout(searchApiForUser(ctrl), 1000));
                } else {
                    $(this).data('timer', setTimeout(callbackFunction(ctrl), 1000));
                }
            }
        }

        function searchApiForUser(ctrl){
           var _url = "/api/v1/users?limit=10&username=" + $(ctrl).val();
           $.get(encodeURI(_url))

            .done(function(d){
               
                if (d.length == 0){
                    return $("#topbarSearchResultList").html('<li><i>*** No results found *** </i></li>');                
                }

                var _html='';

                for(var i = 0; i < d.length; i++){
                    switch(d[i].type){
                        case 'user':
                            _html += '<li><span class="pull-right badge badge-success" style="margin-top:10px;"><small>User&nbsp;</small></span><a href="/profile/' + d[i].username + '" style="text-decoration:none"> <img src="' + d[i].imageAvatar + '"/> <span>' + d[i].username  + '</span></a></li>';
                            break;

                        case 'group':
                            _html += '<li style="padding-top:5px;"><a href="/groups/' + d[i].groupId + '" title="' + d[i].name + '" > \
                                    <span class="pull-left" style="max-width:350px"><b>' +  d[i].name + '</b> <i>' + d[i].owner.username + '</i></span> \
                                    <span class="pull-right badge badge-info" style="margin-top:10px;"><small>Group</small></span> \
                                    </a></li>';
                            break;

                        case 'post':
                            var str = d[i].title;

                            _html += '<li style="padding-top:5px;"><a href="/post/' + d[i].postId + '"  > \
                                        <span class="pull-left" style="max-width:350px"><b>' +  d[i].title + '</b><br /><i>' + d[i].creator.username + '</i></span> \
                                        <span class="pull-right badge badge-warning" style="margin-top:10px;"><small>Post</small></span> \
                                        </a></li>';
                            break;
                        
                        case 'tags':
                            
                                        _html += '<li style="padding-top:5px;"><a href="/post/' + d[i].postId + '"  > \
                                        <span class="pull-left" style="max-width:350px"><b>' +  d[i].matter + '</b><br /><i>' + d[i].creator.username + '</i></span> \
                                        <span class="pull-right badge" style="background: #880085 !important; margin-top: 10px;"><small>Tag</small></span> \
                                        </a></li>';
                            break;
                            
                    }

                }
               
                
                $("#topbarSearchResultList").html(_html + '<li></li><br>');
                if(d.length >10 ){ $('.slimscroll').slimScroll({width:'100%', height:'150px'}) }

            });
        }

        function toggleSearchResult(state){
            if (state == true ){
                return $("#topbarSearchResult").addClass('open');
            }
            return $("#topbarSearchResult").removeClass('open');
        }
    </script>

 @endif