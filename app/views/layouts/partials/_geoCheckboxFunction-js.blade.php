<script>

var ctrCN = '{{ $ctrCN }}';
var ctrCT = '{{ $ctrCT }}';
var ctrR1 = '{{ $ctrR1 }}';
var ctrR2 = '{{ $ctrR2 }}';
var ctrR3 = '{{ $ctrR3 }}';
var ctrR4 = '{{ $ctrR4 }}';
var ctrLC = '{{ $ctrLC }}';

// Loop through geos then append
function loopend(data, geo, element, spinnery){
    for(var i=0; i<data.length; i++) {
        var geos;
        var name;
        switch(geo) {
            case 'continent':
                geos = data[i].continent;
                name = "Continent";
                break;
            case 'country':
                geos = data[i].country;
                name = "Country";
                break;
            case 'region1':
                geos = data[i].region1;
                name = "Region1";
                break;
            case 'region2':
                geos = data[i].region2;
                name = "Region2";
                break;
            case 'region3':
                geos = data[i].region3;
                name = "Region3";
                break;
            case 'region4':
                geos = data[i].region4;
                name = "Region4";
                break;
            case 'locality':
                geos = data[i].locality;
                name = "Locality";
                break;
        }
        var newOption = $('<label class="boxfix"><input type="checkbox" class="chk'+ htmlspecialchars(name) +'" name="geo' + htmlspecialchars(name) + '[]" value="' + htmlspecialchars(geos) + '">' + htmlspecialchars(geos) + '</label><br>');
        $(element).append(newOption);
    }
    ipakita(element);
    ipakita(spinnery, false);
}

// Set to empty then destroy
function emptroy(){
    if(arguments.length == 0) return;
    for(var i=0; i<arguments.length; i++) {
        $(arguments[i]).empty().css('display', 'none');
    }
}

// Check if data has a value
function hasValue(data) {
    if(typeof data !== 'undefined' && data.length > 0){
        return true;
    }
}

// Escape string
function htmlspecialchars(str) {
    if (typeof(str) == "string") {
        str = str.replace(/&/g, "&amp;"); /* must do &amp; first */
        str = str.replace(/"/g, "&quot;");
        str = str.replace(/'/g, "&#039;");
        str = str.replace(/</g, "&lt;");
        str = str.replace(/>/g, "&gt;");
    }
    return str;
}

// Set visibility [ default: visible ]
function ipakita(element, bool){
    // If bool is not set
    if(typeof(bool)==='undefined') {
        $(element).css('display', 'inline-block');
    } else {
        $(element).css('display', 'none');
    }
}

// Set fields to empty values
function emptiness() {
    if(arguments.length == 0) return;
    for(var i=0; i<arguments.length; i++) {
        $(arguments[i]).val('');
    }
}

function getLastSelectedGeo() {
    // Returns the last selected geo, starts with the locality(last).
    if(ctrLC != 0) {
        return 'Locality';
    } else if(ctrR4 != 0) {
        return 'Region 4';
        
    } else if(ctrR3 != 0) {
        return 'Region 3';
        
    } else if(ctrR2 != 0) {
        return 'Region 2';
        
    } else if(ctrR1 != 0) {
        return 'Region 1';
        
    } else if(ctrCT != 0) {
        return 'Country';
        
    } else if(ctrCN != 0) {
        return 'Continent';
    }
}

function setValue(element, data) {
    $(element).empty().append(data).css('font-size', '10px');
}

function copyCheckedValToHiddenVal(classOfCheckedCheckbox, idOfHiddenInput) {
    $("input:checkbox[class=" + classOfCheckedCheckbox + "]:checked").each(function() {
        $(idOfHiddenInput).val($(this).val());
    });
}
</script>