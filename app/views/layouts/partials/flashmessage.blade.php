<div>

@if(Session::has('flashMessageError'))
    <div class="alert alert-danger" style="width:100%">
        <!-- <button data-dismiss="alert" class="close" type="button"> -->
            <!-- <i class="ace-icon fa fa-times"></i> -->
        </button>
        <b>Error</b><br>{{Session::get('flashMessageError')}}
    </div>
    <?php Session::forget('flashMessageError'); ?>
@endif
                   
@if(Session::has('flashMessageInfo'))
    <div class="alert alert-info" style="width:100%" >
        <!-- <button data-dismiss="alert" class="close" type="button"> -->
            <!-- <i class="ace-icon fa fa-times"></i> -->
        </button>
        <b>Notification</b><br>{{Session::get('flashMessageInfo')}}
    </div>
    <?php Session::forget('flashMessageInfo'); ?>
@endif


@if(Session::has('flashMessageSuccess'))
    <div class="alert alert-success" style="width:100%" >
        <!-- <button data-dismiss="alert" class="close" type="button"> -->
            <!-- <i class="ace-icon fa fa-times"></i> -->
        </button>
        {{Session::get('flashMessageSuccess')}}
    </div>
    <?php Session::forget('flashMessageSuccess'); ?>
@endif

</div>