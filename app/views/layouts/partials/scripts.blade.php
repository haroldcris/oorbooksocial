<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 01/29/2015
 * Time: 9:55 PM
 *
 */
 ?>


<!-- basic scripts -->
<script src="/themes/bootstrap/js/bootstrap.js"></script>

<!-- page specific plugin scripts -->
<script src="/themes/ace/js/jquery.js"></script>
<script src="/themes/ace/js/jquery-ui-1.js"></script>
<script src="/themes/ace/js/jquery_007.js"></script> <!-- UI Touch Punch  -->
<script src="/themes/ace/js/jquery_006.js"></script>
<script src="/themes/ace/js/jquery_002.js"></script>

<script src="/themes/ace/js/ace-elements.min.js"></script>
<script src="/themes/ace/js/ace.min.js"></script>

<script src="/controls/cycle2/jquery.cycle2.min.js"></script>
<script src="/controls/cycle2/jquery.cycle2.prevnext.js"></script>
<script src="/controls/cycle2/jquery.cycle2.pager.js"></script>

<script src='/controls/datetimepicker/jquery.datetimepicker.js'></script>


<script src='/assets/js/get-url-params.js'></script>

<script>
    (function($){
        $.fn.loaderIcon = function(_showHide){
            var icon = this.find('i');

            if (icon.attr('data-icon') == undefined) icon.attr('data-icon', icon.attr('class'));
            if(_showHide == 'show') {
                if(icon.length == 0)  return $(this).prepend('<i class="fa fa-refresh fa-spin" data-icon=""></i>');
                return icon.attr('class','fa fa-refresh fa-spin');
            }

            if(_showHide == 'hide') icon.attr('class', icon.attr('data-icon'));
        }
        }(jQuery));
</script>
    @include('layouts.partials._sticky-js')
    @include('layouts.partials._postData-js')
    @include('layouts.partials._postFile-js')

    @include('layouts.partials._sidebar-js')
    @include('layouts.partials._sidebarGroups-js')


    @include('accounts.partials._changepassword-js')
    @include('layouts.partials._select2-js')
    @include('layouts.partials._bootstrapDialog-js')
    @include('layouts.partials._slimScroll')
    @include('layouts.partials._gritter-js')
    @include('layouts.partials._dropzone-js')


    @include ('calendar._calendar-js')

    @include('layouts.menu._location-js')
    @include('layouts.menu._notification-js')
    @include('layouts.menu._weather-js')


    @include('user.post._post-js')
    @include('user.post.attachment._youtube-js')


    @include('layouts.rss._rssScroller-js')
    @include('notice._notice-js')

    @include('layouts.partials._selectize-js')
