<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

@if(Auth::check())
    <script>
       console.log('loading sidebar menu');
        $(function(){

            $.get('/api/v1/subjects').done(function(data){
                var _html = '<ul class="sort-subject nav nav-list">';

                 for( var i = 0 ; i < data.length;i++){
                      _html +='<li id="' + data[i].subjectId + '" class="list-item"><div class="inline position-relative" style="padding-left:5px">' +
                              '<a href="/interest/' + encodeURI (data[i].description) + '" title ="' + data[i].description + '" >' +
                              '<img src="/assets/images/icons/' + data[i].icon + '" class="sideMenu" style="display:none">' +
                              '<span class="menu-text">' + data[i].description + '</span>' +
                              '</a>' +
                              '</div></li>';
                 }
                 _html += '</ul>';

                 $('.sidebar-menu.sidebar').append(_html);

                  // Set sortable list
                  $(function() {
                      $('ul.sort-subject').sortable({
                            onCancel: function ($item, container, _super, event) {
                                console.log('onCancel event');
                            },
                            afterMove: function ($placeholder, container, $closestItemOrContainer) {
                                console.log('afterMove event');
                            },
                            onDrag: function ($item, position, _super, event) {
                                console.log('onCancel event');
                            },
                            onDragStart: function ($item, container, _super, event) {
                                console.log('onDragStart event');
                            },
                            onDrop: function ($item, container, _super, event) {
                                console.log('onDrop event');
                            },
                            onMousedown: function ($item, _super, event) {
                                console.log('onMousedown event');
                            }
                      })
                  });
            });

        });

    </script>
@endif



<script>
    $("#sidebar-collapse").on('click',function () {
        if ($("#sidebar").hasClass('menu-min')) {
                $("img.sideMenu").hide();
                $(".tweeter").appendTo('#sidebarads');
                $('#sidebarads').show();
                $('.main-content').css('margin-left','195px');

        } else {
                $('#sidebarads').hide();
                $("img.sideMenu").show();
                $(".tweeter.twit").appendTo("#bottomads1");
                $(".tweeter.rssfeed").appendTo("#bottomads2");
                $('.main-content').css('margin-left','45px');
        }
    });
</script>


