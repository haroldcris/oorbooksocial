<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/01/2015
 * Time: 6:43 PM
 *sdfsdg
 */ ?>

<div class="breadcrumbs" id="breadcrumbLocation">
    <i class="fa fa-globe bigger-130" ></i>
    @if(isset($selectedGroup))
        @if(!is_null($selectedGroup))
            {{ucfirst($selectedGroup->owner->username)}} » Groups » {{ ucfirst($selectedGroup->name) }}
        @endif
    @else
{{--        @if(strtolower(Auth::user()->securityLevel) != 'master user')--}}
            @if(isset($selectedLocation))
                @if($selectedLocation->continent    )» {{$selectedLocation->continent}}@endif
                @if($selectedLocation->country      )» <a href="{{ URL::route('divisionCountry', $selectedLocation->locationId) }}">{{$selectedLocation->country}}@endif</a>
                @if($selectedLocation->region1      )» <a href="{{ URL::route('divisionRegion1', $selectedLocation->locationId) }}">{{$selectedLocation->region1}}@endif</a>
                @if($selectedLocation->region2      )» <a href="{{ URL::route('divisionRegion2', $selectedLocation->locationId) }}">{{$selectedLocation->region2}}@endif</a>
                @if($selectedLocation->region3      )» <a href="{{ URL::route('divisionRegion3', $selectedLocation->locationId) }}">{{$selectedLocation->region3}}@endif</a>
                @if($selectedLocation->region4      )» <a href="{{ URL::route('divisionRegion4', $selectedLocation->locationId) }}">{{$selectedLocation->region4}}@endif</a>
                @if($selectedLocation->locality     )» <a href="{{ URL::route('divisionLocality', $selectedLocation->locationId) }}">{{$selectedLocation->locality}}@endif</a>
    	        @if($selectedLocation->postal       )» {{$selectedLocation->postal}}@endif
            @endif
    {{--    @else
            @if(isset($selectedLocation))
                @if($selectedLocationScope->continent    )» {{$selectedLocationScope->continent}}@endif
                @if($selectedLocationScope->country      )» <a href="{{ URL::route('locationScopeCountry', $selectedLocationScope->locationId) }}">@endif</a>
                @if($selectedLocationScope->region1      )» <a href="{{ URL::route('locationScopeRegion1', $selectedLocationScope->locationId) }}">@endif</a>
                @if($selectedLocationScope->region2      )» <a href="{{ URL::route('locationScopeRegion2', $selectedLocationScope->locationId) }}">@endif</a>
                @if($selectedLocationScope->region3      )» <a href="{{ URL::route('locationScopeRegion3', $selectedLocationScope->locationId) }}">@endif</a>
                @if($selectedLocationScope->region4      )» <a href="{{ URL::route('locationScopeRegion4', $selectedLocationScope->locationId) }}">@endif</a>
                @if($selectedLocationScope->locality     )» <a href="{{ URL::route('dlocationScopeoLcality', $selectedLocationScope->locationId) }}">@endif</a>
    	        @if($selectedLocationScope->postal       )» {{$selectedLocationScope->postal}}@endif
            @endif
        @endif--}}
    @endif
</div>