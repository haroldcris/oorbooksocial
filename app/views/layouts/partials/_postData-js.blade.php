<script src='/assets/js/promise.js'></script>
<script>
    function postMyData(formName,dialogRef){
        return new RSVP.Promise (function(resolve, reject){
            var form = $('#' + formName);
            //add token
            if($(form).find('input[name="_token"]').length == 0){
                $(form).append('{{Form::token()}}');    
            }
            
            var action = $(form).attr('action');
            if (action == undefined ) { return reject(Error('No URL Action'));}

            if(dialogRef != undefined && typeof dialogRef != 'object' ){
                //console.log('DialogRef must be an object!');
                return reject(Error('DialogRef must be an object!'));
            }

            if (dialogRef != undefined && dialogRef != ''){
                dialogRef.getButton('dialogOkBtn').spin().disable();
                //console.log('Posting inside a Modal Dialog Box');
            }

            $('.errorHeaderStatus').hide();
            $.post(action, form.serialize())
                .fail(function(data){
                    var _err;
                    //console.log('Error posting to ' + action);

                    switch(data.status){
                        case 400:
                        case 406:
                            _err = JSON.parse(data.responseText).error;
                            break;

                        default:
                            _err = "Error: " + data.status + "<br>" + data.statusText;
                            break;
                    }

                    showPostError(dialogRef,_err);
                    return reject(Error(data['error']));
                })

                .done(function(data){
                    if(typeof data != 'object'){
//                        return reject(Error('Post request invalid reply'));
                        return reject(Error(data));
                    }

                    if(data['success'] == "true" || data.success == true){
                        //console.log('success posting to ' + action);
                        return resolve(data);
                    } else{
                        //console.log('success posting to ' + action + ' but with error');
                        showPostError(dialogRef, data['error']);
                        //return reject(Error(data));
                        return reject(data);
                    }
                })

                .always(function(){
                    if (dialogRef != undefined && dialogRef != ''){
                        dialogRef.getButton('dialogOkBtn').stopSpin().enable();
                    }
                });
        });
    }


    function showPostError(dialogRef,_error){
        if (dialogRef != undefined && dialogRef != ''){
            var _errStatus = dialogRef.getModalBody().find('.errorHeaderStatus');
            if(_errStatus.length == 0){
                dialogRef.getModalBody().append('<br><div class="errorHeaderStatus alert alert-danger" style="margin-bottom:0px !important">' + _error  + '</div>');
            }else{
                return $(_errStatus).html(_error).show();
            }
        }
    }

</script>


