<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 01/30/2015
 * Time: 4:17 PM
 *
 */

?>


<div class="sidebar-main" id="sidebar" style="max-width:189px; width:189px !important; height:auto; float:left;" >
    {{--<div id="sidebar-shortcuts-mini" class="sidebar-shortcuts-mini text-center">--}}
        {{--<a href="/dashboard" style="font-size:12pt !important;"><i class="fa fa-home bigger-140"></i></a>--}}
    {{--</div>--}}
    {{--<div id="sidebar-shortcuts-large" class="sidebar-shortcuts-large ">--}}
        {{--<ul class="nav ace-nav" >--}}
            {{--<li ><a href="/dashboard" style="font-size:10pt !important;"><i class="fa fa-home bigger-120"></i> Home</a></li>--}}
        {{--</ul>--}}
    {{--</div>--}}

    {{--<div class="sidebar-collapse" id="sidebar-collapse">--}}
        {{--<i data-icon2="ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-left" class="glyphicon glyphicon-record"></i>--}}
    {{--</div>--}}


    @if(Auth::check())
        <div class="sidebar sidebar-menu" >
            <div class="label label-info" style="width: 100%;"><b style="line-height: 16px;">Interests</b></div>

            {{--Sidebar Menu Subjects will be dynamically inserted here through ajax --}}

        </div>
    
        <div class="sidebarGroups">
            <div class="label label-info" style="width: 100%;margin-top:10px"><b style="line-height: 16px;">Groups</b></div>
            <ul>
                @foreach($groupSubscriptions->data as $item)
                    <li class="sidebar-group"  style="padding-left: 5px; height: 22px; background: #f2f2f2; border-bottom: 1px solid #e5e5e5; border-top: 1px solid #f5f5f5;" id="sidebarGroup{{$item->groupId}}">
                        <div class="inline position-relative" style="max-width: 150px !important; white-space: nowrap; overflow: hidden;">
                            <a href="{{URL::route('groupPosts', $item->groupId)}}" title ="{{ $item->groupId }}" style="color: #428bca;">
                                <span class="menu-text" title="{{$item->name}}"> {{ strlen($item->name) < 22 ? $item->name : substr($item->name,0,22) .'...' }}</span>
                            </a>
                        </div>

                        <div class="inline position-relative pull-right" style="padding-right:5px;">
                            @if($item->creatorId != Auth::user()->userId)
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true">
                                    <i class="ace-icon fa fa-angle-down bigger-120"></i>&nbsp;&nbsp;
                                </a>

                                <ul class="dropdown-menu dropdown-yellow dropdown-caret dropdown-75 pull-right">
                                    <li>
                                        <a href="javascript:void(0);" onclick="leaveFromGroup({{$item->groupId}})">
                                            <i class="ace-icon fa fa-trash-o "></i>
                                            Leave Group
                                        </a>
                                    </li>
                                </ul>
                            @else
                                &nbsp;&nbsp;&nbsp;
                            @endif
                        </div>


                        {{--<span class="pull-right" style="padding-right:5px;">{{ rand(1,1000)}}</span>--}}

                    </li>
                @endforeach
            </ul>

        </div>

        <div class="space-10"></div>
    @endif

    <div style="float:left;width:100%" >

        @include('layouts.rss.notice')


    </div>


    {{--<div class="sidebar" id="sidebarads">--}}

        {{--<div class="space-5"></div>--}}

        {{--<div id="rssfeed" class="tweeter rssfeed" style="height: auto; background-color: white; text-align: center;">--}}
            {{--<!-- start feedwind code --><iframe width="100%" frameborder="0" hspace="0" vspace="0" marginheight="0" marginwidth="0" name="rssmikle_frame" scrolling="no" src="http://feed.mikle.com/widget/?rssmikle_url=http%3A%2F%2Fwww.nytimes.com%2Fservices%2Fxml%2Frss%2Fnyt%2FTechnology.xml&amp;rssmikle_frame_width=100%25&amp;rssmikle_frame_height=500&amp;frame_height_by_article=1&amp;rssmikle_target=_blank&amp;rssmikle_font=Arial%2C%20Helvetica%2C%20sans-serif&amp;rssmikle_font_size=12&amp;rssmikle_border=off&amp;responsive=off&amp;text_align=left&amp;text_align2=left&amp;corner=off&amp;scrollbar=off&amp;autoscroll=on&amp;scrolldirection=up&amp;scrollstep=3&amp;mcspeed=20&amp;sort=New&amp;rssmikle_title=on&amp;rssmikle_title_bgcolor=%230066FF&amp;rssmikle_title_color=%23FFFFFF&amp;rssmikle_item_bgcolor=%23FFFFFF&amp;rssmikle_item_title_length=55&amp;rssmikle_item_title_color=%230066FF&amp;rssmikle_item_border_bottom=on&amp;rssmikle_item_description=on&amp;item_link=off&amp;rssmikle_item_description_length=150&amp;rssmikle_item_description_color=%23666666&amp;rssmikle_item_date=gl1&amp;rssmikle_timezone=Etc%2FGMT&amp;datetime_format=%25b%20%25e%2C%20%25Y%20%25l%3A%25M%3A%25S%20%25p&amp;item_description_style=text&amp;item_thumbnail=full&amp;article_num=15&amp;rssmikle_item_podcast=off&amp;iframe_id=feedwind_6271422590413624&amp;" id="feedwind_6271422590413624" style="height: 147px;"></iframe><div style="font-size:10px; text-align:center; width:100%;"><a style="color:#CCCCCC;" target="_blank" href="http://feed.mikle.com/">RSS Feed Widget</a><!--Please display the above link in your web page according to Terms of Service.--></div><!-- end feedwind code -->--}}
        {{--</div>--}}
    {{--</div>--}}
</div> 

{{-- <script src="{{ URL::to('/controls/html5sortable-master/dist/html.sortable.min.js') }}"></script> --}}
<script src="{{ URL::to('/controls/jquery-sortable-master/source/js/jquery-sortable.js') }}"></script>