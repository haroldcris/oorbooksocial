<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 01/29/2015
 * Time: 9:17 PM
 *
 */

?>

@extends('layouts.template')


@section('primaryPage')
    <div class="page-content" style="width:710px;float:left;padding:5px;background: none;">
        @include('layouts.menu.menu')

        @include ('layouts.partials.flashmessage')

        <!-----------Start Here------------->
        @yield('pageContent')

    </div><!-- /.page-content -->

@stop
