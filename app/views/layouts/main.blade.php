<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 01/29/2015
 * Time: 9:17 PM
 *
 */

?>

@extends('layouts.template')

@section('styles')
<style type="text/css">
#feedwind_7981428636972487 {
    width: 188px !important;
    height: 170px !important;
}

#feedwind_7981428636972487 .scroll-pane {
    height: 130px;
}
</style>
@stop

@section('primaryPage')
    <div class="page-content" style="width:710px;float:left;padding:5px;background: none;">
        @include ('layouts.partials.flashmessage')
        <!--Start Here-->
        @if(Auth::check())
            @include('layouts.menu.menu')
        @endif

        @yield('pageContent')

        <div >
            <!-- <div id="bottomads1" style="width:590px;margin-top:10px;" > -->
                {{--SettingsClass::get('footerWidget',true)--}}
            <!-- </div> -->
            {{--<div id="bottomads2" class="col-md-6"></div>--}}

            <div>
                <footer>
                    <hr/>
                    <p style="text-align: center"><a onclick="return pop_up('/eula', 'Pop Up')" style="text-decoration: none; cursor: pointer;">Terms and Conditions</a></p>
                    <p class="text-center">&copy;2012-2015 Oorbook Inc.</p>
                </footer>
            </div>

        </div>

    </div><!-- /.page-content -->
@stop

@section('rightAds')
    <!--Right Panel Ads-->
    <div id="stickyRightAds" class="rightAds" style="width:200px;max-width: 200px; overflow-y: visible;padding: 0;">
         <style>
            .rightAds-panel p img{
                display:block;
                padding: 0;
                margin:auto;
                margin-left:auto;
                margin-right:auto;
            }

            .cycle-slideshow img { width: 100%; height: auto }
        </style>

        <?php
            $ads = [SettingsClass::get('GoogleAds1', true),
                    SettingsClass::get('GoogleAds2', true),
                    SettingsClass::get('GoogleAds3', true)]
        ?>
        @for($i=0;$i<3;$i++)

        <div class="panel panel-info rightAds-panel" style="width:200px; height:200px;padding: 0;">
            <div class= "cycle-slideshow photo"
                data-cycle-loader=true
                data-cycle-fx="scrollVert"
                data-cycle-timeout=5000
                data-cycle-pause-on-hover="true"
                data-cycle-manual-fx="scrollHorz"
                data-cycle-manual-speed="200"
                data-cycle-slides="> p"
            >
                {{ $ads[$i]; }}
            </div>
        </div>
        @endfor
        
	<div class="panel panel-info rightAds-panel text-center" style="width:200px;height:200px;padding: 0;" >
        	Bulk Sale
        </div>
        
        <div class="panel panel-info rightAds-panel" style="width:200px;height:200px;" >
        	{{SettingsClass::get('footerWidget',true)}}
        </div>
        
        

        {{--<!-- <div id="bottomads1" style="width:590px; margin-top:10px; height: 200px;" > -->--}}
        {{--<div id="bottomads1" style="width:200px; margin-top:10px; height: 350px;" >--}}
            {{--{{SettingsClass::get('footerWidget',true)}}--}}
        {{--</div>--}}

    </div>
@stop