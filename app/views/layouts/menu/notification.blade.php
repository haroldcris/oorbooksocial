<?php
    //$log = LogBook::all();
?>

<div class="dropdown" style="position: absolute">
    <a data-toggle="dropdown" class="dropdown-toggle" data-function="notification" >
        <i data-function="notificationBadge" class="notification-icon icon-animated-vertical hidden"> </i>
        <span class="fa-stack fa-lg blue">
            <i data-function="notificationIcon" class="blue fa fa-bell-o bigger-130"></i>
        </span>
    </a>

    <ul class="navbar-green dropdown-navbar dropdown-menu dropdown-caret " style="width:300px;min-width: 200px">

        <li class="dropdown-header">
            <i class="ace-icon fa fa-bell-o"></i>
            Notifications
        </li>

        <li class="dropdown-contents slimscroll ">
            <ul class="navbar-green dropdown-navbar" data-function="notificationDropDown" style="width:100%;" >
            </ul>
        </li>

        <li class="dropdown-footer">
            <a href="#">
                {{--See all messages--}}
                {{--<i class="ace-icon fa fa-arrow-right"></i>--}}
            </a>
        </li>
    </ul>

</div>