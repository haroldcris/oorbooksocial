{{--Item 6  -- Calendar --}}

<div class="dropdown" style="position: absolute">
    <a data-toggle="dropdown" class="dropdown-toggle"  >
    {{--<a data-toggle="dropdown" class="dropdown-toggle" href="{{URL::route('calendarEvents')}}" >--}}
        {{--<i class="notification-icon icon-animated-vertical">12+ </i>--}}
       <span class="fa-stack fa-lg green">
         <i class="fa fa-calendar"></i>
       </span>
    </a>
        <ul class="navbar-pink dropdown-navbar dropdown-menu dropdown-caret" style="width:400px;min-width: 200px" >
            <li class="dropdown-header"><i class="fa fa-calendar"></i> Calendar Events</li>
            {{--@if(Session::has('selectedUserLocationId'))--}}
                {{--<li><a href="javascript:void(0)" onclick="addEvent()"><i class="fa fa-plus"></i> Create Event</a></li>--}}
                {{--<li><a href="{{ URL::route('calendarEvents', Session::get('selectedLocationId')) }}" id="ProfilePicDelete"><i class="fa fa-calendar-o"></i> Show Events</a></li>--}}
            {{--@else--}}
                {{--<li><a href="javascript:void(0)"><i class="fa fa-plus"></i> Create Event</a></li>--}}
                {{--<li><i>Please select your location before viewing calendar events.</i></li>--}}
            {{--@endif--}}


            @if(count($enrolledLocations)== 0)
                <li><a href="javascript:void(0)"><span style="color:blue">You have not enrolled to any location yet.<br><i>Click Add Location to enroll to a new location.</span></i></a></li>
            @else
                    @if(strtolower(Auth::user()->securityLevel) != 'master user')
                        @foreach ($enrolledLocations as $item)
                            <li >
                                <a href="{{ URL::route('calendarEvents',['locationId' => $item->locationId] ) }}" title = "{{$item->pivot->address}}" style="white-space:normal; padding-top:2px;padding-bottom:2px;">
                                    <div style="margin-left:20px;">
                                        {{$item->name}}
                                    </div>
                                </a>
                            </li>
                        @endforeach
                    @else
                        <li >
                            <a href="{{ URL::route('calendarEvents',['locationId' => $selectedLocation->locationId] ) }}" style="white-space:normal; padding-top:2px;padding-bottom:2px;">
                                <div style="margin-left:20px;">
                                    {{$selectedLocation->name}}
                                </div>
                            </a>
                        </li>
                    @endif
    
                    
            @endif
            
        </ul>
</div>
