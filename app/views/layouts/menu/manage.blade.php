<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/10/2015
 * Time: 11:33 AM
 *
 */?>

<div class="dropdown" style="position: absolute">
       <a data-toggle="dropdown" class="dropdown-toggle"  >
             <span class="fa-stack fa-lg ">
               <i class="fa fa-wrench bigger-120 purple"></i>
             </span>
          </a>

 {{--<a data-toggle="dropdown" href="#" class="dropdown-toggle">--}}
                            {{--Manage <i class="ace-icon fa fa-caret-down"></i>--}}
                        {{--</a>--}}

        <ul class="dropdown-navbar dropdown-menu dropdown-caret dropdown-close pull-right ">
            <li>
                <a id="changePassword" href="javascript:void(0)" class="popup" onclick="changePasswordPost()"><i class="fa fa-key"></i> Change Password</a>
            </li>

		        @if(Auth::user()->canManage('role'))
                <li class="dropdown-header" style="line-height: 2em">Admin Tasks</li>
                @endif
                
                @if(Auth::user()->canManage('sitecontent'))
                <li>
                    <a href="{{ URL::route('settings') }}">
                        <i class="glyphicon glyphicon-cog"></i> Site Content
                    </a>
                </li>
                @endif

                @if(Auth::user()->canManage('poll'))
                <li>
                    <a href="{{ URL::route('managePolls') }}">
                        <i class="glyphicon glyphicon-tasks"> </i> Manage Polls
                    </a>
                </li>
                @endif

                @if(Auth::user()->canManage('interest'))
                <li id="Manage Subjects">
                    <a href="{{ URL::route('manageSubject') }}">
                        <i class="glyphicon glyphicon-book"> </i> Manage Interest
                    </a>
                </li>
                @endif

                @if(Auth::user()->canManage('user'))
                <li id="Manage Users">
                    <a href="{{ URL::route('manageUser') }}">
                        <i class="glyphicon glyphicon-user"> </i> Manage Users
                    </a>
                </li>
                @endif

                @if(Auth::user()->canManage('api'))
                <li>
                    <a href="{{URL::route('manageNoticeApi')}}">
                        <i class="glyphicon glyphicon-star"></i> Manage API Keys
                    </a>
                </li>
                @endif

                @if(Auth::user()->canManage('role'))
                <li>
                    <a href="{{URL::route('manageRoles')}}">
                        <i class="fa fa-users" style="font-size: 15px; padding-right: 3px;"></i> Manage Roles
                    </a>
                </li>
                @endif
                
                @if(Auth::user()->canManage('locationGroup'))
                <li>
                    <a href="{{URL::route('manageLocationGroups')}}">
                        <i class="fa fa-globe bigger-130" style="padding-right: 6px;"></i> Manage Location Groups
                    </a>
                </li>
                @endif
                
                @if(Auth::user()->canManage('locationScope'))
                <li>
                    <a href="{{URL::route('manageLocationScope')}}">
                        <i class="fa fa-globe bigger-130" style="padding-right: 6px;"></i> Manage Location Scope
                    </a>
                </li>
                @endif
                
                @if(strtolower(Auth::user()->securityLevel) != 'user')
                    <li style="padding:15px 0 15px 10px" >
                        <small>Theme Color</small>
                        <div style="background-color:#032ffc;display:inline-block;color:white; cursor: pointer;padding:5px;margin:1px;width:25px" value="#222A2D" data-skin="default" class="theme-color">
                            <i class="{{Auth::user()->themeColor == ''||Auth::user()->themeColor == 'default' ? 'fa fa-check': ''}}">&nbsp;</i>
                        </div>
                        <div style="background-color:#222A2D;display:inline-block;color:white; cursor: pointer;padding:5px;margin:1px;width:25px" value="#222A2D" data-skin="skin-black" class="theme-color">
                            <i class="{{Auth::user()->themeColor == 'skin-black' ? 'fa fa-check': ''}}">&nbsp;</i>
                        </div>
                        <div style="background-color:#C6487E;display:inline-block;color:white; cursor: pointer;padding:5px;margin:1px;width:25px" value="#C6487E" data-skin="skin-2" class="theme-color">
                            <i class="{{Auth::user()->themeColor == 'skin-2' ? 'fa fa-check': ''}}">&nbsp;</i>
                        </div>
                        <div style="background-color:#D0D0D1;display:inline-block;color:white; cursor: pointer;padding:5px;margin:1px;width:25px" value="#D0D0D1" data-skin="skin-3" class="theme-color">
                            <i class="{{Auth::user()->themeColor == 'skin-3' ? 'fa fa-check': ''}}">&nbsp;</i>
                        </div>

                        <script>
                            $(function(){
                                $('.theme-color').click(function(){
                                    var selectedSkin = $(this).attr('data-skin');

                                    $('body').removeClass().addClass(selectedSkin);
                                    $('.theme-color').find('i').removeClass();
                                    $(this).find('i').addClass('fa fa-check');

                                    //Save the settings
                                    $.post('/settings',{attribute:'themeColor', value:selectedSkin, _token: '{{csrf_token()}}'})

                                });
                            })
                        </script>
                    </li>
                @endif

            <li class="dropdown-footer"></li>
        </ul>

 </div>

