<div class="dropdown" style="position: absolute">
    <a href="{{ URL::route('manageGroups') }}" >
        <span class="fa-stack fa-lg blue" >
            <i class="fa fa-group bigger-120"></i>
        </span>
    </a>
</div>
