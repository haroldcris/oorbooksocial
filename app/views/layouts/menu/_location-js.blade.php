{{--script from: http://api.mygeoposition.com/geopicker/--}}

<style>
    .selectedRow{
        background-color: yellow;
    }
</style>

<script>
    function locationDialog_onButtonClick(dialogRef){
        //Tab 1 from table Location
        if( $("div#EnrollLocation div#tab1").hasClass("active") ){
            postMyData ('form-enroll',dialogRef).then(function(success){
                dialogRef.close();
                return location.reload();
            });                    
        }

        //Tab 2 from MAP
        if($("div#EnrollLocation div#tab2").hasClass("active")){ 
            postMyData ('form-mapdata',dialogRef).then(function(success){
                dialogRef.close();
                return location.reload();
            });                    
        }

        //Tab 1 from table Location
        if( $("div#EnrollLocation div#tab3").hasClass("active") ){
            postMyData ('form-enrollGeo',dialogRef).then(function(success){
                dialogRef.close();
                return location.reload();
            });
        }


        if( $("#form-delete").length != 0 ){
            postMyData ('form-delete',dialogRef).then(function(){
                dialogRef.close();
                return location.reload();
            });                                
        }

        if( $("#form-setprimary").length != 0 ){
            postMyData ('form-setprimary',dialogRef).then(function(){
                dialogRef.close();
                return location.reload();
            });                                
        }

    }
</script>




<?php /*******************************
       *  Enroll / Add Location--}}
       ********************************/  ?>
<script>
    
    function enrollLocation(){
        search_string = '';
        search_country = '';

        var dialog = messageBox('locationDialog', '{{Config::get('site.loader')}}' , 'Location','Add Location','Cancel');
        
        $.get('/api/v1/location/enroll').done(function(d){
            dialog.getModalBody().html(d);

        }).error(function(d){
            dialog.getModalBody().html('<span class="label label-danger">' + d.responseText + '</span>');
        });
    }



</script>


<?php
    /*******************************
    *  DELETE  Location
    ********************************/    ?>
<script>
    function deleteLocation(){
        var dialog = messageBox('locationDialog','Loading Data. Please wait...','Location','Delete','Cancel',BootstrapDialog.TYPE_DANGER);

        $.get("/api/v1/location/enrolled")
            .done(function(data){

                // if (data.length !=0){
                    var _html ='';
                    _html += '<p>Select item to Delete</p>';
                    _html += '<form action="/api/v1/location/delete" id="form-delete">';
                    
                    $.each(data,function(i,item){
                        if(data[i].pivot.isPrimary == 1) {
                            _html += '<label><input type="checkbox" name="userLocationId[]" value="' + data[i].pivot.userLocationId + '"> ' + data[i].name + ' <i class="fa fa-flag" style="color:red;"></i></label><br>';
                        } else {
                            _html += '<label><input type="checkbox" name="userLocationId[]" value="' + data[i].pivot.userLocationId + '"> ' + data[i].name + '</label><br>';
                        }

                    });

                    _html += '<form>';
                // } 
                
                return dialog.getModalBody().html(_html);
                
            })
            .fail(function(){
                //alert ('error occurred');
                console.log('Error on enrolledList');
            });
    }
</script>



<?php
    /*******************************
    *  SET PRIMARY Location
    ********************************/    ?>
<script>
    function setPrimaryLocation(){
        
        var dialog = messageBox('locationDialog','Loading Data. Please wait...','Location','Set as Primary Location','Cancel');

        $.get("/api/v1/location/enrolled")
            .done(function(data){

                // if (data.length !=0){
                    var _html ='';
                    _html += '<p>Select Location you wish to make as Primary:</p>';
                    _html += '<form action="/api/v1/location/setprimary" id="form-setprimary">';
                    
                    $.each(data,function(i,item){

                        _html += '<label><input type="radio" name="userLocationId" value="' + data[i].pivot.userLocationId + '"> ' + data[i].name + '</label><br>';

                    });

                    _html += '<form>';
                // } 
                
                return dialog.getModalBody().html(_html);
                
            })
            .fail(function(){
                //alert ('error occurred');
                console.log('Error on enrolledList');
            });
    }

</script>

<?php
    /************************
        Search location
    ***********************/
?>
<script>
    var _searchResult = '<table id="searchResult" class="table"> \
                            <thead> \
                                <tr class="success"><th>GN No.</th> <th>Province</th> <th>District</th> <th>Place Code</th> <th>Village</th></tr> \
                            </thead> \
                            <tbody> \
                                %result%\
                            </tbody> \
                        </table>';

    $(function(){
        $("body").on("keyup","input#searchLocationItem",function(e){
            clearTimeout($.data(this, 'timer'));
            var search_string = $(this).val();
            // Do Search
            if (search_string == '') {
                $("form#form-enroll input#tableLocationId").val('');
                $("#SearchLocationResult").fadeOut();
            }else{
                $("#SearchLocationResult").html('<a>&nbsp;&nbsp;<i class="fa fa-spinner fa-spin bigger-140"></i> Searching...</a>');
                $("#SearchLocationResult").fadeIn();
                $(this).data('timer', setTimeout(searchLocation, 1000));
            }
        });
    });

    function searchLocation(){
        var query_value = $('input#searchLocationItem').val();
        if(query_value !== ''){
            var ajax = $.post ("/api/v1/location/searchtable",{data: query_value,_token: '{{csrf_token()}}'});

            ajax.done(function(data){
                var _html='';
                if (data.length == 0){
                    _html = '<span class="alert alert-danger">** No Results Found **</span>';
                } else {
                    data.forEach(function(item){
                        _html += '<tr id="' + item.tableLocationId + '" > \
                                <td>' + item.GNNum 		+ '</td> \
                                <td>' + item.Province 	+ '</td> \
                                <td>' + item.District 	+ '</td> \
                                <td>' + item.PlaceCode 	+ '</td> \
                                <td>' + item.Village 	+ '</td>';
                    });

                    _html = _searchResult.replace('%result%', _html);
                }

                $("#SearchLocationResult").html(_html);
            })
        }return false;
    }

    $("body").on("click","table#searchResult tr", function(){
        $("table#searchResult tr").removeClass('selectedRow');
        $(this).addClass('selectedRow');

        $("form#form-enroll input#tableLocationId").val($(this).attr('id')).change();
    });
</script>


<?php
    /*************************************
        Search LOCATION FROM DATABASE GEO
    **************************************/
?>
<script>
    var _searchResultGeo = '<table id="searchResultGeo" class="table"> \
                            <thead> \
                                <tr class="success"><th></th><th></th><th></th><th></th><th></th><th>Locality</th></tr> \
                            </thead> \
                            <tbody> \
                                %result%\
                            </tbody> \
                        </table>';

    var search_string = '';

        $("body").on("keyup","input#searchLocationItemGeo",function(e){
            clearTimeout($.data(this, 'timer'));

            //console.log(search_string + '__' +  $(this).val().trim());
            //if(search_string.trim() != $(this).val().trim() ) {
                search_string = $(this).val().trim();

                // Do Search
                if (search_string == '') {
                    $("#tableLocationGeoId").val('');
                    $("#SearchLocationResultGeo").fadeOut();
                }else{
                    $('.errorHeaderStatus').remove();

                    $(this).data('timer', setTimeout(searchLocationGeo, 1000));
                };
            //}
        });


    function searchLocationGeo(){
        var query_value = $('input#searchLocationItemGeo').val();
        var country = $('#form-enrollGeo select.country').val();

        if(query_value !== ''){
            $("#SearchLocationResultGeo").html('<a>&nbsp;&nbsp;<i class="fa fa-spinner fa-spin bigger-140"></i> Searching...</a>');
            $("#SearchLocationResultGeo").fadeIn();

            var ajax = $.post ("/api/v1/location/searchgeo",{ country : country, data: query_value, _token: '{{csrf_token()}}'});

            ajax.done(function(data){
                var _html='';
                if (data.length == 0){
                    _html = '<span class="alert alert-danger">** No Results Found **</span>';
                } else {
                    data.forEach(function(item){
                        _html += '<tr id="' + item.id + '" style="cursor:pointer;" > \
                                <td>' + item.iso 		+ '</td> \
                                <td>' + item.region1    + '</td> \
                                <td>' + item.region2    + '</td> \
                                <td>' + item.region3    + '</td> \
                                <td>' + item.region4    + '</td> \
                                <td>' + item.locality + '<br><small>' + item.postcode + '</small></td>';
                    });

                    _html = _searchResultGeo.replace('%result%', _html);
                }

                $("#SearchLocationResultGeo").html(_html);
            })
        }return false;
    }

    $("body").on("click","table#searchResultGeo tr", function(){
        $("table#searchResultGeo tr").removeClass('selectedRow');
        $(this).addClass('selectedRow');

        $("form#form-enrollGeo input#tableLocationGeoId").val($(this).attr('id')).change();
    });



    var search_country='';
    $('body').on('change', '#countryDropdown', function(e){
        if($('#countryDropdown').length != 0) {
            console.log('dropdown changed...');
            console.log(search_string);

            var country = $('#form-enrollGeo select.country').val();
            var query_value = $('input#searchLocationItemGeo').val();

            if(country != ''){
                $('#searchLocationItemGeo').removeAttr('disabled');
                    if (search_country != country ){
                        search_country = country;
                        $('.errorHeaderStatus').remove();
                        //$("#SearchLocationResultGeo").html('<a>&nbsp;&nbsp;<i class="fa fa-spinner fa-spin bigger-140"></i> Searching...</a>');
                        //$("#SearchLocationResultGeo").fadeIn();
                        searchLocationGeo();
                    }
            } else {
                $('#searchLocationItemGeo').attr('disabled');
            }
        }
    });

</script>
