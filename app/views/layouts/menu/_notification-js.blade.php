<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/06/2015
 * Time: 4:49 PM
 *
 */


 ?>
 <script>

    $(function(){
        $('[data-function="notification"]').click(function(){

            $('[data-function="notificationDropDown"]').html('<li><i class="ace-icon fa fa-spinner fa-spin  bigger-125 text-center"></i></li>');
            
            $.get('/api/v1/followees/updates?limit=30&updatetimecheck=1').done(function(d){
                _html='';
                for(var i = 0; i < d.length; i++){
                    _html += '<li>';
                    _html += '    <div>';
                    _html += '        <img src="/user/' + d[i].userId +'/avatar" class="msg-photo" alt="Avatar" />';
                    _html += '        <span class="msg-body" style="max-width:100% !important;" >';
                    _html += '            <span class="msg-title">';
                    //_html += '                <span class="blue">' + d[i].user.username + ': </span>';
                    _html += d[i].action;
                    _html += '            </span>';

                    _html += '              <span class="msg-time">';
                    _html += '                  <i class="ace-icon fa fa-clock-o"></i>';
                    _html += '                   <span>' + d[i].created + '</span>';
                    _html += '               </span>';
                    _html += '          </span>';
                    _html += '      </div>';
                    _html += '</li>';
                }

                $('[data-function="notificationDropDown"]').html(_html);
                $('.slimscroll').slimScroll({height: '350px', width: '300px'});
                showAlertNotification(false);
            });

        });

    })

 </script>

@if(Auth::check())
 {{--Checking for notification updates--}}
 <script>
    $(function(){
        checkNotificationEvery( 300000 );

        function checkNotificationEvery(ms){
            getFolloweeUpdatesCount();
            setTimeout(function(){checkNotificationEvery(ms)}, ms);
        }

        function getFolloweeUpdatesCount(){
            console.log('checking for notification updates...');
            $.get('/api/v1/followees/updatescount').done(function(d){
                var total = d.updates;
                if (d.updates > 20) { total = "20";}

                if(d.updates != "0"){
                    $('[data-function="notificationBadge"]').text(total + "+");
                    showAlertNotification(true);
                }else {
                    showAlertNotification(false);
                }
            });
        }

    });

    function showAlertNotification(_show){
        if (_show ){
            $('[data-function="notificationBadge"]').removeClass('hidden');
            $('[data-function="notificationIcon"]').addClass('icon-animated-bell');
        } else {
            $('[data-function="notificationBadge"]').addClass('hidden');
            $('[data-function="notificationIcon"]').removeClass('icon-animated-bell');
        }
    }
 </script>

 @endif