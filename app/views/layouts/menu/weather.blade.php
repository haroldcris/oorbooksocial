<style>
    /*#weatherWidget .arrow{*/
        /*top:0;*/
        /*margin-top:14px;*/
    /*}*/
    /*#weatherWidget .popover{*/
        /*top:0 !important;*/
        /*margin-top:10px;*/
    /*}*/
    
    .cur_temp {
        font-size: 133%;
        font-weight: bold;
        margin-top: 2px;
    }
    
    span.cur_desc {
        font-weight: bold;
    }

    #weatherLink img {
        padding-left:6px;
        padding-top:5px
    }
</style>

<div id="weatherWidget" >
    <a href="http://www.worldweatheronline.com/" target="_new"
        data-function="weatherLink"
        rel="popover"
        data-toggle="popover"
        data-placement="left"
        data-original-title="Current Weather"
        data-trigger="hover"
    >
       <span class="fa-stack fa-lg pink" id="weatherIcon">
         <i class="fa fa-sun-o bigger-130 fa-spin"></i>
       </span>
    </a>

    
</div>


<div id="weatherResult" style="display:none">
    <div style="width:100px">
    <table border="0" cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td align="left" valign="top" style="width:175px;">
                    <div class="cur_temp"></div>
                    <p><span class="cur_desc"></span></p>                    
                    
                    <p>
                        <a href="https://developer.worldweatheronline.com/" target="_blank"><img alt="Weather API" src="http://cdn.worldweatheronline.net/staticv3/images/logo_small.png" style="border-style:solid; border-width:0px; height:43px; width:100px" /></a>
                    </p>
                </td>
{{--<!--                <td align="left" valign="top" style="width:75px;"><div class="wxday">Mon</div><div class="wximg"><img src="http://cdn.worldweatheronline.net/images/weather/small/113_day_sm.png" width="60" height="60" align="center"></div>--}}
                    {{--<div class="wxtemp">35°C | 24°C</div>--}}
                    {{--<div class="wxtemp">94°F | 75°F</div>--}}
                {{--</td><td align="left" valign="top" style="width:75px;"><div class="wxday">Tue</div><div class="wximg"><img src="http://cdn.worldweatheronline.net/images/weather/small/113_day_sm.png" width="60" height="60" align="center"></div>--}}
                    {{--<div class="wxtemp">34°C | 23°C</div>--}}
                    {{--<div class="wxtemp">92°F | 74°F</div>--}}
                {{--</td><td align="left" valign="top" style="width:75px;"><div class="wxday">Wed</div><div class="wximg"><img src="http://cdn.worldweatheronline.net/images/weather/small/176_day_sm.png" width="60" height="60" align="center"></div>--}}
                    {{--<div class="wxtemp">31°C | 23°C</div>--}}
                    {{--<div class="wxtemp">88°F | 73°F</div>--}}
                {{--</td><td align="left" valign="top" style="width:75px;"><div class="wxday">Thu</div><div class="wximg"><img src="http://cdn.worldweatheronline.net/images/weather/small/116_day_sm.png" width="60" height="60" align="center"></div>--}}
                    {{--<div class="wxtemp">31°C | 22°C</div>--}}
                    {{--<div class="wxtemp">87°F | 72°F</div>--}}
                {{--</td><td align="left" valign="top" style="width:75px;"><div class="wxday">Fri</div><div class="wximg"><img src="http://cdn.worldweatheronline.net/images/weather/small/113_day_sm.png" width="60" height="60" align="center"></div>--}}
                    {{--<div class="wxtemp">29°C | 21°C</div>--}}
                    {{--<div class="wxtemp">84°F | 71°F</div>--}}
                {{--</td>-->--}}
            </tr>
        </tbody>
    </table>

    </div>
</div>