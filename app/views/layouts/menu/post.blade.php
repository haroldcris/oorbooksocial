<div class="dropdown" style="position: absolute">
    <a href="javascript:void(0)" data-toggle="dropdown" class="dropdown-toggle" >
        <span class="fa-stack fa-lg" style="color:#1b8145">
            <i class="glyphicon glyphicon-pencil"></i>
        </span>
    </a>
    <ul class="dropdown-navbar dropdown-caret dropdown-menu">
        <li class="dropdown-header"><i class="ace-icon fa fa-pencil "></i> Create...</li>
        
	
        <li><a href="javascript:void(0)" onclick="createPost('{{strtolower(Auth::user()->securityLevel)}}')" ><i class="ace-icon fa fa-pencil bigger-150"></i> Create New Post</a>
        @if(strtolower(Auth::user()->securityLevel) != 'master user')
        <li><a href="javascript:void(0)" onclick="createAnnouncement()"><i class="glyphicon glyphicon-bullhorn"></i> Write New Announcement</a>
        @endif
        <li></li>
    </ul>
</div>