<style type="text/css">
.location-member {
    background: transparent;
    color: #3a87ad;
    font-weight: 600;
    font-size: 11px;
}
.location-member:hover {
    color: #00ABFF !important;
    background: transparent !important;
    text-decoration: none !important;
}
.location-name {
    color: #555 !important;
}
.location-name:hover {
    color: #555 !important;
    background: transparent !important;
    text-decoration: none !important;
}
</style>

<!-- <i class="notification-icon icon-animated-vertical">12+ </i> -->
<div class="dropdown" style="position: absolute">
    <a data-toggle="dropdown" class="dropdown-toggle"  >
       <span class="fa-stack fa-lg ">
         <i class="fa fa-globe bigger-120"></i>
       </span>

    </a>
        <ul class="navbar-green dropdown-navbar dropdown-menu  dropdown-caret" style="width:400px;min-width: 200px">
            <li class="dropdown-header"><i class="fa fa-location-arrow"></i> Location</li>
            
            @if(count($enrolledLocations)== 0)
                <li><a href="javascript:void(0)"><span style="color:blue">You have not enrolled to any location yet.<br><i>Click Add Location to enroll to a new location.</i></span></a></li>
            @else
                @foreach ($enrolledLocations as $item)
                    <li>
                        <a class="location-name" href="{{URL::route('setLocation', [$item->slug])}}" title="{{$item->pivot->address}}" style="white-space:normal; padding-top:2px; padding-bottom:2px;">

                            <div style="float:left; width: 20px;">
                                @if(Session::has('selectedUserLocationId'))
                                    @if(Session::get('selectedLocationId') == $item->locationId)
                                        <i class="fa fa-check green"></i>
                                    @endif
                                @else
                                    <i style="padding-left:15px"></i> 
                                @endif

                                @if($item->pivot->isPrimary)
                                    <i class="fa fa-flag" style="color:red;" title="Primary Location"></i>
                                @endif
                            </div>

                            <div style="margin-left:20px; line-height: 17px; font-size: 12px;">
                                {{$item->name}}
                                <?php
                                    $members = $item->members()->count();
                                    $member = ($members > 1 ? ' members' : ' member');
                                ?>
                                <br>
                                <a class="location-member" href="{{URL::route('locationMembers',$item->locationId)}}" title="Show{{$member}}">{{$members . $member}}</a>
                            </div>
                        </a>
                    </li>

                @endforeach
            @endif

            <li class="dropdown-header" style="line-height: 2em">Task</li>
            <li>
               <a href="javascript:void(0)" onclick="enrollLocation()" style="padding-top:2px;padding-bottom:5px;" ><i class="fa fa-plus"></i> Add Location</a>
            </li>
            <li>
               <a href="javascript:void(0)" onclick="setPrimaryLocation()" style="padding-top:2px;padding-bottom:5px;" ><i class="fa fa-flag"></i> Set Primary Location</a>
            </li>
            <li>
               <a href="javascript:void(0)"  onclick="deleteLocation()" style="padding-top:2px;padding-bottom:5px;" ><i class="fa fa-trash-o"></i> Delete Location</a>
            </li>
            <li></li>
        </ul>
</div>