<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/16/2015
 * Time: 6:31 AM
 *
 */ ?>

 <script>
    $(function(){
        getWeather();
    });

    function getWeather(){

        $.get('/api/v1/weather')
//        .error(function(e){
//            console.log('error occurred while getting weather');
//             $('[data-function="weatherLink"]')
//                 .html('<img height=80% width=80% src="/assets/images/weather.png" />')
//                 .popover({
//                    html : true,
//                    content: function() {
//                        return 'Failed getting weather condition';
//                    }
//                 });
//
//        })
        .done(function(json){
            showWeather(json);
        });
    }

    function showWeather(d){
        if(d == undefined || d == '') return Console.log('Unable to retrieve weather condition');

        if(typeof d.data == 'undefined' ) {
            $('[data-function="weatherLink"]')
                    .html('<img height=80% width=80% src="/assets/images/weather.png" />')
                    .popover({html:true, content: 'Failed getting current weather condition'});
            return console.log('Error getting weather status');
        }

        if(typeof d.data.error != 'undefined' ) {
            return console.log(d.data.error[0].msg);
        }

        weatherIcon = d.data.current_condition[0].weatherIconUrl[0].value;
        weatherTempC = d.data.current_condition[0].temp_C;
        weatherTempF = d.data.current_condition[0].temp_F;
        weather = d.data.current_condition[0].weatherDesc[0].value;


        $('[data-function="weatherLink"]')
                .html('<img height=80% width=80% src="' + weatherIcon + '"/>')
                .popover({
                    html : true,
                    content: function() {
                        $('.cur_temp').html(weatherTempC + '°C | ' + weatherTempF + '°F');
                        $('.cur_desc').html(weather);

                        return $('#weatherResult').html();
                    }
                });
    }

 </script>


