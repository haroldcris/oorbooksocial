<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 03/26/2015
 * Time: 7:01 AM
 *
 */
?>

 <div class="dropdown" style="position: absolute">
     <a href="{{ URL::route('managePolls') }}">
         <span class="fa-stack fa-lg purple" >
             <i class="glyphicon glyphicon-tasks"></i>
         </span>
     </a>
 </div>

