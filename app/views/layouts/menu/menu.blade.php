<?php /**
 * Created by Harold Cris D. Abarquez
 * Date: 03/14/2015
 * Time: 2:08 AM
 *
 */ ?>

<div class="primary menu-bar" style="background: white; margin-left:0; margin-bottom: 10px; width:700px;min-width:700px;z-index:999">&nbsp;
    @if(Auth::check())
    {{--Menu -- Location --}}
    <div class="menuPanel" title="Home">
        @include ('layouts.menu.home')
    </div>
    @if(strtolower(Auth::user()->securityLevel) != 'master user')
    <div class="menuPanel" title="Location">
        @include ('layouts.menu.locationList')
    </div>
    @endif
    
    <div class="menuPanel" title="Write...">
        @include ('layouts.menu.post')
    </div>
    
    <div class="menuPanel" title="Calendar Events">
        @include('layouts.menu.calendar')
    </div>
  @if(strtolower(Auth::user()->securityLevel) != 'master user')
        <div class="menuPanel" >
            @include ('layouts.menu.notification')
        </div>
  
	    @if(Auth::user()->canManage('role'))
	    <div class="menuPanel" title="Announcement">
	        @include ('layouts.menu.announcement')
	    </div>
	    @endif

        <div class="menuPanel" title="Groups">
            @include ('layouts.menu.groups')
        </div>

	    @if(Auth::user('Super User')->canManage('poll'))
	    <div class="menuPanel" title="Polls">
	        @include('layouts.menu.poll')
	    </div>
	    @else
	    <div class="menuPanel menuPanel-poll" title="Polls">
	        @include('layouts.menu.pollUser')
	    </div>
	    @endif
	    
	    <div class="menuPanel menuPanel-poll" title="Survey">
	        @include('layouts.menu.survey')
	    </div>
    @endif
    
    <div class="menuPanel pull-right" >
        @include('layouts.menu.manage')
    </div>
    
    <div class="menuPanel pull-right">
        @include ('layouts.menu.tv')
    </div>

    {{--Menu Item -- WEATHER --}}
    <div class="menuPanel pull-right" title="Weather">
        @include ('layouts.menu.weather')
    </div>
    @endif
</div>