<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/09/2015
 * Time: 7:39 AM
 *
 */
 ?>

<style type="text/css">
    #rssNotice{
        width: 100%;
        height: 100%;
        /*border: 1px solid black;*/
        padding: 5px;
        /*background-color: #F0F0F0;*/
        position: absolute;
        line-height: 15px;
    }
    #rssNoticeLocal {
        width: 100%;
        height: 100%;
        /*border: 1px solid black;*/
        padding: 5px;
        /*background-color: #F0F0F0;*/
        position: absolute;
        line-height: 15px;
    }

    .rssclass .rsstitle{
        font-weight: bold;
    }

    .rssclass .rssdate{
        color: gray;
        font-size: 85%;
    }


    .rssclass a{
        text-decoration: none;
        color: #0000ff;
    }
</style>

<div>
    <div id="rssNoticeWrapper" class="panel panel-info" style="height: 100px;"></div>
    <div id="rssNoticeLocalWrapper" class="panel panel-info" style="height: 100px;"></div>
</div>


{{--//new rsspausescroller(RSS_id, divId, divClass, delay, linktarget, optionalswitch)--}}
{{--//1) RSS_id: "Array key of RSS feed in scrollerbridge.php script"--}}
{{--//2) divId: "ID of DIV to display ticker in. DIV is dynamically created"--}}
{{--//3) divClass: "Class name of this ticker, for styling purposes"--}}
{{--//4) delay: delay between message change, in milliseconds--}}
{{--//5) linktarget: Target of links inside RSS feed. Set to "" for current page.--}}
{{--//6) optionalswitch: "optional arbitrary" string to create additional logic for formatrssmessage() to use.--}}
{{--//   By default, optionalswitch supports "date", or "date+description" to also show these parts of a RSS feed.--}}
{{--//new rsspausescroller("cnn", "pscroller1", "rssclass", 3000, "_new")--}}
{{--new rsspausescroller("notice", "pscroller2", "rssclass", 3000, "", "date+description")--}}

<script>
    $(function(){
        var _region= new rsspausescroller("notice", "rssNotice", "rssclass", 3000, "_new", "date+description","rssNoticeWrapper");

        var _local = new rsspausescroller("noticeLocal", "rssNoticeLocal", "rssclass", 3000, "_new", "date+description","rssNoticeLocalWrapper");
    });

</script>
