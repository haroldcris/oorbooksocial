<?php

    $AuthUser   = Auth::user();
    $googleAds1 = SettingsClass::get('googleAds1');
    $googleAds2 = SettingsClass::get('googleAds2');
    $widget1    = SettingsClass::get('widget1',true);
    $themeColor = '';
    
    if(Auth::check() ) {
        $themeColor = $AuthUser->themeColor;
    }
    
 ?>



 <!DOCTYPE html>
 <html lang="en" itemscope itemtype="http://schema.org/Article">

    <head>

        @include('layouts.partials.meta')

        <link href="/themes/bootstrap/css/bootstrap.css" rel="stylesheet">
        <!-- fonts -->
        <!--<link rel="stylesheet" href="/themes/ace/css/ace-fonts.css">-->
        <!-- ace styles -->
        
        <link rel="stylesheet" href="/themes/ace/css/ace.css">
        <link rel="stylesheet" href="/themes/ace/css/ace-skins.css">
        <link rel="stylesheet" href="/themes/ace/css/font-awesome.css">
        <link rel="stylesheet" href="/themes/css/global.css">
        <link rel="stylesheet" href="/themes/css/3dbutton.css">
        <link rel="stylesheet" media="all" href="/themes/css/rss.css" />
        <link rel="stylesheet" media="print" href="/themes/css/print.css" />
        <link rel="stylesheet" href="/themes/css/switchio.css" />
        <link href='/controls/datetimepicker/jquery.datetimepicker.css' rel='stylesheet' />
        <link href='/controls/datetimepicker/jquery.datetimepicker.css' rel='stylesheet' />
        <link rel="stylesheet" href="/themes/custom.css">
        {{--<script src="/packages/jquery/jquery-1.11.2.min.js"></script>--}}
        <script src="/packages/jquery/jquery-2.1.3.min.js"></script>

        @include('layouts.partials._googleAnalytics-js')

        <style>
        .container {width:1132px !important;}
        </style><script type="text/javascript">window.heap=window.heap||[],heap.load=function(t,e){window.heap.appid=t,window.heap.config=e;var a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=("https:"===document.location.protocol?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+t+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(t){return function(){heap.push([t].concat(Array.prototype.slice.call(arguments,0)))}},p=["clearEventProperties","identify","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};heap.load("3122515993");</script>

        @yield('styles')
    </head>
    
    <body class="{{$themeColor}}">

        @include('layouts.partials.topBar')

        <div class="container" >
            <div style="float:left; position:absolute; margin-left:-10px">
                @include ('layouts.partials.header')
    
                <div class="container" id="main-container" style="height: auto; padding-top:40px" >
                    @include('layouts.partials.sidebar')
    
                    <div class="main-container-inner" style="float: none !important;margin-left:189px;">
        
                        @yield('primaryPage')
        
                    </div><!-- /.main-container-inner -->
                    {{--</div><!-- /.main-content -->--}}
    
                </div><!-- /.main-container -->
            </div>
    
            @yield('rightAds')
        </div>
        {{--include($BasePath .'template/navigation_bottom.php');--}}
        {{--include($BasePath .'template/tFooterScript.php');--}}
        <script>
        heap.identify({handle: '{{Auth::check() ? Auth::user()->username : 'guest'}}', name: '{{ Auth::check() ? Auth::user()->username : 'guest'}}', userId : '{{ Auth::check() ? Auth::user()->userId : '0' }}', email_address: '{{ Auth::check() ? Auth::user()->email : 'guest' }}' });
        function pop_up(hyperlink, window_name)
        {
            console.log('popup');
            // if (! window.focus())
            // return true;
            var href;
            
            if (typeof(hyperlink) == 'string')
                href=hyperlink;
            else
                href=hyperlink.href;
                window.open(href,window_name,'width=450,height=400,toolbar=no, scrollbars=yes');
                return false;
        }
        </script>
    
        @include('layouts.partials.scripts')
        @yield('scripts')
        @include('layouts.footer')


 	</body>
 </html>