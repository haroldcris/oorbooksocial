<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    {{--<!-- Mobile Specific Metas--}}
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{Config::get('site.title')}}</title>

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <meta name="keywords" content="@yield('keywords')" />
    <meta name="author" content="@yield('author')" />
    {{--<!-- Google will often use this as its description of your page/site. Make it good. -->--}}
    <meta name="description" content="@yield('description')" />
    {{--<!-- Speaking of Google, don't forget to set your site up: http://google.com/webmasters -->--}}
    <meta name="google-site-verification" content="A8K-Cqvdx8RxklQ-KP1kpA93EpNnmYPsSWcrmCRWm_I" />

	<!-- ace styles -->
	<link rel="stylesheet" href="/themes/ace/css/font-awesome.css">
	<link rel="stylesheet" href="/themes/ace/css/ace.css">
	<link rel="stylesheet" href="/themes/custom.css">

	<link href="/themes/bootstrap/css/bootstrap.css" rel="stylesheet">
	<link href="/controls/bootstrap-dialog/css/bootstrap-dialog.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/themes/css/3dbutton.css">

    <script src="/packages/jquery/jquery-1.11.2.min.js"></script>
    {{--<!-- ace settings handler -->--}}
	<script src="/themes/ace/js/ace-extra.js"></script>
	<script src="/themes/bootstrap/js/bootstrap.min.js"></script>


	@yield('styles')

</head>

<body class="default">
        @yield('pageHeader')
        <div class='container'>
            @yield('pageContent')
        </div>
    <footer>
        <hr/>
        @yield("footer")
        <p class="text-center"><a onclick="return pop_up('/eula', 'Pop Up')" style="text-decoration: none;cursor: pointer;">Terms and Conditions</a></p>
        <p class="text-center">&copy;2012-2015 Oorbook Inc.</p>
    </footer>


    <script src="/controls/bootstrap-dialog/js/bootstrap-dialog.min.js"></script>
    <script>
        function pop_up(hyperlink, window_name)
        {
         if (! window.focus)
             return true;

         var href;
         if (typeof(hyperlink) == 'string')
             href=hyperlink;
         else
             href=hyperlink.href;

         window.open(href,window_name,'width=450,height=400,toolbar=no, scrollbars=yes');
         return false;
        }
    </script>

    @yield('scripts')
	{{--<!--User Settings-->--}}
	{{--<script src="js/panel.js"></script>--}}
	{{--<script src="js/modalDialog.js"></script>--}}
</body>

</html>