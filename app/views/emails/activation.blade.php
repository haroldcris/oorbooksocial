<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 01/29/2015
 * Time: 2:45 PM
 *
 */

 ?>
<!DOCTYPE html>
 <html lang="en-US">
 	<head>
 		<meta charset="utf-8">
 		<style>
            .btn {
              -webkit-border-radius: 28px;
              -moz-border-radius: 28px;
              border-radius: 28px;
              font-family: Arial;
              color: #ffffff;
              font-size: 16px;
              background: #34d97b;
              padding: 10px 20px 10px 20px;
              text-decoration: none;
            }

            .btn:hover {
              background: #2cb011;
              text-decoration: none;
            }
 		</style>
 	</head>
 	<body>
 	    <h2>Hello {{$user->username}},</h2>

 		<p>Welcome to <strong>Oorbook</strong>.</p>
        <br>
        <p><strong>Your account has been created. Click the link below to Activate your account</strong></p>
        <br>
 		<div >
            <a class="btn" target="_blank" href="{{URL::route('accountActivate', $user->activationCode) }}">Activate My Account</a>
 		</div>
 	</body>
 </html>