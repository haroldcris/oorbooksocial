<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 01/30/2015
 * Time: 11:04 AM
 *
 */?>
  <!DOCTYPE html>
  <html lang="en-US">
  	<head>
  		<meta charset="utf-8">
  		<style>
             .btn {
               -webkit-border-radius: 28px;
               -moz-border-radius: 28px;
               border-radius: 28px;
               font-family: Arial;
               color: #ffffff;
               font-size: 16px;
               background: #d9554c;
               padding: 10px 20px 10px 20px;
               text-decoration: none;
             }

             .btn:hover {
               background: #fe0712;
               text-decoration: none;
             }
  		</style>
  	</head>
  	<body>
  	    <h3>Hello {{$user->username}},</h3>

         <div>
             <p>We received a request to reset the password for your account.</p>
             <p>If you made this request, click the button below. </p><br>
             <br>
             <p>
                 <a class="btn " target="_blank" href="{{URL::route('accountReset', ['token' => $user->activationCode]) }}">Reset my password</a>
             </p>
             <br><br>
        	    <p>If you didn't make this request, you can ignore this email.</p>
         </div>


  	</body>
  </html>