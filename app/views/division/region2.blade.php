@extends('layouts.main')

@section('title')
- Region 2 Division
@stop

@section('pageContent')
    
    @if (!is_null($selectedLocation))
        @include('layouts.partials.breadcrumbs')
    @endif

    <div class="row" style="width: 700px; margin: 30px 0px 0px 0px; background: white; padding-bottom: 40px;">
        <div class="col-md-12">
            <h2 style="font-weight: 600; color: #5E5E5E; border-bottom: 1px solid #E5E5E5; font-size: 20px;">{{$selectedLocation->region2}}</h2>
        </div>
        <div class="col-md-12" style="padding: 15px;">
            The content about <strong>{{$selectedLocation->region2}}</strong> goes here...
        </div>
    </div>
@stop