<script>
$(document).ready(function(){

	$('.save-settings').click(function(){
		// alert($(this).attr('id'));
		// alert($(this).closest("form").attr("id"));
		// $.post('{{ URL::route("post-save-settings") }}', $('#eulaContent').serialize(), function(response) {
		// 	messageBoxOk(response, 'File Save', 'OK');
		// })

		var $formId   = $(this).closest('form').attr('id');
		var $buttonId = $(this).attr('id');

		tinymce.triggerSave();

		var _btnIcon = $(this).find('i');
		$(_btnIcon).removeClass().addClass('fa fa-refresh fa-spin');
		postMyData($formId).then(
			function(success) {
				messageBoxOk('Record has been successfully saved.', 'Information', 'OK',BootstrapDialog.TYPE_SUCCESS).setSize(BootstrapDialog.SIZE_SMALL);
				$(_btnIcon).removeClass().addClass('glyphicon glyphicon-ok');

			}, function(err) {
				messageBoxOk(err.message);
				$(_btnIcon).removeClass().addClass('glyphicon glyphicon-ok');
			}
		);
	});

});
</script>