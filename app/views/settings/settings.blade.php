@extends('layouts.main')
@section('styles')
	<!-- Color Picker -->
	<link href="/controls/colorpicker/css/colpick.css" rel="stylesheet">
	<script src="/controls/colorpicker/js/colpick.js"></script>
	<script>
	$(function(){
		$('.colorPicker')
    		.colpick({
                layout:'hex',
                submit:1,
                colorScheme:'dark',
                onSubmit:function(hsb,hex,rgb,el,bySetColor) {
                    $(el).css('border-color','#'+hex);
                    $(el).colpickHide();
                    // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
                    if(!bySetColor) $(el).val(hex);
                }
            })
        	.keyup(function(){
            	$(this).colpickSetColor(this.value);
        	});
    })
    </script>


	<!-- Editor -->
	<script src="/controls/tinymce/tinymce.min.js"></script>
	<script type="text/javascript">
	tinymce.init({
	    mode : "specific_textareas",
	   
        editor_selector : "texteditor",
        editor_deselector : "noeditor",
	    theme: "modern",
	    height: 200,
	    plugins: [
	         "autoresize advlist autolink link image lists charmap  preview hr anchor pagebreak ",
	         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
	         " table contextmenu directionality emoticons template paste textcolor jbimages"
			],
		external_plugins: {"nanospell": "plugins/nanospell/plugin.js"},
        nanospell_server: "php", /* choose "php" "asp" "asp.net" or "java" */
		removed_menuitems: 'newdocument',
		menu : { // this is the complete default configuration
					//file   : {title : 'File'  , items : 'newdocument'},
					edit   : {title : 'Edit'  , items : 'undo redo | cut copy paste pastetext | selectall'},
					insert : {title : 'Insert', items : 'link media jbimages | hr charmap emoticons insertdatetime'},
					//view   : {title : 'View'  , items : 'visualaid'},
					format : {title : 'Format', items : 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
					table  : {title : 'Table' , items : 'inserttable tableprops deletetable | cell row column'},
					tools  : {title : 'Tools' , items : 'spellchecker code'}
				},
	    toolbar_items_size: 'small',
	   	//content_css: "css/content.css",
	   	toolbar: "preview fullscreen | insertfile undo redo | fontsizeselect  styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |  forecolor backcolor emoticons | link media jbimages style_formats", 
	   	relative_urls : true
		//remove_script_host : false,
		//convert_urls : true
	});

	tinymce.init({
		mode : "specific_textareas",

		editor_selector : "tinymceImageUploader",
		theme: "modern",
		height: 200,
		plugins: [
			 "autoresize advlist autolink link image lists charmap  preview hr anchor pagebreak ",
			 "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			 " table contextmenu directionality emoticons template paste textcolor jbimages"
			],
		external_plugins: {"nanospell": "plugins/nanospell/plugin.js"},
		nanospell_server: "php", /* choose "php" "asp" "asp.net" or "java" */
		removed_menuitems: 'newdocument',
		menu : {
				insert : {title : 'Insert', items : 'jbimages'},
				tools  : {title : 'Tools' , items : 'code'}
				},
		toolbar_items_size: 'small',
		//content_css: "css/content.css",
		toolbar: "jbimages",
		relative_urls : true,
		convert_urls: false
	});
	</script>

@stop

@section('pageContent')
	<div class="page-content col-md-12">
		<div class="page-header">

		<!--Page Header Goes Here-->
		    {{-- <h1>Edit Portal Content
				<small>
					<i class="fa fa-angle-double-right"></i>
					&nbsp;
				</small>
			</h1> --}}
			<h1><i class="glyphicon glyphicon-cog" style="font-size: 20px;"></i> Site Content</h1>
		</div><!-- /.page-header -->

		<div class="row" id="content">
			<div class="col-sm-12 col-xs-12">
				<!--Tab Notifications-->						
				<div class="tabbable">
					<ul class="nav nav-tabs" id="myTab">
						<li class="active"><a data-toggle="tab" href="#tab-color"><i class="green"></i>General</a></li>
						<li class=""><a data-toggle="tab" href="#tab-frontPage"><i class="green"></i>Front Page </a></li>
						{{--<li class=""><a data-toggle="tab" href="#tab-Notification"><i class="green"></i>Notification </a></li>--}}
						<li class=""><a data-toggle="tab" href="#tab-Ads"><i class="green"></i>Ads</a></li>
						<li class=""><a data-toggle="tab" href="#tab-Eula"><i class="green"></i>Terms & Condition</a></li>
						<li class=""><a data-toggle="tab" href="#tab-help"><i class="green"></i>Help Topics</a></li>
					</ul>

					<div class="tab-content">
						<!--General Tab-->
						<div id="tab-color" class="tab-pane active">
							<div class="widget-box" style="opacity: 1;">
								<form id="frmEmail" action="{{ URL::route('post-save-settings') }}" onsubmit="return false;">
									<div class="widget-header">
										<h5 class="bigger lighter">
											<i class="fa fa-gear"></i>
											Admin Email Address
										</h5>

										<div class="widget-toolbar">
										  <button class="save-settings btn btn-success btn-xs saveCommand btn-white btn-round" type="button">
												<i class="glyphicon glyphicon-ok"></i>
												Save
										  </button>
										</div>
									</div><!-- /.widget-header -->

									<div class="widget-body">
										<div class="widget-main">
											<span style="color:black"><b>Email Address</b><br></span>
											<div class="row" style="margin-bottom:2px;color:black">
												<div class="col-md-12">
													<input class="form-control" type="email" name="adminEmail" value="{{SettingsClass::get('adminEmail')}}" />
													<input type="hidden" name="attribute" value="adminEmail" >
												</div>
											</div>

										</div><!-- /widget-main -->
									</div><!-- /.widget-body -->

								</form>
							</div><!-- /.widget-box -->
							<div class="widget-box" style="opacity: 1;">
								<form id="frmColor" action="{{ URL::route('post-save-settings') }}" onsubmit="return false;">

									<div class="widget-header">
										<h5 class="bigger lighter">
											<i class="fa fa-gear"></i>
											Color
										</h5>

										<div class="widget-toolbar">
										  <button id='colorFormBtn' class="save-settings btn btn-success btn-xs saveCommand btn-white btn-round" type="button">
												<i class="glyphicon glyphicon-ok"></i>
												Save
										  </button>
										</div>
									</div><!-- /.widget-header -->

									<div class="widget-body">
										<div class="widget-main">
											<span style="color:black"><b>Title Bar</b><br></span>
												<div class="row" style="margin-bottom:2px;color:black">
													<div class="col-md-4 col-xs-4">&nbsp;&nbsp;Header Color: </div>
													<div class="col-md-4 col-xs-4">
														#<input name="colorNavbar" id="colorNavbar" type="text" class="colorPicker" style="color:black; cursor:pointer; margin:0; padding:0; border:0; width:70px; height:20px; border-right:20px solid #{{ SettingsClass::get('colorNavbar') }};  line-height:20px;" value="{{ SettingsClass::get('colorNavbar') }}" />
														<input type="hidden" name="attColorNavbar" value="colorNavbar" />
													</div>
												</div>


											<span style="color:black"><b>Signup Panel</b><br></span>
												<div class="row" style="margin-bottom:2px;color:black;">
													<div class="col-md-4 col-xs-4">&nbsp;&nbsp;Header Color: </div>
													<div class="col-md-4 col-xs-4">
														#<input name="colorPanel" id="colorPanel" type="text" class="colorPicker" style="color:black; cursor:pointer;margin:0; padding:0; border:0; width:70px; height:20px; border-right:20px solid #{{ SettingsClass::get('colorPanel') }}; line-height:20px;" value="{{ SettingsClass::get('colorPanel') }}" />
														<input type="hidden" name="attColorPanel" value="colorPanel" />
													</div>
												</div>
												<div class="row" style="margin-bottom:2px;color:black;">
													<div class="col-md-4 col-xs-4">&nbsp;&nbsp;Text Color: </div>
													<div class="col-md-4 col-xs-4">
														#<input name="colorPanelText" id="colorPanelText" type="text" class="colorPicker" style="color:black; cursor:pointer;margin:0; padding:0; border:0; width:70px; height:20px; border-right:20px solid #{{ SettingsClass::get('colorPanelText') }}; line-height:20px;" value="{{ SettingsClass::get('colorPanelText') }}" />
														<input type="hidden" name="attColorPanelText" value="colorPanelText" />
													</div>
												</div>

												<input type="hidden" name="attColor" value="attColor">
										</div><!-- /widget-main -->
									</div><!-- /.widget-body -->

								</form>
							</div><!-- /.widget-box -->

					        <div class="widget-box" style="opacity: 1;">
		                        <div class="widget-header">
		                            <h5 class="bigger lighter">
		                                <i class="fa fa-gear"></i>
		                                Banner <small>(590px x 100px)</small>
		                            </h5>

		                            <div class="widget-toolbar">
		                            	<button class="btn btn-primary btn-xs btn-white btn-round" id="uploadImage" onclick="updateBanner()">Upload Image</button>
		                            	<button class="btn btn-danger btn-xs btn-white btn-round" onclick="removeBanner(this)">Remove</button>
		                            </div>
		                        </div><!-- /.widget-header -->

		                        <div class="widget-body">
		                            <div class="widget-main">
		                            	<div class="row">
			                            	<div class="col-md-12 col-xs-12">
			                            		@if(! is_null( SettingsClass::get('banner') ) )
						                            <img class="banner img-responsive" style="height:100px" src="{{SettingsClass::get('banner')}}"  />
						                        @endif
			                            	</div>{{-- /.col --}}
		                            	</div>{{-- /.row --}}
		                            </div><!-- /.widget-main -->
		                        </div><!-- /.widget-body -->
		                    </div><!-- /.widget-box -->

						</div><!-- /.tab-color -->



						<!--Editor-->
						<div id="tab-frontPage" class="tab-pane">
							<div>
								<div class="panel-group" id="acoFrontPage" role="tablist" aria-multiselectable="true">

									<div class="panel panel-default">
										<div class="panel-heading" role="tab" id="hFPTitle">
											<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#acoFrontPage" href="#colFPTitle" aria-expanded="true" aria-controls="colFPTitle">
												Front Page Title
											</a>
											</h4>
								    	</div>
										<div id="colFPTitle" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hFPTitle">
											<div class="panel-body">
												<div>
								                    <div class="widget-box" style="opacity: 1;">
						                            	<form id="frmFPTitle" action="{{ URL::route('post-save-settings') }}" onsubmit="return false;">
									                        <div class="widget-header">
									                            <h5></h5>

									                            <div class="widget-toolbar">
									                               <button id='frontPageTitleBtn' class="save-settings btn btn-success btn-xs saveCommand btn-white btn-round" type="button">
									                                    <i class="fa fa-save"></i>&nbsp;&nbsp;Save
									                                </button>
									                            </div><!-- /.widget-toolbar -->
									                        </div><!-- /.widget-header -->

									                        <div class="widget-body">
									                            <div class="widget-main">
																	<textarea name="data" class="form-control texteditor">{{ SettingsClass::get('FrontPageTitle') }}</textarea>
																	<input type="hidden" name="att" value="FrontPageTitle" />
									                            </div><!-- /.widget-main -->
									                        </div><!-- /.widget-body -->
						                                </form>
								                    </div><!-- /.widget-box -->
					        					</div>
											</div><!-- /.panel-body -->
										</div><!-- /.panel-collapse -->
									</div><!-- /.panel -->



									<div class="panel panel-default">
										<div class="panel-heading" role="tab" id="hFPContent">
											<h4 class="panel-title">
												<a class="collapsed" data-toggle="collapse" data-parent="#acoFrontPage" href="#colFPContent" aria-expanded="true" aria-controls="colFPContent">
													Front Page Content
												</a>
											</h4>
										</div>
										<div id="colFPContent" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hFPContent">
											<div class="panel-body">
												<div>
								                    <div class="widget-box" style="opacity: 1;">
						                            	<form id="frmFPContent" action="{{ URL::route('post-save-settings') }}" onsubmit="return false;">
									                        <div class="widget-header">
									                            <h5></h5>

									                            <div class="widget-toolbar">
									                               <button id='frontPageContentBtn' class="save-settings btn btn-success btn-xs saveCommand btn-white btn-round" type="button">
									                                    <i class="fa fa-save"></i>&nbsp;&nbsp;Save
									                                </button>
									                            </div><!-- /.widget-toolbar -->
									                        </div><!-- /.widget-header -->

									                        <div class="widget-body">
									                            <div class="widget-main">
																	<textarea name="data" class="form-control texteditor">{{ SettingsClass::get('FrontPage') }}</textarea>
																	<input type="hidden" name="att" value="FrontPage" />
									                            </div><!-- /.widget-main -->
									                        </div><!-- /.widget-body -->
						                                </form>
								                    </div><!-- /.widget-box -->
					        					</div>
											</div><!-- /.panel-body -->
										</div><!-- /.panel-collapse -->
									</div><!-- /.panel -->

								</div><!-- /.panel-group -->
							</div>
        				</div><!-- /.tab-frontPage -->




						<!--Notification-->
						<div id="tab-Notification" class="tab-pane">
			                <div>
			                    <div class="widget-box" style="opacity: 1;">
	                            	<form id="frmNotification" action="{{ URL::route('post-save-settings') }}" onsubmit="return false;">
				                        <div class="widget-header">
				                            <h5>
				                               Notification
				                            </h5>

				                            <div class="widget-toolbar">
				                               	<button id='NotificationBtn' class="save-settings btn btn-success btn-xs saveCommand btn-white btn-round" type="button">
				                                    <i class="glyphicon glyphicon-ok"></i>
				                                    Save
				                                </button>
				                            </div><!-- /widget-toolbar -->
				                        </div><!-- /widget-header -->

				                        <div class="widget-body">
				                            <div class="widget-main">
												<textarea name="data" class="form-control texteditor">{{ SettingsClass::get('notification') }}</textarea>
												<input type="hidden" name="att" value="notification" />
				                            </div><!-- /widget-main -->
				                        </div><!-- /widget-body -->
	                                </form>
			                    </div><!-- /widget-box -->
        					</div>
        				</div><!-- /.tab-Notification -->


        				<!--Ads-->
						<div id="tab-Ads" class="tab-pane">
			                <div>
			                    <div class="widget-box" style="opacity: 1;">
	                            	<form id="frmAds" action="{{ URL::route('post-save-settings') }}" onsubmit="return false;">
	                            		<input type="hidden" name="adsSettings" value="adsSettings" >
			                        	<div class="widget-header">
			                        		<h5 class="bigger lighter">
												<i class="fa fa-gear"></i>
												Ads
											</h5>
				                            <div class="widget-toolbar">
				                               	<button id='AdsBtn' class="save-settings btn btn-success btn-xs saveCommand btn-white btn-round" type="button">
												<i class="glyphicon glyphicon-ok"></i>
				                                	Save
				                                </button>
				                            </div><!-- /.widget-toolbar -->
				                        </div><!-- /.widget-header -->

				                        <div class="widget-body">
				                            <div class="widget-main">
			                            	    <div class="panel-group" id="acoAds" role="tablist" aria-multiselectable="true">

													{{--Footer Widget--}}
													<div class="panel panel-default">
														<div class="panel-heading" role="tab" id="hHWidget">
															<h4 class="panel-title">
															<a data-toggle="collapse" data-parent="#acoAds" href="#colHWidget" aria-expanded="true" aria-controls="colHWidget">
																Footer Widget
															</a>
															</h4>
												    	</div>
												    	<div id="colHWidget" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hHWidget">
															<div class="panel-body">
																<textarea name="footerWidget" class="form-control NoEditor" style="height:300px;">{{ SettingsClass::get('footerWidget') }}</textarea>
															</div>
														</div>
													</div>

													{{--Right Ads 1--}}
													<div class="panel panel-default">
														<div class="panel-heading" role="tab" id="hAds1">
															<h4 class="panel-title">
																<a class="collapsed" data-toggle="collapse" data-parent="#acoAds" href="#colAds1" aria-expanded="false" aria-controls="colAds1">
																	Ads 1
																</a>
															</h4>
														</div>
														<div id="colAds1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hAds1">
															<div class="panel-body">
																<textarea name="googleAds1" class="form-control tinymceImageUploader" style="height:300px;">{{ SettingsClass::get('GoogleAds1') }}</textarea>
															</div>
														</div>
													</div>
													{{--Right Ads 2--}}
													<div class="panel panel-default">
														<div class="panel-heading" role="tab" id="hAds2">
															<h4 class="panel-title">
																<a class="collapsed" data-toggle="collapse" data-parent="#acoAds" href="#colAds2" aria-expanded="false" aria-controls="colAds2">
																	Ads 2
																</a>
															</h4>
														</div>
														<div id="colAds2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hAds2">
															<div class="panel-body">
																<textarea name="googleAds2" class="form-control tinymceImageUploader" style="height:300px;">{{ SettingsClass::get('GoogleAds2') }}</textarea>
															</div>
														</div>
													</div>
													{{--Right Ads 3--}}
													<div class="panel panel-default">
														<div class="panel-heading" role="tab" id="hAds3">
															<h4 class="panel-title">
																<a class="collapsed" data-toggle="collapse" data-parent="#acoAds" href="#colAds3" aria-expanded="false" aria-controls="colAds3">
																	Ads 3
																</a>
															</h4>
														</div>
														<div id="colAds3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hAds3">
															<div class="panel-body">
																<textarea name="googleAds3" class="form-control tinymceImageUploader" style="height:300px;">{{ SettingsClass::get('GoogleAds3') }}</textarea>
															</div>
														</div>
													</div>

													{{--<div class="panel panel-default">--}}
														{{--<div class="panel-heading" role="tab" id="hWidget1">--}}
															{{--<h4 class="panel-title">--}}
																{{--<a class="collapsed" data-toggle="collapse" data-parent="#acoAds" href="#colWidget1" aria-expanded="false" aria-controls="colWidget1">--}}
																	{{--Widget 01--}}
																{{--</a>--}}
															{{--</h4>--}}
														{{--</div>--}}
														{{--<div id="colWidget1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hWidget1">--}}
															{{--<div class="panel-body">--}}
																{{--<textarea name="valWidget1" class="form-control NoEditor" style="height:300px;">{{ SettingsClass::get('Widget1') }}</textarea>--}}
																{{--<input type="hidden" name="attWidget1" value="Widget1" />--}}
															{{--</div>--}}
														{{--</div>--}}
													{{--</div>--}}

													{{--<div class="panel panel-default">--}}
														{{--<div class="panel-heading" role="tab" id="hWidget2">--}}
															{{--<h4 class="panel-title">--}}
																{{--<a class="collapsed" data-toggle="collapse" data-parent="#acoAds" href="#colWidget2" aria-expanded="false" aria-controls="colWidget2">--}}
																	{{--Widget 02--}}
																{{--</a>--}}
															{{--</h4>--}}
														{{--</div>--}}
														{{--<div id="colWidget2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hWidget2">--}}
															{{--<div class="panel-body">--}}
																{{--<textarea name="valWidget2" class="form-control NoEditor" style="height:300px;">{{ SettingsClass::get('Widget2') }}</textarea>--}}
																{{--<input type="hidden" name="attWidget2" value="Widget2" />--}}
															{{--</div>--}}
														{{--</div>--}}
													{{--</div>--}}

													{{--<div class="panel panel-default">--}}
														{{--<div class="panel-heading" role="tab" id="hTwitter1">--}}
															{{--<h4 class="panel-title">--}}
																{{--<a class="collapsed" data-toggle="collapse" data-parent="#acoAds" href="#colTwitter1" aria-expanded="false" aria-controls="colTwitter1">--}}
																	{{--Twitter 01--}}
																{{--</a>--}}
															{{--</h4>--}}
														{{--</div>--}}
														{{--<div id="colTwitter1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hTwitter1">--}}
															{{--<div class="panel-body">--}}
																{{--<textarea name="valTwitter1" class="form-control NoEditor" style="height:300px;">{{ SettingsClass::get('Tweeter1') }}</textarea>--}}
																{{--<input type="hidden" name="attTwitter1" value="Tweeter1" />--}}
															{{--</div>--}}
														{{--</div>--}}
													{{--</div>--}}

													{{--<div class="panel panel-default">--}}
														{{--<div class="panel-heading" role="tab" id="hTwitter2">--}}
															{{--<h4 class="panel-title">--}}
																{{--<a class="collapsed" data-toggle="collapse" data-parent="#acoAds" href="#colTwitter2" aria-expanded="false" aria-controls="colTwitter2">--}}
																	{{--Twitter 02--}}
																{{--</a>--}}
															{{--</h4>--}}
														{{--</div>--}}
														{{--<div id="colTwitter2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hTwitter2">--}}
															{{--<div class="panel-body">--}}
																{{--<textarea name="valTwitter2" class="form-control NoEditor" style="height:300px;">{{ SettingsClass::get('Tweeter2') }}</textarea>--}}
																{{--<input type="hidden" name="attTwitter2" value="Tweeter2" />--}}
															{{--</div>--}}
														{{--</div>--}}
													{{--</div>--}}

													<div class="panel panel-default">
														<div class="panel-heading" role="tab" id="hGAnalytics">
															<h4 class="panel-title">
																<a class="collapsed" data-toggle="collapse" data-parent="#acoAds" href="#colGAnalytics" aria-expanded="false" aria-controls="colGAnalytics">
																	Google Analytics
																</a>
															</h4>
														</div>
														<div id="colGAnalytics" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hGAnalytics">
															<div class="panel-body">
																<textarea name="googleAnalytics" class="form-control NoEditor" style="height:300px;">{{ SettingsClass::get('googleAnalytics',true) }}</textarea>
															</div>
														</div>
													</div>

													<div class="panel panel-default">
														<div class="panel-heading" role="tab" id="hGRemarketing">
															<h4 class="panel-title">
																<a class="collapsed" data-toggle="collapse" data-parent="#acoAds" href="#colGRemarketing" aria-expanded="false" aria-controls="colGRemarketing">
																	Google Remarketing
																</a>
															</h4>
														</div>
														<div id="colGRemarketing" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hGRemarketing">
															<div class="panel-body">
																<textarea name="googleRemarketing" class="form-control NoEditor" style="height:300px;">{{ SettingsClass::get('googleRemarketing') }}</textarea>
															</div>
														</div>
													</div>

												</div>
													
				                            </div><!-- /.widget-main -->
				                        </div><!-- /.widget-body -->
				                        
	                                </form>
			                    </div><!-- /.widget-box -->
        					</div>
        				</div><!-- /.tab-Ads -->


						<!--Terms & Condition-->
						<div id="tab-Eula" class="tab-pane">
			                <div>
			                    <div class="widget-box" style="opacity: 1;">
			                    	<form id="frmTermCon" action="{{ URL::route('post-save-settings') }}" onsubmit="return false;">
										<div class="widget-header">
											<h5 class="bigger lighter">
												<i class="fa fa-gear"></i>
												Terms and Condition
											</h5>

											<div class="widget-toolbar">
												<button id='EulaBtn' class="save-settings btn btn-success btn-xs saveCommand btn-white btn-round" type="button">
													<i class="glyphicon glyphicon-ok"></i>
													Save
												</button>
											</div><!-- /.widget-toolbar -->
										</div><!-- /.widget-header -->

										<div class="widget-body">
											<div class="widget-main">
												<textarea name="data" class="form-control texteditor">{{ SettingsClass::get('eula') }}</textarea>
												<input type="hidden" name="att" value="eula" />
											</div><!-- /.widget-main -->
				                        </div><!-- /.widget-body -->
			                        </form>
			                    </div><!-- /.widget-box -->
        					</div>
        				</div><!-- /.tab-Eula -->

						{{--<!--Help Topics-->--}}
						<div id="tab-help" class="tab-pane">
							<div>
								<div class="widget-box" style="opacity: 1;">
									<form id="frmHelpTopics" action="{{ URL::route('post-save-settings') }}" onsubmit="return false;">
										<div class="widget-header">
											<h5 class="bigger lighter">
												<i class="fa fa-gear"></i>
												Help Topics
											</h5>

											<div class="widget-toolbar">
												<button class="save-settings btn btn-success btn-xs saveCommand btn-white btn-round" type="button">
													<i class="glyphicon glyphicon-ok"></i>
													Save
												</button>
											</div><!-- /.widget-toolbar -->
										</div><!-- /.widget-header -->

										<div class="widget-body">
											<div class="widget-main">
												<textarea name="helpTopics" class="form-control texteditor">{{ SettingsClass::get('helpTopics') }}</textarea>
											</div><!-- /.widget-main -->
										</div><!-- /.widget-body -->
									</form>
								</div><!-- /.widget-box -->
							</div>
						</div> {{--<!-- Help Topics -->--}}

					</div><!-- /.tab-content -->
				</div><!-- /.tabbbable -->
			</div><!-- /.col -->



		</div><!-- /.row id:content -->
	</div><!-- /.page-content -->
@stop


@section('scripts')
	@include('settings._eula-js')
	@include('settings._banner-js')
@stop