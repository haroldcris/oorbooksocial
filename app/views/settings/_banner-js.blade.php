
<script>
    function updateBanner() {
        var _html ='<form id="frmBanner" action = "{{ URL::route('post-save-settings-banner') }}" > \
                        <div class="space-4"></div> \
                            <div><input id="banner" type="file" name="banner" /></div> \
                        </div>\
                    </form> \
                    <div style="margin-top:20px;"> \
                    <div class="progress"> \
                        <div class="bar" style="width: 0%;"></div> \
                        <div class="percent">0%</div> \
                    </div>';

        var dialog = messageBox('bannerDialog', _html,'Banner', 'Set Banner');
        dialog.setSize(BootstrapDialog.SIZE_SMALL);
    }

    function bannerDialog_onButtonClick(dialogRef){
        postMyFile('frmBanner', dialogRef).then(function(data){
            console.log(data.filename);
            var path = data.filename;
            $('img.banner').attr('src', path + '?rnd=' + Math.random());
            dialogRef.close();
        }, function(error){
            console.log(error);
            alert(error.message);
        });
    }



    function removeBanner(caller) {

        $(caller).prepend('<i class="fa fa-refresh fa-spin" style="margin-right:3px"></i>');
        $.post("{{ URL::route("post-save-settings") }}",{'att' : 'banner', 'data' : '', '_token':'{{csrf_token()}}' })
            .done(function(data){
                $(caller).find('i').remove();
                if(typeof data != 'object'){
                    return messageBoxOk ('dialog', data);
                }
                if(data['success'] == "true" || data.success == true){
                    $('img.banner').remove();
                } else{
                    return messageBoxOk ('dialog', data['error']);
                }
        });
    }
</script>