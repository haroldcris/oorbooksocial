<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Document</title>
</head>
<body>
    <form id="frmPic" action="/upload" method="post" enctype="multipart/form-data">
        <input type="file" name="avatar">
        {{Form::token()}}
        <button>send me</button>
    </form>


    <div class="progress">
        <div class="bar" style="width: 0%;"></div>
        <div class="percent">0%</div>
    </div>

    <script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="http://malsup.github.com/jquery.form.js"></script>


    <style>
        .progress { position:relative; width:100%; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
        .bar { background-color: #B4F5B4; width:0%; height:20px; border-radius: 3px; }
        .percent { position:absolute; display:inline-block; top:3px; left:48%;}
    </style>

    <script>
        $(function() {

        var bar = $('.bar');
        var percent = $('.percent');
        var status = $('#status');

       $('#frmPic').ajaxForm({
           beforeSend: function() {
               status.empty();
               var percentVal = '0%';
               bar.width(percentVal)
               percent.html(percentVal);
           },
           uploadProgress: function(event, position, total, percentComplete) {
               var percentVal = percentComplete + '%';
               bar.width(percentVal)
               percent.html('<i class="fa fa-spin fa-refresh"></i> ' + percentVal);
               console.log(percentVal);
           },
           success: function() {
               var percentVal = '100%';
               bar.width(percentVal)
               percent.html(percentVal);
           },
       	complete: function(xhr) {
       		status.html(xhr.responseText);
       	}
       });
    });
    </script>
</body>
</html>