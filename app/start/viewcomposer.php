<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/24/2015
 * Time: 5:27 PM
 *
 */

View::composer( ['user.profile',
                 'location.searchLocation',
				 'api.apiCreateNoticeKey'] , function($view){

	$listOfCountries = Cache::rememberForever('listOfCountries',function(){
		return DB::table('table_country')->orderBy('country')->get();
	});

	$view->with('listOfCountries',$listOfCountries);

});


View::composer(['layouts.menu.locationList',
				'layouts.menu.calendar',
				'calendar.index',
				'calendar.create'], function($view){

    // Cache::forget('enrolledLocations');
    $enrolledLocations = Auth::user()->enrolledLocations()
                        ->orderBy('isPrimary','desc')
                        ->orderBy('name')
                        ->get();

	$view->with('enrolledLocations', $enrolledLocations);
});


View::composer( ['layouts.partials.sidebar'] , function($view) {
//	$response = Session::get('groupSubscriptions');
//	if ( is_null($response) ) {
//		myLog::Error('Run query for Group Subscriptions ' . Auth::user()->username);
//		$response = ResponseClass::create('get', URL::route('apiGroupSubscriptions'));
//		Session::set('groupSubscriptions', $response);
//	};

//	$response = Cache::rememberForever('userGroupSubscription' . Auth::user()->userId, function(){
//						myLog::Error('Run query for Group Subscriptions ' . Auth::user()->username);
//						$response = ResponseClass::create('get', URL::route('apiGroupSubscriptions'));
//				});

	if(Auth::check()) {
		$response = ResponseClass::create( 'get', URL::route( 'apiGroupSubscriptions' ) );
		$view->with( 'groupSubscriptions', $response );
	}

});