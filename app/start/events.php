<?php
/**
 * Created by Harold Cris D. Abarquez
 * Date: 02/18/2015
 * Time: 6:16 PM
 *
 */

Event::listen('accountCreated', function($user){
	$path = public_path() . '/users/' . strtolower($user->username);
	if( !file_exists($path)) {
		File::makeDirectory( $path,0755 ,true,true);
	}
	return;
});


Event::listen('postCreated', function($data){
	//user, post
	LogBook::logAction('%username% published a new post. <a href="/post/' . $data->postId .  '">Read now</a>' );
});

Event::listen('commentCreated',function($data){
	//comment
	$post = Post::select('postId','userId')->where('postId', $data->postId)->first();
	$user = User::select('username')->where('userId', $post->userId)->first();

	if(Auth::user()->userId == $post->userId){
		LogBook::logAction('%username% left a comment on %sex% post. <a href="/post/' . $data->postId .  '">Read now</a>' );
	} else {
		LogBook::logAction( '%username% left a comment on a post published by ' . $user->username . ' . <a href="/post/' . $data->postId . '">Read now</a>' );
	}
});


Event::listen('changeAvatar',function(){
	//changed his profile photo. Take a look
	LogBook::logAction('%username% changed %sex% profile photo. <a href="/profile/' . Auth::user()->username .  '">Take a look</a>' );
});


Event::listen('events.created', function($event) {
	$url = URL::route('calendarEvents') . '?locationId=%1&start=%2';
	$url = str_replace('%1', $event->locationId, $url);
	$url = str_replace('%2', date('Y-m-d', strtotime($event->eventDate)), $url);

	LogBook::logAction('%username% published a new event on %sex% calendar. <a href="'. $url .'">Take a look</a>' );
});
