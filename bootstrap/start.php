<?php

$app = new Illuminate\Foundation\Application;

/*
|--------------------------------------------------------------------------
| Detect The Application Environment
|--------------------------------------------------------------------------
|
| Laravel takes a dead simple approach to your application environments
| so you can just specify a machine name for the host that matches a
| given environment, then we will automatically detect it for you.
|
*/

//$env = $app->detectEnvironment(array(
//	'local' => array('homestead','dev.*','myPC'),
//    'main'  => array('oorbook.com'),
//));


$env = $app->detectEnvironment(function () {
        $environments = array(
            'local'      => array( 'homestead', 'dev.' ,'localhost'),
            'review'     => 'social.',
            'production' => 'oorbook.',
        );

        $server_name = !empty( $_SERVER['SERVER_NAME'] ) ? $_SERVER['SERVER_NAME'] : 'localhost';

        foreach ( $environments AS $key => $env ) {
            if ( is_array( $env ) ) {
                foreach ( $env as $option ) {
                    if ( stristr( $server_name, $option ) ) {
                        return $key;
                    }
                }
            } else {
                if ( strstr( $server_name, $env ) ) {
                    return $key;
                }
            }
        }
        // default to production
        return 'production';
    }
);


/*
|--------------------------------------------------------------------------
| Bind Paths
|--------------------------------------------------------------------------
|
| Here we are binding the paths configured in paths.php to the app. You
| should not be changing these here. If you need to change these you
| may do so within the paths.php file and they will be bound here.
|
*/

$app->bindInstallPaths(require __DIR__.'/paths.php');

/*
|--------------------------------------------------------------------------
| Load The Application
|--------------------------------------------------------------------------
|
| Here we will load this Illuminate application. We will keep this in a
| separate location so we can isolate the creation of an application
| from the actual running of the application with a given request.
|
*/

$framework = $app['path.base'].
                 '/vendor/laravel/framework/src';

require $framework.'/Illuminate/Foundation/start.php';

/*
|--------------------------------------------------------------------------
| Return The Application
|--------------------------------------------------------------------------
|
| This script returns the application instance. The instance is given to
| the calling script so we can separate the building of the instances
| from the actual running of the application and sending responses.
|
*/

return $app;
